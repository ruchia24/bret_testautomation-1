package com.bret.enums;

/**
 * Enumeration for BRET credentials.
 * 
 * @author CheyenneFreyLazo
 *
 */
public enum RoleCredentialsE {
	
	BRET_USERNAME("bretUsername"),
	BRET_PASSWORD("bretPassword"),
	
	ADMIN_USERNAME("adminUsername"),
	ADMIN_PASSWORD("adminPassword"),
	
	LEAD_REVIEWER_USERNAME("leadReviewerUsername"),
	LEAD_REVIEWER_PASSWORD("leadReviewerPassword"),
	
	SUPPORT_USERNAME("supportUsername"),
	SUPPORT_PASSWORD("supportPassword"),
	
	READER_USERNAME("readerUsername"),
	READER_PASSWORD("readerPassword"),
	
	LEGAL_USERNAME("legalUsername"),
	LEGAL_PASSWORD("legalPassword"),
	
	ACCESS_ADMIN_USERNAME("accessAdminUsername"),
	ACCESS_ADMIN_PASSWORD("accessAdminPassword"),
	
	DEV_USERNAME("devUsername"),
	DEV_PASSWORD("devPassword"),
	
	REVIEWER_USERNAME("reviewerUsername"),
	REVIEWER_PASSWORD("reviewerPassword");

	/**
	 * Represents the current key for the properties file
	 */
	private String key;

	/**
	 * Private constructor for the enums of this type.
	 * 
	 * @param key
	 */
	private RoleCredentialsE(String key) {
		this.key = key;
	}

	/**
	 * Returns the key text of properties file for credentials.
	 * 
	 * @return
	 */
	public String getKey() {
		return key;
	}
}
