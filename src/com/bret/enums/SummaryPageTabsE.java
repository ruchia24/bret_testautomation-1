package com.bret.enums;

public enum SummaryPageTabsE {

	MY_TASK("My Task"), NON_FOCUS_BIDS("Non-Focus Bids"), FOCUS_BIDS(
			"Focus Bids"), ARCHIVE("Archive"), SEARCH("Search"), WAITING_BIDS(
			"Waiting Bids"), ERROR_BIDS("Error Bids"), ADVANCED_SEARCH(
			"Advanced Search"), MANUAL_REVIEW("Manual Review"), GOE_DETERMINATION(
			"Goe Determination"), GOE_DETERMINATION_2("Goe Determination"),
			REPORT("Report"), DETAILED_REPORT("Detailed Report"), 
			REMEDIATION_LOG("Remediation Log"), USER_MANAGEMENT("User Management"), 
			BRET_DASHBOARD("BRET Dashboard"),
			BELOW_CLIP_BIDS("Below Clip Bids");

	private String label;

	private SummaryPageTabsE(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}
