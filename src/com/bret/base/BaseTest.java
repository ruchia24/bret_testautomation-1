package com.bret.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import com.bret.enums.RoleCredentialsE;
import com.bret.util.BretTestUtils;

/**
 * Base class for the tests classes.
 * 
 * Handles the retrieval of BRET Web URL, ,username and password
 * 
 * @author CheyenneFreyLazo
 *
 */
public class BaseTest {

	// Username and password instance variables for each user roles.
	private String bretUsername;
	private String bretPassword;
	
	private String adminUsername;
	private String adminPassword;

	private String leadReviewerUsername;
	private String leadReviewerPassword;

	private String reviewerUsername;
	private String reviewerPassword;

	private String supportUsername;
	private String supportPassword;

	private String readerUsername;
	private String readerPassword;

	private String legalUsername;
	private String legalPassword;

	private String accessAdminUsername;
	private String accessAdminPassword;

	private String devUsername;
	private String devPassword;

	// URL for BRETWEB test and test data resources
	private String bretwebURL;
	private String testDataSource;
	private String screenshotPath;
	public static int maxTimeOut;

	// Constants for file paths
	private static final String TEST_DATA_PATH = "./datasource/BRET DataSource.xls";
	private static final String PROPERTIES_KEY_BRETURL = "BRETURL";
	private static final String PROPERTIES_SCREENSHOT_PATH = "ScreenshotPath";
	private static final String PROPERTIES_PATH = "./properties/brettest.properties";
	private static final String PROPERTIES_KEY_MAX_TIMEOUT = "maxTimeOut";
	private Properties prop = new Properties();
	protected WebDriver driver;
	
	/**
	 * Constructor.
	 * 
	 * Initializes web driver, credentials, urls and data sources.
	 * 
	 */
	public BaseTest() {
		setupCredentials();
		setupBretURL();
		setTestDataSource();
		setMaxTimeOut();
			
		String geckoExePath = "./drivers2/geckodriver.exe";
		System.setProperty("webdriver.gecko.driver", geckoExePath);
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE,"true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"/dev/null");
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		desiredCapabilities.setAcceptInsecureCerts(true);
		
		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk",
				"application/csv,application/excel,application/vnd.ms-excel,application/vnd.msexcel,text/anytext,text/comma-separated-values,text/csv,text/plain,text/x-csv,application/x-csv,text/x-comma-separated-values,text/tab-separated-values");
		desiredCapabilities.setCapability(FirefoxDriver.PROFILE, profile);
		
		driver = new FirefoxDriver(desiredCapabilities);				
	}

	@BeforeTest
	// Before each test case, use BeforeClass for before each test class
	protected void setUpBeforeTestCase() {
		driver.get(getBretWebURL());
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@AfterTest
	protected void tearDownAfterTestCase() {
		driver.quit();
	}

	protected void setupNewURL(String newURL) {
		setBretwebURL(newURL);
		driver.get(newURL);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	/**
	 * Gets the credentials username and password per role and sets it to the
	 * variable.
	 */
	private void setupCredentials() {
		String credPropFilePath = PROPERTIES_PATH;

		try (InputStream credInput = new FileInputStream(new File(credPropFilePath))) {

			prop.load(credInput);

			setBretUsername(getPropValueRoleCred(RoleCredentialsE.BRET_USERNAME));
			setBretPassword(getPropValueRoleCred(RoleCredentialsE.BRET_PASSWORD));
			
			setAdminUsername(getPropValueRoleCred(RoleCredentialsE.ADMIN_USERNAME));
			setAdminPassword(getPropValueRoleCred(RoleCredentialsE.ADMIN_PASSWORD));

			setLeadReviewerUsername(getPropValueRoleCred(RoleCredentialsE.LEAD_REVIEWER_USERNAME));
			setLeadReviewerPassword(getPropValueRoleCred(RoleCredentialsE.LEAD_REVIEWER_PASSWORD));

			setReviewerUsername(getPropValueRoleCred(RoleCredentialsE.REVIEWER_USERNAME));
			setReviewerPassword(getPropValueRoleCred(RoleCredentialsE.REVIEWER_PASSWORD));

			setSupportUsername(getPropValueRoleCred(RoleCredentialsE.SUPPORT_USERNAME));
			setSupportPassword(getPropValueRoleCred(RoleCredentialsE.SUPPORT_PASSWORD));

			setReaderUsername(getPropValueRoleCred(RoleCredentialsE.READER_USERNAME));
			setReaderPassword(getPropValueRoleCred(RoleCredentialsE.READER_USERNAME));

			setLegalUsername(getPropValueRoleCred(RoleCredentialsE.LEGAL_USERNAME));
			setLegalPassword(getPropValueRoleCred(RoleCredentialsE.LEGAL_PASSWORD));

			setAccessAdminUsername(getPropValueRoleCred(RoleCredentialsE.ACCESS_ADMIN_USERNAME));
			setAccessAdminPassword(getPropValueRoleCred(RoleCredentialsE.ACCESS_ADMIN_PASSWORD));

			setDevUsername(getPropValueRoleCred(RoleCredentialsE.DEV_USERNAME));
			setDevPassword(getPropValueRoleCred(RoleCredentialsE.DEV_PASSWORD));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sets up the BRET url.
	 */
	private void setupBretURL() {
		String urlFilePath = PROPERTIES_PATH;
		try (InputStream credInput = new FileInputStream(new File(urlFilePath))) {

			prop.load(credInput);
			setBretwebURL(getPropValue(PROPERTIES_KEY_BRETURL));
			setScreenshotPath(getPropValue(PROPERTIES_SCREENSHOT_PATH));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sets up the maxTimeOut.
	 */
	private void setMaxTimeOut() {
		
		String urlFilePath = PROPERTIES_PATH;
		try (InputStream credInput = new FileInputStream(new File(urlFilePath))) {

			prop.load(credInput);
			maxTimeOut = Integer.parseInt(getPropValue(PROPERTIES_KEY_MAX_TIMEOUT));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * @return the maxTimeOut
	 */
	public static int getMaxTimeOut() {
		return maxTimeOut;
	}
	
	
	/**
	 * Handles the retrieval of Role Credentials value for username/password based
	 * on the parameter.
	 * 
	 * @param roleCreds
	 *            enums for role credentials.
	 * @return String property value.
	 */
	private String getPropValueRoleCred(RoleCredentialsE roleCreds) {
		return getPropValue(roleCreds.getKey());
	}

	/**
	 * Gets the value from the property file given the corresponding key.
	 * 
	 * @param key
	 *            of the property file
	 * @return String property value
	 */
	private String getPropValue(String key) {
		return prop.getProperty(key);
	}

	/**
	 * @return the bretUsername
	 */
	public String getBretUsername() {
		return bretUsername;
	}

	/**
	 * @param bretUsername
	 *            the bretUsername to set
	 */
	public void setBretUsername(String bretUsername) {
		this.bretUsername = bretUsername;
	}

	/**
	 * @return the bretPassword
	 */
	public String getBretPassword() {
		return bretPassword;
	}

	/**
	 * @param bretPassword
	 *            the bretPassword to set
	 */
	public void setBretPassword(String bretPassword) {
		this.bretPassword = bretPassword;
	}

	/**
	 *
	 */
	
	/**
	 * @return the adminUsername
	 */
	public String getAdminUsername() {
		return adminUsername;
	}

	/**
	 * @param adminUsername
	 *            the adminUsername to set
	 */
	public void setAdminUsername(String adminUsername) {
		this.adminUsername = adminUsername;
	}

	/**
	 * @return the adminPassword
	 */
	public String getAdminPassword() {
		return adminPassword;
	}

	/**
	 * @param adminPassword
	 *            the adminPassword to set
	 */
	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}

	/**
	 * @return the leadReviewerUsername
	 */
	public String getLeadReviewerUsername() {
		return leadReviewerUsername;
	}

	/**
	 * @param leadReviewerUsername
	 *            the leadReviewerUsername to set
	 */
	public void setLeadReviewerUsername(String leadReviewerUsername) {
		this.leadReviewerUsername = leadReviewerUsername;
	}

	/**
	 * @return the leadReviewerPassword
	 */
	public String getLeadReviewerPassword() {
		return leadReviewerPassword;
	}

	/**
	 * @param leadReviewerPassword
	 *            the leadReviewerPassword to set
	 */
	public void setLeadReviewerPassword(String leadReviewerPassword) {
		this.leadReviewerPassword = leadReviewerPassword;
	}

	/**
	 * @return the reviewerUsername
	 */
	public String getReviewerUsername() {
		return reviewerUsername;
	}

	/**
	 * @param reviewerUsername
	 *            the reviewerUsername to set
	 */
	public void setReviewerUsername(String reviewerUsername) {
		this.reviewerUsername = reviewerUsername;
	}

	/**
	 * @return the reviewerPassword
	 */
	public String getReviewerPassword() {
		return reviewerPassword;
	}

	/**
	 * @param reviewerPassword
	 *            the reviewerPassword to set
	 */
	public void setReviewerPassword(String reviewerPassword) {
		this.reviewerPassword = reviewerPassword;
	}

	/**
	 * @return the supportUsername
	 */
	public String getSupportUsername() {
		return supportUsername;
	}

	/**
	 * @param supportUsername
	 *            the supportUsername to set
	 */
	public void setSupportUsername(String supportUsername) {
		this.supportUsername = supportUsername;
	}

	/**
	 * @return the supportPassword
	 */
	public String getSupportPassword() {
		return supportPassword;
	}

	/**
	 * @param supportPassword
	 *            the supportPassword to set
	 */
	public void setSupportPassword(String supportPassword) {
		this.supportPassword = supportPassword;
	}

	/**
	 * @return the readerUsername
	 */
	public String getReaderUsername() {
		return readerUsername;
	}

	/**
	 * @param readerUsername
	 *            the readerUsername to set
	 */
	public void setReaderUsername(String readerUsername) {
		this.readerUsername = readerUsername;
	}

	/**
	 * @return the readerPassword
	 */
	public String getReaderPassword() {
		return readerPassword;
	}

	/**
	 * @param readerPassword
	 *            the readerPassword to set
	 */
	public void setReaderPassword(String readerPassword) {
		this.readerPassword = readerPassword;
	}

	/**
	 * @return the legalUsername
	 */
	public String getLegalUsername() {
		return legalUsername;
	}

	/**
	 * @param legalUsername
	 *            the legalUsername to set
	 */
	public void setLegalUsername(String legalUsername) {
		this.legalUsername = legalUsername;
	}

	/**
	 * @return the legalPassword
	 */
	public String getLegalPassword() {
		return legalPassword;
	}

	/**
	 * @param legalPassword
	 *            the legalPassword to set
	 */
	public void setLegalPassword(String legalPassword) {
		this.legalPassword = legalPassword;
	}

	/**
	 * @return the accessAdminUsername
	 */
	public String getAccessAdminUsername() {
		return accessAdminUsername;
	}

	/**
	 * @param accessAdminUsername
	 *            the accessAdminUsername to set
	 */
	public void setAccessAdminUsername(String accessAdminUsername) {
		this.accessAdminUsername = accessAdminUsername;
	}

	/**
	 * @return the accessAdminPassword
	 */
	public String getAccessAdminPassword() {
		return accessAdminPassword;
	}

	/**
	 * @param accessAdminPassword
	 *            the accessAdminPassword to set
	 */
	public void setAccessAdminPassword(String accessAdminPassword) {
		this.accessAdminPassword = accessAdminPassword;
	}

	/**
	 * @return the devUsername
	 */
	public String getDevUsername() {
		return devUsername;
	}

	/**
	 * @param devUsername
	 *            the devUsername to set
	 */
	public void setDevUsername(String devUsername) {
		this.devUsername = devUsername;
	}

	/**
	 * @return the devPassword
	 */
	public String getDevPassword() {
		return devPassword;
	}

	/**
	 * @param devPassword
	 *            the devPassword to set
	 */
	public void setDevPassword(String devPassword) {
		this.devPassword = devPassword;
	}

	/**
	 * @return the bretwebURL
	 */
	public String getBretWebURL() {
		return bretwebURL;
	}

	/**
	 * @param bretwebURL
	 *            the bretwebURL to set
	 */
	public void setBretwebURL(String bretwebURL) {
		this.bretwebURL = bretwebURL;
	}

	/**
	 * @return the testDataSource
	 */
	public String getTestDataSource() {
		return testDataSource;
	}

	/**
	 * @param testDataSource
	 *            the testDataSource to set
	 */
	public void setTestDataSource(String testDataSource) {
		this.testDataSource = testDataSource;
	}

	private void setTestDataSource() {
		this.testDataSource = TEST_DATA_PATH;

	}

	/**
	 * @return the screenshotPath
	 */
	public String getScreenshotPath() {
		return screenshotPath;
	}

	/**
	 * @param screenshotPath
	 *            the screenshotPath to set
	 */
	public void setScreenshotPath(String screenshotPath) {
		this.screenshotPath = screenshotPath;
	}

	/**
	 * Handles the logout from summary page.
	 * 
	 * @param driver
	 */
	public void logOutSummaryPage(WebDriver driver) {
		System.out.println("Logging out from the Summary page");
		String xPathLogout = "//td[text()='Log out']";
		String xPathBackToLogin = "//*[contains(@class, 'pageLink')]";
		
		driver.findElement(By.xpath("//*[@id=\"idx_form_DropDownLink_0\"]")).click();
		driver.findElement(By.xpath(xPathLogout)).click();
		driver.findElement(By.xpath(xPathBackToLogin)).click();
	}

	/**
	 * Handles the logout from details page.
	 * 
	 * @param driver
	 */
	public void logOutDetailsPage(WebDriver driver) {
		System.out.println("Logging out from the Details page");
		String xPathLogout = "//td[text()='Log out']";
		String xPathBackToLogin = "//*[contains(@class, 'pageLink')]";
		
		driver.findElement(By.xpath("//*[@id=\"idx_form_DropDownLink_1\"]")).click();
		driver.findElement(By.xpath(xPathLogout)).click();
		driver.findElement(By.xpath(xPathBackToLogin)).click();
	}
	
	/**
	 * TODO: Frey - update this to use log4j2.
	 * 
	 * @param tcNumber
	 * @param pageName
	 */
	protected void displayLogHeader(String tcNumber, String pageName) {
		System.out.println();
		System.out.println("========== TC Number: " + tcNumber + " ==========");
		System.out.println("========== " + pageName 
				+ " page ==========");
	}
	
	/**
	 * TODO: Frey - update this to use log4j2.
	 * 
	 * @param tcNumber
	 * @param pageName
	 */
	protected void displayLogHeader(String tcNumber) {
		System.out.println();
		System.out.println("========== TC Number: " + tcNumber + " ==========");

	}
	
	/**
	 * Displays the expected and actual result of the element.
	 * 
	 * @param elemText the element we are checking..
	 * @param expectedValue expected value.
	 * @param actualValue actual value.
	 */
	protected void logExpectedActualText(String elemText, String expectedValue, String actualValue) {
		System.out.println();
		System.out.println("Expected " + elemText + ": " + expectedValue);
		System.out.println("Actual " + elemText + ": " + actualValue);
		System.out.println();
	}
	
	/**
	 * Logs the navigation from one page to another
	 * 
	 * @param isFrom true if the navigation flag is a 'From' log 'To' if otherwise.
	 * @param page String page label for the page to be logged.
	 */
	protected void logNavigation(boolean isFrom, String page) {
		String navLoc = "";
		
		if(isFrom) {
			navLoc = "FROM: "; 
		} else {
			navLoc = "TO: ";
		}
		printToConsole("Navigating " + navLoc + ": " + page);
	}
	
	/**
	 * Can accept and display multiple lines of of text
	 * 
	 * @param texts
	 */
	protected void printToConsole(String... texts) {
		BretTestUtils.printToConsole(texts);
	}
	

	/**
	 * Method for taking screenshots
	 * 
	 * @param tcNumber
	 * @param pageLabel
	 * @param curClass
	 */
	protected void takeScreenshot(String tcNumber, String pageLabel, Class curClass) {
		BretTestUtils.screenCapture(
				driver,
				getScreenshotPath(),
				BretTestUtils.getImgFilename(tcNumber + "_" + pageLabel,
						curClass));
	}
	
	/**
	 * Do the sleep.
	 */
	protected void pausePage() {
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Custom sleep
	 * 
	 * @param durationInMillis
	 */
	protected void pausePage(int durationInMillis) {
		try {
			Thread.sleep(durationInMillis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Handles the Windows switching
	 */
	protected void switchToNewWindow() {
		/* SWITCH Control to New Window */
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
	}
}
