package com.bret.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import com.bret.enums.RoleCredentialsE;
import com.bret.pages.AdvancedSearchPage;
import com.bret.pages.ArchivePage;
import com.bret.pages.DetailedReportPage;
import com.bret.pages.FocusPage;
import com.bret.pages.GoeDeterminationPage;
import com.bret.pages.ManualReviewPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.NonFocusPage;
import com.bret.pages.SummaryPage;
import com.bret.pages.UserDetailPage;
import com.bret.pages.UserManagementPage;
import com.bret.util.BretTestUtils;


/**
 * Base class for the tests classes.
 * 
 * Handles the retrieval of BRET Web URL, ,username and password
 * 
 * @author CheyenneFreyLazo
 *
 */
public class BaseTest2  {

	// Username and password instance variables for each user roles.
	private String bretUsername;
	private String bretPassword;

	// URL for BRETWEB test and test data resources
	private String bretwebURL;
	private String testDataSource;
	private String screenshotPath;

	// Constants for file paths
	private static final String TEST_DATA_PATH = "./datasource/BRET DataSource.xls";
	private static final String PROPERTIES_KEY_BRETURL = "BRETURL";
	private static final String PROPERTIES_SCREENSHOT_PATH = "ScreenshotPath";
	private static final String PROPERTIES_PATH = "./properties/brettest.properties";

	private Properties prop = new Properties();

	protected WebDriver driver;

	/**
	 * Constructor.
	 * 
	 * Initializes web driver, credentials, urls and data sources.
	 * 
	 */
	public BaseTest2() {
		setupCredentials();
		setupBretURL();
		setTestDataSource();

		String geckoExePath = "./drivers/geckodriver.exe";
		System.setProperty("webdriver.gecko.driver", geckoExePath);
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE,"true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"/dev/null");
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		desiredCapabilities.setAcceptInsecureCerts(true);
		
		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk",
				"application/csv,application/excel,application/vnd.ms-excel,application/vnd.msexcel,text/anytext,text/comma-separated-values,text/csv,text/plain,text/x-csv,application/x-csv,text/x-comma-separated-values,text/tab-separated-values");
		desiredCapabilities.setCapability(FirefoxDriver.PROFILE, profile);
		
		driver = new FirefoxDriver(desiredCapabilities);
	}

	@BeforeTest
	// Before each test case, use BeforeClass for before each test class
	protected void setUpBeforeTestCase() {
		driver.get(getBretWebURL());
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@AfterTest
	protected void tearDownAfterTestCase() {
		driver.quit();
	}

	protected void setupNewURL(String newURL) {
		setBretwebURL(newURL);
		driver.get(newURL);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	/**
	 * Gets the credentials username and password per role and sets it to the
	 * variable.
	 */
	private void setupCredentials() {
		String credPropFilePath = PROPERTIES_PATH;

		try (InputStream credInput = new FileInputStream(new File(credPropFilePath))) {

			prop.load(credInput);

			setBretUsername(getPropValueRoleCred(RoleCredentialsE.BRET_USERNAME));
			setBretPassword(getPropValueRoleCred(RoleCredentialsE.BRET_PASSWORD));
			

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sets up the BRET url.
	 */
	private void setupBretURL() {
		String urlFilePath = PROPERTIES_PATH;
		try (InputStream credInput = new FileInputStream(new File(urlFilePath))) {

			prop.load(credInput);
			setBretwebURL(getPropValue(PROPERTIES_KEY_BRETURL));
			setScreenshotPath(getPropValue(PROPERTIES_SCREENSHOT_PATH));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Handles the retrieval of Role Credentials value for username/password based
	 * on the parameter.
	 * 
	 * @param roleCreds
	 *            enums for role credentials.
	 * @return String property value.
	 */
	private String getPropValueRoleCred(RoleCredentialsE roleCreds) {
		return getPropValue(roleCreds.getKey());
	}

	/**
	 * Gets the value from the property file given the corresponding key.
	 * 
	 * @param key
	 *            of the property file
	 * @return String property value
	 */
	private String getPropValue(String key) {
		return prop.getProperty(key);
	}

	/**
	 * @return the bretUsername
	 */
	public String getBretUsername() {
		return bretUsername;
	}

	/**
	 * @param bretUsername
	 *            the bretUsername to set
	 */
	public void setBretUsername(String bretUsername) {
		this.bretUsername = bretUsername;
	}

	/**
	 * @return the bretPassword
	 */
	public String getBretPassword() {
		return bretPassword;
	}

	/**
	 * @param bretPassword
	 *            the bretPassword to set
	 */
	public void setBretPassword(String bretPassword) {
		this.bretPassword = bretPassword;
	}

	/**
	 *
	 */
	

	/**
	 * @return the bretwebURL
	 */
	public String getBretWebURL() {
		return bretwebURL;
	}

	/**
	 * @param bretwebURL
	 *            the bretwebURL to set
	 */
	public void setBretwebURL(String bretwebURL) {
		this.bretwebURL = bretwebURL;
	}

	/**
	 * @return the testDataSource
	 */
	public String getTestDataSource() {
		return testDataSource;
	}

	/**
	 * @param testDataSource
	 *            the testDataSource to set
	 */
	public void setTestDataSource(String testDataSource) {
		this.testDataSource = testDataSource;
	}

	private void setTestDataSource() {
		this.testDataSource = TEST_DATA_PATH;

	}

	/**
	 * @return the screenshotPath
	 */
	public String getScreenshotPath() {
		return screenshotPath;
	}

	/**
	 * @param screenshotPath
	 *            the screenshotPath to set
	 */
	public void setScreenshotPath(String screenshotPath) {
		this.screenshotPath = screenshotPath;
	}

	/**
	 * Handles the logout from summary page.
	 * 
	 * @param driver
	 */
	public void logOutSummaryPage(WebDriver driver) {
		System.out.println("Logging out from the Summary page");
		String xPathLogout = "//td[text()='Log out']";
		String xPathBackToLogin = "//*[contains(@class, 'pageLink')]";
		
		driver.findElement(By.xpath("//*[@id=\"idx_form_DropDownLink_0\"]")).click();
		driver.findElement(By.xpath(xPathLogout)).click();
		driver.findElement(By.xpath(xPathBackToLogin)).click();
	}

	/**
	 * Handles the logout from details page.
	 * 
	 * @param driver
	 */
	public void logOutDetailsPage(WebDriver driver) {
		System.out.println("Logging out from the Details page");
		String xPathLogout = "//td[text()='Log out']";
		String xPathBackToLogin = "//*[contains(@class, 'pageLink')]";
		
		driver.findElement(By.xpath("//*[@id=\"idx_form_DropDownLink_1\"]")).click();
		driver.findElement(By.xpath(xPathLogout)).click();
		driver.findElement(By.xpath(xPathBackToLogin)).click();
	}
	
	/**
	 * TODO: Frey - update this to use log4j2.
	 * 
	 * @param tcNumber
	 * @param pageName
	 */
	protected void displayLogHeader(String tcNumber, String pageName) {
		System.out.println();
		System.out.println("========== TC Number: " + tcNumber + " ==========");
		System.out.println("========== " + pageName 
				+ " page ==========");
	}
	
	/**
	 * TODO: Frey - update this to use log4j2.
	 * 
	 * @param tcNumber
	 * @param pageName
	 */
	protected void displayLogHeader(String tcNumber) {
		System.out.println();
		System.out.println("========== TC Number: " + tcNumber + " ==========");

	}
	
	/**
	 * Displays the expected and actual result of the element.
	 * 
	 * @param elemText the element we are checking..
	 * @param expectedValue expected value.
	 * @param actualValue actual value.
	 */
	protected void logExpectedActualText(String elemText, String expectedValue, String actualValue) {
		System.out.println();
		System.out.println("Expected " + elemText + ": " + expectedValue);
		System.out.println("Actual " + elemText + ": " + actualValue);
		System.out.println();
	}
	
	/**
	 * Logs the navigation from one page to another
	 * 
	 * @param isFrom true if the navigation flag is a 'From' log 'To' if otherwise.
	 * @param page String page label for the page to be logged.
	 */
	protected void logNavigation(boolean isFrom, String page) {
		String navLoc = "";
		
		if(isFrom) {
			navLoc = "FROM: "; 
		} else {
			navLoc = "TO: ";
		}
		printToConsole("Navigating " + navLoc + ": " + page);
	}
	
	/**
	 * Can accept and display multiple lines of of text
	 * 
	 * @param texts
	 */
	protected void printToConsole(String... texts) {
		BretTestUtils.printToConsole(texts);
	}
	

	/**
	 * Method for taking screenshots
	 * 
	 * @param tcNumber
	 * @param pageLabel
	 * @param curClass
	 */
	protected void takeScreenshot(String tcNumber, String pageLabel, Class curClass) {
		BretTestUtils.screenCapture(
				driver,
				getScreenshotPath(),
				BretTestUtils.getImgFilename(tcNumber + "_" + pageLabel,
						curClass));
	}
	
	/**
	 * Do the sleep.
	 */
	protected void pausePage() {
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Custom sleep
	 * 
	 * @param durationInMillis
	 */
	protected void pausePage(int durationInMillis) {
		try {
			Thread.sleep(durationInMillis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Handles the Windows switching
	 */
	protected void switchToNewWindow() {
		/* SWITCH Control to New Window */
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
	}
	
	/**
	 * Explicit wait by Title
	 */
	protected void explicitByTitle(String titleOfPage) {
		WebDriverWait wait=new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.titleContains(titleOfPage));

		}
	}

	

