package com.bret.test.bretweb.roles;

import java.util.Map;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.AdvancedSearchPage;
import com.bret.pages.ArchivePage;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.BRETSearchPage;
import com.bret.pages.FocusPage;
import com.bret.pages.GoeDeterminationPage;
import com.bret.pages.ManualReviewPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.NonFocusPage;
import com.bret.pages.SummaryPage;
import com.bret.util.BretTestUtils;

/**
 * June 13, 2018
 * 
 * @author LhoydCastillo
 *
 */
public class BidAdministratorLhoyd extends BaseTest {

	private MyTaskPage myTaskPage;
	private SummaryPage summaryPage;
	private BRETSearchPage searchPage;
	private NonFocusPage nonFocusPage;
	private FocusPage focusPage;
	private ArchivePage archivePage;
	private AdvancedSearchPage advancedSearch;
	private ManualReviewPage manualReviewPage;
	private GoeDeterminationPage goeDeterminationPage;
	
	private static final String ARCHIVE_NON_FOCUS_BID_ID_AT_MTTAB = "MTtab_ArchiveNonFocusBid";
	private static final String ARCHIVE_FOCUS_BID_ID_AT_MTTAB = "MTtab_ArchiveFocusBid";
	private static final String CHANGE_ERROR_TO_FOCUS_BID_ID_AT_MTTAB = "MTtab_ErrorToFocusBid";
	private static final String WAITING_FOR_PRICING_TO_NEW_AT_MTTAB = "MTtab_WaitingForPricingToNewBid";

	private static final String ARCHIVE_NON_FOCUS_BID_ID_AT_NFTAB = "NFtab_ArchiveNonFocusBid";

	private static final String ARCHIVE_FOCUS_BID_ID_AT_FTAB = "Ftab_ArchiveFocusBid";
	
	private static final String sheetName = "BRETWEB Role - Bid Admin";

	@BeforeTest
	public void loginAsBidAdministrator() {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getAdminUsername(), getAdminPassword());
	}

	@Test(priority = 1)
	public void showAccessDetailsSourceSystem() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		String tcNumber = "TC_BWRBA-001 ";
		displayLogHeader(tcNumber);

		myTaskPage.clickUserName(driver);
		myTaskPage.clickShowMyPrivilege(driver);

		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

	}

	@Test(priority = 2)
	public void showAccessDetailsRegion() throws InterruptedException {

		String tcNumber = "TC_BWRBA-002 ";
		displayLogHeader(tcNumber);

		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());
		Thread.sleep(3000);

//		WebElement closeButton = driver.findElement(By.xpath("//*[@id=\"dijit_form_Button_2_label\"]"));
//		closeButton.click();
		
		myTaskPage.closeMyPrivilege(driver);

	}

//	@Test(priority = 3)
//	public void showMyTaskTab() throws InterruptedException {
//
//		String tcNumber = "TC_BWRBA-003 ";
//		displayLogHeader(tcNumber);
//		
//		WebElement MyTaskTab = driver
//				.findElement(By.xpath("//*[@id=\"dijit_layout_TabContainer_0_tablist_idx_layout_ContentPane_0\"]"));
//		// My task tab xpath, to add it on My Task Page
//
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());
//
//		Assert.assertTrue(MyTaskTab.isDisplayed(), "My Task Tab is not enabled.");
//		System.out.println("My Task Tab is displayed.");
//
//	}
//
//	@Test(priority = 4)
//	public void archiveNonFocusBidAtMyTaskTab() throws InterruptedException {
//		summaryPage = new SummaryPage();
//		searchPage = new BRETSearchPage(driver);
//		myTaskPage = new MyTaskPage();
//
//		String tcNumber = "TC_BWRBA-004 ";
//		displayLogHeader(tcNumber);
//
//		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());
//		
//		String archiveNonFocusBid = testData.get(ARCHIVE_NON_FOCUS_BID_ID_AT_MTTAB);
//		
//		int rowAtMyTaskTab = myTaskPage.searchRowIndex(driver, testData.get(ARCHIVE_NON_FOCUS_BID_ID_AT_MTTAB));
//		
//		Thread.sleep(3000);
//		myTaskPage.clickCheckbox(rowAtMyTaskTab);
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());
//
//		myTaskPage.clickArchiveButton(driver);
//
//		Thread.sleep(3000);
//		summaryPage.navigateToTab(driver, SummaryPageTabsE.SEARCH);
//
//		Thread.sleep(3000);
//		searchPage.enterBid(archiveNonFocusBid);
//		searchPage.clickSearch();
//		Thread.sleep(5000);
//		
//	
//		String resultField = searchPage.getResultFieldText(driver);
//		
//		System.out.println("Bid can be found on: " + resultField);
//		
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.SEARCH.getLabel());
//
//		Assert.assertTrue(resultField.contains("Archive"), "The Bid was not moved into Archive Tab.");
//		System.out.println("Bid " + archiveNonFocusBid + " was moved to Archive.");
//
//	}
//
//	
//	@Test(priority = 5)
//	public void archiveFocusBidAtMyTaskTab() throws InterruptedException {
//		summaryPage = new SummaryPage();
//		searchPage = new BRETSearchPage(driver);
//		myTaskPage = new MyTaskPage();
//
//		// String sheetName = "BRET Web Role - Bid Admin";
//		String tcNumber = "TC_BWRBA-005 ";
//		displayLogHeader(tcNumber);
//
//		Thread.sleep(3000);
//		summaryPage.navigateToTab(driver, SummaryPageTabsE.MY_TASK);
//		
//		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());
//		
//		String archiveFocusBid = testData.get(ARCHIVE_FOCUS_BID_ID_AT_MTTAB);
//		
//		int rowAtMyTaskTab = myTaskPage.searchRowIndex(driver, testData.get(ARCHIVE_FOCUS_BID_ID_AT_MTTAB));
//	
//		myTaskPage.clickCheckbox(rowAtMyTaskTab);
//
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());
//
//		myTaskPage.clickArchiveButton(driver);
//
//		Thread.sleep(3000);
//		summaryPage.navigateToTab(driver, SummaryPageTabsE.SEARCH);
//
//		Thread.sleep(3000);
//		searchPage.enterBid(archiveFocusBid);
//		searchPage.clickSearch();
//
//		Thread.sleep(3000);
//		String resultField = searchPage.getResultFieldText(driver);
//		
//		System.out.println("Bid can be found on: " + resultField);
//		
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.SEARCH.getLabel());
//
//		Assert.assertTrue(resultField.contains("Archive"), "The Bid was not moved into Archive Tab.");
//	
//		System.out.println("Bid " + archiveFocusBid + " was moved to Archive.");
//	}
//
//	@Test(priority = 6)
//	public void changeWaitingForPricingToNewAtMyTaskTab() throws InterruptedException {
//		summaryPage = new SummaryPage();
//		searchPage = new BRETSearchPage(driver);
//		myTaskPage = new MyTaskPage();
//
//		String tcNumber = "TC_BWRBA-006 ";
//		displayLogHeader(tcNumber);
//
//		Thread.sleep(3000);
//		summaryPage.navigateToTab(driver, SummaryPageTabsE.MY_TASK);
//		
//		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());
//		
//		String waitingForPricingBid = testData.get(WAITING_FOR_PRICING_TO_NEW_AT_MTTAB);
//
//		int rowAtMyTaskTab = myTaskPage.searchRowIndex(driver, waitingForPricingBid);
//
//		Thread.sleep(3000);
//		myTaskPage.clickCheckbox(rowAtMyTaskTab);
//
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());
//
//		myTaskPage.clickReadyForReviewButton(driver);
//
//		Thread.sleep(3000);
//		summaryPage.navigateToTab(driver, SummaryPageTabsE.SEARCH);
//
//		Thread.sleep(3000);
//		searchPage.enterBid(waitingForPricingBid);
//		searchPage.clickSearch();
//
//		Thread.sleep(3000);
//		String resultField = searchPage.getResultFieldText(driver);
//		
//		System.out.println("Bid can be found on: " + resultField);
//		
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.SEARCH.getLabel());
//
//		Assert.assertTrue(resultField.contains("Focus Bids tab"), "The Bid was not moved into New Status.");
//		
//		System.out.println("Bid " + waitingForPricingBid + " was moved to Focus.");
//
//	}
//
//	@Test(priority = 7)
//	public void changeErrorToFocusAtMyTaskTab() throws InterruptedException {
//		summaryPage = new SummaryPage();
//		searchPage = new BRETSearchPage(driver);
//		myTaskPage = new MyTaskPage();
//
//		// String sheetName = "BRET Web Role - Bid Admin";
//		String tcNumber = "TC_BWRBA-007 ";
//		displayLogHeader(tcNumber);
//
//		Thread.sleep(3000);
//		summaryPage.navigateToTab(driver, SummaryPageTabsE.MY_TASK);
//		
//		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());
//		int rowAtMyTaskTab = myTaskPage.searchRowIndex(driver, testData.get(CHANGE_ERROR_TO_FOCUS_BID_ID_AT_MTTAB));
//
//		String errorToFocusBid = testData.get(CHANGE_ERROR_TO_FOCUS_BID_ID_AT_MTTAB);
//
//		Thread.sleep(3000);
//		myTaskPage.clickCheckbox(rowAtMyTaskTab);
//
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());
//
//		myTaskPage.clickReadyForReviewButton(driver);
//
//		Thread.sleep(3000);
//		summaryPage.navigateToTab(driver, SummaryPageTabsE.SEARCH);
//
//		Thread.sleep(3000);
//		searchPage.enterBid(errorToFocusBid);
//		searchPage.clickSearch();
//
//		Thread.sleep(3000);
//		String resultField = searchPage.getResultFieldText(driver);
//		
//		System.out.println("Bid can be found on: " + resultField);
//		
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.SEARCH.getLabel());
//
//		Assert.assertTrue(resultField.contains("Focus Bids tab"), "The Bid was not moved into a Focus Bid.");
//		
//		System.out.println("Bid " + errorToFocusBid + " was moved to Focus.");
//		
//	}
//
//	@Test(priority = 8)
//	public void exportSummaryTableToCSVAtMyTaskTab() throws InterruptedException {
//		summaryPage = new SummaryPage();
//		myTaskPage = new MyTaskPage();
//
//		String tcNumber = "TC_BWRBA-008 ";
//		displayLogHeader(tcNumber);
//		
//		Thread.sleep(3000);
//		summaryPage.navigateToTab(driver, SummaryPageTabsE.MY_TASK);
//
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());
//
//		boolean exportButtonIsDisplayed = myTaskPage.exportButtonIsDisplayed(driver);
//
//		Assert.assertTrue(exportButtonIsDisplayed, "Export Button is not Displayed.");
//		System.out.println("Export Button is displayed!");
//	}
//
//	@Test(priority = 9)
//	public void showNonFocusTab() throws InterruptedException {
//		summaryPage = new SummaryPage();
//		myTaskPage = new MyTaskPage();
//
//		String tcNumber = "TC_BWRBA-009 ";
//		displayLogHeader(tcNumber);
//
//		boolean nonFocusTabIsPresent = false;
//
//		try {
//			summaryPage.navigateToTab(driver, SummaryPageTabsE.NON_FOCUS_BIDS);
//			nonFocusTabIsPresent = true;
//		} catch (NoSuchElementException e) {
//			nonFocusTabIsPresent = false;
//		}
//
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.NON_FOCUS_BIDS.getLabel());
//
//		Assert.assertTrue(nonFocusTabIsPresent, "Non Focus Tab is not Present!");
//		System.out.println("Non Focus Tab is displayed!");
//		Thread.sleep(5000);
//	}
//
//	@Test(priority = 10)
//	public void archiveNonFocusBidAtNonFocusTab() throws InterruptedException {
//		summaryPage = new SummaryPage();
//		nonFocusPage = new NonFocusPage();
//		searchPage = new BRETSearchPage(driver);
//		
//		String tcNumber = "TC_BWRBA-010 ";
//		displayLogHeader(tcNumber);
//
//		Thread.sleep(5000);
//		
//		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());
//		String archiveNonFocusBid = testData.get(ARCHIVE_NON_FOCUS_BID_ID_AT_NFTAB);
//		
//		System.out.println("archiveNonFocusBid: " + archiveNonFocusBid);
//		
//		int rowAtNonFocusTab = nonFocusPage.searchRowIndex(driver, archiveNonFocusBid);
//		
//		System.out.println("rowAtNonFocusTab: " + rowAtNonFocusTab);
//		
//		Thread.sleep(3000);
//		nonFocusPage.clickCheckbox(rowAtNonFocusTab);
//		
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());
//
//		nonFocusPage.clickArchiveButton(driver);
//
//		Thread.sleep(3000);
//		summaryPage.navigateToTab(driver, SummaryPageTabsE.SEARCH);
//
//		Thread.sleep(3000);
//		searchPage.enterBid(archiveNonFocusBid);
//		searchPage.clickSearch();
//		
//		Thread.sleep(3000);
//		String resultFieldText = searchPage.getResultFieldText(driver);
//	
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.SEARCH.getLabel());
//
//		Assert.assertTrue(resultFieldText.contains("Archive"), "The Bid was not moved into Archive Tab.");
//		System.out.println("The Bid was moved into Archive Tab.!");
//	}
//
//	
//	@Test(priority = 11)
//	public void exportSummaryTableToCSVAtNonFocusTab() throws InterruptedException {
//		summaryPage = new SummaryPage();
//		nonFocusPage = new NonFocusPage();
//
//		String tcNumber = "TC_BWRBA-011 ";
//		displayLogHeader(tcNumber);
//
//		Thread.sleep(3000);
//		summaryPage.navigateToTab(driver, SummaryPageTabsE.NON_FOCUS_BIDS);
//		
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.NON_FOCUS_BIDS.getLabel());
//
//		boolean exportButtonIsDisplayed = nonFocusPage.exportButtonIsDisplayed(driver);
//
//		Assert.assertTrue(exportButtonIsDisplayed, "Export Button is not Displayed.");
//		System.out.println("Export Button is Displayed.");
//
//	}
//	
//	@Test(priority = 12)
//	public void showFocusTab() throws InterruptedException {
//		summaryPage = new SummaryPage();
//
//		String tcNumber = "TC_BWRBA-012 ";
//		displayLogHeader(tcNumber);
//
//		boolean FocusTabIsPresent = false;
//
//		try {
//			summaryPage.navigateToTab(driver, SummaryPageTabsE.FOCUS_BIDS);
//			FocusTabIsPresent = true;
//		} catch (NoSuchElementException e) {
//			FocusTabIsPresent = false;
//		}
//
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());
//
//		Assert.assertTrue(FocusTabIsPresent, "Focus Tab is not Present!");
//		System.out.println("Focus Tab is Present!");
//		Thread.sleep(5000);
//	}
//	
//	@Test(priority = 13)
//	public void archiveFocusBidAtFocusTab() throws InterruptedException {
//		summaryPage = new SummaryPage();
//		focusPage = new FocusPage();
//		searchPage = new BRETSearchPage(driver);
//		
//		String tcNumber = "TC_BWRBA-013 ";
//		displayLogHeader(tcNumber);
//		
//		Thread.sleep(5000);
//		
//		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());
//		String archiveFocusBid = testData.get(ARCHIVE_FOCUS_BID_ID_AT_FTAB);
//		
//		System.out.println("archiveFocusBid: " + archiveFocusBid);
//		
//		Thread.sleep(10000);
//
//		int rowAtFocusTab = focusPage.searchRowIndex(driver, testData.get(ARCHIVE_FOCUS_BID_ID_AT_FTAB));
//		System.out.println("rowAtFocusTab: " + rowAtFocusTab);
//
//		
//		Thread.sleep(3000);
//		focusPage.clickCheckbox(rowAtFocusTab);
//		
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());
//
//		focusPage.clickArchiveButton(driver);
//
//		Thread.sleep(3000);
//		summaryPage.navigateToTab(driver, SummaryPageTabsE.SEARCH);
//
//		Thread.sleep(3000);
//		searchPage.enterBid(archiveFocusBid);
//		searchPage.clickSearch();
//		
//		Thread.sleep(3000);
//		String resultFieldText = searchPage.getResultFieldText(driver);
//	
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.SEARCH.getLabel());
//
//		Assert.assertTrue(resultFieldText.contains("Archive"), "The Bid was not moved into Archive Tab.");
//		System.out.println("The Bid was moved into Archive Tab.");
//	}
//	
//	@Test(priority = 14)
//	public void exportSummaryTableToCSVAtFocusTab() throws InterruptedException {
//		summaryPage = new SummaryPage();
//		focusPage = new FocusPage();
//
//		String tcNumber = "TC_BWRBA-014 ";
//		displayLogHeader(tcNumber);
//		
//		Thread.sleep(3000);
//		summaryPage.navigateToTab(driver, SummaryPageTabsE.FOCUS_BIDS);
//
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());
//
//		boolean exportButtonIsDisplayed = focusPage.exportButtonIsDisplayed(driver);
//
//		Assert.assertTrue(exportButtonIsDisplayed, "Export to CSV Button is not Displayed.");
//		System.out.println("Export To CSV Button is Displayed");
//	}
//	
//	@Test(priority = 15)
//	public void clickStatusReportAtFocusTab() throws InterruptedException {
//		summaryPage = new SummaryPage();
//		focusPage = new FocusPage();
//
//		String tcNumber = "TC_BWRBA-015 ";
//		displayLogHeader(tcNumber);
//
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());
//
//		boolean statusReportIsDisplayed = focusPage.statusReportButtonIsDisplayed(driver);
//		
//		System.out.println("statusReportIsDisplayed: " + statusReportIsDisplayed);
//			
//		Assert.assertTrue(!statusReportIsDisplayed, "Export Button is Displayed.");
//
//	}
//
//	@Test(priority = 16)
//	public void showArchiveTab() throws InterruptedException {
//		summaryPage = new SummaryPage();
//	
//		String tcNumber = "TC_BWRBA-016 ";
//		displayLogHeader(tcNumber);
//
//		boolean archiveTabIsPresent = false;
//
//		try {
//			summaryPage.navigateToTab(driver, SummaryPageTabsE.ARCHIVE);
//			archiveTabIsPresent = true;
//		} catch (NoSuchElementException e) {
//			archiveTabIsPresent = false;
//		}
//
//		Thread.sleep(8000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());
//
//		Assert.assertTrue(archiveTabIsPresent, "Archive Tab is not Present!");
//		System.out.println("Archive Tab is Present!");
//		Thread.sleep(5000);
//
//	}
//	
//	@Test(priority = 17)
//	public void exportSummaryTableToCSVAtArchiveTab() throws InterruptedException {
//		summaryPage = new SummaryPage();
//		archivePage = new ArchivePage();
//
//		String tcNumber = "TC_BWRBA-017 ";
//		displayLogHeader(tcNumber);
//		
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());
//
//		boolean exportSummaryTableToCSVIsDisplayed = archivePage.exportButtonIsDisplayed(driver);
//
//		Assert.assertTrue(exportSummaryTableToCSVIsDisplayed, "Export to CSV Button is not Displayed.");
//		System.out.println("Export To CSV Button is Displayed");
//	}
//
//	@Test(priority = 18)
//	public void exportStatusReportAtArchiveTab() throws InterruptedException {
//		summaryPage = new SummaryPage();
//		archivePage = new ArchivePage();
//
//		String tcNumber = "TC_BWRBA-018 ";
//		displayLogHeader(tcNumber);
//		
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());
//
//		boolean exportStatusReportIsDisplayed = archivePage.statusReportButtonIsDisplayed(driver);
//
//		Assert.assertTrue(!exportStatusReportIsDisplayed, "Export Status Report Button is Displayed.");
//		System.out.println("Export To CSV Button is Not Displayed");
//	}
//	
//	@Test(priority = 19)
//	public void uploadRemediationFollowUpButtonAtArchiveTab() throws InterruptedException {
//		summaryPage = new SummaryPage();
//		archivePage = new ArchivePage();
//
//		String tcNumber = "TC_BWRBA-019 ";
//		displayLogHeader(tcNumber);
//		
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());
//
//		boolean uploadRemediationFollowUpButtonIsDisplayed = archivePage.uploadRemediationFollowUpButtonIsDisplayed(driver);
//
//		Assert.assertTrue(!uploadRemediationFollowUpButtonIsDisplayed, "Upload Remediation Follow Up Button is Displayed.");
//		System.out.println("Upload Remediation Follow Up Button is Not Displayed.");
//	}
//	
//	
//	@Test(priority = 20)
//	public void showSearchTab() throws InterruptedException {
//		summaryPage = new SummaryPage();
//	
//		String tcNumber = "TC_BWRBA-020 ";
//		displayLogHeader(tcNumber);
//
//		boolean archiveTabIsPresent = false;
//
//		try {
//			summaryPage.navigateToTab(driver, SummaryPageTabsE.SEARCH);
//			archiveTabIsPresent = true;
//		} catch (NoSuchElementException e) {
//			archiveTabIsPresent = false;
//		}
//
//		Thread.sleep(8000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.SEARCH.getLabel());
//
//		Assert.assertTrue(archiveTabIsPresent, "Search Tab is not Present!");
//		System.out.println("Search Tab is Present!");
//		Thread.sleep(5000);
//
//	}
//	
//	@Test(priority = 21)
//	public void showWaitingBidsTab() throws InterruptedException {
//		summaryPage = new SummaryPage();
//	
//		String tcNumber = "TC_BWRBA-021 ";
//		displayLogHeader(tcNumber);
//
//		boolean archiveTabIsPresent = false;
//
//		try {
//			summaryPage.navigateToTab(driver, SummaryPageTabsE.WAITING_BIDS);
//			archiveTabIsPresent = true;
//		} catch (NoSuchElementException e) {
//			archiveTabIsPresent = false;
//		}
//
//		Thread.sleep(8000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.WAITING_BIDS.getLabel());
//
//		Assert.assertTrue(archiveTabIsPresent, "Waiting Bids Tab is not Present!");
//		System.out.println("Waiting Bids Tab is Present!");
//		Thread.sleep(5000);
//
//	}
//	
//	@Test(priority = 22)
//	public void showErrorBidsTab() throws InterruptedException {
//		summaryPage = new SummaryPage();
//	
//		String tcNumber = "TC_BWRBA-022 ";
//		displayLogHeader(tcNumber);
//
//		boolean archiveTabIsPresent = false;
//
//		try {
//			summaryPage.navigateToTab(driver, SummaryPageTabsE.ERROR_BIDS);
//			archiveTabIsPresent = true;
//		} catch (NoSuchElementException e) {
//			archiveTabIsPresent = false;
//		}
//
//		Thread.sleep(8000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.ERROR_BIDS.getLabel());
//
//		Assert.assertTrue(archiveTabIsPresent, "Error Bids Tab is not Present!");
//		System.out.println("Error Bids Tab is Present!");
//		Thread.sleep(5000);
//
//	}
//	
//	@Test(priority = 23)
//	public void showAdvancedSearchTab() throws InterruptedException {
//		summaryPage = new SummaryPage();
//	
//		String tcNumber = "TC_BWRBA-023 ";
//		displayLogHeader(tcNumber);
//
//		boolean archiveTabIsPresent = false;
//
//		try {
//			summaryPage.navigateToTab(driver, SummaryPageTabsE.ADVANCED_SEARCH);
//			archiveTabIsPresent = true;
//		} catch (NoSuchElementException e) {
//			archiveTabIsPresent = false;
//		}
//
//		Thread.sleep(8000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
//
//		Assert.assertTrue(archiveTabIsPresent, "Advanced Search Tab is not Present!");
//		System.out.println("Advanced Search Tab is Present!");
//		Thread.sleep(5000);
//
//	}
//
//	@Test(priority = 24)
//	public void exportSummaryTableToCSVAtAdvancedSearchTab() throws InterruptedException {
//		summaryPage = new SummaryPage();
//		advancedSearch = new AdvancedSearchPage();
//
//		String tcNumber = "TC_BWRBA-024 ";
//		displayLogHeader(tcNumber);
//		
//		Thread.sleep(3000);
//		takeScreenshot(tcNumber, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
//
//		boolean exportSummaryTableToCSVIsDisplayed = advancedSearch.exportButtonIsDisplayed(driver);
//
//		Assert.assertTrue(exportSummaryTableToCSVIsDisplayed, "Export to CSV Button is not Displayed.");
//		System.out.println("Export To CSV Button is Displayed");
//	}
	
	@Test(priority = 25)
	public void showManualReviewTab() throws InterruptedException {
		summaryPage = new SummaryPage();
	
		String tcNumber = "TC_BWRBA-025 ";
		displayLogHeader(tcNumber);

		boolean manualReviewTabIsPresent = false;
		
		try {
			summaryPage.navigateToTab(driver, SummaryPageTabsE.MANUAL_REVIEW);
			manualReviewTabIsPresent = true;
		} catch (org.openqa.selenium.NoSuchElementException | NoSuchElementException e) {
			manualReviewTabIsPresent = false;
		}

		Thread.sleep(8000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MANUAL_REVIEW.getLabel());

		Assert.assertTrue(!manualReviewTabIsPresent, "Manual Review Tab is Present!");
		System.out.println("Manual Review Tab is Not Present!");
		Thread.sleep(5000);

	}
	
	@Test(priority = 26)
	public void downloadTheLatestFileAtManualReviewTab() throws InterruptedException {
		summaryPage = new SummaryPage();
		manualReviewPage = new ManualReviewPage();
	
		String tcNumber = "TC_BWRBA-026 ";
		displayLogHeader(tcNumber);

		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MANUAL_REVIEW.getLabel());

		boolean downloadTheLatestFileIsDisplayed = false;
				
		try {
			manualReviewPage.downloadTheLatestFileIsDisplayed(driver);
			downloadTheLatestFileIsDisplayed = true;
		} catch (org.openqa.selenium.NoSuchElementException | NoSuchElementException e) {
			downloadTheLatestFileIsDisplayed = false;
			System.out.println("Manual Review Tab is Not Present! ");
		}
		
		
		Assert.assertFalse(downloadTheLatestFileIsDisplayed, "Download the latest file Button is Displayed.");
		System.out.println("Download the latest file Button is Not Displayed");

	}

	
	@Test(priority = 27, dependsOnMethods = {"showManualReviewTab"})
	public void uploadANewFileAtManualReviewTab() throws InterruptedException {
		summaryPage = new SummaryPage();
		manualReviewPage = new ManualReviewPage();
	
		String tcNumber = "TC_BWRBA-026 ";
		displayLogHeader(tcNumber);

		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.MANUAL_REVIEW.getLabel());

		boolean uploadANewIsDisplayed = false;
				
		try {
			manualReviewPage.downloadTheLatestFileIsDisplayed(driver);
			uploadANewIsDisplayed = true;
		} catch (org.openqa.selenium.NoSuchElementException | NoSuchElementException e) {
			uploadANewIsDisplayed = false;
			System.out.println("Manual Review Tab is Not Present! ");
		}
		
		Assert.assertFalse(uploadANewIsDisplayed, "Upload a New file Button is Displayed.");
		System.out.println("Upload a New file Button is Not Displayed");

	}
	
	@Test(priority = 28)
	public void showGoeDeterminationTab() throws InterruptedException {
		summaryPage = new SummaryPage();
	
		String tcNumber = "TC_BWRBA-028 ";
		displayLogHeader(tcNumber);

		boolean manualReviewTabIsPresent = false;
		
		try {
			summaryPage.navigateToTab(driver, SummaryPageTabsE.GOE_DETERMINATION);
			manualReviewTabIsPresent = true;
		} catch (org.openqa.selenium.NoSuchElementException | NoSuchElementException e) {
			manualReviewTabIsPresent = false;
		}

		Thread.sleep(8000);
		takeScreenshot(tcNumber, SummaryPageTabsE.GOE_DETERMINATION.getLabel());

		Assert.assertTrue(!manualReviewTabIsPresent, "GOE Determination Tab is Present!");
		System.out.println("GOE Determination Tab is Not Present!");
		Thread.sleep(5000);

	}
	

	@Test(priority = 29)
	public void searchCmrAtGoeDeterminationTab() throws InterruptedException {
		summaryPage = new SummaryPage();
		goeDeterminationPage = new GoeDeterminationPage();
	
		String tcNumber = "TC_BWRBA-029 ";
		displayLogHeader(tcNumber);

		Thread.sleep(3000);
		takeScreenshot(tcNumber, SummaryPageTabsE.GOE_DETERMINATION.getLabel());

		boolean searchCmrButtonIsDisplayed = false;
				
		try {
			goeDeterminationPage.searchButtonIsDisplayed(driver);
			searchCmrButtonIsDisplayed = true;
		} catch (org.openqa.selenium.NoSuchElementException | NoSuchElementException e) {
			searchCmrButtonIsDisplayed = false;
			System.out.println("GOE Determination Tab is Not Present! ");
		}
		
		
		Assert.assertFalse(searchCmrButtonIsDisplayed, "Search CMR Button is Displayed.");
		System.out.println("search CMR Button is Not Displayed");

	}
	
	private void takeScreenshot(String tcNumber, String tabLabel) {
		BretTestUtils.screenCapture(driver, getScreenshotPath(),
				BretTestUtils.getImgFilename(tcNumber + "_" + tabLabel, this.getClass()));
	}

}
