package com.bret.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.PageFactory;

public class GoeDeterminationPage extends SummaryPage {
//	private WebDriver driver;

	private static final String XPATH_SEARCH_BUTTON = "//*[@id=\"goeSearchByCmrNbrBtn\"]";

//	public GoeDeterminationPage() {
//		// Do nothing
//	}
//
//	/**
//	 * Constructor - for initializing the elements again. Used for switching back to
//	 * Summary page from Details page.
//	 * 
//	 * @param driver
//	 */
//	public GoeDeterminationPage(WebDriver driver) {
//		this.driver = driver;
//		PageFactory.initElements(driver, this);
//	}

	public boolean searchButtonIsDisplayed(WebDriver driver) {
		WebElement searchButton = driver.findElement(By.xpath(XPATH_SEARCH_BUTTON));
		boolean searchButtonIsDisplayed = searchButton.isDisplayed();
		return searchButtonIsDisplayed;

	}

}