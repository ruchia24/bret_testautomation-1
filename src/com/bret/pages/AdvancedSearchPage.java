package com.bret.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.bret.util.BretTestUtils;

/**
 * @author CheyenneFreyLazo
 *
 */
public class AdvancedSearchPage extends SummaryPage {

	//Commented by sarita as the xpath is not correct
	//public static final String XPATH_EXPORT_CSV_BTN = "//*[@id=\"dijit_form_Button_2\"]";
	public static final String XPATH_EXPORT_CSV_BTN = "//*[@id='dijit_form_Button_4_label']";

	public static final String XPATH_SEARCH_BUTTON = ".//*[@id='locateBidSearchBtn_label']";
	//public static final String XPATH_SEARCH_BUTTON = ".//*[@id='advSearchSearchPane_pane']/div/div[6]/div[4]/span/span";

	
	// Table column constants for Search Result
		public static final int FLAG_COL = 1;
		public static final int BID_ID_COL = 2;
		public static final int ADDITIONAL_ID_COL = 3;
		public static final int DETAIL_LINK_COL = 4;
		public static final int BID_DATE_COL = 5;
		public static final int SOURCE_SYSTEM_COL = 6;
		public static final int REGION_COL = 7;
		public static final int COUNTRY_COL = 8;
		public static final int REVIEWER_DECISION_COL = 9;
		public static final int REVIEWER_COL = 10;
		public static final int RELEASED_ON_COL = 11 ;
		public static final int TIER1_CEID_COL = 12;
		public static final int TIER2_CEID_COL = 13;
		
	
	
	private WebDriver driver;

	/**
	 * Click the Export To CSV Button
	 * 
	 * @param driver
	 */
	public void clickExportToCsvButton(WebDriver driver) {
		this.driver = driver;

		BretTestUtils.printToConsole("Advanced Search Page -- Clicking 'Export to CSV' Button");
		if (checkElementVisibility(driver, XPATH_EXPORT_CSV_BTN)) {
			driver.findElement(By.xpath(XPATH_EXPORT_CSV_BTN)).click();
		} else {
			BretTestUtils.printToConsole("Advanced Search Page -- Sorry 'Export to CSV' button is not visible. ");
		}
	}

	/*
	 * To click Search Button
	 */
	public void clickSearchButton() {
		WebElement searchButton = driver.findElement(By.xpath(XPATH_SEARCH_BUTTON));
		searchButton.click();
	}
	


	/**
	 * Click OK in the alert.
	 * 
	 * @param driver
	 */
	public void clickOkInAlert(WebDriver driver) {
		BretTestUtils
				.printToConsole("Advanced Search Page -- Clicking 'OK' in the 'There is no data to export' prompt");
		this.driver = driver;

		try {
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (Exception e) {
			BretTestUtils.printToConsole("Advanced Search Page -- No Alert was found. Please revisit your code.");
		}
	}
	
	public boolean exportButtonIsDisplayed(WebDriver driver) {
		try {
		WebElement exportButton = driver.findElement(By.xpath(XPATH_EXPORT_CSV_BTN));
		boolean exportButtonIsDisplayed = exportButton.isDisplayed();
		return exportButtonIsDisplayed;
		
		} catch (Exception e) {
			boolean exportButtonIsDisplayed = false;
			return exportButtonIsDisplayed;
		}

	}
}
