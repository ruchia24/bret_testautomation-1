package com.bret.pages;

//import java.util.NoSuchElementException;

import org.openqa.selenium.By;
//import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.PageFactory;

public class UserDetailPage extends UserManagementPage {

	private static final String XPATH_USER_DETAIL_PAGE = "//*[@id=\"dijit_layout_ContentPane_0\"]";
	
	
	public String getUserDetailPageText(WebDriver driver) {
		WebElement userDetailPageHeader = driver.findElement(By.xpath(XPATH_USER_DETAIL_PAGE));
		String userDetailPageHeaderText = userDetailPageHeader.getText();
		return userDetailPageHeaderText;
	}
}
