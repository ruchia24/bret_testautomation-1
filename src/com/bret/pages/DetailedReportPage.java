package com.bret.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DetailedReportPage extends SummaryPage {
	// Table column constants of MyTaskTabPage
	// Constants variable that you can use (static final int CAPITAL
	// LETTERS,separated by UnderScore)
	public static final int FLAG_COL = 1;
	public static final int BID_ID_COL = 2;
	public static final int ADDITIONAL_ID_COL = 3;
	public static final int DETAIL_LINK_COL = 4;
	public static final int BID_DATE_COL = 5;
	public static final int REVIEWER_DECISION_COL = 6;
	public static final int REVIEWER_COL = 7;
	public static final int RELEASED_ON_COL = 8;
	public static final int COUNTRY_COL = 9;
	public static final int IBM_SELL_PRICE_COL = 10;
	public static final int PRIMARY_BRAND_COL = 11;
	public static final int DISTRIBUTOR_COL = 12;
	public static final int CUST_FACING_BP_COL = 13;
	public static final int CUST_NAME = 14;

	public static final String SOURCE_SYS_ALL = "ALL";
	public static final String SOURCE_SYS_EPRICER = "EPRICER";
	public static final String SOURCE_SYS_BH = "BH";
	public static final String SOURCE_SYS_BPMS = "BPMS";
	public static final String SOURCE_SYS_OPRA = "OPRA";
	public static final String SOURCE_SYS_SQO = "SQO";
	public static final String SOURCE_SYS_EORDER = "EORDER";

	public static final int A_COL = 15;
	public static final int B_COL = 16;
	public static final int C_COL = 17;
	public static final int D_COL = 18;

	// Added by Calvin on June 27
	private static final String XPATH_TAKE_THIS_BID_BTN = "//*[@id=\"assignBtn_label\"]";
	
	// Added by Calvin on June 27
	//Added by Sarita on june 24, 2019
	
	//private static final String XPATH_EXPORT_CSV_BTN = "//*[@id=\"exportToExcelBtn\"]";
	
	
	private static final String XPATH_EXPORT_CSV_BTN = "//*[@id='exportToExcelBtn']";
	//Added by Sarita on june 24, 2019
	//private static final String XPATH_PRINT_VERSION_BTN = "//*[@id=\"printedVersionBtn_label\"]";
	
	private static final String XPATH_PRINT_VERSION_BTN = "//*[@id='printedVersionBtn_label']";

	// Added by Calvin on June 27
	private static final String XPATH_EDIT_REMEDIATION = "//*[@id=\"idx_form_Link_0\"]";

	// Added by Calvin on June 28
//	private static final String XPATH_UPLOAD_REMEDIATION = "//*[@id=\"remediationActionLink\"]";
	//Added by sarita as xpath is not correct
	private static final String XPATH_UPLOAD_REMEDIATION = "//*[@id='remediationActionLink']";

	private static final String XPATH_ROLLBACK_BTN = "//*[@id=\"rollbackReleasedBtn_label\"]";
	
	// Added by Calvin on June 29 
	//private static final String XPATH_EXPORT_CSV_BELOW_CLIP_BTN = "//*[@id=\"focusExportToExcelBtn_label\"]";
	
	//Added by Sarita on june 24, 2019
	private static final String XPATH_EXPORT_CSV_BELOW_CLIP_BTN = "//*[@id='focusExportToExcelBtn_label']";
	
	// Added by Calvin on June 27	
	public boolean searchNewInMyTask2(WebDriver driver, String reviewerDecision)
			throws InterruptedException {

		System.out.println("Finding bids with \"New\" status");
		System.out.println();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		driver.findElement(By.xpath(".//*[@id='dijit_layout_TabContainer_0_tablist']/div[4]/div/div[1]")).click();

		for (int rownumMyTask = 2; rownumMyTask < 25; rownumMyTask++) {

			String decisionMyTask = driver.findElement(By.xpath(
					".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div[" + rownumMyTask + "]/table/tbody/tr/td[6]"))
					.getText();
			String bidMyTask = driver.findElement(By.xpath(
					".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div[" + rownumMyTask + "]/table/tbody/tr/td[2]"))
					.getText();

			if (decisionMyTask.equals(reviewerDecision)) {
				System.out.println("Reviewer Decision: " + decisionMyTask);
				System.out.println("Bid with New Status: " + bidMyTask);

				//driver.findElement(By.xpath(
				//	".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div[" + rownumMyTask + "]/table/tbody/tr/td[4]")).click();
				
				driver.findElement(By.xpath(
						"/html[1]/body[1]/div[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[5]/div[3]/div[2]/div[" + rownumMyTask + "]/table[1]/tbody[1]/tr[1]/td[4]/span[1]")).click();
					
				return true;
			}
		}
		return false;
	}
	
	// Added by Calvin on June 27
	public boolean takeThisBidButtonIsDisplayed(WebDriver driver) {
		try {
		WebElement takeThisBidButton = driver.findElement(By.xpath(XPATH_TAKE_THIS_BID_BTN));
		boolean takeThisBidButtonIsDisplayed = takeThisBidButton.isDisplayed();
		return takeThisBidButtonIsDisplayed;
		
		} catch (Exception e) {
			boolean takeThisBidButtonIsDisplayed = false;
			return takeThisBidButtonIsDisplayed;
			
		}
	}
	
	// Added by Calvin on June 26
	public boolean exportCSVButtonIsDisplayed(WebDriver driver) {
		try {
		WebElement exportButton = driver.findElement(By.xpath(XPATH_EXPORT_CSV_BTN));
		boolean exportButtonIsDisplayed = exportButton.isDisplayed();
		return exportButtonIsDisplayed;
		
		} catch (Exception e) {
			boolean exportButtonIsDisplayed = false;
			return exportButtonIsDisplayed;
		}
	}
	
	
	public boolean printVersionButtonIsDisplayed(WebDriver driver) {
		try {
		WebElement printVersionButton = driver.findElement(By.xpath(XPATH_PRINT_VERSION_BTN));
		boolean printVersionButtonIsDisplayed = printVersionButton.isDisplayed();
		return printVersionButtonIsDisplayed;
		
		} catch (Exception e) {
			boolean printVersionButtonIsDisplayed = false;
			return printVersionButtonIsDisplayed;
		}
	}
	
	public boolean editRemediationButtonIsDisplayed(WebDriver driver) {	
		try {
			WebElement editRemediationButton = driver.findElement(By.xpath(XPATH_EDIT_REMEDIATION));
			boolean editRemediationButtonIsDisplayed = editRemediationButton.isDisplayed();
			return editRemediationButtonIsDisplayed;
			
		} catch (Exception e) {
			boolean editRemediationButtonIsDisplayed = false;
			return editRemediationButtonIsDisplayed;
		}
	}
	
	public boolean uploadAttachmentsButtonIsDisplayed(WebDriver driver) {
		try {
		WebElement uploadAttachmentsButton = driver.findElement(By.xpath(XPATH_UPLOAD_REMEDIATION));
		boolean uploadAttachmentsButtonIsDisplayed = uploadAttachmentsButton.isDisplayed();
		return uploadAttachmentsButtonIsDisplayed;
		
		} catch (Exception e) {
			boolean uploadAttachmentsButtonIsDisplayed = false;
			return uploadAttachmentsButtonIsDisplayed;
		}
	}
	
	
	
	public boolean searchRemediatedBidInArchive(WebDriver driver)
			throws InterruptedException {

		System.out.println("Finding Remediated Bid in Archive Tab");
		System.out.println();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		for (int rownumMyTask = 3; rownumMyTask < 25; rownumMyTask++) {

//			String reviewerReleaseOn = driver.findElement(By.xpath(
//					".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div[" + rownumMyTask + "]/table/tbody/tr/td[7]"))
//					.getText();
			String reviewerReleaseOn = driver.findElement(By.xpath(
					"/html[1]/body[1]/div[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[1]/div[3]/div[4]/div[1]/div[5]/div[3]/div[2]/div[" + rownumMyTask + "]/table[1]/tbody[1]/tr[1]/td[6]"))
					.getText();
		
			if (reviewerReleaseOn.length() == 7) {
//				driver.findElement(By.xpath(
//					".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div[" + rownumMyTask + "]/table/tbody/tr/td[4]")).click();
								driver.findElement(By.xpath(
					"/html[1]/body[1]/div[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[1]/div[3]/div[4]/div[1]/div[5]/div[3]/div[2]/div[" + rownumMyTask + "]/table[1]/tbody[1]/tr[1]/td[4]/span[1]")).click();
			return true;
			}
		}
		return false;
	}
	
	public boolean rollbackButtonIsDisplayed(WebDriver driver) {
		try {
		WebElement rollbackButton = driver.findElement(By.xpath(XPATH_ROLLBACK_BTN));
		boolean rollbackButtonIsDisplayed = rollbackButton.isDisplayed();
		return rollbackButtonIsDisplayed;
		
		} catch (Exception e) {
			boolean rollbackButtonIsDisplayed = false;
			return rollbackButtonIsDisplayed;
		}
	}
	
	
	public boolean exportToCSVInBelowClipBidsTabIsDisplayed(WebDriver driver) {
		try {
		WebElement exportToCSVInBelowClipBidsTab = driver.findElement(By.xpath(XPATH_EXPORT_CSV_BELOW_CLIP_BTN));
		boolean exportToCSVInBelowClipBidsTabIsDisplayed = exportToCSVInBelowClipBidsTab.isDisplayed();
		return exportToCSVInBelowClipBidsTabIsDisplayed;
		
		} catch (Exception e) {
			boolean exportToCSVInBelowClipBidsTabIsDisplayed = false;
			return exportToCSVInBelowClipBidsTabIsDisplayed;
		}
	}
}
