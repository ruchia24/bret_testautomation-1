package com.bret.pages;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.bret.obj.export.ExportNonFocusData;
import com.bret.util.BretTestUtils;

public class ExportNonFocusProcessor {

	private static final int TYPE = 0;
	private static final int SOURCE_SYSTEM = 1;
	private static final int REGION = 2;
	private static final int BID_ID = 3;
	private static final int ADDITIONAL_ID = 4;
	private static final int BID_DATE = 5;
	private static final int COUNTRY = 6;
	private static final int IBM_SELL_PRICE = 7;
	private static final int PRIMARY_BRAND = 8;
	private static final int DISTRIBUTOR = 9;
	private static final int CUSTOMER_FACING_BP = 10;
	private static final int CUSTOMER_NAME = 11;
	private static final int REVIEWER_COMMENTS = 12;

	public ExportNonFocusData getExportNonFocusData(String filePath) {
		List<String> csvContents = BretTestUtils.readFile(filePath);
		csvContents.remove(0);
		csvContents.remove(0);

		// ExportMyTaskData exportData =
		// mapExportCSVMyTaskToObj(cleanupCSVData(csvContents));
		List<String> arrangedList = arrangeContents(csvContents.get(0));
		List<String> cleanedUpList = cleanupCSVData(arrangedList);

		ExportNonFocusData exportData = mapExportCSVNonFocusToObj(cleanedUpList);

		return exportData;
	}

	private List<String> arrangeContents(String content) {
		List<String> arrangedContent = Arrays.asList(content.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"));
		return arrangedContent;
	}

	private ExportNonFocusData mapExportCSVNonFocusToObj(List<String> csvContents) {
		ExportNonFocusData exportData = new ExportNonFocusData();

		exportData.setType(csvContents.get(TYPE));
		exportData.setSourceSystem(csvContents.get(SOURCE_SYSTEM));
		exportData.setRegion(csvContents.get(REGION));
		exportData.setBidId(csvContents.get(BID_ID));
		exportData.setAdditionalId(csvContents.get(ADDITIONAL_ID));
		exportData.setBidDate(csvContents.get(BID_DATE));
		exportData.setCountry(csvContents.get(COUNTRY));
		exportData.setIbmSellPrice(formatToDecimal(csvContents.get(IBM_SELL_PRICE)));
		exportData.setPrimaryBrand(csvContents.get(PRIMARY_BRAND));
		exportData.setDistributor(csvContents.get(DISTRIBUTOR));
		exportData.setCustomerFacingBP(csvContents.get(CUSTOMER_FACING_BP));
		exportData.setCustomerName(csvContents.get(CUSTOMER_NAME));
		exportData.setReviewerComments(csvContents.get(REVIEWER_COMMENTS));

		return exportData;

	}

	private List<String> cleanupCSVData(List<String> origCsvData) {
		List<String> cleanCsvData = new ArrayList<>();

		for (String data : origCsvData) {
			cleanCsvData.add(cleanupDetailData(data));
		}

		return cleanCsvData;
	}

	private String cleanupDetailData(String data) {
		String cleanData = "";
		cleanData = data.replaceAll("\"=\"", "").replaceAll("\"", "").trim();

		return cleanData;
	}

	public List<String> getExportedDataDifference(ExportNonFocusData exportedCsv, ExportNonFocusData bretWebData) {
		List<String> difference = new ArrayList<>();

		for (Field field : ExportNonFocusData.class.getDeclaredFields()) {
			field.setAccessible(true);
			try {
				if ((null != field.get(exportedCsv) && null != field.get(bretWebData))
						&& !field.get(exportedCsv).equals(field.get(bretWebData))) {

					difference.add(field.getName() + ": " + "[CSV Value: " + field.get(exportedCsv) + " : "
							+ "Bret web Value: " + field.get(bretWebData) + "]");
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		return difference;
	}

	private String formatToDecimal(String number) {
		double amount = Double.parseDouble(number);
		DecimalFormat formatter = new DecimalFormat("#,###.00");

		return "$" + formatter.format(amount);
	}

}
