package com.bret.pages;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.bret.obj.export.ExportArchiveData;
import com.bret.util.BretTestUtils;

public class ExportArchiveProcessor {

	private static final int TYPE = 0;
	private static final int SOURCE_SYSTEM = 1;
	private static final int REGION = 2;
	private static final int BID_ID = 3;
	private static final int ADDITIONAL_ID = 4;
	private static final int BID_DATE = 5;
	private static final int REVIEWER_DECISION = 6;
	private static final int REVIEWER = 7;
	private static final int REVIEWER_RELEASED_ON = 8;
	private static final int BID_RELEASE_BY = 9;
	private static final int BID_RELEASE_ON = 10;
	private static final int COUNTRY = 11;
	private static final int IBM_SELL_PRICE = 12;
	private static final int PRIMARY_BRAND = 13;
	private static final int DISTRIBUTOR = 14;
	private static final int CUSTOMER_FACING_BP = 15;
	private static final int CUSTOMER_NAME = 16;
	private static final int MARGIN_OR_DISCOUNT_FLAG = 17;
	private static final int BID_QUESTIONS_FLAG = 18;
	private static final int PBC_HISTORY_FLAG = 19;
	private static final int COMPOSITE_SCORE_FLAG = 20;
	private static final int REVIEWER_COMMENTS = 21;
	private static final int READY_TO_REVIEW_ON = 22;
	private static final int CONFIDENTIALITY = 23;
	private static final int ROUTE_TO_MARKET = 24;
	private static final int OFFSHORE_PAYMT_TERMS = 25;
	private static final int BUNDLED_SOLUTIONS = 26;
	private static final int CONTINGENCY_FEE = 27;
	private static final int SOLE_SOURCE_PROC = 28;
	private static final int FOCUS_BP = 29;
	private static final int BP_MARG_PERC = 30;
	private static final int DISCOUNT_PERC = 31;
	private static final int BP_MARG_TOTAL = 32;
	private static final int DISCOUNT_TOTAL = 33;
	private static final int REMEDIATION_LOG = 34;
	private static final int ACTION_OWNER = 35;
	private static final int COMPLETED_ON = 36;
	private static final int CLOSED_DATE = 37;
	private static final int BID_EXPIRED = 38;
	private static final int EX_PRE_FULFILLMENT = 39;
	private static final int POST_VERIFY_DATE = 40;
	private static final int POST_SHIP_AUDIT_STAT = 41;
	private static final int WAITING_FOR_EXTENSION = 42;
	private static final int PROBLEMS_WITH_CHASING = 43;
	private static final int REQUEST_SENT_TOO_EARLY = 44;
	private static final int REALISTICALLY_DUE = 45;
	private static final int TEST_NOTE_STARTED_YET = 46;
	private static final int POTENTIAL_ISSUES = 47;
	private static final int FOLLOW_UP_LOG = 48;
	private static final int DEADLINE_OF_REQUEST = 49;
	private static final int BID_EVENT = 50;
	private static final int REMEDIATION_COMMENT = 51;

	public ExportArchiveData getExportArchiveData(String filePath) {
		List<String> csvContents = BretTestUtils.readFile(filePath);
		csvContents.remove(0);
		csvContents.remove(0);

		// ExportArchiveData exportData =
		// mapExportCSVArchiveToObj(cleanupCSVData(csvContents));
		List<String> arrangedList = arrangeContents(csvContents.get(0));
		List<String> cleanedUpList = cleanupCSVData(arrangedList);

		ExportArchiveData exportData = mapExportCSVArchiveToObj(cleanedUpList);

		return exportData;
	}

	private List<String> arrangeContents(String content) {
		List<String> arrangedContent = Arrays.asList(content.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"));
		return arrangedContent;
	}

	private ExportArchiveData mapExportCSVArchiveToObj(List<String> csvContents) {
		ExportArchiveData exportData = new ExportArchiveData();

		exportData.setType(csvContents.get(TYPE));
		exportData.setSourceSystem(csvContents.get(SOURCE_SYSTEM));
		exportData.setRegion(csvContents.get(REGION));
		exportData.setBidId(csvContents.get(BID_ID));
		exportData.setAdditionalId(csvContents.get(ADDITIONAL_ID));
		exportData.setBidDate(csvContents.get(BID_DATE));
		exportData.setReviewerDecision(csvContents.get(REVIEWER_DECISION));
		exportData.setReviewer(csvContents.get(REVIEWER));
		exportData.setReviewerReleasedOn(csvContents.get(REVIEWER_RELEASED_ON));
		exportData.setBidReleasedBy(csvContents.get(BID_RELEASE_BY));
		exportData.setBidReleasedOn(csvContents.get(BID_RELEASE_ON));
		exportData.setCountry(csvContents.get(COUNTRY));
		exportData.setIbmSellPrice(formatToDecimal(csvContents.get(IBM_SELL_PRICE)));
		exportData.setPrimaryBrand(csvContents.get(PRIMARY_BRAND));
		exportData.setDistributor(csvContents.get(DISTRIBUTOR));
		exportData.setCustomerFacingBP(csvContents.get(CUSTOMER_FACING_BP));
		exportData.setCustomerName(csvContents.get(CUSTOMER_NAME));
		exportData.setMarginOrDiscountFlag(csvContents.get(MARGIN_OR_DISCOUNT_FLAG));
		exportData.setBidQuestionsFlag(csvContents.get(BID_QUESTIONS_FLAG));
		exportData.setPbcHistoryFlag(csvContents.get(PBC_HISTORY_FLAG));
		exportData.setCompositeScoreFlag(csvContents.get(COMPOSITE_SCORE_FLAG));
		exportData.setReviewerComments(csvContents.get(REVIEWER_COMMENTS));
		exportData.setReadyToReviewOn(csvContents.get(READY_TO_REVIEW_ON));
		exportData.setConf(csvContents.get(CONFIDENTIALITY));
		exportData.setRtm(csvContents.get(ROUTE_TO_MARKET));
		exportData.setNstdPmt(csvContents.get(OFFSHORE_PAYMT_TERMS));
		
		exportData.setBundle(csvContents.get(BUNDLED_SOLUTIONS));
		exportData.setFee(csvContents.get(CONTINGENCY_FEE));
		exportData.setSoleSourceProc(csvContents.get(SOLE_SOURCE_PROC));
		exportData.setFocusBp(csvContents.get(FOCUS_BP));
		exportData.setBpmPerc(csvContents.get(BP_MARG_PERC));
		exportData.setDiscPerc(csvContents.get(DISCOUNT_PERC));
		exportData.setBpmTotal(csvContents.get(BP_MARG_TOTAL));
		exportData.setDiscTotal(csvContents.get(DISCOUNT_TOTAL));
		exportData.setRemediationLog(csvContents.get(REMEDIATION_LOG));
		exportData.setActionOwner(csvContents.get(ACTION_OWNER));
		exportData.setCompletedOn(csvContents.get(COMPLETED_ON));
		exportData.setClosedDate(csvContents.get(CLOSED_DATE));
		exportData.setBidExpired(csvContents.get(BID_EXPIRED));
		exportData.setExPreFulfilment(csvContents.get(EX_PRE_FULFILLMENT));
		exportData.setPostVerifyDate(csvContents.get(POST_VERIFY_DATE));
		exportData.setPostShipAuditFollowUpStatus(csvContents.get(POST_SHIP_AUDIT_STAT));
		exportData.setWaitingForExtension(csvContents.get(WAITING_FOR_EXTENSION));
		exportData.setProblemsWithChasing(csvContents.get(PROBLEMS_WITH_CHASING));
		exportData.setRequestSentTooEarly(csvContents.get(REQUEST_SENT_TOO_EARLY));
		exportData.setRealisticallyDue(csvContents.get(REALISTICALLY_DUE));
		exportData.setTestNoteStartedYet(csvContents.get(TEST_NOTE_STARTED_YET));
		exportData.setPotentialIssues(csvContents.get(POTENTIAL_ISSUES));
		exportData.setFollowUpLog(csvContents.get(FOLLOW_UP_LOG));
		exportData.setDeadlineOfRequest(csvContents.get(DEADLINE_OF_REQUEST));
		exportData.setBidEvent(csvContents.get(BID_EVENT));
		exportData.setRemediationComment(csvContents.get(REMEDIATION_COMMENT));

		return exportData;

	}

	private List<String> cleanupCSVData(List<String> origCsvData) {
		List<String> cleanCsvData = new ArrayList<>();

		for (String data : origCsvData) {
			cleanCsvData.add(cleanupDetailData(data));
		}

		return cleanCsvData;
	}

	private String cleanupDetailData(String data) {
		String cleanData = "";
		cleanData = data.replaceAll("\"=\"", "").replaceAll("\"", "").trim();

		return cleanData;
	}

	public List<String> getExportedDataDifference(ExportArchiveData exportedCsv, ExportArchiveData bretWebData) {
		List<String> difference = new ArrayList<>();

		for (Field field : ExportArchiveData.class.getDeclaredFields()) {
			field.setAccessible(true);
			try {
				if ((null != field.get(exportedCsv) && null != field.get(bretWebData))
						&& !field.get(exportedCsv).equals(field.get(bretWebData))) {

					difference.add(field.getName() + ": " + "[CSV Value: " + field.get(exportedCsv) + " : "
							+ "Bret web Value: " + field.get(bretWebData) + "]");
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		return difference;
	}
	
	private String formatToDecimal(String number) {
		double amount = Double.parseDouble(number);
		DecimalFormat formatter = new DecimalFormat("#,###.00");


		return "$" + formatter.format(amount);
	}

}
