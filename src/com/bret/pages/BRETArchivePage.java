package com.bret.pages;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.bret.util.ScreenCapture;

/** 
* [added-Belle Pondiong]<br>
* This page class contained all the methods on what to perform on BRET Archive tab/page. <br>
*/
public class BRETArchivePage {
	
	private static final Object NULL = null;

	//Declaring variables
	public WebDriver driver;

	@FindBy (id = "dijit_layout_TabContainer_0_tablist_idx_layout_ContentPane_3")   // Archive Tab
	private WebElement archiveTab;

	@FindBy (id = "rollbackReleasedBtn")						//Rollback  Button
	private WebElement rollBackBtn;
	
	@FindBy (id = "assignee")
	private WebElement assignReviewer;
	
	public static String bidReviewer = null;
    //                     bidFlagArchive = null, 
   //                      bidRevDecArchive = null;
	
	// This is a web page class constructor		 
	public BRETArchivePage (WebDriver driver) {
			 this.driver= driver;
			 PageFactory.initElements(driver, this);
	}

//--------------------- find Bid Released in Archive Routines  ---------------------------------- 	
	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that will check the manually released NF Bid in Archive. <br>
	 * This will return void.<br>
	 * This method will display in the console the bid details.<br>
	 * <br>Test Verification: <br>
	 * Verify that Bid Released By is Admin's name and  Bid Released On is current date.<br>
	 */
	public void releasedtoArchiveChck_NF (String bidNum, String adminName) throws Exception {
 
		int grdCntrfound = 0; 
		String bidcheck = null, 
			   bidFlag = null, 
			   bidDate = null,
			   bidReviewerDec = null,
		       bidReleasedBy = null, 
		       bidReleasedOn = null;
		//final Object NULL = null;
		
		//TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		String getSystemTime = new SimpleDateFormat("MM/d/yy").format(Calendar.getInstance().getTime());
		System.out.println("trace: system time: " +getSystemTime);
		
		archiveTab.click(); 						  // loads Archive Tab first
		grdCntrfound = bidFindonArchiveTab (bidNum);  //call the routine to locate the bid
		
		// checkbox checked on found bid 
		driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[4]/div["+grdCntrfound+"]/table/tbody/tr/td/span")).click();
		
		//get the details of the found bid
		bidcheck = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[2]")).getText();
		bidFlag = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[1]")).getText();
		bidDate = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[5]")).getText();
		bidReviewerDec = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[6]")).getText();
		bidReleasedBy = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[8]")).getText();
		bidReleasedOn = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[9]")).getText();
		
		//Printing located bid and details on the logs
		// manual tracing
		System.out.println("\n=====Bid located in Archive Tab=====");
		System.out.println("   Bid Num: " +bidcheck);	
		System.out.println("   Flag:    " +bidFlag);	
		System.out.println("   Bid Date:  " +bidDate);
		System.out.println("   Reviewer Decesion:  " +bidReviewerDec);
		System.out.println("   Bid Released By:  " +bidReleasedBy);
		System.out.println("   Bid Released On:  " +bidReleasedOn);
				
		// Routine to verify Bid Released By is Admin's name and  Bid Released On is current date
		if ((bidReleasedBy.equals(adminName)) & ((bidReleasedOn.equals(getSystemTime)))) {
			
			// Routine to verify details from My Task (before release)  vs Archive
			if ((bidFlag.equals(BRETMyTaskPage.bidFlagMyTask)) & (bidDate.equals(BRETMyTaskPage.bidDateMyTask)) & (bidReviewerDec.equals(BRETMyTaskPage.bidRevDecMyTask))) {
			
				System.out.println("\n Bid Found in Archived with correct details.");
			}
			else 
				Assert.fail("Failed. Bid found in Archive but with wrong/incomplete details (Flag/Bid Date/Reviwer Decesion)."); // Will fail the test case if IF-condition is FALSE
		}
		else
			Assert.fail("Failed. Bid found in Archive but with incorrect Admin name/Bid Released On us not current date."); // Will fail the test case if IF-condition is FALSE
		
		//TimeZone.setDefault(TimeZone.getTimeZone("GMT+8"));
	}
	
	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that will check the manually released Focus Bid in Archive. <br> 
	 * This will return void.<br>
	 * This method will display in the console the bid details.<br>
	 * <br>Test Verification: <br>
	 * Verify that Bid Released By is Admin's name and  Bid Released On is current date.<br>
	 */
	public void releasedtoArchiveChck_F (String bidNum, String adminName) throws Exception {
 
		int grdCntrfound = 0; 
		String bidcheck = null, 
			   bidFlag = null, 
			   bidDate = null,
			   bidReviewerDec = null,
		       bidReleasedBy = null, 
		       bidReleasedOn = null,
		       bidReviewerReleaseOn = null;
		String getSystemTime = new SimpleDateFormat("MM/d/yy").format(Calendar.getInstance().getTime());
		System.out.println("trace: system time: " +getSystemTime);
		
		archiveTab.click(); 						  // loads Archive Tab first
		grdCntrfound = bidFindonArchiveTab (bidNum);  //call the routine to locate the bid
		
		// checkbox checked on found bid 
		driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[4]/div["+grdCntrfound+"]/table/tbody/tr/td/span")).click();
		
		//get the details of the found bid
		bidcheck = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[2]")).getText();
		bidFlag = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[1]")).getText();
		bidDate = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[5]")).getText();
		bidReviewerDec = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[6]")).getText();
		bidReviewerReleaseOn = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[7]")).getText();
		bidReleasedBy = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[8]")).getText();
		bidReleasedOn = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[9]")).getText();
		
		//Printing located bid and details on the logs
		// manual tracing
		System.out.println("\n=====Bid located in Archive Tab=====");
		System.out.println("   Bid Num: " +bidcheck);	
		System.out.println("   Flag:    " +bidFlag);	
		System.out.println("   Bid Date:  " +bidDate);
		System.out.println("   Reviewer Decesion:  " +bidReviewerDec);
		System.out.println("   Reviewer Released On:  " +bidReviewerReleaseOn);
		System.out.println("   Bid Released By:  " +bidReleasedBy);
		System.out.println("   Bid Released On:  " +bidReleasedOn);
				
		// Routine to verify Bid Released By is Admin's name and  Bid Released On is current date
		if ((bidReleasedBy.equals(adminName)) & ((bidReleasedOn.equals(getSystemTime)))) {
			
			// Routine to verify details from My Task (before release)  vs Archive
			if ((bidFlag.equals(BRETMyTaskPage.bidFlagMyTask)) & (bidDate.equals(BRETMyTaskPage.bidDateMyTask)) & (bidReviewerDec.equals(BRETMyTaskPage.bidRevDecMyTask)) & (bidReviewerReleaseOn.equals(BRETMyTaskPage.bidReviewerRelOnDecMyTask))) {
			
				System.out.println("\n Bid Found in Archived with correct details.");
			}
			else 
				Assert.fail("Failed. Bid found in Archive but with wrong/incomplete details (Flag/Bid Date/Reviwer Decesion/Reviewer Released On)."); // Will fail the test case if IF-condition is FALSE
		}
		else
			Assert.fail("Failed. Bid found in Archive but with incorrect Admin name/Bid Released On us not current date."); // Will fail the test case if IF-condition is FALSE
		
	}

	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that will check the bids that are automatically released to Archived by CRON jobs. <br>
	 * This will return void.<br>
	 * This method will display in the console the bid details.<br>
	 * <br>Test Verification: <br>
	 * Verify that Bid Released By is BRET Automatic and  Bid Released On is not NULL.<br>
	 * Verify that Bid is remediated.<br>
	 */
	public void autoreleasedtoArchiveChck_F (String bidNum, String revDesc, String adminName) throws Exception  {

 		int grdCntrfound = 0; 
		String bidcheck = null, 
			   bidFlag = null, 
			   bidDate = null,
			   bidReviewerDec = null,
		       bidReleasedBy = null, 
		       bidReleasedOn = null,
		       bidReviewerReleaseOn = null;
	 		
		
		//System.out.println("Trace: check on value being passed " +bidReleasedBy);
		
		archiveTab.click(); 						  // loads Archive Tab first
		grdCntrfound = bidFindonArchiveTab (bidNum);  //call the routine to locate the bid
		
		// checkbox checked on found bid 
		driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[4]/div["+grdCntrfound+"]/table/tbody/tr/td/span")).click();
		
		//get the details of the found bid
		bidcheck = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[2]")).getText();
		bidFlag = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[1]")).getText();
		bidDate = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[5]")).getText();
		bidReviewerDec = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[6]")).getText();
		bidReviewerReleaseOn = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[7]")).getText();
		bidReleasedBy = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[8]")).getText();
		bidReleasedOn = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[9]")).getText();
		
		//Printing located bid and details on the logs
		printLogs (bidcheck, bidFlag, bidDate, bidReviewerDec, bidReviewerReleaseOn, bidReleasedBy, bidReleasedOn);	
			
		// Routine to verify Bid Released By is BRET Automatic and  Bid Released On is not NULL and bid is remediated
		if (!(bidReleasedBy.equals("BRET Automatic"))) {
			
			Assert.fail("Failed. Bid found in Archive but Bid Released By is not BRET automatic");
			
			}
		else if (((bidReleasedOn.equals(NULL))) & ((bidReviewerReleaseOn.equals(NULL))) & ((bidDate.equals(NULL)))) {
			
			Assert.fail("Failed. Bid found in Archive but Bid dates/Reviwer Released On/Bid Released on are empty");
		}
		else if (!(bidReviewerDec.equals(revDesc))) {
			
			Assert.fail("Failed. Bid found in Archive but incorrect reviewer decesion.");
		}  
		else if (!(bidFlag.equals("Y"))) {
			
			Assert.fail("Failed. Bid found in Archive but incorrect BRET focus flag.");
		}  
		else
			System.out.println("\n Bid Found in Archived with correct details."); // bid found with no errors
		
	
		
	}
	
	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that will check the bids that are automatically released to Archived by CRON jobs. <br>
	 * This will return void.<br>
	 * This method will display in the console the bid details.<br>
	 * <br>Test Verification: <br>
	 * Verify that Bid Released By is BRET Automatic and  Bid Released On is not NULL.<br>
	 */ 
	public void autoreleasedtoArchiveChck_NF (String bidNum, String adminName) throws Exception  {

		int grdCntrfound = 0; 
		String bidcheck = null, 
			   bidFlag = null, 
			   bidDate = null,
			   bidReviewerDec = null,
		       bidReleasedBy = null, 
		       bidReleasedOn = null,
		       bidReviewerReleaseOn = null;
	 			
		archiveTab.click(); 						  // loads Archive Tab first
		grdCntrfound = bidFindonArchiveTab (bidNum);  //call the routine to locate the bid
		
		// checkbox checked on found bid 
		driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[4]/div["+grdCntrfound+"]/table/tbody/tr/td/span")).click();
		
		//get the details of the found bid
		bidcheck = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[2]")).getText();
		bidFlag = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[1]")).getText();
		bidDate = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[5]")).getText();
		bidReviewerDec = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[6]")).getText();
		bidReviewerReleaseOn = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[7]")).getText();
		bidReleasedBy = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[8]")).getText();
		bidReleasedOn = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[9]")).getText();
		
		//Printing located bid and details on the logs
		printLogs (bidcheck, bidFlag, bidDate, bidReviewerDec, bidReviewerReleaseOn, bidReleasedBy, bidReleasedOn);	
			
		// Routine to verify Bid Released By is BRET Automatic and  Bid Released On is not NULL
		if (!(bidReleasedBy.equals("BRET Automatic"))) {
			
			Assert.fail("Failed. Bid found in Archive but Bid Released By is not BRET automatic");
			
			}
		else if (((bidReleasedOn.equals(NULL))) & ((bidReviewerReleaseOn.equals(NULL))) & ((bidDate.equals(NULL)))) {
			
			Assert.fail("Failed. Bid found in Archive but Bid dates/Reviwer Released On/Bid Released on are empty");
		}
		else if (!(bidReviewerDec.equals("Release"))) {
			
			Assert.fail("Failed. Bid found in Archive but incorrect reviewer decesion.");
		}  
		else if (!(bidFlag.equals("N"))) {
			
			Assert.fail("Failed. Bid found in Archive but incorrect BRET focus flag.");
		}  
		else
			System.out.println("\n Bid Found in Archived with correct details."); // bid found with no errors
		
	}
	
	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that will check the automatically released SQO NF Bid in Archive. <br>
	 * This will return void.<br>
	 * This method will display in the console the bid details.<br>
	 * <br>Test Verification: <br>
	 * Verify that Bid Released By is BRET Automatic and  Bid Released On is not NULL.<br>
	 * Verify that bid's Brand is SWG.<br>
	 */ 
	public void releasedtoArchiveChck_NFSW (String bidNum) throws Exception{

		int grdCntrfound = 0; 
		String bidcheck = null, 
			   bidFlag = null, 
			   bidDate = null,
			   bidReviewerDec = null,
		       bidReleasedBy = null, 
		       bidReleasedOn = null,
		       bidBrand = null;
	   
	    String getSystemTime = new SimpleDateFormat("MM/d/yy").format(Calendar.getInstance().getTime());
		
		archiveTab.click(); 						  // loads Archive Tab first
		grdCntrfound = bidFindonArchiveTab (bidNum);  //call the routine to locate the bid
		
		// checkbox checked on found bid 
		driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[4]/div["+grdCntrfound+"]/table/tbody/tr/td/span")).click();
		
		//get the details of the found bid
		bidcheck = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[2]")).getText();
		bidFlag = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[1]")).getText();
		bidDate = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[5]")).getText();
		bidReviewerDec = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[6]")).getText();
		bidReleasedBy = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[8]")).getText();
		bidReleasedOn = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[9]")).getText();
		bidBrand = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[12]")).getText();
		
		//Printing located bid and details on the logs
		// manual tracing
		System.out.println("\n=====Bid located in Archive Tab=====");
		System.out.println("   Bid Num: " +bidcheck);	
		System.out.println("   Flag:    " +bidFlag);	
		System.out.println("   Bid Date:  " +bidDate);
		System.out.println("   Reviewer Decesion:  " +bidReviewerDec);
		System.out.println("   Bid Released By:  " +bidReleasedBy);
		System.out.println("   Bid Released On:  " +bidReleasedOn);
		
		// routine to check SWG brand
		
		if (bidBrand.equals("SWG")) {
			
			// Routine to verify that automatically released and  Bid Released On is current date
			if ((bidReleasedBy.equals("BRET automatic")) & ((bidReleasedOn.equals(getSystemTime)))) {
				
				System.out.println("\n Bid Found in Archived with correct details.");
			}
			else
				Assert.fail("Failed. Bid found in Archive but with incorrect Bid Released By/Bid Released On."); // Will fail the test case if IF-condition is FALSE
		}else
			Assert.fail("Failed. Bid found in Archived tab is not SQO bid. Please check your data.");
			
	}

//------------- find Bid Released in Archive Routines - ends here  ------------------------------ 
	
	
	
//----------------------- Rollback Bid Routines  ---------------------------------------- 	
	
	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that will rollback the NF bid. <br>
	 * This will return void.<br>
	 */ 
	public void bidrollBack (String bidNum, String tcMethod) throws Exception {
		
		int grdCntrfound = 0;
		//String winHandleBefore = driver.getWindowHandle();
		
		archiveTab.click(); 						  // loads Archive Tab first
		grdCntrfound = bidFindonArchiveTab (bidNum);  //call the routine to locate the bid
		
		// checkbox checked on found bid 
		driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[4]/div["+grdCntrfound+"]/table/tbody/tr/td/span")).click();
				
		System.out.println("\n Bid Found in Archived prior to RollBack.");
		
		//Screen capture the found bid
		ScreenCapture screenShots = new ScreenCapture(driver);  
        screenShots.screenCaptures(tcMethod);
		
		// click Bid Detials icon
        driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[4]")).click();
        driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[4]/span")).click();                                   
              // System.out.println("tracing.. bid detail icon clicked.");
               
        // Switch to new window opened
          for(String winHandle : driver.getWindowHandles()){
                driver.switchTo().window(winHandle);
            }
          
              while (!(driver.findElement(By.xpath(".//*[@id='idx_layout_ContentPane_0']")).isDisplayed())) { // wait for the content to display
         	   
        	  // wait for 30 secounds
		      driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
              }    
              
              rollBackBtn.click();
              System.out.println("\n Bid rollback button clicked.");
              
            // Close the new window
            //driver.close();

            //Switch back to original browser (first window)
            //driver.switchTo().window(winHandleBefore);
		
	}
	
	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that will rollback the Focus bid. <br>
	 * This will return void.<br>
	 */
	public void bidrollBack_F (String bidNum, String tcMethod) throws Exception {
		
		int grdCntrfound = 0;
		//String winHandleBefore = driver.getWindowHandle();
		
		archiveTab.click(); 						  // loads Archive Tab first
		grdCntrfound = bidFindonArchiveTab (bidNum);  //call the routine to locate the bid
		
		// checkbox checked on found bid 
		driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[4]/div["+grdCntrfound+"]/table/tbody/tr/td/span")).click();
				
		System.out.println("\n Bid Found in Archived prior to RollBack.");
		
		//Screen capture the found bid
		ScreenCapture screenShots = new ScreenCapture(driver);  
        screenShots.screenCaptures(tcMethod);
		
		// click Bid Detials icon
        driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[4]")).click();
        driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[4]/span")).click();                                   
             // System.out.println("tracing.. bid detail icon clicked.");
              
       // Switch to new window opened
         for(String winHandle : driver.getWindowHandles()){
               driver.switchTo().window(winHandle);
           }
                                               
         while (!(driver.findElement(By.xpath(".//*[@id='idx_layout_ContentPane_0']")).isDisplayed())) { // wait for the content to display
      	   
        	  // wait for 30 secounds
		      driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		          
         }    
         
         BRETArchivePage.bidReviewer = assignReviewer.getText();
         
         rollBackBtn.click();
         System.out.println("\nBid rollback button clicked.");
         System.out.println("Bid is assigned to: " +BRETArchivePage.bidReviewer);
		
	}
	
//---------------- Rollback Bid Routines - ends here  ----------------------------------- 

//------------------ Call Routines  --------------------------------------- 
	
	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that will look for the bid in Archive Tab Grid. <br>
	 * This will return the location of the bid in the grid.<br>
	 */ 
	protected int bidFindonArchiveTab (String bidNumber) throws Exception {

		int bidlocatecntr = 0,
		    grdCntr = 0, 
		    pgLink = 1; 
		String bidcheck = null;
		WebElement element;
					
		driver.findElement(By.xpath(".//*[@id='dijit__FocusMixin_4']/span[3]")).click();  //Clicks '25' records per page
		
		// Routine to find the bids on Archive Tab
        do {
        	
			driver.findElement(By.xpath(".//*[@id='dijit__FocusMixin_3']/span[2]/span["+pgLink+"]")).click();  // click on the page link
			     //System.out.println("Tracing...page number " +pgLink);
			
			 grdCntr = 0;  // set to zero, this will reset the row # on the grid should the code enters the next page
				 //System.out.println("Tracing...grid number init" +grdCntr);
			
			//Routine to check the bid per page 			
			for (int pgDataCntr = 0; pgDataCntr < 25; pgDataCntr ++) {	
				   
			    grdCntr = grdCntr +1;
			    
			    // scroll until element is in view since grid is dynamic with scroll bars
				element = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntr+"]/table/tbody/tr/td[2]"));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
				Thread.sleep(500); 
				
				bidcheck = driver.findElement(By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["+grdCntr+"]/table/tbody/tr/td[2]")).getText();
					//System.out.println("Tracing...grid number inside FOR LOOP  " +grdCntr);
					//System.out.println("Tracing...bid number " +bidcheck);
													
					if (bidNumber.equals(bidcheck)) { // break if bid is found
						break;
					}					
			}
			
			pgLink ++;  // go to next page
						
		} while (!(bidNumber.equals(bidcheck)));  //checking the specified bid on the grid
		
        bidlocatecntr = grdCntr;
        
		return bidlocatecntr;			
	}

	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine to printing on logs for manual trace. <br>
	 * This will return void.<br>
	 */ 
	protected void printLogs (String bidNum, String bidFlag, String bidDate, String bidRdesc, String dateRdesc, String bidRelby, String dateRelby) {
    	 //------------------------------------------------------ 
		 // Routine to printing on logs for manual trace
	     //------------------------------------------------------

    	System.out.println("\n=====Bid located in Archive Tab=====");
		System.out.println("   Bid Num: " +bidNum);	
		System.out.println("   Flag:    " +bidFlag);	
		System.out.println("   Bid Date:  " +bidDate);
		System.out.println("   Reviewer Decesion:  " +bidRdesc);
		System.out.println("   Reviewer Released On:  " +dateRdesc);
		System.out.println("   Bid Released By:  " +bidRelby);
		System.out.println("   Bid Released On:  " +dateRelby);
    }
    

	
}


