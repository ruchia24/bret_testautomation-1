package com.bret.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MyTaskPage extends SummaryPage {
	// Table column constants of MyTaskTabPage
	// Constants variable that you can use (static final int CAPITAL
	// LETTERS,separated by UnderScore)
	public static final int FLAG_COL = 1;
	public static final int BID_ID_COL = 2;
	public static final int ADDITIONAL_ID_COL = 3;
	public static final int DETAIL_LINK_COL = 4;
	public static final int BID_DATE_COL = 5;
	public static final int REVIEWER_DECISION_COL = 6;
	public static final int REVIEWER_COL = 7;
	public static final int RELEASED_ON_COL = 8;
	public static final int COUNTRY_COL = 9;
	public static final int IBM_SELL_PRICE_COL = 10;
	public static final int PRIMARY_BRAND_COL = 11;
	public static final int DISTRIBUTOR_COL = 12;
	public static final int CUST_FACING_BP_COL = 13;
	public static final int CUST_NAME = 14;

	public static final String SOURCE_SYS_ALL = "ALL";
	public static final String SOURCE_SYS_EPRICER = "EPRICER";
	public static final String SOURCE_SYS_BH = "BH";
	public static final String SOURCE_SYS_BPMS = "BPMS";
	public static final String SOURCE_SYS_OPRA = "OPRA";
	public static final String SOURCE_SYS_SQO = "SQO";
	public static final String SOURCE_SYS_EORDER = "EORDER";

	public static final int A_COL = 15;
	public static final int B_COL = 16;
	public static final int C_COL = 17;
	public static final int D_COL = 18;

	private static final String XPATH_PART_1 = ".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div[";
	private static final String XPATH_PART_2 = "]/table/tbody/tr/td[";
	private static final String XPATH_PART_3 = "]";

	private static final String XPATH_CHECKBOX_PART_1 = "/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[3]/div[1]/div/div[5]/div[3]/div[4]/div[";
	private static final String XPATH_CHECKBOX_PART_2 = "]/table/tbody/tr/td/span";

	private static final String XPATH_RELEASE_BTN = "//*[@id=\"myTaskReleaseBidsBtn\"]";
	private static final String XPATH_READY_TO_REVIEW_BTN = "//*[@id=\"myTaskReadyToReviewBtn\"]";

	private static final String XPATH_MY_PRIVILEGE_CLOSE_BTN = "//*[@id=\"dijit_form_Button_2_label\"]"; 
	
	private static final String XPATH_EXPORT_BTN = "//*[@id=\"myTaskExportToExcelBtn\"]";

	private static final String XPATH_USER_NAME = "//*[@id=\"idx_form_DropDownLink_0_link\"]";
	private static final String XPATH_SHOW_MY_PRIVILEGE = "//*[@id=\"dijit_MenuItem_0_text\"]";
	
	//private static final String XPATH_ROLE_VIEWER_WINDOW_POPUP ="//div[@class='dijitDialogPaneContent']";
	private static final String XPATH_SHOW_MY_PRIVILEGE_DETAILS = "//div[@class='dijitDialogPaneContent']";

	//private static final String XPATH_SHOW_MY_PRIVILEGE_DETAILS = "//*[@id=\"myPrivilegeDlg\"]";

	private static final String XPATH_ARCHIVE_BTN = "/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[3]/div[1]/div/div[2]/span[2]/span";
	
	private static final String XPATH_BID_FIELD = "//*[@class=\"gridxBodyEmpty\"]";
	
	// Added by Calvin on June 27
	private static final String XPATH_TAKE_THIS_BID_BTN = "//*[@id=\"assignBtn_label\"]";
	

	/**
	 * Added by Lhoyd Castillo - 1/31/2018 this is to get the bid number on the
	 * search result
	 */
	private static final String XPATH_RESULT_1 = ".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[";
	private static final String XPATH_RESULT_2 = "]";

	private WebDriver driver;

	private String constructXpath(int rowIndex, int colIndex) {
		return XPATH_PART_1 + rowIndex + XPATH_PART_2 + colIndex + XPATH_PART_3;
	}

	public void clickFlag(WebDriver driver) {
		driver.findElement(By.cssSelector(".//*[@id='myTaskBidsGridDiv']/div[3]/div[4]/div[1]/table/tbody/tr/td/span"))
				.click();

	}

	/**
	 * Gets the value at index given the row index and the column index
	 * 
	 * @param driver
	 * @param rowIndex
	 * @param colIndex
	 * @return the text of the Web element
	 */
	public String getValueAtIndex(WebDriver driver, int rowIndex, int colIndex) {
		WebElement col = driver.findElement(By.xpath(constructXpath(rowIndex, colIndex)));

		String value = "";

		if (null != value) {
			value = col.getText();
		}

		return value;

	}

	/**
	 * Search the row index from the table given the bid id. Returns 0 if not found.
	 * 
	 * @param driver
	 * @param bidId
	 * @return
	 */
	public int searchRowIndex(WebDriver driver, String bidId) {
		this.driver = driver;
		int rowIndex = 0;
		try {
			rowIndex = (findBidIdRow(bidId));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return rowIndex;
	}

	
	public boolean aHasMarker(int bidRow) {
		String currentXpath = constructXpath(bidRow, A_COL);
		WebElement col = driver.findElement(By.xpath(currentXpath));
		try {
			col.findElement(By.className("idxGridIconFailed"));
			return true;
		} catch (NoSuchElementException e) {
			// System.out.println("Col: " + A_COL + "has no element");
			return false;
		}

	}

	public boolean bHasMarker(int bidRow) {
		String currentXpath = constructXpath(bidRow, B_COL);
		WebElement col = driver.findElement(By.xpath(currentXpath));
		try {
			col.findElement(By.className("idxGridIconFailed"));
			return true;
		} catch (NoSuchElementException e) {
			// System.out.println("Col: " + B_COL + "has no element");
			return false;
		}
	}

	public boolean cHasMarker(int bidRow) {
		String currentXpath = constructXpath(bidRow, C_COL);
		WebElement col = driver.findElement(By.xpath(currentXpath));
		try {
			col.findElement(By.className("idxGridIconFailed"));
			return true;
		} catch (NoSuchElementException e) {
			// System.out.println("Col: " + C_COL + "has no element");
			return false;
		}
	}

	public boolean dHasMarker(int bidRow) {
		String currentXpath = constructXpath(bidRow, D_COL);
		WebElement col = driver.findElement(By.xpath(currentXpath));
		try {
			col.findElement(By.className("idxGridIconFailed"));
			return true;
		} catch (NoSuchElementException e) {
			// System.out.println("Col: " + D_COL + "has no element");
			return false;
		}
	}

	public void clickDetailLink(WebDriver driver, int myTaskRowIndex) {
		driver.findElement(By.xpath(constructXpath(myTaskRowIndex, DETAIL_LINK_COL) + "/span")).click();
	}

	public void clickCheckbox(int rowIndex) {
		WebElement checkbox = driver.findElement(By.xpath(XPATH_CHECKBOX_PART_1 + rowIndex + XPATH_CHECKBOX_PART_2));

		checkbox.click();
	}

	public void clickCheckbox(WebDriver driver, int rowIndex) {
		this.driver = driver;
		WebElement checkbox = driver.findElement(By.xpath(XPATH_CHECKBOX_PART_1 + rowIndex + XPATH_CHECKBOX_PART_2));

		checkbox.click();
	}

	/**
	 * TODO: Frey we can refactor this
	 * 
	 * @param bidNumber
	 * @return
	 * @throws Exception
	 */
	private int findBidIdRow(String bidNumber) throws Exception {
		int bidlocatecntr = 0, grdCntr = 0, pgLink = 1;
		String bidcheck = null;
		WebElement element;

		driver.findElement(By.xpath(".//*[@id='dijit__FocusMixin_1']/span[3]")).click(); // Clicks '25' records per page
		do {

			driver.findElement(By.xpath(".//*[@id='dijit__FocusMixin_0']/span[2]/span[" + pgLink + "]")).click(); // click
																													// on
																													// the
																													// page
																													// link
			grdCntr = 0; // set to zero, this will reset the row # on the grid
							// should the code enters the next page
			// System.out.println("Tracing...grid number init" +grdCntr);

			// Routine to check the bid per page
			for (int pgDataCntr = 0; pgDataCntr < 25; pgDataCntr++) {

				grdCntr = grdCntr + 1;

				// scroll until element is in view since grid is dynamic with
				// scroll bars
				element = driver.findElement(By.xpath(
						".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div[" + grdCntr + "]/table/tbody/tr/td[2]"));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
				Thread.sleep(500);

				bidcheck = driver.findElement(By
						.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div[" + grdCntr + "]/table/tbody/tr/td[2]"))
						.getText();

				if (bidNumber.equals(bidcheck)) { // break if bid is found
					break;
				}
			}

			pgLink++; // go to next page

		} while (!(bidNumber.equals(bidcheck))); // checking the specified bid
													// on the grid

		bidlocatecntr = grdCntr;

		return bidlocatecntr;
	}

	// return boolean
	public boolean searchNewInMyTask(WebDriver driver, String newBid, String reviewerDecision)
			throws InterruptedException {

		System.out.println("trace: displaying the bid number and reviewer decision from properties file");
		System.out.println("BID NUMBER:        " + newBid);
		System.out.println("REVIEWER DECISION: " + reviewerDecision);
		System.out.println();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		driver.findElement(By.xpath(".//*[@id='dijit_layout_TabContainer_0_tablist']/div[4]/div/div[1]")).click();

		for (int rownumMyTask = 1; rownumMyTask < 25; rownumMyTask++) {

			String decisionMyTask = driver.findElement(By.xpath(
					".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div[" + rownumMyTask + "]/table/tbody/tr/td[6]"))
					.getText();
			String bidMyTask = driver.findElement(By.xpath(
					".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div[" + rownumMyTask + "]/table/tbody/tr/td[2]"))
					.getText();
			System.out.println();

			if (decisionMyTask.equals(reviewerDecision) && bidMyTask.equals(newBid)) {
				System.out.println("rDecision & bidNew: " + decisionMyTask + " & " + bidMyTask);
				System.out.println("reviewerDecision & bidNumberNew: " + reviewerDecision + " & " + newBid);
				return true;
			}
		}
		return false;
	}

	/**
	 * Added by Lhoyd Castillo - 1/31/2018 this is to get the bid number on the
	 * search result
	 * 
	 */
	private String constructSearchResultXpath(int colIndex) {
		return XPATH_RESULT_1 + colIndex + XPATH_RESULT_2;
	}

	public String getSearchResultValue(WebDriver driver, int colIndex) {
		WebElement col = driver.findElement(By.xpath(constructSearchResultXpath(colIndex)));

		String value = "";

		if (null != value) {
			value = col.getText();
		}
		return value;
	}

	/**
	 * Checks the release button visibility.
	 * 
	 * @param driver
	 * @return
	 */
	public boolean checkReleaseButtonVisibility(WebDriver driver) {
		boolean isVisible = false;

		try {
			driver.findElement(By.xpath(XPATH_RELEASE_BTN));
			isVisible = true;
		} catch (NoSuchElementException e) {
			isVisible = false;
		}

		return isVisible;
	}

	/**
	 * Method for clicking the 'Release Button'.
	 * 
	 * @param driver
	 */
	public void clickReleaseButton(WebDriver driver) {
		driver.findElement(By.xpath(XPATH_RELEASE_BTN)).click();
	}

	/**
	 * Checks the ready for review button visibility.
	 * 
	 * @param driver
	 * @return
	 */
	public boolean checkReadyForReviewButtonVisibility(WebDriver driver) {
		boolean isVisible = false;

		try {
			driver.findElement(By.xpath(XPATH_READY_TO_REVIEW_BTN));
			isVisible = true;
		} catch (NoSuchElementException e) {
			isVisible = false;
		}

		return isVisible;
	}

	/**
	 * Method for clicking the 'Ready for Review Button'.
	 * 
	 * @param driver
	 */
	public void clickReadyForReviewButton(WebDriver driver) {
		driver.findElement(By.xpath(XPATH_READY_TO_REVIEW_BTN)).click();
	}

	public void clickExportToCSVButton(WebDriver driver) {
		driver.findElement(By.xpath(XPATH_EXPORT_BTN)).click();
	}

	/**
	 * Selects a source system in multi-select list
	 * 
	 * @param driver
	 * @param sourceSystem
	 *            the source system you wish to select
	 */
	public void selectSourceSystem(WebDriver driver, String sourceSystem) {
		// Deselect "ALL" before selecting a source system
		driver.findElement(By.xpath("//select[@id='myTask_srcSystemSelect']/option[text() = 'ALL']")).click();
		String selectedSourceSysXpath = "//select[@id='myTask_srcSystemSelect']/option[text() = '" + sourceSystem
				+ "']";
		driver.findElement(By.xpath(selectedSourceSysXpath)).click();
	}

	/**
	 * Added for the BRET WEB Role - 6/13/2018
	 * 
	 * @author LhoydCastillo
	 * @param driver
	 */
	public void clickUserName(WebDriver driver) {
		driver.findElement(By.xpath(XPATH_USER_NAME)).click();
	}

	/**
	 * Added for the BRET WEB Role - 6/13/2018
	 * 
	 * @author LhoydCastillo
	 * @param driver
	 */
	public void clickShowMyPrivilege(WebDriver driver) {
		driver.findElement(By.xpath(XPATH_SHOW_MY_PRIVILEGE)).click();
	}
	
	public void closeMyPrivilege(WebDriver driver) {
		driver.findElement(By.xpath(XPATH_MY_PRIVILEGE_CLOSE_BTN)).click();
	}

	public void clickArchiveButton(WebDriver driver) {
		driver.findElement(By.xpath(XPATH_ARCHIVE_BTN)).click();
	}

	public boolean exportButtonIsDisplayed(WebDriver driver) {
		WebElement exportButton = driver.findElement(By.xpath(XPATH_EXPORT_BTN));
		boolean exportButtonIsDisplayed = exportButton.isDisplayed();
		return exportButtonIsDisplayed;
	}
	
	public String getBidFieldText(WebDriver driver) {
		WebElement bidLocator = driver.findElement(By.xpath(XPATH_BID_FIELD));
		String fieldText = bidLocator.getText();
		return fieldText;
	}

	public String getAccessDetails(WebDriver driver) {
		WebElement showMyPrivilegeField = driver.findElement(By.xpath(XPATH_SHOW_MY_PRIVILEGE_DETAILS));
		String fieldText = showMyPrivilegeField.getText();

		return fieldText;
	}
	
	// Added by Calvin on June 27	
	public boolean searchNewInMyTask2(WebDriver driver, String reviewerDecision)
			throws InterruptedException {

		System.out.println("Finding bids with \"New\" status");
		System.out.println();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		driver.findElement(By.xpath(".//*[@id='dijit_layout_TabContainer_0_tablist']/div[4]/div/div[1]")).click();

		for (int rownumMyTask = 1; rownumMyTask < 25; rownumMyTask++) {

			String decisionMyTask = driver.findElement(By.xpath(
					".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div[" + rownumMyTask + "]/table/tbody/tr/td[6]"))
					.getText();
			String bidMyTask = driver.findElement(By.xpath(
					".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div[" + rownumMyTask + "]/table/tbody/tr/td[2]"))
					.getText();

			if (decisionMyTask.equals(reviewerDecision)) {
				System.out.println("Reviewer Decision: " + decisionMyTask);
				System.out.println("Bid with New Status: " + bidMyTask);

				driver.findElement(By.xpath(
					".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div[" + rownumMyTask + "]/table/tbody/tr/td[4]")).click();
					
				return true;
			}
		}
		return false;
	}
	
	// Added by Calvin on June 27
	public boolean takeThisBidButtonIsDisplayed(WebDriver driver) {
		WebElement takeThisBidButton = driver.findElement(By.xpath(XPATH_TAKE_THIS_BID_BTN));
		boolean takeThisBidButtonIsDisplayed = takeThisBidButton.isDisplayed();
		return takeThisBidButtonIsDisplayed;
	}
	
/* added by sarita , thhis function is for searching for a bid and clicking on ready to review button */
	
	public boolean searchBidsInMyTaskAndClickArchive(WebDriver driver, String reviewerDecision)
			throws InterruptedException {

		System.out.println("Finding bids with " + reviewerDecision + " status");
		System.out.println();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		//driver.findElement(By.xpath(".//*[@id='dijit_layout_TabContainer_0_tablist']/div[4]/div/div[1]")).click();

		for (int nextClick = 1; nextClick < 25; nextClick++) {
			
			for(int rownumMyTask= 1; rownumMyTask<11 ;rownumMyTask++)
			{

			String decisionMyTask = driver.findElement(By.xpath(
					".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div[" + rownumMyTask + "]/table/tbody/tr/td[6]"))
					.getText();
			String bidMyTask = driver.findElement(By.xpath(
					".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div[" + rownumMyTask + "]/table/tbody/tr/td[2]"))
					.getText();

			if (decisionMyTask.equals(reviewerDecision)) {
				System.out.println("Reviewer Decision: " + decisionMyTask);
				System.out.println("Bid with " + reviewerDecision + " Status: " + bidMyTask);

				driver.findElement(By.xpath(
					"/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[3]/div[1]/div/div[5]/div[3]/div[4]/div[" + rownumMyTask + "]/table/tbody/tr/td/span")).click();
                //driver.findElement(By.xpath("//*[@id='myTaskReadyToReviewBtn']")).click();
				driver.findElement(By.xpath(XPATH_ARCHIVE_BTN)).click();
				return true;
					}
		 
		}
			driver.findElement(By.xpath("//span[@class='gridxPagerStepperBtn gridxPagerNextPage']")).click();
	}
		return false;
	}
	public boolean searchBidsInMyTaskandClickReadytoReview(WebDriver driver, String reviewerDecision)
			throws InterruptedException {

		System.out.println("Finding bids with " + reviewerDecision + " status");
		System.out.println();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		//driver.findElement(By.xpath(".//*[@id='dijit_layout_TabContainer_0_tablist']/div[4]/div/div[1]")).click();

		for (int nextClick = 1; nextClick < 25; nextClick++) {
			
			for(int rownumMyTask= 1; rownumMyTask<11 ;rownumMyTask++)
			{

			String decisionMyTask = driver.findElement(By.xpath(
					".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div[" + rownumMyTask + "]/table/tbody/tr/td[6]"))
					.getText();
			String bidMyTask = driver.findElement(By.xpath(
					".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div[" + rownumMyTask + "]/table/tbody/tr/td[2]"))
					.getText();

			if (decisionMyTask.equals(reviewerDecision)) {
				System.out.println("Reviewer Decision: " + decisionMyTask);
				System.out.println("Bid with " + reviewerDecision + " Status: " + bidMyTask);

				driver.findElement(By.xpath(
					"/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[3]/div[1]/div/div[5]/div[3]/div[4]/div[" + rownumMyTask + "]/table/tbody/tr/td/span")).click();
                //driver.findElement(By.xpath("//*[@id='myTaskReadyToReviewBtn']")).click();
				driver.findElement(By.xpath(XPATH_READY_TO_REVIEW_BTN)).click();
				return true;
					}
		 
		}
			driver.findElement(By.xpath("//span[@class='gridxPagerStepperBtn gridxPagerNextPage']")).click();
	  }
		return false;
	}
	//Created by Sarita , this is used when you want to check My privilege text
	public boolean getMyPrivilegeText(WebDriver driver, String role) {
		WebElement myPrivilege = driver.findElement(By.xpath(XPATH_SHOW_MY_PRIVILEGE_DETAILS));
		String fieldText = myPrivilege.getText();
		if(fieldText.contains(role))
		{
		
		return true;
	}
	return false;
  }
	public boolean searchBidsByFlagInMyTaskandClickReadyToReview(WebDriver driver, String flag)
			throws InterruptedException {

		System.out.println("Finding bids with " + flag + " status");
		System.out.println();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		//driver.findElement(By.xpath(".//*[@id='dijit_layout_TabContainer_0_tablist']/div[4]/div/div[1]")).click();

		for (int nextClick = 1; nextClick < 25; nextClick++) {
			
			for(int rownumMyTask= 1; rownumMyTask<11 ;rownumMyTask++)
			{

			String flagStatus = driver.findElement(By.xpath(
					"/html[1]/body[1]/div[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[5]/div[3]/div[2]/div[" + rownumMyTask + "]/table[1]/tbody[1]/tr[1]/td[1]"))
					.getText();
			String bidMyTask = driver.findElement(By.xpath(
					".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div[" + rownumMyTask + "]/table/tbody/tr/td[2]"))
					.getText();
			String decisionMyTask = driver.findElement(By.xpath(
					".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div[" + rownumMyTask + "]/table/tbody/tr/td[6]"))
					.getText();

			if (flagStatus.equals(flag)) {
				System.out.println("Bid with Flag" + flag + " is: " + bidMyTask);
				System.out.println("Reviewer Decision: " + decisionMyTask);
				

				driver.findElement(By.xpath(
					"/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[3]/div[1]/div/div[5]/div[3]/div[4]/div[" + rownumMyTask + "]/table/tbody/tr/td/span")).click();
                //driver.findElement(By.xpath("//*[@id='myTaskReadyToReviewBtn']")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath(XPATH_READY_TO_REVIEW_BTN)).click();
				return true;
					}
		 
		}
			driver.findElement(By.xpath("//span[@class='gridxPagerStepperBtn gridxPagerNextPage']")).click();
	}
		return false;
}
	public void searchBidsByFlagAndReviewedDecisionInMyTask(WebDriver driver, String flag, String reviewerDecision)
			throws InterruptedException {

		System.out.println("Finding bids with " + flag + " status and reviewer decision " + reviewerDecision );
		System.out.println();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		//driver.findElement(By.xpath(".//*[@id='dijit_layout_TabContainer_0_tablist']/div[4]/div/div[1]")).click();

		for (int nextClick = 1; nextClick < 25; nextClick++) {
			
			for(int rownumMyTask= 1; rownumMyTask<11 ;rownumMyTask++)
			{

			String flagStatus = driver.findElement(By.xpath(
					"/html[1]/body[1]/div[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[5]/div[3]/div[2]/div[" + rownumMyTask + "]/table[1]/tbody[1]/tr[1]/td[1]"))
					.getText();
			String bidMyTask = driver.findElement(By.xpath(
					".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div[" + rownumMyTask + "]/table/tbody/tr/td[2]"))
					.getText();
			String decisionMyTask = driver.findElement(By.xpath(
					".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div[" + rownumMyTask + "]/table/tbody/tr/td[6]"))
					.getText();

			if (flagStatus.equals(flag) && decisionMyTask.equals(reviewerDecision) ) {
				System.out.println("Bid with Flag" + flag + " is: " + bidMyTask);
				System.out.println("Reviewer Decision: " + decisionMyTask);
				

				driver.findElement(By.xpath(
					"/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[3]/div[1]/div/div[5]/div[3]/div[4]/div[" + rownumMyTask + "]/table/tbody/tr/td/span")).click();
                //driver.findElement(By.xpath("//*[@id='myTaskReadyToReviewBtn']")).click();
				Thread.sleep(1000);
				//driver.findElement(By.xpath(XPATH_READY_TO_REVIEW_BTN)).click();
				//return true;
					}
		 
		   }
			driver.findElement(By.xpath("//span[@class='gridxPagerStepperBtn gridxPagerNextPage']")).click();
	 }
		//return false;
  }
	   public boolean ClickExportToExcelAndSaveTheFile(WebDriver driver)
	   {
		    System.out.println("Clicking on Export to CSV button");
			driver.findElement(By.xpath(XPATH_EXPORT_BTN)).click();
			
			Robot robot;
			try {
				robot = new Robot();
				robot.keyPress(KeyEvent.VK_DOWN);
	            robot.keyPress(KeyEvent.VK_ENTER);
				
				robot.keyRelease(KeyEvent.VK_DOWN);
				robot.keyRelease(KeyEvent.VK_ENTER);
				return true;
			} catch (AWTException e) 
			{
				// TODO Auto-generated catch block
				return false;
			}
			
			
	   }
	  
}
	  



