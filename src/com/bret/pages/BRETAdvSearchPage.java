package com.bret.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.bret.util.BretTestUtils;

/**
 *TODO: Create Xpath for Buttons
 *
 * @author LhoydCastillo
 *
 */
public class BRETAdvSearchPage {
	
	
	// Table column constants of Advanced Search Page
	public static final int FLAG_COL = 1;
	public static final int BID_ID_COL = 2;
	public static final int ADDITIONAL_ID_COL = 3;
	public static final int DETAIL_LINK_COL = 4;
	public static final int BID_DATE_COL = 5;
	public static final int SOURCE_SYSTEM_COL = 6;
	public static final int REGION_COL = 7;
	public static final int COUNTRY_COL = 8;
	public static final int REVIEWER_DECISION_COL = 9;
	public static final int REVIEWER_COL = 10;
	public static final int RELEASED_ON_COL = 11;
	public static final int TIER1_COL = 12;
	public static final int TIER2_COL = 13;

	public WebDriver driver;

	//CheckBox
	
	private static final String XPATH_CHECKBOX_PART_1 = "/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[3]/div[8]/div/div[4]/div/div[3]/div[4]/div[";
	private static final String XPATH_CHECKBOX_PART_2 = "]/table/tbody/tr/td/span";
	
	// This is a web page class constructor
	public BRETAdvSearchPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	BretTestUtils utils = new BretTestUtils(driver);
	public static final String XPATH_ADVSRCH_BID_ID = ".//*[@id='locateBidInputBox']";
	public static final String XPATH_ADVSRCH_ADDITIONAL_ID = ".//*[@id='locateAdditionalInputBox']";
	public static final String XPATH_BID_ID = ".//*[@id='locateBidInputBox']";
	public static final String XPATH_ADDITIONAL_ID = ".//*[@id='locateAdditionalInputBox']";
	public static final String XPATH_RECORD_ID = ".//*[@id='locateRecordIDInputBox']";
	public static final String XPATH_REVIEWER = ".//*[@id='locateReviewerInputBox']";
	public static final String XPATH_CHANNEL_PROGRAM = ".//*[@id='locateChannelProgramInputBox']";
	public static final String XPATH_TIER_1_CEID = ".//*[@id='locateTier1InputBox']";
	public static final String XPATH_TIER_2_CEID = ".//*[@id='locateTier2InputBox']";
	public static final String XPATH_COUNTRY = ".//*[@id='locateCountryInputBox']";

	// Drop down
	public static final String XPATH_BID_FROM = ".//*[@id='bidFromDate']";

	public static final String XPATH_BID_TO = ".//*[@id='bidToDate']";

	public static final String XPATH_BID_EXPIRY_DATE_FROM = ".//*[@id='bidExpiryFromDate']";

	public static final String XPATH_BID_EXPIRY_DATE_TO = ".//*[@id='bidExpiryToDate']";

	public static final String XPATH_CLOSE_DATE_FROM = ".//*[@id='closeDateFrom']";
	public static final String XPATH_CLOSE_DATE_TO = ".//*[@id='closeDateTo']";
	public static final String XPATH_DEADLINE_OF_REQUEST_FROM = ".//*[@id='deadLineOfRequestFromID']";
	public static final String XPATH_DEADLINE_OF_REQUEST_TO = ".//*[@id='deadLineOfRequestToID']";

	// Button Down
	public static final String XPATH_REVIEWER_DECISION = ".//*[@id='locateReviewerDecisionInputBox']/tbody/tr/td[1]";
	public static final String XPATH_OUTCOME = ".//*[@id='outcome']/tbody/tr/td[1]";
	public static final String XPATH_REMEDIATION_CLOSED = "";
	public static final String XPATH_BID_TYPE = "";
	public static final String XPATH_EXCEPTION_FLAG = "";
	public static final String XPATH_TRANSPARENCY_LETTER = "";
	public static final String XPATH_REGION = "";
	public static final String XPATH_SOURCE_SYSTEM = ".//*[@id='sourceSystem']/tbody/tr/td[1]";
	public static final String XPATH_CONDITION = "";
	public static final String XPATH_POST_SHIP = "";
	public static final String XPATH_BID_EVENT = "";

	public static final String XPATH_SEARCH_BUTTON = "//span[@id='locateBidSearchBtn']";
	public static final String XPATH_SEARCH_CONDITION = ".//*[@id='advSearchSearchPane_titleBarNode']/div/span[1]";

	 public static final String XPATH_DISPLAY_MESSAGE_NO_ITEM =
	 ".//*[@class='gridxMain']/div[contains(text(),'No items to display')]";

	public static final String XPATH_DISPLAY_MESSAGE = "a[@id = 'unique-id']/@href";

	public void setInputField(String field, String bidData) {
		WebElement enterBid = driver.findElement(By.xpath(field));
		enterBid.sendKeys(bidData);
	}

	public void clearInputField(String field) {
		WebElement searchField = driver.findElement(By.xpath(field));
		searchField.clear();
	}

	public void clickSearch() {
		WebElement searchButton = driver.findElement(By.xpath(XPATH_SEARCH_BUTTON));
		searchButton.click();
	}

	public void clickSearchCondition() {
		WebElement clickHere = driver.findElement(By.xpath(XPATH_SEARCH_CONDITION));
		clickHere.click();
	}

	public String getValueAt(String xpath) {
		WebElement element = driver.findElement(By.xpath(xpath));
		String value = element.getText();
		return value;
	}

	public boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * @author LhoydCastillo 1/31/2018 this is to get the bid number on the search
	 *         result
	 * 
	 */
	private static final String XPATH_RESULT_1 = ".//*[@id='gridNode']/div[3]/div[2]/div/table/tbody/tr/td[";
	private static final String XPATH_RESULT_2 = "]";

	private String constructSearchResultXpath(int colIndex) {
		return XPATH_RESULT_1 + colIndex + XPATH_RESULT_2;
	}

	public String getSearchResultValue(WebDriver driver, int colIndex) {
		WebElement col = driver.findElement(By.xpath(constructSearchResultXpath(colIndex)));

		String value = "";

		if (null != value) {
			value = col.getText();
		}
		return value;
	}
	
	/*
	 * Added by Jash, to click Checkbox of search result 
	 */
	
	public void clickCheckbox(int rowIndex) {
		WebElement checkbox = driver.findElement(By.xpath(XPATH_CHECKBOX_PART_1 + rowIndex + XPATH_CHECKBOX_PART_2));

		checkbox.click();
	}

	public void clickDetailLinkSingleRecord(WebDriver driver) {
		driver.findElement(By.xpath(constructSearchResultXpath( DETAIL_LINK_COL) + "/span")).click();
	}

}
