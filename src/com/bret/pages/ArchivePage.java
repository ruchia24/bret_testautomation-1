package com.bret.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ArchivePage extends SummaryPage {
	public static final int FLAG_COL = 1;
	public static final int BID_ID_COL = 2;
	public static final int ADDITIONAL_ID_COL = 3;
	public static final int DETAIL_LINK_COL = 4;
	public static final int BID_DATE_COL = 5;
	public static final int REVIEWER_DECISION_COL = 6;
	public static final int REVIEWER_RELEASED_ON_COL = 7;
	public static final int BID_RELEASED_BY_COL = 8;
	public static final int BID_RELEASED_ON_COL = 9;
	public static final int COUNTRY_COL = 10;
	public static final int IBM_SELL_PRICE_COL = 11;
	public static final int PRIMARY_BRAND_COL = 12;
	public static final int DISTRIBUTOR_COL = 13;
	public static final int CUST_FACING_BP_COL = 14;
	public static final int CUST_NAME = 15;

	public static final int A_COL = 16;
	public static final int B_COL = 17;
	public static final int C_COL = 18;
	public static final int D_COL = 19;

	private static final String XPATH_PART_1 = ".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div[";
	private static final String XPATH_PART_2 = "]/table/tbody/tr/td[";
	private static final String XPATH_PART_3 = "]";

	private static final String XPATH_CHECKBOX_PART_1 = "/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[3]/div[4]/div/div[5]/div[3]/div[4]/div[";
	private static final String XPATH_CHECKBOX_PART_2 = "]/table/tbody/tr/td/span";

	private static final String XPATH_EXPORT_BTN = "//*[@id=\"archiveExportToExcelBtn\"]";
	
	private static final String XPATH_FOLLOWUP_BTN = "//*[@id=\"archiveRemediationFollowUpBtn\"]";
	
	//Added by Calvin on June 25
	private static final String XPATH_STATUS_REPORT = "//*[@id=\"archiveRemediationStatusReportBtn_label\"]";
	
	private WebDriver driver;

	private String constructXpath(int rowIndex, int colIndex) {
		return XPATH_PART_1 + rowIndex + XPATH_PART_2 + colIndex + XPATH_PART_3;
	}

	/**
	 * Gets the value at index given the row index and the column index
	 * 
	 * @param driver
	 * @param rowIndex
	 * @param colIndex
	 * @return the text of the Web element
	 */
	public String getValueAtIndex(WebDriver driver, int rowIndex, int colIndex) {
		WebElement col = driver.findElement(By.xpath(constructXpath(rowIndex,
				colIndex)));

		String value = "";

		if (null != value) {
			value = col.getText();
		}

		return value;

	}

	/**
	 * Search the row index from the table given the bid id. Returns 0 if not
	 * found.
	 * 
	 * @param driver
	 * @param bidId
	 * @return
	 */
	public int searchRowIndex(WebDriver driver, String bidId) {
		this.driver = driver;
		int rowIndex = 0;
		try {
			rowIndex = (findBidIdRow(bidId));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return rowIndex;
	}

	public boolean aHasMarker(int bidRow) {
		String currentXpath = constructXpath(bidRow, A_COL);
		WebElement col = driver.findElement(By.xpath(currentXpath));
		try {
			col.findElement(By.className("idxGridIconFailed"));
			return true;
		} catch (NoSuchElementException e) {
			// System.out.println("Col: " + A_COL + "has no element");
			return false;
		}

		
	}
	

	public boolean bHasMarker(int bidRow) {
		String currentXpath = constructXpath(bidRow, B_COL);
		WebElement col = driver.findElement(By.xpath(currentXpath));
		try {
			col.findElement(By.className("idxGridIconFailed"));
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public boolean cHasMarker(int bidRow) {
		String currentXpath = constructXpath(bidRow, C_COL);
		WebElement col = driver.findElement(By.xpath(currentXpath));
		try {
			col.findElement(By.className("idxGridIconFailed"));
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public boolean dHasMarker(int bidRow) {
		String currentXpath = constructXpath(bidRow, D_COL);
		WebElement col = driver.findElement(By.xpath(currentXpath));
		try {
			col.findElement(By.className("idxGridIconFailed"));
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public void clickDetailLink(WebDriver driver, int myTaskRowIndex) {
		driver.findElement(
				By.xpath(constructXpath(myTaskRowIndex, DETAIL_LINK_COL)
						+ "/span")).click();
	}

	public void clickCheckbox(int rowIndex) {
		WebElement checkbox = driver.findElement(By.xpath(XPATH_CHECKBOX_PART_1 + rowIndex + XPATH_CHECKBOX_PART_2));

		checkbox.click();
	}
	
	public void clickCheckbox(WebDriver driver, int rowIndex) {
		this.driver = driver;
		WebElement checkbox = driver.findElement(By.xpath(XPATH_CHECKBOX_PART_1 + rowIndex + XPATH_CHECKBOX_PART_2));

		checkbox.click();
	}
	
//	public void clickCheckBox (int rowIndex){
//	WebElement checkbox = driver.findElement(By.xpath(XPATH_CHECKBOX_PART_1 + rowIndex + XPATH_CHECKBOX_PART_2));
//
//	checkbox.click();
//}
	

	/**
	 * 
	 * @param bidNumber
	 * @return
	 * @throws Exception
	 */
	private int findBidIdRow(String bidNumber) throws Exception {
		int bidlocatecntr = 0;
		int grdCntr = 0;
		int pgLink = 1;
		String bidcheck = null;
		WebElement element;
		Thread.sleep(15000);
		driver.findElement(By.xpath(".//*[@id='dijit__FocusMixin_4']/span[3]"))
				.click();
		do {

			driver.findElement(
					By.xpath(".//*[@id='dijit__FocusMixin_3']/span[2]/span["
							+ pgLink + "]")).click();

			grdCntr = 0;

			for (int pgDataCntr = 0; pgDataCntr < 25; pgDataCntr++) {
				grdCntr = grdCntr + 1;
				element = driver
						.findElement(By
								.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["
										+ grdCntr + "]/table/tbody/tr/td[2]"));
				((JavascriptExecutor) driver).executeScript(
						"arguments[0].scrollIntoView(true);", element);
				Thread.sleep(500);

				bidcheck = driver
						.findElement(
								By.xpath(".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div["
										+ grdCntr + "]/table/tbody/tr/td[2]"))
						.getText();
				if (bidNumber.equals(bidcheck)) {
					break;
				}
			}
			pgLink++;
		} while (!(bidNumber.equals(bidcheck)));
		bidlocatecntr = grdCntr;

		return bidlocatecntr;
	}

	
	/**
	 * Added by Lhoyd Castillo - 1/31/2018
	 * this is to get the bid number on the search result
	 * 
	 */
	private static final String XPATH_RESULT_1 = ".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[";
	private static final String XPATH_RESULT_2 = "]";

	
	private String constructSearchResultXpath(int colIndex) {
		return XPATH_RESULT_1 + colIndex + XPATH_RESULT_2;
	}
	
	public String getSearchResultValue(WebDriver driver, int colIndex) {
		WebElement col = driver.findElement(By.xpath(constructSearchResultXpath(colIndex)));

		String value = "";

		if (null != value) {
			value = col.getText();
		}
		return value;
	}
	
	public void clickExportToCSVButton(WebDriver driver) {
		driver.findElement(By.xpath(XPATH_EXPORT_BTN )).click();
	}
	
	public boolean exportButtonIsDisplayed(WebDriver driver) {
		try {
		WebElement exportButton = driver.findElement(By.xpath(XPATH_EXPORT_BTN));
		boolean exportButtonIsDisplayed = exportButton.isDisplayed();
		return exportButtonIsDisplayed;
		
		} catch (Exception e) {
			boolean exportButtonIsDisplayed = false;
			return exportButtonIsDisplayed;
		}
	}
	
	public boolean uploadRemediationFollowUpButtonIsDisplayed(WebDriver driver) {
		try {
		WebElement followUpButton = driver.findElement(By.xpath(XPATH_FOLLOWUP_BTN));
		boolean followUpButtonIsDisplayed = followUpButton.isDisplayed();
		return followUpButtonIsDisplayed;
		
		} catch (Exception e) {
			boolean followUpButtonIsDisplayed = false;
			return followUpButtonIsDisplayed;
		}

	}
	
	//Added by Calvin on June 25, 2018
	public boolean statusReportButtonIsDisplayed(WebDriver driver) {
		try {
		WebElement statusReportButton = driver.findElement(By.xpath(XPATH_STATUS_REPORT));
		boolean statusReportButtonIsDisplayed = statusReportButton.isDisplayed();
		return statusReportButtonIsDisplayed;

		} catch (Exception e) {
			boolean statusReportButtonIsDisplayed = false;
			return statusReportButtonIsDisplayed;
		}

	}
	
}
