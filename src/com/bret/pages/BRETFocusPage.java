package com.bret.pages;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.bret.util.ScreenCapture;

/** 
* [added-Belle Pondiong]<br>
* This page class contained all the methods on what to perform on BRET Focus page. <br>
*/
public class BRETFocusPage {
	
	//Declaring variables
	
	public WebDriver driver;
	
	@FindBy (id = "dijit_layout_TabContainer_0_tablist_idx_layout_ContentPane_2") // Focusd Tab
	private WebElement focusTab;
	
	@FindBy (id = "assignBtn")              // Take this bid button on Bid Details page
	private WebElement takeThisBidBtn;
	
	@FindBy (id = "assignee")
	private WebElement assigneeName; 	    // name of the reviewer on bid details page
	
	@FindBy (id = "dijit_layout_TabContainer_0_tablist_remediationTab")
	private WebElement remediationTab;
	
	@FindBy (id = "idx_form_Link_0")
	private WebElement remediationEditLink;
	
	public static String bidDateFocus = null;  
	
	public static String bidDateFocusTab = null;  //[added-9/13] To check date on bid with NEW status
	
	// This is a web page class constructor		 
	public BRETFocusPage (WebDriver driver) {
			 this.driver= driver;
			 PageFactory.initElements(driver, this);
	}
	
//------------------ Bid Assign and Check Routine ------------------------------------------------  	

	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that will assigned a focus bid to self.<br>
	 * This will return void.<br>
	 * This method will display in the console the bid details.<br>
	 * <br>Test Verification: <br>
	 * Verify that bid is NEW and is not assigned to any reviewer. <br>
	 */
	public void bidAssignToSefl (String bidNum, String mtdName) throws Exception{

		int grdCntrfound = 0; 
		String bidcheck = null, 
			   bidFlag = null, 
			   bidDate = null,
			   bidReviewerDec = null,
			   bidReviewer = null;
		
		String winHandleBefore = driver.getWindowHandle();
		
				
		focusTab.click();						     //load the Focus Tab first
		grdCntrfound = bidFindonFocusTab(bidNum);    //call the routine to locate the bid
		
	
		//click on the found bid
		driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[4]/div["+grdCntrfound+"]/table/tbody/tr/td/span")).click();
		
		//get details of the found bid
		bidcheck = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[2]")).getText();
		bidFlag = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[1]")).getText();
		bidDate = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[5]")).getText();
		bidReviewerDec = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[6]")).getText();
		bidReviewer = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[7]")).getText();
		
		// Printing located bid and details on the logs
		System.out.println("\n===== Bid details of the found bid on FOCUS TAB before ASSIGNED ===== ");	
		System.out.println("   Bid Num: " +bidcheck);	
		System.out.println("   Flag:    " +bidFlag);	
		System.out.println("   Bid Date:    " +bidDate);
		System.out.println("   Reviwer Decesion:  " +bidReviewerDec);
		System.out.println("   Bid reviewer:  " +bidReviewer);
		
		BRETFocusPage.bidDateFocus = bidDate;        // get the bid date prior to assigning bid to self
		
		if (((bidReviewerDec.equals("New")))) { // check if status is NEW
			
			System.out.println("Bid found in Focus Tab.");
			
		    //click on the Bid Details button
			driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[4]/span")).click();
		
		    //Switch to new window opened
            for(String winHandle : driver.getWindowHandles()) {
                driver.switchTo().window(winHandle);
            }
            
	         takeThisBidBtn.click();   //assigned found bid to self
		     System.out.println("\n'Take this bid' button is clicked.");
            
             while (!(driver.findElement(By.xpath(".//*[@id='idx_layout_ContentPane_0']")).isDisplayed())) { // wait for the content to display
            	   
             	  // wait for 30 secounds
 		          driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
 		          
              }    
			     
			 //Screen capture 
			 ScreenCapture screenShots = new ScreenCapture(driver);  
			 screenShots.screenCaptures(mtdName);
                       
             // Close the new window
             driver.close();

             //Switch back to original browser (first window)
             driver.switchTo().window(winHandleBefore);
             
             //refresh the page
             driver.navigate().refresh();   
				
		}
		else {
		
			Assert.fail("Failed. The bid already has a reviewer or the bid status is not NEW.");   // Will fail the test case if IF-condition is FALSE
			
		}
		
	}
	
	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that will check the assigned bid on Focus Tab.<br>
	 * This will return void.<br>
	 * This method will display in the console the bid details.<br>
	 * <br>Test Verification: <br>
	 * Verify correct Bid date (did not change), reviewer's name and Reviewer Decision (assigned). <br>
	 */
	public void assignedBidChckFocusTab (String bidNum, String reviewerName) throws Exception{
		
		int grdCntrfound = 0; 
		String bidcheck = null, 
			   bidFlag = null, 
			   bidDATE = null,
			   bidReviewerDec = null,
			   bidReviewer = null;
						
		focusTab.click();						     //load the Focus Tab first
		grdCntrfound = bidFindonFocusTab(bidNum);    //call the routine to locate the bid
		
		//click on the found bid
		driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[4]/div["+grdCntrfound+"]/table/tbody/tr/td/span")).click();
		
		//get details of the found bid
		bidcheck = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[2]")).getText();
		bidFlag = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[1]")).getText();
		bidDATE = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[5]")).getText();
		bidReviewerDec = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[6]")).getText();
		bidReviewer = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[7]")).getText();
		
		// Printing located bid and details on the logs
		System.out.println("\n===== Bid details of the found bid on FOCUS TAB after ASSIGNED ===== ");	
		System.out.println("   Bid Num: " +bidcheck);	
		System.out.println("   Flag:    " +bidFlag);	
		System.out.println("   Bid Date:    " +bidDATE);
		System.out.println("   Reviwer Decesion:  " +bidReviewerDec);
		System.out.println("   Bid reviewer:  " +bidReviewer);
		System.out.println("   Bid Date found when bid was not yet assigned:  " +BRETFocusPage.bidDateFocus);
		
		if (!(BRETFocusPage.bidDateFocus.equals(bidDATE))) {   			// check that date on assigned is the same prior to assigned
			
			Assert.fail("Failed. Bid date changes after bid is Assigned to a reviewer.");
		}
 
		if (!(bidReviewerDec.equals("Assigned"))){  					// check Status should be ASSIGNED
			
			Assert.fail("Failed. Bid Reviewer Decision is not 'Assigned'.");
		}
		
		if (!(bidReviewer.equals(reviewerName))) {						// check correct reviewer's name
			
			Assert.fail("Failed. Bid Reviewer name is not correct.");
		}
		
		System.out.println("\nAssigned bid is found in Focus Tab with correct details.");     // Will fail the test case if IF-condition is FALSE
	}
	
	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that will checked the assigned bid on Bid Details Page.<br>
	 * This will return void.<br>
	 * <br>Test Verification: <br>
	 * Verify correct reviewer's name on the page and TAKE THIS BID button is no longer displayed . <br>
	 */
	public void assignedBidChckbidDetails (String bidNum, String reviewerName ) throws Exception {
		
		int grdCntrfound = 0; 
		String assigneeChck = null;

		focusTab.click();						     //load the Focus Tab first
		grdCntrfound = bidFindonFocusTab(bidNum);    //call the routine to locate the bid
		
		//click on the Bid Details button
		driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[4]/span")).click();
	
	    //Switch to new window opened
        for(String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
               
         while (!(driver.findElement(By.xpath(".//*[@id='idx_layout_ContentPane_0']")).isDisplayed())) { // wait for the content to display
        	   
         	  // wait for 30 secounds
		      driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		          
          }    
         
         assigneeChck = assigneeName.getText();           // get assignee name of the found bid
         System.out.println("\nReviewer name on Bid Details page after bid is assigned:  \n     " +assigneeChck);
         
         if (takeThisBidBtn.isDisplayed()) {				// check if TAKE THIS BID button is available on the page
        	 
        	 Assert.fail("Failed. Take this button is displayed for bid already assigned.");
        	 
         }
         if (!(assigneeChck.equals(reviewerName))){			// check correct reviewer's name
        	 
        	 Assert.fail("Failed. Bid Reviewer name is not correct.");
        	 
         }      
         
         System.out.println("\nCorect reviewer name displayed on bid details page for the assigned bid.");
  	
	}

//------------------ Bid Assign and Check Routine - end here --------------------------------------- 
	
	
//------------------ Bid Remediation and Check Routine ------------------------------------------------ 	
	
	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine for Bid Remediation.<br>
	 * This will return void.<br>
	 * This method will display in the console the bid details.<br>
	 * <br>Test Verification: <br>
	 * Verify that bid is currently assign prior to executing the remediation process. <br>
	 */
	public void bidRemediation (String bidNum, String reviewerName, String outcomeStatus, String outcomeComment) throws Exception {

		int grdCntrfound = 0; 
		String revNamechck = null,
			   bidcheck = null,
			   bidDATE = null,
			   bidReviewerDec = null;
			
		
		String winHandleBefore = driver.getWindowHandle();
		
				
		focusTab.click();						     //load the Focus Tab first
		grdCntrfound = bidFindonFocusTab(bidNum);    //call the routine to locate the bid
		
		//click on the found bid
		driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[4]/div["+grdCntrfound+"]/table/tbody/tr/td/span")).click();
		System.out.println("\nBid is found in Focus Tab.");
		
		//get bid number, status and Bid date 
		bidcheck = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[2]")).getText();
		bidReviewerDec = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[6]")).getText();
		bidDATE = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[5]")).getText();
		
		BRETFocusPage.bidDateFocus = bidDATE;    //get bid date prior to remediation
		
		System.out.println("\n===== Bid details on FOCUS tab before REMEDIATION.===== ");
		System.out.println("   Bid Num: " +bidcheck);	
		System.out.println("   Bid Date:    " +bidDATE);
		System.out.println("   Reviwer Decesion:  " +bidReviewerDec);
		//System.out.println("   Bid Date found when bid was not yet remediated:  " +BRETFocusPage.bidDateFocus);	
		
		if (!(bidReviewerDec.equals("Assigned"))) {   // Fail if Status is not Assigned
			
			Assert.fail("Failed. The bid status is not 'ASSIGNED' Please select another bid."); 
			
		}
		
		//click on the Bid Details button
		driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[4]/span")).click();
		
		//Switch to new window opened
        for(String winHandle : driver.getWindowHandles()) {
        	driver.switchTo().window(winHandle);
         }
        
        while (!(driver.findElement(By.xpath(".//*[@id='idx_layout_ContentPane_0']")).isDisplayed())) {   // wait for the content to display
     	   
       	          // wait for 30 secounds
		         driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);        
         } 
        
    	revNamechck = assigneeName.getText();
        System.out.println("\n Bid Reviewer: " +revNamechck);
        
        if ((takeThisBidBtn.isDisplayed()) || (!(revNamechck.equals(reviewerName)))) {    // check TAKE THIS BID button if present and assignee not the login reviewer
        	
            	Assert.fail("Failed. This bid is assigned to another person or not yet assigned. Please select another bid.");   
	    } 
        else 
        	
           remediationTab.click();					// click on remediation tab
           System.out.println("\n trace: Remediation Tab was clicked.");
                   
           while (!(driver.findElement(By.xpath(".//*[@id='remediationTab']")).isDisplayed())) { // wait for the content to display on remediation log
      	   
 	             // wait for 30 secounds
	            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);        
           }
                      
           remediationEditLink.click(); 			// click on EDIT link for remediation
           System.out.println("\n trace: Remediation Tab EDIT link was clicked.");   
           
           //Select outcome = new Select(driver.findElement(By.id("outcomeSelectBox")));
   		   //outcome.selectByVisibleText(outcomeStatus);  
           
           // ------- routine to select from Outcome dropdown, enter comment and save changes ------- 
           
           driver.findElement(By.id("outcomeSelectBox")).click();						// select outcome status	
           driver.findElement(By.id("outcomeSelectBox")).sendKeys(outcomeStatus);
   		   System.out.println(" trace: selected from Outcome dropdown"); 
   		   
   		   driver.findElement(By.id("decisionOutcomeCommentText")).clear(); //[added-Belle-9/14/2017g]
   		   driver.findElement(By.id("decisionOutcomeCommentText")).sendKeys(outcomeComment);      //enter text on Outcome Comment
   		   System.out.println(" trace: add text on Outcome Commentn"); 
   		   
   		   driver.findElement(By.xpath(".//*[@id='idx_layout_ButtonBar_0']/div[2]/span[1]/span")).click();      //click the SAVE button
   		    System.out.println(" trace: Save button clicked"); 
   		   
   		   driver.switchTo().activeElement();						//switch to the dialogue box
   		   
   		   driver.findElement(By.id("dijit_form_Button_5")).click();			//continue button on the dialogue box
   		    System.out.println(" trace: Continue button click"); 
   		   
   		   System.out.println("\nBid Remediation saved!.");
   		   
           // Close the new window
           driver.close();

           //Switch back to original browser (first window)
           driver.switchTo().window(winHandleBefore);
           
           //refresh the page of the orinal browser
           driver.navigate().refresh();  

	}
	
	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that check details of the remediated bid in focus tab.<br>
	 * This will return void.<br>
	 * This method will display in the console the bid details.<br>
	 * <br>Test Verification: <br>
	 * Verify correct Bid date (did not change), reviewer's name and Reviewer Decision (selected outome). <br>
	 * Verify correct Reviewer Release On (current date).<br>
	 */
	public void bidRemediationCheckFocusTab (String bidNum, String reviewerName, String outcomeStatus) throws Exception {

		int grdCntrfound = 0; 
		String bidcheck = null, 
			   bidFlag = null, 
			   bidDATE = null,
			   bidReviewerDec = null,
			   bidReviewer = null,
			   bidReviwerReleaseOn = null;
		
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		String getSystemTime = new SimpleDateFormat("MM/d/yy").format(Calendar.getInstance().getTime());
		//String getSystemTime = new SimpleDateFormat("M/d/yy").format(new Date().getTime());
		System.out.println("Trace: print system time " +getSystemTime);
		
		focusTab.click();						     //load the Focus Tab first
        //driver.findElement(By.xpath(".//*[@id='dijit_layout_TabContainer_0_tablist_idx_layout_ContentPane_2']")).click();
		grdCntrfound = bidFindonFocusTab(bidNum);    //call the routine to locate the bid
		
		//click on the found bid
		driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[4]/div["+grdCntrfound+"]/table/tbody/tr/td/span")).click();
		
		//get details of the found bid
		bidcheck = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[2]")).getText();
		bidFlag = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[1]")).getText();
		bidDATE = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[5]")).getText();
		bidReviewerDec = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[6]")).getText();
		bidReviewer = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[7]")).getText();
		bidReviwerReleaseOn = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[8]")).getText();
		
		// Printing located bid and details on the logs
		System.out.println("\n===== Bid details on FOCUS tab after REMEDIATION =====");
		System.out.println("   Bid Num: " +bidcheck);	
		System.out.println("   Flag:    " +bidFlag);	
		System.out.println("   Bid Date:    " +bidDATE);
		System.out.println("   Reviewer Decision:  " +bidReviewerDec);
		System.out.println("   Bid reviewer:  " +bidReviewer);
		System.out.println("   Reviewer Release On:  " +bidReviwerReleaseOn);
		System.out.println("   Bid Date found when bid was not yet assigned:  " +BRETFocusPage.bidDateFocus);
		
		if (!(bidReviwerReleaseOn.equals(getSystemTime))) {
			
			Assert.fail("Failed. Reviewer Release On is not the current date or Empty.");
		}
		
		if (!(BRETFocusPage.bidDateFocus.equals(bidDATE))) {   			// check that date on assigned is the same prior to assigned
			
			Assert.fail("Failed. Bid date changes after bid is Assigned to a reviewer.");
		}

		if (!(bidReviewerDec.equals(outcomeStatus))){  					// check Status on how bid is remediated
			
			Assert.fail("Failed. Bid Reviewer Decision is not "+outcomeStatus);
		}
		
		if (!(bidReviewer.equals(reviewerName))) {						// check correct reviewer's name
			
			Assert.fail("Failed. Bid Reviewer name is not correct.");
		}
		
		System.out.println("\nRemediated bid is found in Focus Tab with correct details.");     // Will fail the test case if IF-condition is FALSE
		TimeZone.setDefault(TimeZone.getTimeZone("GMT+8"));
          
	}
	
	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that check details of the remediated bid in Bid Details.<br>
	 * This will return void.<br>
	 * This method will display in the console the bid details.<br>
	 * <br>Test Verification: <br>
	 * Verify correct Outcome Status (selected outome), Outcome Comment (entered comment). <br>
	 * <br>[updated-Belle Pondiong-9/14/2017]<br>
	 * Added brand checking since placement for the checked webelement differs for HW and SW.
	 * Added pre ship verification hold for SQO
	 */
	public void bidRemediatonChckbidDetails (String bidNumber, String outcomeStatus, String outcomeComment, String brand, String methodName) throws Exception {

		int grdCntrfound = 0; 
		String outcomeStatusCheck = null,
			   outcomeCommentCheck = null,
			   preShipVerificationHold = null;	
		String winHandleBefore = driver.getWindowHandle();
		
		focusTab.click();						     //load the Focus Tab first
		grdCntrfound = bidFindonFocusTab(bidNumber);    //call the routine to locate the bid
		
		//click on the found bid
		driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[4]/div["+grdCntrfound+"]/table/tbody/tr/td/span")).click();
		System.out.println("\n Bid is found in Focus Tab.");
		
		//click on the Bid Details button
		driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[4]/span")).click();
		
		//Switch to new window opened
        for(String winHandle : driver.getWindowHandles()) {
        	driver.switchTo().window(winHandle);
         }
        
        while (!(driver.findElement(By.xpath(".//*[@id='idx_layout_ContentPane_0']")).isDisplayed())) { // wait for the content to display
     	   
       	          // wait for 30 secounds
		         driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);        
         } 
        
    	 remediationTab.click();					// click on remediation tab
         System.out.println("\n trace: Remediation Tab was clicked.");
        
           while (!(driver.findElement(By.xpath(".//*[@id='remediationTab']")).isDisplayed())) { // wait for the content to display on remediation log
      	   
 	             // wait for 30 secounds
	            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);        
           }
           
         // remediationEditLink.click(); 			// click on EDIT link for remediation
         // System.out.println("\n trace: Remediation Tab EDIT link was clicked.");   
         
         if (brand.equals("SW")){ //[updated-Belle-9/14/2017]
        	 driver.findElement(By.id("idx_grid_PropertyFormatter_81_format")).click();  
        	 outcomeStatusCheck = driver.findElement(By.id("idx_grid_PropertyFormatter_81_format")).getText();   	  //get the outcome status
     		 outcomeCommentCheck = driver.findElement(By.id("idx_grid_PropertyFormatter_82_format")).getText();    	  //get the outcome comment 
     		 preShipVerificationHold = driver.findElement(By.id("idx_grid_PropertyFormatter_72_format")).getText();    	  //get the pre Ship Verification Hold value
     		 
     		 System.out.println("\n===== Outcome Status and Comment found on the bid on Bid Details page after REMEDIATION ====="); 
     		 System.out.println("Outcome Status: " +outcomeStatusCheck);  
     		 System.out.println("Outcome Comment: "+outcomeCommentCheck); 
     		 System.out.println("PRE-SHIP VERIFICATION HOLD: "+preShipVerificationHold); 
     		 
     		 Assert.assertEquals("NONE", preShipVerificationHold, "PRE-SHIP VERIFICATION HOLD is not NONE.");
     		 //System.out.println("trace: outcome comment passed: "+outcomeComment); 
         }
         
         if (brand.equals("HW")){ //[updated-Belle-9/14/2017]
        	 driver.findElement(By.id("idx_grid_PropertyFormatter_80_format")).click();   
        	 outcomeStatusCheck = driver.findElement(By.id("idx_grid_PropertyFormatter_80_format")).getText();   	  //get the outcome status
     		 outcomeCommentCheck = driver.findElement(By.id("idx_grid_PropertyFormatter_81_format")).getText();    	  //get the outcome comment
         
     		 System.out.println("\n===== Outcome Status and Comment found on the bid on Bid Details page after REMEDIATION ====="); 
     		 System.out.println("Outcome Status: " +outcomeStatusCheck);  
     		 System.out.println("Outcome Comment: "+outcomeCommentCheck); 
     		 //System.out.println("trace: outcome comment passed: "+outcomeComment); 
         }
  		 
 		 
 		 if ((outcomeStatusCheck.equals(outcomeStatus)) && ((outcomeCommentCheck.equals(outcomeComment)))){
 			 
 			System.out.println("\nCorect Outcome Status and comment found on the bid."); 			 
 		 }
 		 else
 			Assert.fail("Failed. Outcome Status/comment on the bid not what is expected.");   // Will fail the test case if IF-condition is FALSE

         
 		 //Screen capture 
		 ScreenCapture screenShots = new ScreenCapture(driver);  
		 screenShots.screenCaptures(methodName);
		 
         // Close the new window
         driver.close();

         //Switch back to original browser (first window)
         driver.switchTo().window(winHandleBefore);
         
         //refresh the page of the orinal browser
         driver.navigate().refresh();  
 		 
	}
	
//------------------ Bid Remediation and Check Routine - ends here--------------------------------------- 
	
	
//------------------ Rolled Back bid Check Routines  --------------------------------------- 
	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that check details of the rolled back bid in focus tab.<br>
	 * This will return void.<br>
	 * This method will display in the console the bid details.<br>
	 * <br>Test Verification: <br>
	 * Verify correct Reviewer Decision (Assigned), reviewer's name. <br>
	 */
	public void rolledBackbidChck (String bidNum) throws Exception {

		int grdCntrfound = 0; 
		String bidcheck = null, 
			   bidReviewerDec = null,
			   bidReviewer = null;
			   		
		focusTab.click();						     //load the Focus Tab first
		grdCntrfound = bidFindonFocusTab(bidNum);    //call the routine to locate the bid
		
		//click on the found bid
		driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[4]/div["+grdCntrfound+"]/table/tbody/tr/td/span")).click();
		
		//get details of the found bid
		bidcheck = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[2]")).getText();
		bidReviewerDec = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[6]")).getText();
		bidReviewer = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[7]")).getText();
				
		// Printing located bid and details on the logs
		System.out.println("\n===== Bid details on FOCUS tab after Rollback =====");
		System.out.println("   Bid Num: " +bidcheck);	
		System.out.println("   Reviwer Decesion:  " +bidReviewerDec);
		System.out.println("   Bid reviewer:  " +bidReviewer);
		
		if (bidReviewerDec.equals("Assigned") & (bidReviewer.equals(BRETArchivePage.bidReviewer))){ // Need to add validation on Bid date once fix (rtc 1047098)
			
			System.out.println("\nRolled back bid found in Focus Tab.");
		}
		else {
			Assert.fail("Failed. Bid found in Focus Tab but incorrect details on Reviewer/Reviewer Descision.");
		}
	}
	
 //-------------- Rolled Back bid Check Routines - ends here ------------------------------ 

//------------------ Check focus bids in Focus Tab  --------------------------------------- 

	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that check details of a non remediated bid in focus tab.<br>
	 * This will return void.<br>
	 * This method will display in the console the bid details.<br>
	 * <br>Test Verification: <br>
	 * Verify correct Bid date (not NULL), Reviewer Decision (New). <br>
	 * <br>[updated-Belle Pondiong-9/12/2017] <br>
	 * Added Primary brand "SW" for SWO, "HW" - Hardwares, "swNEW" - W to N on SWO
	 * Added test verification that bid date is current when status changes from W to N for SQO
	 */
	public void bidCheckFocusTab (String bidNum, String bidStat, String brand) throws Exception {

		int grdCntrfound = 0; 
		String bidcheck = null, 
			   bidFlag = null, 
			   bidDATE = null,
		       bidReviewerDec = null,
		       primaryBrand = null;  //[added-9/13] 
		final Object NULL = null;
		
//		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
//		String getSystemTime = new SimpleDateFormat("M/d/yy").format(Calendar.getInstance().getTime());
		
		//String getSystemTime = new SimpleDateFormat("M/d/yy").format(new Date().getTime());
//		System.out.println("Trace: print system time " +getSystemTime);
		
		//System.out.println("Trace: passed parameter-BidNUm " +bidNum);
		//System.out.println("Trace: passed parameter-Staus " +bidStat);
		
		focusTab.click();						     //load the Focus Tab first
		grdCntrfound = bidFindonFocusTab(bidNum);    //call the routine to locate the bid
		
		//Aug17: Added to check if bid was not found
		System.out.println("trace: row count "  +grdCntrfound);
		
		if (grdCntrfound == 0){
			Assert.fail("The Bid "+bidNum+" was not fround in BRET.");
		}
		
		//click on the found bid
		driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[4]/div["+grdCntrfound+"]/table/tbody/tr/td/span")).click();
		
		//get details of the found bid
		bidcheck = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[2]")).getText();
		bidFlag = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[1]")).getText();
		bidDATE = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[5]")).getText();
		bidReviewerDec = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[6]")).getText();
		primaryBrand = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntrfound+"]/table/tbody/tr/td[11]")).getText(); //[added-9/13] 
		
		// Printing located bid and details on the logs
		System.out.println("\n===== Bid details on FOCUS tab =====");
		System.out.println("   Bid Num: " +bidcheck);	
		System.out.println("   Flag:    " +bidFlag);	
		System.out.println("   Bid Date:    " +bidDATE);
		System.out.println("   Bid Status:    " +bidReviewerDec);
		System.out.println("   Primary Brand:    " +primaryBrand);
		
		if (brand.equals("swNEW")){ //[added-belle-9/13] To check that bid date is current on SQO bid that changes from W to N
			 
			 TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
			 String getSystemTime = new SimpleDateFormat("MM/d/yy").format(new Date().getTime());
	 		 System.out.println("\nTrace: print system time " +getSystemTime);

	 		
	 		 if (!(bidDATE.equals(getSystemTime))) {
				
				Assert.fail("Failed. Bid found in Focus tab but Bid date is not the current date.");
			 }
				
 			 else if (!(bidReviewerDec.equals(bidStat))) {
				Assert.fail("Failed. Bid found in Focus tab but Status is not NEW");
		     }
		     else  		
			System.out.println("\nSW Bid is found in Focus Tab with correct details.");    
	 		
	 		 TimeZone.setDefault(TimeZone.getTimeZone("GMT+8"));
		}
		
		if (brand.equals("HW")){ //[updated-belle-9/13]
			
 				if (bidDATE.equals(NULL)) {
				
					Assert.fail("Failed. Bid found in Focus tab but Bid date is Empty.");
				}
 				
     			else if (!(bidReviewerDec.equals(bidStat))) {
					Assert.fail("Failed. Bid found in Focus tab but Status is not NEW");
			}
			else  		
    			System.out.println("\nHW Bid is found in Focus Tab with correct details.");     
		}
		
		if (brand.equals("SW")){ //[updated-belle-9/13]
			
				if (bidDATE.equals(NULL)) {
			
				Assert.fail("Failed. Bid found in Focus tab but Bid date is Empty.");
			}
				
 			else if (!(bidReviewerDec.equals(bidStat))) {
				Assert.fail("Failed. Bid found in Focus tab but Status is not NEW");
		}
		else  		
			System.out.println("\nSW Bid is found in Focus Tab with correct details.");     
	}
		
		// bidDateFocusTab = bidDATE; //[added-9/13] 
         
	}
	
	
//------------------ Check focus bids in Focus Tab - ends here  --------------------------------------- 
	
	
//------------------ Call Routines  --------------------------------------- 
	 /** 
	 * [added-Belle Pondiong]<br>
	 * Routine that will look for the bid in Focus Tab Grid. <br>
	 * This will return the location of the bid in the grid.<br>
	 */ 
	protected int bidFindonFocusTab (String bidNumber) throws Exception {
		
		int bidlocatecntr = 0,
		    grdCntr = 0, 
		    pgLink = 1; 
		String bidcheck = null;
		WebElement element;
					
		driver.findElement(By.xpath(".//*[@id='dijit__FocusMixin_4']/span[3]")).click();  //Clicks '25' records per page
		
		// Routine to find the bids on Archive Tab
      do {
      	
			driver.findElement(By.xpath(".//*[@id='dijit__FocusMixin_3']/span[2]/span["+pgLink+"]")).click();  // click on the page link
			     //System.out.println("Tracing...page number " +pgLink);
			
			 grdCntr = 0;  // set to zero, this will reset the row # on the grid should the code enters the next page
				 //System.out.println("Tracing...grid number init" +grdCntr);
			
			//Routine to check the bid per page 			
			for (int pgDataCntr = 0; pgDataCntr < 25; pgDataCntr ++) {	
				   
			    grdCntr = grdCntr +1;
			    
			    // scroll until element is in focus since grid is dynamic with scroll bars
				element = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntr+"]/table/tbody/tr/td[2]"));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
				Thread.sleep(500); 
				
				bidcheck = driver.findElement(By.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div["+grdCntr+"]/table/tbody/tr/td[2]")).getText();
					//System.out.println("Tracing...grid number inside FOR LOOP  " +grdCntr);
					//System.out.println("Tracing...bid number " +bidcheck);
													
					if (bidNumber.equals(bidcheck)) { // break if bid is found
						break;
					}					
			}
			
			pgLink ++;  // go to next page
						
		} while (!(bidNumber.equals(bidcheck)));  //checking the specified bid on the grid
		
      bidlocatecntr = grdCntr;      
	  return bidlocatecntr;			
	}

	
	/**
	 * Added by Lhoyd Castillo - 1/31/2018
	 * this is to get the bid number on the search result
	 * 
	 */
	private static final String XPATH_RESULT_1 = ".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[";
	private static final String XPATH_RESULT_2 = "]";

	
	private String constructSearchResultXpath(int colIndex) {
		return XPATH_RESULT_1 + colIndex + XPATH_RESULT_2;
	}
	
	public String getSearchResultValue(WebDriver driver, int colIndex) {
		WebElement col = driver.findElement(By.xpath(constructSearchResultXpath(colIndex)));

		String value = "";

		if (null != value) {
			value = col.getText();
		}
		return value;
	}
	
}
