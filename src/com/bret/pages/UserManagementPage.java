package com.bret.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

//import com.bret.util.BretTestUtils;

/**
 * TODO:
 * 
 * 
 * @author DK Alba
 * 
 */

public class UserManagementPage extends SummaryPage {

	private WebDriver driver;
	
	
	private static final String XPATH_PART_1 = ".//*[@id='gridDiv']/div[3]/div[2]/div[";
	private static final String XPATH_PART_2 = "]/table/tbody/tr/td[";
	private static final String XPATH_PART_3 = "]";
	
	private static final String XPATH_USER_MANAGEMENT_PAGE = "/html/body/div[1]/div[3]/div/div[3]/div/div[1]";
	private static final String XPATH_ADD_USERS_BTN = "//*[@id=\"AddUsersBtn\"]";
	private static final String XPATH_EXPORT_BTN = "//*[@id=\"ExportUserBtn\"]";
//	private static final String TEST_USER_ID = "userId";
	

			
			
	// Table column constants of UserManagementPage
		public static final int USER_ID = 1;
		public static final int USER_NAME = 2;
		public static final int DETAIL_LINK_COL = 3;
		public static final int ROLE = 4;
		public static final int STATUS = 5;
		public static final int SOURCE_SYSTEM = 6;
		
		/**
		 * Default constructor
		 */
		public UserManagementPage() {
			// Do nothing
		}

		/**
		 * Constructor - for initializing the elements again. Used for switching back to
		 * Summary page from Details page.
		 * 
		 * @param driver
		 */
		public UserManagementPage(WebDriver driver) {
			this.driver = driver;
			PageFactory.initElements(driver, this);
		}
		
		/**
		 * Returns the text of the web element based on the row index and column index.
		 * 
		 * @param driver
		 *            current WebDriver.
		 * @param rowIndex
		 *            int row index.
		 * @param colIndex
		 *            int column index
		 * @return String text of the current web element.
		 */
		public String getValueAtIndex(WebDriver driver, int rowIndex, int colIndex) {
			WebElement col = driver.findElement(By.xpath(constructXpath(rowIndex, colIndex)));

			String value = "";

			if (null != value) {
				value = col.getText();
			}

			return value;

		}

		/**
		 * Returns the row index of the given userId.
		 * 
		 * @param driver
		 *            the current WebDriver.
		 * @param userId
		 *            bid id to be searched.
		 * @return index of the given userId.
		 */
		public int searchRowIndex(WebDriver driver, String userId) {
			this.driver = driver;
			int rowIndex = 0;
			try {
				rowIndex = (finduserIdRow(userId));
			} catch (Exception e) {
		//		e.printStackTrace();
			}

			return rowIndex;
		}

		
		private String constructXpath(int rowIndex, int colIndex) {
			return XPATH_PART_1 + rowIndex + XPATH_PART_2 + colIndex + XPATH_PART_3;
		}
		
		
		private int finduserIdRow(String userIntranet) throws Exception {

			int userlocatecntr = 0, grdCntr = 0, pgLink = 1;
			String usercheck = null;
			WebElement element;

			driver.findElement(By.xpath(".//*[@id='dijit__FocusMixin_1']/span[5]")).click();
			do {

				driver.findElement(By.xpath(".//*[@id='dijit__FocusMixin_0']/span[2]/span[" + pgLink + "]")).click();
				grdCntr = 0;
				for (int pgDataCntr = 0; pgDataCntr < 30; pgDataCntr++) {
					grdCntr = grdCntr + 1;
					element = driver.findElement(By.xpath(
							".//*[@id='gridDiv']/div[3]/div[2]/div[" + grdCntr + "]/table/tbody/tr/td[2]"));
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
					Thread.sleep(500);

					usercheck = driver.findElement(By.xpath(
							".//*[@id='gridDiv']/div[3]/div[2]/div[" + grdCntr + "]/table/tbody/tr/td[1]"))
							.getText();
					if (userIntranet.equals(usercheck)) {
						break;
					}
				}

				pgLink++;

			} while (!(userIntranet.equals(usercheck)));
			userlocatecntr = grdCntr;

			return userlocatecntr;
		}
		
		
		public void clickAddUsersButton(WebDriver driver) {
			driver.findElement(By.xpath(XPATH_ADD_USERS_BTN)).click();
		}

		public boolean addUsersButtonIsDisplayed(WebDriver driver) {
			try {
			WebElement addUserButton = driver.findElement(By.xpath(XPATH_ADD_USERS_BTN));
			boolean addUsersButtonIsDisplayed = addUserButton.isDisplayed();
			return addUsersButtonIsDisplayed;
			} catch (Exception e) {
				boolean addUsersButtonIsDisplayed = false;
				return addUsersButtonIsDisplayed;
			}
		}
		
		
		public boolean exportButtonIsDisplayed(WebDriver driver) {
			try {
			WebElement exportButton = driver.findElement(By.xpath(XPATH_EXPORT_BTN));
			boolean exportButtonIsDisplayed = exportButton.isDisplayed();
			return exportButtonIsDisplayed;
			
			} catch (Exception e) {
				boolean exportButtonIsDisplayed = false;
				return exportButtonIsDisplayed;
			}
			
		}
		
//		public boolean chainIconIsDisplayed(WebDriver driver) {
//			WebElement chainIcon = driver.findElement(By.xpath(XPATH_EXPORT_BTN));
//			boolean chainIconIsDisplayed = chainIcon.isDisplayed();
//			return chainIconIsDisplayed;
//		}
		
		public String getUserManagementPageText(WebDriver driver) {
			WebElement userManagementPageHeader = driver.findElement(By.xpath(XPATH_USER_MANAGEMENT_PAGE));
			String userManagementHeaderText = userManagementPageHeader.getText();
			return userManagementHeaderText;
		}
		
		
		// Added by Calvin on June 29
		public boolean userDetailPageButtonIsDisplayed(WebDriver driver, int chainBidDetails) {
			
			try {
				WebElement userDetailPageButton = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div/div[3]/div/div[3]/div[3]/div[2]/div[\" + chainBidDetails + \"]/table/tbody/tr/td[3]"));
				boolean userDetailPageButtonIsDisplayed = userDetailPageButton.isDisplayed();
				return userDetailPageButtonIsDisplayed;
			
			} catch (Exception e) {
				boolean userDetailPageButtonIsDisplayed = false;
				return userDetailPageButtonIsDisplayed;
			}
		}
		
		
}
