package com.bret.pages;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.bret.obj.export.ExportDetailData;
import com.bret.util.BretTestUtils;

public class ExportProcessor {
	
	private static final int TYPE_INDEX = 1;
	private static final int BID_ID_INDEX = 2;
	private static final int SOURCE_SYSTEM_INDEX = 3;
	private static final int BID_DATE_INDEX = 4;
	private static final int COUNTRY_NAME_INDEX = 5;
	private static final int REGION_INDEX = 6;
	private static final int PRIMARY_BRAND_INDEX = 7;
	private static final int DISTRIBUTOR_INDEX = 8;
	private static final int DISTRIBUTOR_CEID_INDEX = 9;
	private static final int CUST_FACING_BP_INDEX = 10;
	private static final int CUST_FACING_BP_CEID_INDEX = 11;
	private static final int CUSTOMER_NAME_INDEX = 12;
	private static final int CUSTOMER_CMR_INDEX = 13;
	private static final int ADDITIONAL_ID_INDEX = 14;
	private static final int BID_LEVEL_MARGIN_INDEX = 15;
	private static final int BID_LEVEL_DISC_INDEX = 16;
	private static final int AGGR_SCORE_NDM_INDEX = 17;
	private static final int TOTAL_BID_SCORE_INDEX = 18;
	private static final int REVENUE_SIZE_INDEX = 19;
	private static final int CONFIDENTIALITY_INDEX = 20;
	private static final int ROUTE_TO_MARKET_INDEX = 21;
	private static final int OFFSHORE_PAYMT_TERMS_INDEX = 22;
	private static final int BUNDLED_SOLUTIONS_INDEX = 23;
	private static final int CONTINGENCY_FEE_INDEX = 24;
	private static final int SOLE_SOURCE_PROC_INDEX = 25;
	private static final int FOCUS_BP_INDEX = 26;
	private static final int BP_MARG_PERC_INDEX = 27;
	private static final int DISCOUNT_PERC_INDEX = 28;
	private static final int BP_MARG_TOTAL_INDEX = 29;
	private static final int DISCOUNT_TOTAL_INDEX = 30;
	private static final int SUPPORTING_FACTORS_INDEX = 31;
	private static final int JUSTIFICATION_QUESTION_INDEX = 32;
	private static final int OPERATION_OWNER_INDEX = 33;
	private static final int BID_MGR_INDEX = 34;
	private static final int REMEDIATION_USER_INDEX = 35;
	private static final int T3_NAME_INDEX = 36;
	private static final int T4_NAME_INDEX = 37;
	private static final int T5_NAME_INDEX = 38;
	private static final int BPM_T1_FIN_INDEX = 39;
	private static final int BPM_T1_SERV_INDEX = 40;
	private static final int BPM_T1_INS_INDEX = 41;
	private static final int BPM_T1_PROFIT_INDEX = 42;
	private static final int BPM_T2_FIN_INDEX = 43;
	private static final int BPM_T2_SERV_INDEX = 44;
	private static final int BPM_T2_INS_INDEX = 45;
	private static final int BPM_T2_PROFIT_INDEX = 46;
	private static final int BPM_OTHER_INDEX = 47;
	private static final int LOG_INDEX = 48;
	private static final int CHANNEL_PROGRAM_INDEX = 49;
	private static final int ACTION_OWNER_INDEX = 50;
	private static final int CLOSED_INDEX = 51;
	private static final int CLOSE_DATE_INDEX = 52;
	private static final int OUTCOME_COMMENT_INDEX = 53;
	private static final int CONDITION_INDEX = 54;
	private static final int TRANSPARENCY_LETTER_INDEX = 55;
	private static final int CYCLE_INDEX = 56;
	private static final int EXCEPTION_FLAG_INDEX = 57;
	private static final int FULLFILLED_INDEX = 58;
	private static final int COMMENT_INDEX = 59;
	private static final int BID_EXPIRED_INDEX = 60;
	private static final int EX_PRE_FULFILLMENT_INDEX = 61;
	private static final int POST_VERIFY_DATE_INDEX = 62;
	private static final int POST_SHIP_AUDIT_STAT_INDEX = 63;
	private static final int WAITING_FOR_EXTENSION_INDEX = 64;
	private static final int PROBLEMS_WITH_CHASING_INDEX = 65;
	private static final int REQUEST_SENT_TOO_EARLY_INDEX = 66;
	private static final int REALISTICALLY_DUE_INDEX = 67;
	private static final int TEST_NOTE_STARTED_YET_INDEX = 68;
	private static final int POTENTIAL_ISSUES_INDEX = 69;
	private static final int FOLLOW_UP_LOG_INDEX = 70;
	private static final int DEADLINE_OF_REQUEST_INDEX = 71;
	private static final int BID_EVENT_INDEX = 72;
	
	
	public ExportDetailData getExportDetailData(String filePath) {
		List<String> csvContents = BretTestUtils.readFile(filePath);
		ExportDetailData exportData = mapExportCSVDetailToObj(cleanupCSVData(csvContents));
		
		return exportData;
	}
	
	private ExportDetailData mapExportCSVDetailToObj(List<String> csvContents) {
		ExportDetailData exportData = new ExportDetailData();
		
		exportData.setType(csvContents.get(TYPE_INDEX));
		exportData.setBidId(csvContents.get(BID_ID_INDEX));
		exportData.setSourceSystem(csvContents.get(SOURCE_SYSTEM_INDEX));
		exportData.setBidDate(csvContents.get(BID_DATE_INDEX));
		exportData.setCountryName(csvContents.get(COUNTRY_NAME_INDEX));
		exportData.setRegion(csvContents.get(REGION_INDEX));
		exportData.setPrimaryBrand(csvContents.get(PRIMARY_BRAND_INDEX));
		exportData.setDistributor(csvContents.get(DISTRIBUTOR_INDEX));
		exportData.setDistributorCeid(csvContents.get(DISTRIBUTOR_CEID_INDEX));
		exportData.setCustomerFacingBP(csvContents.get(CUST_FACING_BP_INDEX));
		exportData.setCustomerFacingBPCeid(csvContents.get(CUST_FACING_BP_CEID_INDEX));
		exportData.setCustomerName(csvContents.get(CUSTOMER_NAME_INDEX));
		exportData.setCustomerCMR(csvContents.get(CUSTOMER_CMR_INDEX));
		exportData.setAdditionalId(csvContents.get(ADDITIONAL_ID_INDEX));
		exportData.setBidLevelMargin(formatToPercent(csvContents.get(BID_LEVEL_MARGIN_INDEX)));
		exportData.setBidLevelDiscount(formatToPercent(csvContents.get(BID_LEVEL_DISC_INDEX)));
		exportData.setAggregateScoreNdm(csvContents.get(AGGR_SCORE_NDM_INDEX));
		exportData.setTotalBidScore(csvContents.get(TOTAL_BID_SCORE_INDEX));
		exportData.setRevenueSize(formatToDecimalNonDollar(csvContents.get(REVENUE_SIZE_INDEX)));
		exportData.setConfidentiality(csvContents.get(CONFIDENTIALITY_INDEX));
		exportData.setRouteToMarket(csvContents.get(ROUTE_TO_MARKET_INDEX));
		exportData.setOffshorePaymentTerms(csvContents.get(OFFSHORE_PAYMT_TERMS_INDEX));
		exportData.setBundledSolutions(csvContents.get(BUNDLED_SOLUTIONS_INDEX));
		exportData.setContingencyFee(csvContents.get(CONTINGENCY_FEE_INDEX));
		exportData.setSoleSourceProc(csvContents.get(SOLE_SOURCE_PROC_INDEX));
		exportData.setFocusBP(csvContents.get(FOCUS_BP_INDEX));
		exportData.setBpMargPerc(csvContents.get(BP_MARG_PERC_INDEX));
		exportData.setDiscountPerc(csvContents.get(DISCOUNT_PERC_INDEX));
		exportData.setBpMarginTotal(csvContents.get(BP_MARG_TOTAL_INDEX));
		exportData.setDiscountTotal(csvContents.get(DISCOUNT_TOTAL_INDEX));
		exportData.setSupportingFactors(csvContents.get(SUPPORTING_FACTORS_INDEX));
		exportData.setJustificationQuestions(csvContents.get(JUSTIFICATION_QUESTION_INDEX));
		exportData.setOperationOwner(csvContents.get(OPERATION_OWNER_INDEX));
		exportData.setBidMgr(csvContents.get(BID_MGR_INDEX));
		exportData.setRemediationUser(csvContents.get(REMEDIATION_USER_INDEX));
		exportData.setT3Name(csvContents.get(T3_NAME_INDEX));
		exportData.setT4Name(csvContents.get(T4_NAME_INDEX));
		exportData.setT5Name(csvContents.get(T5_NAME_INDEX));
		exportData.setBpmT1Fin(formatToPercent(csvContents.get(BPM_T1_FIN_INDEX)));
		exportData.setBpmT1Serv(formatToPercent(csvContents.get(BPM_T1_SERV_INDEX)));
		exportData.setBpmT1Ins(formatToPercent(csvContents.get(BPM_T1_INS_INDEX)));
		exportData.setBpmT1Profit(formatToPercent(csvContents.get(BPM_T1_PROFIT_INDEX)));
		exportData.setBpmT2Fin(formatToPercent(csvContents.get(BPM_T2_FIN_INDEX)));
		exportData.setBpmT2Serv(formatToPercent(csvContents.get(BPM_T2_SERV_INDEX)));
		exportData.setBpmT2Ins(formatToPercent(csvContents.get(BPM_T2_INS_INDEX)));
		exportData.setBpmT2Profit(formatToPercent(csvContents.get(BPM_T2_PROFIT_INDEX)));		
		exportData.setBpmOther(formatToPercent(csvContents.get(BPM_OTHER_INDEX)));
		exportData.setLog(csvContents.get(LOG_INDEX));
		exportData.setChannelProgram(csvContents.get(CHANNEL_PROGRAM_INDEX));
		exportData.setActionOwner(csvContents.get(ACTION_OWNER_INDEX));
		exportData.setClosed(csvContents.get(CLOSED_INDEX));
		exportData.setClosedDate(csvContents.get(CLOSE_DATE_INDEX));
		exportData.setOutcomeComment(csvContents.get(OUTCOME_COMMENT_INDEX));
		exportData.setCondition(csvContents.get(CONDITION_INDEX));
		exportData.setTransparencyLetter(csvContents.get(TRANSPARENCY_LETTER_INDEX));
		exportData.setCycle(csvContents.get(CYCLE_INDEX));
		exportData.setExceptionFlag(csvContents.get(EXCEPTION_FLAG_INDEX));
		exportData.setFulfilled(csvContents.get(FULLFILLED_INDEX));
		exportData.setComment(csvContents.get(COMMENT_INDEX));
		exportData.setBidExpired(csvContents.get(BID_EXPIRED_INDEX));
		exportData.setExPreFulfillment(csvContents.get(EX_PRE_FULFILLMENT_INDEX));
		exportData.setPostVerifyDate(csvContents.get(POST_VERIFY_DATE_INDEX));
		exportData.setPostShipAuditFollowUpStatus(csvContents.get(POST_SHIP_AUDIT_STAT_INDEX));
		exportData.setWaitingForExtension(csvContents.get(WAITING_FOR_EXTENSION_INDEX));
		exportData.setProblemsWithChasing(csvContents.get(PROBLEMS_WITH_CHASING_INDEX));
		exportData.setRequestSentTooEarly(csvContents.get(REQUEST_SENT_TOO_EARLY_INDEX));
		exportData.setRealisticallyDue(csvContents.get(REALISTICALLY_DUE_INDEX));
		exportData.setTestNoteStartedYet(csvContents.get(TEST_NOTE_STARTED_YET_INDEX));
		exportData.setPotentialIssues(csvContents.get(POTENTIAL_ISSUES_INDEX));
		exportData.setFollowUpLog(csvContents.get(FOLLOW_UP_LOG_INDEX));
		exportData.setDeadlineOfRequest(csvContents.get(DEADLINE_OF_REQUEST_INDEX));
		exportData.setBidEvent(csvContents.get(BID_EVENT_INDEX));

		
		return exportData;
	}
	
	private List<String> cleanupCSVData(List<String> origCsvData) {
		List<String> cleanCsvData = new ArrayList<>();
		
		for(String data : origCsvData) {
			cleanCsvData.add(cleanupData(data).trim());
		}
		
		return cleanCsvData;
	}
	
	private String cleanupData(String data) {
		
		String[] separatedData = data.split(",");
		String cleanData = "";
		
		if(separatedData.length >= 2) {
			cleanData = data.split(",")[1].replaceAll("\"=\"", "").replaceAll("\"", "");	
		}
		
		return cleanData;
		
	}
	
	public List<String> getExportedDataDifference(ExportDetailData exportedCsv,
			ExportDetailData bretWebData) {
		List<String> difference = new ArrayList<>();
		
		for (Field field : ExportDetailData.class.getDeclaredFields()) {
			field.setAccessible(true);
			try {
				if ((null != field.get(exportedCsv) && null != field.get(bretWebData))
						&& !field.get(exportedCsv).equals(field.get(bretWebData))) {
					
					difference.add(field.getName() + ": " + "[CSV Value: " + field.get(exportedCsv) + " : " + "Bret web Value: " + field.get(bretWebData) + "]");
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		return difference;
	}
	
	private String formatToPercent(String number) {
		try {  
			if(number.equalsIgnoreCase("null")) {
				number = "0 ";
			}
			
			double amount = Double.parseDouble(number);
			DecimalFormat formatter = new DecimalFormat("#0.0");


			return formatter.format(amount) + "%";

		} catch (Exception e) {
			return "";
		}
	} 
	
	private String formatToDecimalNonDollar(String number) {
		double amount = Double.parseDouble(number);
		DecimalFormat formatter = new DecimalFormat("#,###.00");


		return formatter.format(amount);
	}

}
