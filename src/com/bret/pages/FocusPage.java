package com.bret.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

/**
 * TODO: FREY - Think of a way to handle the XPATH_PARTS, bid row search and
 * index value retrieval because tabs with table have these common features.
 * 
 * 
 */
public class FocusPage extends SummaryPage {
	private WebDriver driver;

	private static final String XPATH_PART_1 = ".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div[";
	private static final String XPATH_PART_2 = "]/table/tbody/tr/td[";
	private static final String XPATH_PART_3 = "]";

	private static final String XPATH_CHECKBOX_PART_1 = "/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[3]/div[3]/div/div[5]/div[3]/div[4]/div[";
	private static final String XPATH_CHECKBOX_PART_2 = "]/table/tbody/tr/td/span";

	private static final String XPATH_EXPORT_BTN = "//*[@id=\"focusExportToExcelBtn\"]";

	// private static final String XPATH_ARCHIVE_BTN =
	// "/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[3]/div[3]/div/div[2]/span[2]/span";
	private static final String XPATH_ARCHIVE_BTN = "//*[@id=\"releaseFocusBidsBtn\"]";

	// private static final String XPATH_STATUS_REPORT =
	// "/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[3]/div[3]/div/div[2]/span[3]/span";
	private static final String XPATH_STATUS_REPORT = "//*[@id=\"focusRemediationStatusReportBtn\"]";
	// *[@id="focusRemediationStatusReportBtn_label"]

	// Table column constants of NonFocusTabPage
	public static final int FLAG_COL = 1;
	public static final int BID_ID_COL = 2;
	public static final int ADDITIONAL_ID_COL = 3;
	public static final int DETAIL_LINK_COL = 4;
	public static final int BID_DATE_COL = 5;
	public static final int REVIEWER_DECISION_COL = 6;
	public static final int REVIEWER_COL = 7;
	public static final int RELEASED_ON_COL = 8;
	public static final int COUNTRY_COL = 9;
	public static final int IBM_SELL_PRICE_COL = 10;
	public static final int PRIMARY_BRAND_COL = 11;
	public static final int DISTRIBUTOR_COL = 12;
	public static final int CUST_FACING_BP_COL = 13;
	public static final int CUST_NAME = 14;

	public static final int A_COL = 15;
	public static final int B_COL = 16;
	public static final int C_COL = 17;
	public static final int D_COL = 18;

	/**
	 * Default constructor
	 */
	public FocusPage() {
		// Do nothing
	}

	/**
	 * Constructor - for initializing the elements again. Used for switching back to
	 * Summary page from Details page.
	 * 
	 * @param driver
	 */
	public FocusPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public String getValueAtIndex(WebDriver driver, int rowIndex, int colIndex) {
		WebElement col = driver.findElement(By.xpath(constructXpath(rowIndex, colIndex)));

		String value = "";

		if (null != value) {
			value = col.getText();
		}

		return value;

	}

	public int searchRowIndex(WebDriver driver, String bidId) {
		this.driver = driver;
		int rowIndex = 0;
		try {
			rowIndex = (findBidIdRow(bidId));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return rowIndex;
	}

	public void clickCheckbox(int rowIndex) {
		WebElement checkbox = driver.findElement(By.xpath(XPATH_CHECKBOX_PART_1 + rowIndex + XPATH_CHECKBOX_PART_2));

		checkbox.click();
	}

	public void clickCheckbox(WebDriver driver, int rowIndex) {
		this.driver = driver;
		WebElement checkbox = driver.findElement(By.xpath(XPATH_CHECKBOX_PART_1 + rowIndex + XPATH_CHECKBOX_PART_2));

		checkbox.click();
	}

	/**
	 * TODO: Frey similar method with MyTaskTabPage.. Maybe we can refactor this
	 * 
	 * @param rowIndex
	 * @param colIndex
	 * @return
	 */
	private String constructXpath(int rowIndex, int colIndex) {
		return XPATH_PART_1 + rowIndex + XPATH_PART_2 + colIndex + XPATH_PART_3;
	}

	private int findBidIdRow(String bidNumber) throws Exception {

		int bidlocatecntr = 0, grdCntr = 0, pgLink = 1;
		String bidcheck = null;
		WebElement element;

		driver.findElement(By.xpath(".//*[@id='dijit__FocusMixin_4']/span[3]")).click(); // Clicks '25' records per page
		// driver.findElement(By.xpath("/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[3]/div[3]/div/div[5]/div[4]/div[2]/table/tbody/tr/td[3]/div/span[3]")).click();
		// // Clicks '25' records per page
		System.out.println("25 per page clicked.");
		// Routine to find the bids on Archive Tab
		do {

			driver.findElement(By.xpath(".//*[@id='dijit__FocusMixin_3']/span[2]/span[" + pgLink + "]")).click(); // click
																													// on
																													// the
																													// page
																													// link
			// System.out.println("Tracing...page number " +pgLink);

			grdCntr = 0; // set to zero, this will reset the row # on the grid should the code enters the
							// next page
			// System.out.println("Tracing...grid number init" +grdCntr);

			// Routine to check the bid per page
			for (int pgDataCntr = 0; pgDataCntr < 25; pgDataCntr++) {

				grdCntr = grdCntr + 1;

				// scroll until element is in focus since grid is dynamic with scroll bars
				element = driver.findElement(By
						.xpath(".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div[" + grdCntr + "]/table/tbody/tr/td[2]"));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
				Thread.sleep(500);

				bidcheck = driver
						.findElement(By.xpath(
								".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div[" + grdCntr + "]/table/tbody/tr/td[2]"))
						.getText();
				// System.out.println("Tracing...grid number inside FOR LOOP " +grdCntr);
				// System.out.println("Tracing...bid number " +bidcheck);

				if (bidNumber.equals(bidcheck)) { // break if bid is found
					break;
				}
			}
			if (pgLink < 5) {
				pgLink++; // go to next page
			}
		} while (!(bidNumber.equals(bidcheck))); // checking the specified bid on the grid

		bidlocatecntr = grdCntr;
		return bidlocatecntr;
	}

	/**
	 * Added by Lhoyd Castillo - 1/31/2018 this is to get the bid number on the
	 * search result
	 * 
	 */
	private static final String XPATH_RESULT_1 = ".//*[@id='focusBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[";
	private static final String XPATH_RESULT_2 = "]";

	private String constructSearchResultXpath(int colIndex) {
		return XPATH_RESULT_1 + colIndex + XPATH_RESULT_2;
	}

	public String getSearchResultValue(WebDriver driver, int colIndex) {
		WebElement col = driver.findElement(By.xpath(constructSearchResultXpath(colIndex)));

		String value = "";

		if (null != value) {
			value = col.getText();
		}
		return value;
	}

	public void clickDetailLink(WebDriver driver, int focuskRowIndex) {
		driver.findElement(By.xpath(constructXpath(focuskRowIndex, DETAIL_LINK_COL) + "/span")).click();
	}

	public boolean aHasMarker(int bidRow) {
		String currentXpath = constructXpath(bidRow, A_COL);
		WebElement col = driver.findElement(By.xpath(currentXpath));
		try {
			col.findElement(By.className("idxGridIconFailed"));
			return true;
		} catch (NoSuchElementException e) {
			// System.out.println("Col: " + A_COL + "has no element");
			return false;
		}

	}

	public boolean bHasMarker(int bidRow) {
		String currentXpath = constructXpath(bidRow, B_COL);
		WebElement col = driver.findElement(By.xpath(currentXpath));
		try {
			col.findElement(By.className("idxGridIconFailed"));
			return true;
		} catch (NoSuchElementException e) {
			// System.out.println("Col: " + B_COL + "has no element");
			return false;
		}
	}

	public boolean cHasMarker(int bidRow) {
		String currentXpath = constructXpath(bidRow, C_COL);
		WebElement col = driver.findElement(By.xpath(currentXpath));
		try {
			col.findElement(By.className("idxGridIconFailed"));
			return true;
		} catch (NoSuchElementException e) {
			// System.out.println("Col: " + C_COL + "has no element");
			return false;
		}
	}

	public boolean dHasMarker(int bidRow) {
		String currentXpath = constructXpath(bidRow, D_COL);
		WebElement col = driver.findElement(By.xpath(currentXpath));
		try {
			col.findElement(By.className("idxGridIconFailed"));
			return true;
		} catch (NoSuchElementException e) {
			// System.out.println("Col: " + D_COL + "has no element");
			return false;
		}
	}

	/**
	 * @author CalvinNadua copied by LhoydCastillo used in MTPCFocusTest 03/01/2018
	 * @param bidRow
	 * @return
	 */
	public boolean bidDetails(int bidRow) {
		String currentXpath = constructXpath(bidRow, DETAIL_LINK_COL);
		WebElement col = driver.findElement(By.xpath(currentXpath));
		try {
			col.click();

			return true;
		} catch (NoSuchElementException e) {

			return false;
		}
	}

	/**
	 * @author CalvinNadua copied by LhoydCastillo used in MTPCFocusTest 03/01/2018
	 * @param bidRow
	 * @return
	 */
	public String bidFactors(WebDriver driver) {
		this.driver = driver;
		WebElement col = driver.findElement(By.className("bidFactorsStyle"));
		try {

			String xFactor = col.getText();
			return xFactor;
		} catch (NoSuchElementException e) {
			// System.out.println("Col: " + A_COL + "has no element");
			return null;
		}

	}

	/**
	 * Clicks the export button
	 * 
	 * @param driver
	 */
	public void clickExportToCSVButton(WebDriver driver) {
		driver.findElement(By.xpath(XPATH_EXPORT_BTN)).click();
	}

	public void clickArchiveButton(WebDriver driver) {
		driver.findElement(By.xpath(XPATH_ARCHIVE_BTN)).click();
	}

	public boolean exportButtonIsDisplayed(WebDriver driver) {
		WebElement exportButton = driver.findElement(By.xpath(XPATH_EXPORT_BTN));
		boolean exportButtonIsDisplayed = exportButton.isDisplayed();
		return exportButtonIsDisplayed;

	}

	public boolean statusReportButtonIsDisplayed(WebDriver driver) {
		WebElement statusReportButton = driver.findElement(By.xpath(XPATH_STATUS_REPORT));
		boolean statusReportButtonIsDisplayed = statusReportButton.isDisplayed();
		return statusReportButtonIsDisplayed;

	}
	
	//DK - for access admin
	public boolean archiveButtonIsDisplayed(WebDriver driver) {
		WebElement archiveButton = driver.findElement(By.xpath(XPATH_ARCHIVE_BTN));
		try {
			archiveButton.isDisplayed();
			return true;
		} catch (NoSuchElementException e) {
			// System.out.println("exportButton is not present.");
			return false;
		}

	}
}