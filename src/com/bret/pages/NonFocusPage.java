package com.bret.pages;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

/**
 * TODO: FREY - Think of a way to handle the XPATH_PARTS, bid row search and
 * index value retrieval because tabs with table have these common features.
 * 
 * @author CheyenneFreyLazo
 * 
 */
public class NonFocusPage extends SummaryPage {
	private WebDriver driver;

	private static final String XPATH_PART_1 = ".//*[@id='nonFocusBidsGridDiv']/div[3]/div[2]/div[";
	private static final String XPATH_PART_2 = "]/table/tbody/tr/td[";
	private static final String XPATH_PART_3 = "]";

	private static final String XPATH_CHECKBOX_PART_1 = "/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[3]/div[2]/div/div[5]/div[3]/div[4]/div[";
	private static final String XPATH_CHECKBOX_PART_2 = "]/table/tbody/tr/td/span";

	private static final String XPATH_EXPORT_BTN = "//*[@id=\"nonfocusExportToExcelBtn\"]";

	private static final String XPATH_ARCHIVE_BTN = "//*[@id=\"processChecksBtn\"]";//DK - for access admin

	// Table column constants of NonFocusTabPage
	public static final int FLAG_COL = 1;
	public static final int BID_ID_COL = 2;
	public static final int ADDITIONAL_ID_COL = 3;
	public static final int DETAIL_LINK_COL = 4;
	public static final int BID_DATE_COL = 5;
	public static final int COUNTRY_COL = 6;
	public static final int IBM_SELL_PRICE_COL = 7;
	public static final int PRIMARY_BRAND_COL = 8;
	public static final int DISTRIBUTOR_COL = 9;
	public static final int CUST_FACING_BP_COL = 10;
	public static final int CUST_NAME = 11;

	/**
	 * Default constructor
	 */
	public NonFocusPage() {
		// Do nothing
	}

	/**
	 * Constructor - for initializing the elements again. Used for switching back to
	 * Summary page from Details page.
	 * 
	 * @param driver
	 */
	public NonFocusPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * Returns the text of the web element based on the row index and column index.
	 * 
	 * @param driver
	 *            current WebDriver.
	 * @param rowIndex
	 *            int row index.
	 * @param colIndex
	 *            int column index
	 * @return String text of the current web element.
	 */
	public String getValueAtIndex(WebDriver driver, int rowIndex, int colIndex) {
		WebElement col = driver.findElement(By.xpath(constructXpath(rowIndex, colIndex)));

		String value = "";

		if (null != value) {
			value = col.getText();
		}

		return value;

	}

	/**
	 * Returns the row index of the given bidId.
	 * 
	 * @param driver
	 *            the current WebDriver.
	 * @param bidId
	 *            bid id to be searched.
	 * @return index of the given bidId.
	 */
	public int searchRowIndex(WebDriver driver, String bidId) {
		this.driver = driver;
		int rowIndex = 0;
		try {
			rowIndex = (findBidIdRow(bidId));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return rowIndex;
	}

	/**
	 * Searching the row index after navigating back from detail page.
	 * 
	 * @param driver
	 * @param bidId
	 * @return
	 */
	public int searchRowIndexNavFromDetail(WebDriver driver, String bidId) {
		this.driver = driver;
		int rowIndex = 0;
		try {
			rowIndex = (findBidIdRowNavFromDetail(bidId));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return rowIndex;
	}

	public void clickCheckbox(int rowIndex) {
		WebElement checkbox = driver.findElement(By.xpath(XPATH_CHECKBOX_PART_1 + rowIndex + XPATH_CHECKBOX_PART_2));

		checkbox.click();
	}

	public void clickCheckbox(WebDriver driver, int rowIndex) {
		this.driver = driver;
		WebElement checkbox = driver.findElement(By.xpath(XPATH_CHECKBOX_PART_1 + rowIndex + XPATH_CHECKBOX_PART_2));

		checkbox.click();
	}

	/**
	 * TODO: Frey similar method with MyTaskTabPage.. Maybe we can refactor this
	 * 
	 * @param rowIndex
	 * @param colIndex
	 * @return
	 */
	private String constructXpath(int rowIndex, int colIndex) {
		return XPATH_PART_1 + rowIndex + XPATH_PART_2 + colIndex + XPATH_PART_3;
	}

	private int findBidIdRow(String bidNumber) throws Exception {

		int bidlocatecntr = 0, grdCntr = 0, pgLink = 1;
		String bidcheck = null;
		WebElement element;

		driver.findElement(By.xpath(".//*[@id='dijit__FocusMixin_4']/span[3]")).click();
		do {

			driver.findElement(By.xpath(".//*[@id='dijit__FocusMixin_3']/span[2]/span[" + pgLink + "]")).click();
			grdCntr = 0;
			for (int pgDataCntr = 0; pgDataCntr < 30; pgDataCntr++) {
				grdCntr = grdCntr + 1;
				element = driver.findElement(By.xpath(
						".//*[@id='nonFocusBidsGridDiv']/div[3]/div[2]/div[" + grdCntr + "]/table/tbody/tr/td[2]"));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
				Thread.sleep(500);

				bidcheck = driver.findElement(By.xpath(
						".//*[@id='nonFocusBidsGridDiv']/div[3]/div[2]/div[" + grdCntr + "]/table/tbody/tr/td[2]"))
						.getText();
				if (bidNumber.equals(bidcheck)) {
					break;
				}
			}

			pgLink++;

		} while (!(bidNumber.equals(bidcheck)));
		bidlocatecntr = grdCntr;

		return bidlocatecntr;
	}

	/**
	 * Searching interactable elements after navigation from detail page.
	 * 
	 * @param bidNumber
	 * @return
	 * @throws Exception
	 */
	private int findBidIdRowNavFromDetail(String bidNumber) throws Exception {

		int bidlocatecntr = 0, grdCntr = 0, pgLink = 1;
		String bidcheck = null;
		WebElement element;

		driver.findElement(By.xpath(
				"/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[3]/div[2]/div/div[5]/div[4]/div[2]/table/tbody/tr/td[3]/div/span[3]"))
				.click();
		do {

			driver.findElement(By.xpath(
					"/html/body/div[1]/div[3]/div/div[3]/div/div[2]/div/div[3]/div[2]/div/div[5]/div[4]/div[2]/table/tbody/tr/td[2]/div/span[2]/span["
							+ pgLink + "]"))
					.click();
			grdCntr = 0;
			for (int pgDataCntr = 0; pgDataCntr < 30; pgDataCntr++) {
				grdCntr = grdCntr + 1;
				element = driver.findElement(By.xpath(
						".//*[@id='nonFocusBidsGridDiv']/div[3]/div[2]/div[" + grdCntr + "]/table/tbody/tr/td[2]"));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
				Thread.sleep(500);

				bidcheck = driver.findElement(By.xpath(
						".//*[@id='nonFocusBidsGridDiv']/div[3]/div[2]/div[" + grdCntr + "]/table/tbody/tr/td[2]"))
						.getText();
				if (bidNumber.equals(bidcheck)) {
					break;
				}
			}

			pgLink++;

		} while (!(bidNumber.equals(bidcheck)));
		bidlocatecntr = grdCntr;

		return bidlocatecntr;
	}

	/**
	 * @author LhoydCastillo 1/31/2018 this is to get the bid number on the search
	 *         result
	 * 
	 */
	private static final String XPATH_RESULT_1 = ".//*[@id='nonFocusBidsGridDiv']/div[3]/div[2]/div/table/tbody/tr/td[";
	private static final String XPATH_RESULT_2 = "]";

	private String constructSearchResultXpath(int colIndex) {
		return XPATH_RESULT_1 + colIndex + XPATH_RESULT_2;
	}

	public String getSearchResultValue(WebDriver driver, int colIndex) {
		WebElement col = driver.findElement(By.xpath(constructSearchResultXpath(colIndex)));

		String value = "";

		if (null != value) {
			value = col.getText();
		}
		return value;
	}

	/**
	 * @author CalvinNadua copied by LhoydCastillo used in MTPCNonFocusTest
	 *         03/01/2018
	 * @param bidRow
	 * @return
	 */
	public boolean bidDetails(int bidRow) {
		String currentXpath = constructXpath(bidRow, DETAIL_LINK_COL);
		WebElement col = driver.findElement(By.xpath(currentXpath));
		try {
			col.click();

			return true;
		} catch (NoSuchElementException e) {

			return false;
		}

	}

	/**
	 * @author CalvinNadua copied by LhoydCastillo used in MTPCNonFocusTest
	 *         03/01/2018
	 * @param bidRow
	 * @return
	 */
	public String bidFactors(WebDriver driver) {
		this.driver = driver;
		WebElement col = driver.findElement(By.className("bidFactorsStyle"));
		try {

			String xFactor = col.getText();
			return xFactor;
		} catch (NoSuchElementException e) {
			// System.out.println("Col: " + A_COL + "has no element");
			return null;
		}

	}

	public void clickExportToCSVButton(WebDriver driver) {
		driver.findElement(By.xpath(XPATH_EXPORT_BTN)).click();
	}

	public void clickDetailLink(WebDriver driver, int nonFocuskRowIndex) {
		driver.findElement(By.xpath(constructXpath(nonFocuskRowIndex, DETAIL_LINK_COL) + "/span")).click();
	}


	
	public void clickArchiveButton(WebDriver driver) {
		driver.findElement(By.xpath(XPATH_ARCHIVE_BTN)).click();
	}
	
	
	public boolean archiveButtonIsDisplayed(WebDriver driver) {
		WebElement archiveButton = driver.findElement(By.xpath(XPATH_ARCHIVE_BTN));
		boolean archiveButtonIsDisplayed = archiveButton.isDisplayed();
		return archiveButtonIsDisplayed;
	}
	
	public boolean exportButtonIsDisplayed(WebDriver driver) {
		WebElement exportButton = driver.findElement(By.xpath(XPATH_EXPORT_BTN));
		boolean exportButtonIsDisplayed = exportButton.isDisplayed();
		return exportButtonIsDisplayed;
	}
	

	// Added by Calvin on June 26
	public void clickDetailLinkOf1stBid(WebDriver driver) {
		driver.findElement(By.xpath(constructXpath(1, DETAIL_LINK_COL) + "/span")).click();

	}
	
	

}
