package com.bret.testcases.hw.remediation;

import org.testng.annotations.Test;

import com.bret.pages.BRETArchivePage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

public class AutoAchiveFbids extends BRETmainTestNGbase {

	 
	// reading from data source
	
	 String xlpath =  "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
	 String sheet1 ="Credentials"; //Sheet name on excel
	 String Uname = Excel.getCellValue (xlpath, sheet1, 3, 1);   //(path, sheet name, row, column)
	 String Pword = Excel.getCellValue (xlpath, sheet1, 3, 2); 
	 String sheet2 ="HW Remediation"; //Sheet name on excel
	 String bidNumber = Excel.getCellValue (xlpath, sheet2, 7, 1);  
	 String revDesc = Excel.getCellValue (xlpath, sheet2, 7, 2);   
	 String className = AutoAchiveFbids.class.getSimpleName();
	 
	 @Test
	 public void tc_RHW_014_autoArch () throws Exception {
		 
		 //------------------------------------------------------------------------------------
		 // This test case will verify the remediated Focus bid released to Archive after CRON job
		 //------------------------------------------------------------------------------------
		 		 
		 //Performs the Login event
		 BRETValidLogin userLogin = new BRETValidLogin(driver);  
		 userLogin.userValidLogin(Uname, Pword);
		 
		 // check bid in Archive
		 BRETArchivePage releaseFbidChck =  new BRETArchivePage(driver);
		 releaseFbidChck.autoreleasedtoArchiveChck_F(bidNumber, revDesc, Uname);
		 
		 //Screen capture 
		 ScreenCapture screenShots = new ScreenCapture(driver);  
		 String methodName = ("TC_RHW-014_" + className );
		 screenShots.screenCaptures(methodName);
				 
	 }
}
