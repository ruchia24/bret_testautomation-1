package com.bret.testcases.hw.remediation;

import org.testng.annotations.Test;

import com.bret.pages.BRETFocusPage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

public class AutoArchNonRemFbid extends BRETmainTestNGbase{
	
	// reading from data source
	
//	 String xlpath = "/home/bellep/BRET_CICD_in_Jenkins/BRET_FE_Test/Data Inputs/BRET DataSource.xls";  //path server
	 String xlpath =  "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
	 String sheet1 ="Credentials"; //Sheet name on excel
	 String Uname = Excel.getCellValue (xlpath, sheet1, 3, 1);   //(path, sheet name, row, column)
	 String Pword = Excel.getCellValue (xlpath, sheet1, 3, 2); 
	 String sheet2 ="HW Remediation"; //Sheet name on excel
	 String bidNumber = Excel.getCellValue (xlpath, sheet2, 9, 1);  
	 String bidStat = Excel.getCellValue (xlpath, sheet2, 9, 2);   
	 String className = AutoArchNonRemFbid.class.getSimpleName();
	 
	 @Test
	 public void tc_nonRemFbid () throws Exception {
		 
		 //------------------------------------------------------------------------------------
		 // This test case will verify thst the non remediated Focus bid will not 
		//  move to Archived after the cron job.
		 //------------------------------------------------------------------------------------
		 		 
		 //Performs the Login event
		 BRETValidLogin userLogin = new BRETValidLogin(driver);  
		 userLogin.userValidLogin(Uname, Pword);
		 
		 // check bid in Focus Tab
		 BRETFocusPage nonremFidChck =  new BRETFocusPage(driver);
		 nonremFidChck.bidCheckFocusTab(bidNumber, bidStat, "HW"); //[updated-9/13] Added the brand 
		 
		 //Screen capture 
		 ScreenCapture screenShots = new ScreenCapture(driver);  
		 String methodName = ("TC_nonRemFbid_" + className );
		 screenShots.screenCaptures(methodName);
				 
	 }

}
