package com.bret.testcases.hw.scoring;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.BidQPage;
import com.bret.pages.DetailPage;
import com.bret.pages.DiscountMarginPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.FocusPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.SupportDataPage;
import com.bret.util.BretTestUtils;

/**
 * @author 147305
 * 
 */
public class HardwareFocusTest extends BaseTest {

	private static final String BID_ID = "bidId";
	private static final String REVIEWER_DECISION = "reviewerDecision";
	private static final String DISC_MARG_FILE = "discMargFilePath";
	private static String winHandleBefore;

	private MyTaskPage myTaskPage;
	private FocusPage focusPage;
	private DetailPage detailPage;
	private SupportDataPage supportdataPage;
	private FlaggingBidDataPage flagAndbid_page;

	@Test(priority = 1, description = "Logging in using credentials")
	public void logIn() {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getReviewerUsername(), getReviewerPassword());
		
		// System.out.println(getReviewerUsername());
		// System.out.println(getLeadReviewerPassword());
	}

	/**
	 * [BSHW-005] Focus Bids: Flagging To verify that the Reviewer is able to
	 * see the bid in My Task Tab / Focus Bid tab with status = "New"
	 * 
	 * Step/s:
	 * 
	 * 1.Launch BRET web.
	 * 
	 * 2. Login using Reviewer user.
	 * 
	 * 3. Go to My Task Tab/Focus Bid tab.
	 * 
	 * 4. Search for the newly loaded focus bid.
	 * 
	 * 5. Verify that the newly loaded bid is present in My Task Tab with status
	 * = NEW.
	 */

	/**
	 * @throws InterruptedException
	 */
	@Test(priority = 2, description = "[BSHW-005] Focus Bids: Flagging - To verify that the Reviewer is able to see the bid in My Task Tab / Focus Bid tab with status = New")
	public void focusBidsFlagging() throws InterruptedException {

		String tcNumber = "TC_BSHW-005";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(
				tcNumber, getTestDataSource());

		System.out
				.println("================= START [BSHW-005] Focus Bids: Flagging ==================\n");
		System.out
				.println("DESCRIPTION: To verify that the Reviewer is able to see the bid in My Task Tab / Focus Bid tab with status = New\n");

		myTaskPage = new MyTaskPage();
		myTaskPage.closeNPSSurvey(driver);
		System.out.println("You are currently at My Task Tab");
		/* Currently at MY TASK TAB */
		/* GET the row number of bid */
		int rowAtMyTaskTab = myTaskPage.searchRowIndex(driver,
				testData.get(BID_ID));

		/* CLICK on the radio button / check box */
		myTaskPage.clickCheckbox(rowAtMyTaskTab);

		/* GET the the value: BID, REVIEWER DECISION - MYTASK */
		String bidAtMyTaskTab = myTaskPage.getValueAtIndex(driver,
				rowAtMyTaskTab, myTaskPage.BID_ID_COL);

		String reviewerDecisionAtMyTaskTab = myTaskPage.getValueAtIndex(driver,
				rowAtMyTaskTab, myTaskPage.REVIEWER_DECISION_COL);

		Thread.sleep(5000);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		System.out.println("Navigating to Focus Bids Tab");
		/* NAVIGATE to FOCUS BIDS TAB, check if bid is present */
		myTaskPage.navigateToTab(driver, SummaryPageTabsE.FOCUS_BIDS);
		Thread.sleep(5000);

		focusPage = new FocusPage();
		int rowAtFocusTab = focusPage.searchRowIndex(driver,
				testData.get(BID_ID));

		/* CLICK on the flag */
		focusPage.clickCheckbox(rowAtFocusTab);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());

		/* get the the value: BID, REVIEWER DECISION - FOCUSTAB */
		String bidAtFocusTab = focusPage.getValueAtIndex(driver, rowAtFocusTab,
				focusPage.BID_ID_COL);
		String reviewerDecisionAtFocusTab = focusPage.getValueAtIndex(driver,
				rowAtFocusTab, focusPage.REVIEWER_DECISION_COL);

		/* SHOW bid values from prop, my task and focus tabs */
		System.out.println("\nBID VALUE ON: ");
		System.out.println("Excel file  (Expected) - " + testData.get(BID_ID));
		System.out.println("My Task Tab (Actual)   - " + bidAtMyTaskTab);
		System.out.println("Focus Tab   (Actual)   - " + bidAtFocusTab);

		/* SHOW Reviewer decision values from prop file, my task and focus tabs */
		System.out.println("\nREVIEWER DECISION VALUE ON: ");
		System.out.println("Excel file  (Expected) - "
				+ testData.get(REVIEWER_DECISION));
		System.out.println("My Task Tab (Actual)   - "
				+ reviewerDecisionAtMyTaskTab);
		System.out.println("Focus Tab   (Actual)   - "
				+ reviewerDecisionAtFocusTab);

		Assert.assertEquals(
				bidAtMyTaskTab.equals(bidAtFocusTab)
						&& reviewerDecisionAtMyTaskTab
								.equals(reviewerDecisionAtFocusTab), true);

		// System.out
		// .println("\n================= END [BSHW-005] Focus Bids: Flagging ==================\n\n\n\n");
		Thread.sleep(2000);
		/* YOU ARE AT FOCUS BIDS TAB */
	}

	/**
	 * 1154015: [BSHW-006] Focus Bids: ABCD indicators on Summary Page To verify
	 * that the ABCD indicators on the summary page is displayed correctly.
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Reviewer user.
	 * 
	 * 3. Go to My Task Tab/Focus Bid tab.
	 * 
	 * 4. Search for the newly loaded focus bid.
	 * 
	 * 5. Verify that the newly loaded bid is present in My Task Tab with status
	 * = NEW.
	 * 
	 * 6. Verify that the ABCD indicators on the summary page is displayed
	 * correctly.
	 */

	@Test(priority = 3, description = "1154015: [BSHW-006] Focus Bids: ABCD indicators on Summary Page - To verify that the ABCD indicators on the summary page is displayed correctly.")
	public void focusBidsABCDindicators() throws InterruptedException {

		String tcNumber = "TC_BSHW-006";
		myTaskPage = new MyTaskPage();

		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(
				tcNumber, getTestDataSource());

		System.out
				.println("\n\n====== START [BSHW-006] Focus Bids: ABCD indicators on Summary Page ======\n");
		System.out
				.println("DESCRIPTION: To verify that the ABCD indicators on the summary page is displayed correctly.\n");

		System.out.println("You are currently at Focus Bids Tab");
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel());
		Thread.sleep(5000);

		/* NAVIGATE from FOCUS BIDS TAB to MY TASK Tab */
		System.out.println("\nNavigating to My Task Tab");
		myTaskPage.navigateToTab(driver, SummaryPageTabsE.MY_TASK);
		Thread.sleep(5000);

		int rowAtMyTaskTab = myTaskPage.searchRowIndex(driver,
				testData.get(BID_ID));

		String bidAtMyTask = myTaskPage.getValueAtIndex(driver, rowAtMyTaskTab,
				myTaskPage.BID_ID_COL);

		/* CLICK on the flag */
		myTaskPage.clickCheckbox(rowAtMyTaskTab);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

		/* CHECK if Indicators are present. True=Present, False=Not */
		boolean aActual = myTaskPage.aHasMarker(rowAtMyTaskTab);
		boolean bActual = myTaskPage.bHasMarker(rowAtMyTaskTab);
		boolean cActual = myTaskPage.cHasMarker(rowAtMyTaskTab);
		boolean dActual = myTaskPage.dHasMarker(rowAtMyTaskTab);

		/* CHECK for the indicators in Properties File. True=Present, False=Not */
		boolean aExpected = Boolean.valueOf(testData.get("A"));
		boolean bExpected = Boolean.valueOf(testData.get("B"));
		boolean cExpected = Boolean.valueOf(testData.get("C"));
		boolean dExpected = Boolean.valueOf(testData.get("D"));

		System.out.println("\nBID VALUE ON: ");
		System.out.println("Excel file  (Expected) - " + testData.get(BID_ID));
		System.out.println("My Task Tab (Actual)   - " + bidAtMyTask);

		System.out
				.println("\nINDICATOR\tDATA INPUT (Expected)\tMY TASK TAB (Actual)");
		System.out.println("    A:\t\t\t" + aExpected + "\t\t\t" + bExpected);
		System.out.println("    B:\t\t\t" + bExpected + "\t\t\t" + bExpected);
		System.out.println("    C:\t\t\t" + cExpected + "\t\t\t" + cExpected);
		System.out.println("    D:\t\t\t" + dExpected + "\t\t\t" + dExpected);

		/* COMPARE the Indicators in My Task Tab and in Properties */

		Assert.assertEquals(aActual, aExpected);
		Assert.assertEquals(bActual, bExpected);
		Assert.assertEquals(cActual, cExpected);
		Assert.assertEquals(dActual, dExpected);

		// System.out
		// .println("\n====== END [BSHW-006] Focus Bids: ABCD indicators on Summary Page ======\n");
		Thread.sleep(2000);
		/* YOU ARE AT MY TASK TAB */
	}

	/**
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Reviewer user.
	 * 
	 * 3. Go to My Task Tab/Focus Bid tab.
	 * 
	 * 4. Search for the newly loaded focus bid.
	 * 
	 * 5. Verify that the newly loaded bid is present in My Task Tab with status
	 * = NEW.
	 * 
	 * 6. Verify that the ABCD indicators on the summary page is displayed
	 * correctly.
	 * 
	 * 7. Verify that the correct focus factors are being flagged and being
	 * displayed on "Factors" under "Flagging and Bid Data" tab on the summary
	 * page of the bid detail.
	 * 
	 * @throws InterruptedException
	 */

	@Test(priority = 4, description = "1154020: [BSHW-007] Focus Bids: Focus factors -To verify that the correct focus factors are being flagged and being displayed on Factors under Flagging and Bid Data tab on the summary page of the bid detail.")
	public void focusBidsFocusFactors() throws InterruptedException {

		String tcNumber = "TC_BSHW-007";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(
				tcNumber, getTestDataSource());

		System.out
				.println("============== START [BSHW-007] Focus Bids: Focus factors ==============\n");
		System.out
				.println("DESCRIPTION: To verify that the correct focus factors are being flagged and being \ndisplayed on Factors under Flagging and Bid Data tab on the summary page of the bid detail.\n");

		/* YOU ARE AT MY TASK TAB */
		System.out.println("You are currently at My Task Tab");
		myTaskPage = new MyTaskPage();

		/*
		 * GET the current row of the Bid and use it to click on the details of
		 * the bid
		 */
		int rownum_mytask = myTaskPage.searchRowIndex(driver,
				testData.get(BID_ID));
		String bidnum_mytask = myTaskPage.getValueAtIndex(driver,
				rownum_mytask, myTaskPage.BID_ID_COL);

		/* CHECK first if the indicators are Active on MYTASK tab */
		boolean aActual, bActual, cActual, dActual;
		aActual = myTaskPage.aHasMarker(rownum_mytask);
		bActual = myTaskPage.bHasMarker(rownum_mytask);
		cActual = myTaskPage.cHasMarker(rownum_mytask);
		dActual = myTaskPage.dHasMarker(rownum_mytask);

		/*
		 * CLICK on the chain to see bid details
		 * 
		 * TO DO: add xpath to Detail Page - navigate
		 */

		driver.findElement(
				By.xpath(".//*[@id='myTaskBidsGridDiv']/div[3]/div[2]/div["
						+ rownum_mytask + "]/table/tbody/tr/td[4]/span"))
				.click();
		System.out.println("Navigating to Bid Details Page");

		/* GET the control of the current window */
		winHandleBefore = driver.getWindowHandle();

		/* SWITCH Control to New Window */
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		/* you are now at BID Details Page */
		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		IndicatorCheck focusFactors = new IndicatorCheck();

		System.out.println("\nNow Checking on Indicators");
		/* Evaluate Indicator A */
		boolean a_indicator = focusFactors.aChecker(driver, bidnum_mytask,
				aActual);

		Thread.sleep(10000);
		/* Evaluate Indicator B */
		boolean bIndicator = focusFactors.bChecker(driver, bidnum_mytask,
				bActual, tcNumber);

		Thread.sleep(10000);
		/* Evaluate Indicator C */
		boolean cIndicator = focusFactors.cChecker(driver, bidnum_mytask,
				cActual);

		Thread.sleep(10000);
		/* Evaluate Indicator C */
		boolean dIndicator = focusFactors.dChecker(driver, bidnum_mytask,
				dActual);

		Assert.assertEquals(a_indicator, true);
		Assert.assertEquals(bIndicator, true);
		Assert.assertEquals(cIndicator, true);
		Assert.assertEquals(dIndicator, true);
		
		/* Switch to Original Window */
		// driver.switchTo().window(winHandleBefore);
		Thread.sleep(5000);

		// System.out
		// .println("============ END [BSHW-007] Focus Bids: Focus factors ============\n");
		System.out.println();
		setupNewURL(driver.getCurrentUrl());
		/* YOU ARE AT FLAGGING AND BID DATA TAB */

	}

	/**
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Reviewer user.
	 * 
	 * 3. Go to My Task Tab/Focus Bid tab.
	 * 
	 * 4. Search for the newly loaded focus bid.
	 * 
	 * 5. Verify that the newly loaded bid is present in My Task Tab with status
	 * = NEW.
	 * 
	 * 6. Verify that the ABCD indicators on the summary page is displayed
	 * correctly.
	 * 
	 * 7. Verify that the correct focus factors are being flagged and being
	 * displayed on Factors under Flagging and Bid Data tab on the summary
	 * page of the bid detail.
	 * 
	 * 8. Verify that the score under the supporting factors tab is correctly
	 * displayed on the summary page of the Bid Detail.
	 * 
	 * @throws InterruptedException
	 * 
	 */

	@Test(priority = 5, description = "[BSHW-008] Focus Bids: Supporting Factors - To verify that the score under the supporting factors tab is correctly displayed on the summary page of the Bid Detail.")
	public void focusBidsSupportingFactors() throws InterruptedException {

		String tcNumber = "TC_BSHW-008";

		/* YOU ARE AT FLAGGING AND BID DATA TAB */

		System.out
				.println("============ START [BSHW-008] Focus Bids: Supporting Factors ============\n");
		System.out
				.println("DESCRIPTION: To verify that the score under the supporting factors tab is correctly displayed on the summary page of the Bid Detail.\n");
		FlaggingBidDataPage flagAndBid_page = new FlaggingBidDataPage(driver);

		/*
		 * actual_score is the score in Flagging and Bid Data Tab (CURRENTLY AT
		 * FLAGGING AND BID DATA TAB)
		 */
		String actual_score = flagAndBid_page.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_SUPPORTING_FACTORS,
				FlaggingBidDataPage.COL_SCORE);

		Thread.sleep(3000);
		System.out.println("You are currently at Flagging and  Bid Data Tab");
		/* NAVIGATE to Support Data Tab to get the scores */
		detailPage = new DetailPage();
		detailPage.navigateToTab(driver, DetailPageTabsE.SUPPORT_DATA);

		Thread.sleep(5000);
		supportdataPage = new SupportDataPage(driver);
		/* SCREENSHOT, testcase#, currentpage */
		// takeScreenshot(tcNumber, DetailPageTabsE.SUPPORT_DATA.getLabel());
		supportdataPage.scrollingScreenshot(
				driver,
				getScreenshotPath(),
				BretTestUtils.getImgFilenameNoFormat(tcNumber
						+ DetailPageTabsE.SUPPORT_DATA.getLabel(),
						this.getClass()));

		/* expected_score is the score in Support Data Tab */
		int expected_score = supportdataPage.computeTotalScore(driver);

		System.out
				.println("Supporting Factors Score in Flagging and Bid Data Tab (Actual) : "
						+ actual_score);
		System.out
				.println("Supporting Factors Score in Support Data Tab (Expected)      : "
						+ expected_score);

		Assert.assertTrue(actual_score.equalsIgnoreCase(String
				.valueOf(expected_score)));

		// System.out
		// .println("============ END [BSHW-008] Focus Bids: Supporting Factors ============\n");

		Thread.sleep(5000);
		/* YOU ARE AT SUPPORT DATA PAGE */
	}

	/**
	 * Step/s: 1. Launch BRET web.
	 * 
	 * 2. Login using Reviewer user.
	 * 
	 * 3. Go to My Task Tab/Focus Bid tab.
	 * 
	 * 4. Search for the newly loaded focus bid.
	 * 
	 * 5. Verify that the newly loaded bid is present in My Task Tab with status
	 * = NEW.
	 * 
	 * 6. Verify that the ABCD indicators on the summary page is displayed
	 * correctly.
	 * 
	 * 7. Verify that the correct focus factors are being flagged and being
	 * displayed on Factors under Flagging and Bid Data tab on the summary
	 * page of the bid detail.
	 * 
	 * 8. Verify that the score under the supporting factors tab is correctly
	 * displayed on the summary page of the Bid Detail.
	 * 
	 * 9. Verify that the scores form the focus factors are being added to the
	 * total bid score and being displayed correctly on the summary page of bid
	 * detail.
	 * 
	 * @throws InterruptedException
	 * 
	 * */

	@Test(priority = 6, description = "1154028: [BSHW-009] Focus Bids: Scores - To verify that the score of the bid is being added to the total bid score and being displayed correctly on the summary page of bid detail.")
	public void focusBidsScores() throws InterruptedException {
		String tcNumber = "TC_BSHW-009";
		SupportDataPage supportdata_page = new SupportDataPage(driver);

		/* NAVIGATE to Flagging Bid Data */
		supportdata_page.navigateToTab(driver,
				DetailPageTabsE.FLAGGING_BID_DATA);

		System.out
				.println("============ START [BSHW-009] Focus Bids: Scores ============\n");
		System.out
				.println("DESCRIPTION: To verify that the score of the bid is being added to the total bid score and being displayed correctly on the summary page of bid detail.\n");

		Thread.sleep(15000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		FlaggingBidDataPage flagAndBid_page = new FlaggingBidDataPage(driver);

		/* actual_score is the score in Flagging and Bid Data Tab */
		String total_bidscore_actual = flagAndBid_page.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_TOTAL_BID_LEVEL_SCORE,
				FlaggingBidDataPage.COL_SCORE);

		/* expected_score is the summed up values in Flagging and Bid Data Tab */
		int total_bidscore_expected = flagAndBid_page.getTotalBidLevelScore();

		/* Checking for the Value of the Total Bid Level Score */
		System.out.println("Total Bid Level Score (actual)     : "
				+ total_bidscore_actual);
		System.out.println("Total Bid Level Score (calculated) : "
				+ total_bidscore_expected);
		System.out.println();

		Assert.assertTrue(total_bidscore_actual.equalsIgnoreCase(String
				.valueOf(total_bidscore_expected)));

		/* get the score value at index of Questions and BPCOPS */
		String qbpcops_actual = flagAndBid_page.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_QUEST_BPCOPS,
				FlaggingBidDataPage.COL_SCORE);
		
		String tier1Ceid = flagAndBid_page.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_DISTRIBUTOR_CEID);
		String tier2Ceid = flagAndBid_page.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_CUSTOMER_FACTING_BP_CEID);
		
		Thread.sleep(5000);

		System.out
				.println("Navigating to BID Q Page to get the QBPCOPS Expected Score");
		/* NAVIGATE to Bid Q's to get the score */
		DetailPage detail_page = new DetailPage();
		detail_page.navigateToTab(driver, DetailPageTabsE.BID_Q);

		// ---- > having black page while taking screenshot
		Thread.sleep(10000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel());
		Thread.sleep(5000);

		/* qbpcops_expected is the summed up values in BidQ Tab */
		BidQPage bidq_page = new BidQPage(driver);
		int qbpcops_expected = bidq_page.getTotalForBPCops(driver, tier1Ceid.equalsIgnoreCase(tier2Ceid));

		/* Checking for the QBCPCOPS Score */
		System.out.println("\nQuestions & BPCOPS (actual)   : "
				+ qbpcops_actual);
		System.out.println("Questions & BPCOPS (expected) : "
				+ qbpcops_expected);
		System.out.println();

		Assert.assertTrue(qbpcops_actual.equalsIgnoreCase(String
				.valueOf(qbpcops_expected)));

		// System.out
		// .println("============ END [BSHW-009] Focus Bids: Scores ============\n");
		detail_page.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);
		/* YOU ARE NOW AT FLAGGING BID DATA PAGE */

	}

	/**
	 * 1154036: [BSHW-010] Focus Bids: Display To verify that correctLeading
	 * product (Power / Storage for HW) , bid date, region, country and line
	 * items are being displayed correctly on the bid detail page.
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Reviewer user.
	 * 
	 * 3. Go to My Task Tab/Focus Bid tab.
	 * 
	 * 4. Search for the newly loaded focus bid.
	 * 
	 * 5. Verify that the newly loaded bid is present in My Task Tab with status
	 * = W.
	 * 
	 * 6. Verify that the ABCD indicators on the summary page is displayed
	 * correctly.
	 * 
	 * 7. Go to bid details.
	 * 
	 * 8. Verify thatcorrectLeading product (Power / Storage for HW) , bid
	 * date, region, country and line items are being displayed correctly on the
	 * bid detail page.
	 */

	@Test(priority = 7, description = "[BSHW-010] Focus Bids: Display - To verify that correct  Leading product (Power / Storage for HW) , bid date, region, country and line items are being displayed correctly on the bid detail page.")
	public void focusBidsDisplay() {

		String tcNumber = "TC_BSHW-010";

		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(
				tcNumber, getTestDataSource());

		System.out
				.println("============ START [BSHW-010] Focus Bids: Display ============\n");

		/* YOU ARE CURRENTLY AT FLAGGING BID DATA PAGE */

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		/* Getting expected values from Excel File */
		String expectedPrimaryBrand = testData.get("primaryBrand");
		String expectedBidDate = testData.get("bidDate");
		String expectedRegion = testData.get("region");
		String expectedCountry = testData.get("country");

		flagAndbid_page = new FlaggingBidDataPage(driver);

		/* Getting expected values from Flagging and Bid DAta Tab */
		String primaryBrand = flagAndbid_page.getValueAtIndexBidDataGrid(
				driver, FlaggingBidDataPage.ROW_PRIMARY_BRAND);

		String bidDate = flagAndbid_page.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_BID_DATE);

		String region = flagAndbid_page.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_REGION);

		String country = flagAndbid_page.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_COUNTRY_NAME);

		System.out
				.println("\n  DETAILS\tDATA INPUT (Expected)\tFLAGGING AND BID DATA TAB (Actual)");
		System.out.println("Primary Brand:\t\t" + expectedPrimaryBrand + "\t\t"
				+ primaryBrand);
		System.out.println("Country Name :\t\t" + expectedCountry + "\t\t"
				+ country);
		System.out.println("Region       :\t\t" + expectedRegion + "\t\t"
				+ region);
		System.out.println("Bid Date     :\t\t" + expectedBidDate + "\t\t"
				+ bidDate);
		System.out.println();

		Assert.assertEquals(primaryBrand, expectedPrimaryBrand);
		Assert.assertEquals(bidDate, expectedBidDate);
		Assert.assertEquals(region, expectedRegion);
		Assert.assertEquals(country, expectedCountry);

		/* NAVIGATE to Bid Margin And Discount */
		// --------->>> Added 9/27/2017

		DetailPage detail_page = new DetailPage();
		detail_page.navigateToTab(driver, DetailPageTabsE.DISCOUNT_MARGIN);
		boolean isDiscMargValid = checkDiscountMarginValidity(testData
				.get(DISC_MARG_FILE), tcNumber);
		
		Assert.assertTrue(isDiscMargValid);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.DISCOUNT_MARGIN.getLabel());

	}

	private boolean checkDiscountMarginValidity(String filePath, String tcNumber) { // --------->>>
																	// Added
																	// 9/27/2017
		List<String> testDataMargDisc = BretTestUtils.readFile(filePath);

		DiscountMarginPage discMargPage = new DiscountMarginPage(driver);
		return discMargPage.checkIsDiscountMarginValid(testDataMargDisc, getScreenshotPath(), BretTestUtils.getImgFilenameNoFormat(tcNumber, this.getClass()));
	}

	private void takeScreenshot(String tcNumber, String tabLabel) {
		BretTestUtils.screenCapture(
				driver,
				getScreenshotPath(),
				BretTestUtils.getImgFilename(tcNumber + "_" + tabLabel,
						this.getClass()));
	}

	@AfterTest
	public void exit() throws InterruptedException {
		/* Switch to Original Window */
		// driver.switchTo().window(winHandleBefore);
		Thread.sleep(5000);
		driver.quit();
	}

}
