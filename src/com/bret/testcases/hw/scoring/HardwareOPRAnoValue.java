package com.bret.testcases.hw.scoring;

import java.util.Map;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.ArchivePage;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.BRETSearchPage;
import com.bret.pages.BidQPage;
import com.bret.pages.DetailPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.MyTaskPage;
//import com.bret.pages.NonFocusPage;
import com.bret.pages.RemediationLogPage;
import com.bret.util.BretTestUtils;

public class HardwareOPRAnoValue extends com.bret.base.BaseTest {
	private static final String BID_ID = "bidId";
	private static final String BLANK = "<Blank>";
	private static final String EMPTY = "";

	private MyTaskPage myTaskPage;
	//private NonFocusPage nonFocusPage;
	private ArchivePage archivePage; // go back to using non focus once executing RT
	private BRETSearchPage searchPage;
	private FlaggingBidDataPage flagAndbidPage;
	private BidQPage bidQPage;
	private RemediationLogPage remediationPage;
	private DetailPage detailPage;

	@BeforeTest
	public void login() {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getAdminUsername(), getAdminPassword());
	}

	/**
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using any user role.
	 * 
	 * 3. Go to Search Tab
	 * 
	 * 4. Search for the newly loaded OPRA AP bid.
	 * 
	 * 5. Go to Bid Details
	 * 
	 * 6. Click Bid Q Tab
	 */
	@Test(priority = 1, description = "[BSHW-046] BRET Web Details > SQO Channel Override Reason: Field display for OPRA bid")
	public void testOpraSqoChannelOverrideReason() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		searchPage = new BRETSearchPage(driver);
		//nonFocusPage = new NonFocusPage();
		archivePage = new ArchivePage(); // go back to using non focus once
											// executing RT
		bidQPage = new BidQPage(driver);
		detailPage = new DetailPage();

		/* Get the test data from excel */
		String tcNumber = "TC_BSHW-046";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		String newlyLoadedBidId = testData.get(BID_ID);

		System.out.println(
				"================= START [BSHW-046] BRET Web Details > SQO Channel Override Reason: Field display for OPRA bid ==================\n");

		System.out.println("You are currently at My Task Tab");

		myTaskPage.navigateToTab(driver, SummaryPageTabsE.SEARCH);

		searchPage.enterBid(newlyLoadedBidId);
		Thread.sleep(3000);

		searchPage.clickSearch();
		Thread.sleep(3000);

		searchPage.clickHere();
		Thread.sleep(5000);

		int chainBidDetails = archivePage.searchRowIndex(driver, testData.get(BID_ID));

		driver.findElement(By.xpath(
				".//*[@id='archiveBidsGridDiv']/div[3]/div[2]/div[" + chainBidDetails + "]/table/tbody/tr/td[4]/span"))
				.click();
		System.out.println("Navigating to Bid Details Page");

		/* GET the control of the current window */
		//String winHandleBefore = driver.getWindowHandle();

		/* SWITCH Control to New Window */
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		Thread.sleep(5000);
		/* NAVIGATE to Bid Q's for SQO Channel Override Reason */
		detailPage.navigateToTab(driver, DetailPageTabsE.BID_Q);
		Thread.sleep(5000);
		
		System.out.println("\nVerify that SQO Channel Override Reason is not displayed for OPRA bids.");
		
		boolean actualChannelOverrideFieldPresent = bidQPage.channelOverrideReasonPresent(driver);
		
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel());
		Assert.assertFalse(actualChannelOverrideFieldPresent);

	}

	/**
	 * Step/s:
	 * 
	 * 1. Click Flagging and Bid Data Tab
	 * 
	 * 2. Locate OPPTY# field
	 * 
	 * 3. Click Remediation Log Tab
	 * 
	 * 4. Locate OPPTY# field
	 */
	@Test(priority = 2, description = "[BSHW-047] BRET Web Details > OPPTY# (OPRA bids): Loaded bid with no value on OPPTY#")
	public void testNoOPPTY() throws InterruptedException {
		bidQPage = new BidQPage(driver);
		flagAndbidPage = new FlaggingBidDataPage(driver);
		remediationPage = new RemediationLogPage(driver);

		String tcNumber = "TC_BSHW-047";
		
		/* NAVIGATE to Flagging Bid Data */

		detailPage.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);
		Thread.sleep(5000);

		System.out.println();
		System.out.println(
				"================= START [BSHW-047] BRET Web Details > OPPTY# (OPRA bids): Loaded bid with no value on OPPTY# ==================");
		
		String actualFlagAndBidOPPTYnum = flagAndbidPage.getValueAtIndexBidDataGrid(driver,	FlaggingBidDataPage.ROW_OPPTY_NUMBER);

		System.out.println("Verify that Opportunity Number of Flagging and Bid Data Tab and Remediation Log Tab should not have a value.\n");
		System.out.println("Expected Opportunity Number: " + EMPTY);
		System.out.println("Actual Opportunity Number  : " + actualFlagAndBidOPPTYnum);
		System.out.println();
		
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		Assert.assertEquals(actualFlagAndBidOPPTYnum, EMPTY);
		
		System.err.println();
		/* NAVIGATE to Remediation Log */
		detailPage.navigateToTab(driver, DetailPageTabsE.REMEDIATION_LOG);
		Thread.sleep(5000);

		String actualRemedLogOPPTYnum = remediationPage.getGenBidInforValueAtIndex(driver, RemediationLogPage.ROW_GEN_BID_OPPTY);

		System.out.println("\nExpected OPPTY #: " + EMPTY);
		System.out.println("Actual OPPTY #  : " + actualRemedLogOPPTYnum);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.REMEDIATION_LOG.getLabel());
		Assert.assertEquals(actualRemedLogOPPTYnum, EMPTY);

	}
	
	 /**
	 * Step/s:
	 *
	 * 1. Locate OO and BID MGR fields
	 *
	 */
	 @Test(priority = 3, description = "[BSHW-048] BRET Web Details > Bid Manager and Opportunity Owner (OPRA bids): Loaded bid with no value on Bid Manager and Opportunity Owner")
	 public void testNoBMandOO() throws InterruptedException {
	 remediationPage = new RemediationLogPage(driver);
	
	 String tcNumber = "TC_BSHW-048";
	
	 System.out.println();
	 System.out.println(
				"================= START [BSHW-048] BRET Web Details > Bid Manager and Opportunity Owner (OPRA bids): Loaded bid with no value on Bid Manager and Opportunity Owner ==================");
		
	 String actualNoBM = getContactsSubTableValue(RemediationLogPage.ROW_CONTACTS_BID_MGR);
	 String actualNoOO = getContactsSubTableValue(RemediationLogPage.ROW_CONTACTS_OO);
	 
	 System.out.println("Verify that values on the OO and BID MGR fields should not have a value.\n");
	 System.out.println("Expected Opportunity Owner: " + EMPTY);
	 System.out.println("Actual  Opportunity Owner : " + actualNoOO);
	 System.out.println("--------------------------------------------------------");
	 System.out.println("Expected Bid Manager: " + EMPTY);
	 System.out.println("Actual Bid Manager  : " + actualNoBM);
	
	 System.out.println();
	 
	 takeScreenshot(tcNumber, DetailPageTabsE.REMEDIATION_LOG.getLabel());
	 Assert.assertEquals(actualNoBM, EMPTY);
	 Assert.assertEquals(actualNoOO, EMPTY);
	
	 }
	
	 /**
	 * Step/s:
	 *
	 * 1. Locate the T3 ID/NAME and T4 ID/NAME fields
	 *
	 * 2. Click the Flagging and Bid Data Tab
	 *
	 * 3. Locate the Additional BP (CEID) field
	 */
	 @Test(priority = 4, description = "[BSHW-049] BRET Web Details > CEID T3/4 (OPRA bids): Loaded bid with no value on CEID T3/4")
	 public void testNoCEID3and4() throws InterruptedException {
	 remediationPage = new RemediationLogPage(driver);
	
	 String tcNumber = "TC_BSHW-049";
	 	
	 System.out.println(
				"================= START [BSHW-049] BRET Web Details > CEID T3/4 (OPRA bids): Loaded bid with no value on CEID T3/4");
	
	 String actualT3Name = getRTMSubTableValue(RemediationLogPage.ROW_RTM_T3_NAME);
	 String actualT3Id = getRTMSubTableValue(RemediationLogPage.ROW_RTM_T3_ID);
	 String actualT4Name = getRTMSubTableValue(RemediationLogPage.ROW_RTM_T4_NAME);
	 String actualT4Id = getRTMSubTableValue(RemediationLogPage.ROW_RTM_T4_ID);

	 System.out.println("Verify that values on the CEID T3/T4 ID/NAME and Additional BP (CEID) fields should not have a value.");
	 System.out.println();
	 System.out.println("Expected T3 Name: " + addBlankIndicator(EMPTY));
	 System.out.println("Actual T3 Name  : " + addBlankIndicator(actualT3Name));
	 System.out.println();
	 System.out.println("Expected T3 ID: " + addBlankIndicator(EMPTY));
	 System.out.println("Actual T3 ID  : " + addBlankIndicator(actualT3Id));
	 System.out.println("--------------------------------------------------------");
	 System.out.println("Expected T4 Name: " + addBlankIndicator(EMPTY));
	 System.out.println("Actual T4 Name) : " + addBlankIndicator(actualT3Name));
	 System.out.println();
	 System.out.println("Expected T4 Id: " + addBlankIndicator(EMPTY));
	 System.out.println("Actual T4 ID  : " + addBlankIndicator(actualT3Id));
	 System.out.println();
	
	 takeScreenshot(tcNumber, DetailPageTabsE.REMEDIATION_LOG.getLabel());
	 Assert.assertEquals(actualT3Name, EMPTY);
	 Assert.assertEquals(actualT3Id, EMPTY);
	 Assert.assertEquals(actualT4Name, EMPTY);
	 Assert.assertEquals(actualT4Id, EMPTY);
	
	
	 /* NAVIGATE to Flagging and Bid Data Page */
	 detailPage.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);
	
	 String actualNoT3T4 = flagAndbidPage.getValueAtIndexBidDataGrid(driver,
	 FlaggingBidDataPage.ROW_ADDITIONAL_BP_CEID);
	
	 System.out.println();
	 System.out.println("Expected Additional BP (CEID): " + addBlankIndicator(EMPTY));
	 System.out.println("Actual Additional BP (CEID)  : " + addBlankIndicator(actualNoT3T4));
	 System.out.println();
	
	 takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
	 Assert.assertEquals(actualNoT3T4, EMPTY);
	
	 }

	@AfterTest
	public void exit() throws InterruptedException {
		Thread.sleep(5000);
		driver.quit();
	}

	private void takeScreenshot(String tcNumber, String tabLabel) {
		BretTestUtils.screenCapture(driver,	getScreenshotPath(),BretTestUtils.getImgFilename(tcNumber + "_" + tabLabel,	this.getClass()));
	}
	
	private String getContactsSubTableValue(int rowIndex) {
		return remediationPage.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_CONTACTS, rowIndex);
	}

	private String getRTMSubTableValue(int rowIndex) {
		return remediationPage.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_RTM, rowIndex);
	}
	
	private String addBlankIndicator(String str) {
		if (str.isEmpty()) {
			return BLANK;
		}

		return str;
	}
}
