package com.bret.testcases.export;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.obj.export.ExportArchiveData;
import com.bret.pages.ArchivePage;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.ExportArchiveProcessor;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.RemediationLogPage;
import com.bret.util.BretTestUtils;

public class ExportArchiveTest extends BaseTest {

	private static final String DOWNLOAD_PATH_PROP = "exportDownloadPath";
	private static final String ARCHIVE_PROP = "exportArchiveSummary";

	private ExportArchiveProcessor exportArchiveProc = new ExportArchiveProcessor();

	@Test(priority = 4)
	public void testExportArchive() {

		/*
		 * Steps 1. Launch BRET web. 2. Login using Admin/Reviewer role . 3. Go to Non
		 * Focus tab. 4. Click checkbox of the corresponding bid/s to be exported 5.
		 * Click export to csv button.
		 */

		// Log in as Reviewer

		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getReviewerUsername(), getReviewerPassword());

		/*
		 * The landing page upon login is the My Task Tab page - so need to navigate to
		 * ARCHIVE tab
		 */

		MyTaskPage myTaskPage = new MyTaskPage();
		myTaskPage.closeNPSSurvey(driver);
		myTaskPage.navigateToTab(driver, SummaryPageTabsE.ARCHIVE);
		pausePage();
		ArchivePage archivePage = new ArchivePage();

		String tcNumber = "TC_EXPT-008";

		/* This closes the NPS Survey */
		archivePage.closeNPSSurvey(driver);
		pausePage();

		/*
		 * To identify what checkbox will be click and avoid Magic Number
		 */
		int ArchivePageFirstIndex = 1;

		/* Click the first checkbox */
		archivePage.clickCheckbox(driver , ArchivePageFirstIndex);
		pausePage(1000);

		takeScreenshot(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel(), this.getClass());

		/*
		 * Click Export Button - Automatically saves the CSV File - no more pop up
		 * message
		 */
		archivePage.clickExportToCSVButton(driver);
		pausePage(1000);

		String flag = archivePage.getValueAtIndex(driver, ArchivePageFirstIndex, archivePage.FLAG_COL);
		String bidId = archivePage.getValueAtIndex(driver, ArchivePageFirstIndex, archivePage.BID_ID_COL);
		String ibmSellPrice = archivePage.getValueAtIndex(driver, ArchivePageFirstIndex,
				archivePage.IBM_SELL_PRICE_COL);
		String reviewerDecision = archivePage.getValueAtIndex(driver, ArchivePageFirstIndex,
				archivePage.REVIEWER_DECISION_COL);

		// String reviewer = ArchivePage.getValueAtIndex(driver, ArchivePageFirstIndex,
		// ArchivePage.REVIEWER);

		String reviewerReleasedOn = archivePage.getValueAtIndex(driver, ArchivePageFirstIndex,
				archivePage.REVIEWER_RELEASED_ON_COL);
		String bidReleasedBy = archivePage.getValueAtIndex(driver, ArchivePageFirstIndex,
				archivePage.BID_RELEASED_BY_COL);
		String bidReleasedOn = archivePage.getValueAtIndex(driver, ArchivePageFirstIndex,
				archivePage.BID_RELEASED_ON_COL);
		
		String marginOrDiscountFlag = archivePage.aHasMarker(ArchivePageFirstIndex) ? "Y" : "N";
		String bidQuestionsFlag = archivePage.bHasMarker(ArchivePageFirstIndex) ? "Y" : "N";
		String pbcHistoryFlag = archivePage.cHasMarker(ArchivePageFirstIndex) ? "Y" : "N";
		String compositeScoreFlag = archivePage.aHasMarker(ArchivePageFirstIndex) ? "Y" : "N";
		String completedOn = archivePage.getValueAtIndex(driver, ArchivePageFirstIndex,
				archivePage.REVIEWER_RELEASED_ON_COL);

		archivePage.clickDetailLink(driver, ArchivePageFirstIndex);
		pausePage(5500);

		switchToNewWindow();
		setupNewURL(driver.getCurrentUrl());
		pausePage();

		FlaggingBidDataPage flaggingBidPage = new FlaggingBidDataPage(driver);

		pausePage(8000);

		ExportArchiveData exportedCsv = getDataFromExportCSV(ARCHIVE_PROP);
		ExportArchiveData bretWebData = getDataFromBretWebFields(flaggingBidPage, flag, bidId, ibmSellPrice,
				reviewerDecision, reviewerReleasedOn, bidReleasedBy, bidReleasedOn, marginOrDiscountFlag,
				bidQuestionsFlag, pbcHistoryFlag, compositeScoreFlag, completedOn);

		List<String> dataDifference = exportArchiveProc.getExportedDataDifference(exportedCsv, bretWebData);

		int dataDiffCount = dataDifference.size();

		if (dataDiffCount > 0) {
			for (String data : dataDifference) {
				System.out.println(data);
			}
		}

		Assert.assertEquals(0, dataDiffCount);

		logOutDetailsPage(driver);
		pausePage();
		pausePage();
	}

	private ExportArchiveData getDataFromExportCSV(String propertyKey) {
		return exportArchiveProc
				.getExportArchiveData(composeExportedFilepath(BretTestUtils.getPropertyValue(propertyKey)));
	}

	private String composeExportedFilepath(String fileName) {
		return BretTestUtils.getPropertyValue(DOWNLOAD_PATH_PROP) + fileName;
	}

	private ExportArchiveData getDataFromBretWebFields(FlaggingBidDataPage flaggingBidPage, String flag, String bidId,
			String ibmSellPrice, String reviewerDecision, String reviewerReleasedOn, String bidReleasedBy,
			String bidReleasedOn, String marginOrDiscountFlag, String bidQuestionsFlag, String pbcHistoryFlag,
			String compositeScoreFlag, String completedOn) {

		ExportArchiveData bretWebData = new ExportArchiveData();

		// Y = Focus , N= Non Focus , E = Error
		String type = "From BRET Web";

		if (flag.equalsIgnoreCase("Y")) {
			type = "focus";
		} else if (flag.equalsIgnoreCase("N")) {
			type = "nonfocus";
		} else {
			type = "error";
		}

		bretWebData.setType(type);
		bretWebData
				.setSourceSystem(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_SOURCE_SYSTEM));
		bretWebData.setRegion(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_REGION));
		bretWebData.setBidId(bidId);
		bretWebData
				.setAdditionalId(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_ADDITIONAL_ID));
		bretWebData.setBidDate(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_BID_DATE));
		bretWebData.setReviewerDecision(reviewerDecision);
		bretWebData.setReviewerReleasedOn(reviewerReleasedOn);
		bretWebData.setBidReleasedBy(bidReleasedBy);
		bretWebData.setBidReleasedOn(bidReleasedOn);
		bretWebData.setCountry(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_COUNTRY_NAME));
		bretWebData.setIbmSellPrice(ibmSellPrice);
		bretWebData
				.setPrimaryBrand(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_PRIMARY_BRAND));
		bretWebData.setDistributor(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_DISTRIBUTOR));
		bretWebData.setCustomerFacingBP(
				flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_CUSTOMER_FACING_BP));
		bretWebData
				.setCustomerName(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_CUSTOMER_NAME));
		bretWebData.setMarginOrDiscountFlag(marginOrDiscountFlag);
		bretWebData.setBidQuestionsFlag(bidQuestionsFlag);
		bretWebData.setPbcHistoryFlag(pbcHistoryFlag);
		bretWebData.setCompositeScoreFlag(compositeScoreFlag);

		// navigate to Remediation log Tab
		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.REMEDIATION_LOG);
		RemediationLogPage remediationLog = new RemediationLogPage(driver);

		bretWebData.setReviewerComments(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_OUTCOME_COMMENT));
		bretWebData.setReadyToReviewOn(
				remediationLog.getGenBidInforValueAtIndex(driver, RemediationLogPage.ROW_GEN_BID_DATE));

		bretWebData.setConf(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_CONFIDENTIALITY));
		bretWebData
				.setRtm(remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_ROUTE_TO_MARKET));
		bretWebData.setNstdPmt(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_OFFSHORE_PAYMENT_TERMS));
		bretWebData.setBundle(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_BUNDLED_SOLUTIONS));
		bretWebData
				.setFee(remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_CONTINGENCY_FEE));
		bretWebData.setSoleSourceProc(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_SOLE_SOURCE_PROC));
		bretWebData.setFocusBp(remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_FOCUS_BP));
		bretWebData.setBpmPerc(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_BP_MARGIN_PERC));
		bretWebData.setDiscPerc(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_DISCOUNT_PERC));
		bretWebData.setBpmTotal(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_BP_MARGIN_TOTAL));
		bretWebData.setDiscTotal(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_DISCOUNT_TOTAL));
		bretWebData.setRemediationLog(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_LOG));
		bretWebData.setActionOwner(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_ACTION_OWNER));
		bretWebData.setCompletedOn(completedOn);
		bretWebData.setClosedDate(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_CLOSE_DATE));

		// How to set Outcome Value?
		bretWebData.setOutcome(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_REMEDIATION,
				RemediationLogPage.ROW_REM_OUTCOME));
		// Bid Expired = Y (If Outcome = Expired), N for other outcome
		String outcome = "From BRET Web";
		String isExpired = "";

		if (outcome.equalsIgnoreCase("EXPIRED")) {
			isExpired = "Y";
		} else {
			isExpired = "N";
		}

		bretWebData.setBidExpired(isExpired);

		bretWebData.setExPreFulfilment(
				remediationLog.getPostShipmentValueAtIndex(driver, RemediationLogPage.ROW_POSTSHIP_EX_PRE_FULFILMENT));
		bretWebData.setPostVerifyDate(
				remediationLog.getPostShipmentValueAtIndex(driver, RemediationLogPage.ROW_POSTSHIP_POST_VERIFY_DATE));
		bretWebData.setPostShipAuditFollowUpStatus(remediationLog.getPostShipmentValueAtIndex(driver,
				RemediationLogPage.ROW_POSTSHIP_POST_SHIP_AUDIT_FOLLOW_UP_STATUS));
		bretWebData.setWaitingForExtension(remediationLog.getPostShipmentValueAtIndex(driver,
				RemediationLogPage.ROW_POSTSHIP_WAITING_FOR_EXTENSION));
		bretWebData.setProblemsWithChasing(remediationLog.getPostShipmentValueAtIndex(driver,
				RemediationLogPage.ROW_POSTSHIP_PROBLEMS_WITH_CHASING));
		bretWebData.setRequestSentTooEarly(remediationLog.getPostShipmentValueAtIndex(driver,
				RemediationLogPage.ROW_POSTSHIP_REQUEST_SENT_TOO_EARLY));
		bretWebData.setRealisticallyDue(
				remediationLog.getPostShipmentValueAtIndex(driver, RemediationLogPage.ROW_POSTSHIP_REALISTICALLY_DUE));
		bretWebData.setTestNoteStartedYet(remediationLog.getPostShipmentValueAtIndex(driver,
				RemediationLogPage.ROW_POSTSHIP_TEST_NOT_STARTED_YET));
		bretWebData.setPotentialIssues(
				remediationLog.getPostShipmentValueAtIndex(driver, RemediationLogPage.ROW_POSTSHIP_POTENTIAL_ISSUES));
		bretWebData.setFollowUpLog(
				remediationLog.getPostShipmentValueAtIndex(driver, RemediationLogPage.ROW_POSTSHIP_FOLLOW_UP_LOG));
		bretWebData.setDeadlineOfRequest(remediationLog.getPostShipmentValueAtIndex(driver,
				RemediationLogPage.ROW_POSTSHIP_DEADLINE_OF_REQUEST));
		bretWebData.setBidEvent(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_FOLLOWUP,
				RemediationLogPage.ROW_FOLLOWUP_FULFILLED));
		
		

		return bretWebData;

	}

}
