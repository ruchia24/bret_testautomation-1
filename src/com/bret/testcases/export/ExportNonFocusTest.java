package com.bret.testcases.export;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.obj.export.ExportNonFocusData;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.ExportNonFocusProcessor;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.NonFocusPage;
import com.bret.pages.RemediationLogPage;
import com.bret.util.BretTestUtils;

public class ExportNonFocusTest extends BaseTest {

	private static final String DOWNLOAD_PATH_PROP = "exportDownloadPath";
	private static final String NON_FOCUS_PROP = "exportNonFocusSummary";

	private ExportNonFocusProcessor exportNonFocusProc = new ExportNonFocusProcessor();

	@Test(priority = 2)
	public void testExportNonFocus() {

		/*
		 * Steps 1. Launch BRET web. 2. Login using Admin/Reviewer role . 3. Go to Non
		 * Focus tab. 4. Click checkbox of the corresponding bid/s to be exported 5.
		 * Click export to csv button.
		 */

		// Log in as Reviewer

		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getReviewerUsername(), getReviewerPassword());

		/*
		 * The landing page upon login is the My Task Tab page - so need to navigate to
		 * Non_focus tab
		 */

		MyTaskPage myTaskPage = new MyTaskPage();
		myTaskPage.closeNPSSurvey(driver);
		myTaskPage.navigateToTab(driver, SummaryPageTabsE.NON_FOCUS_BIDS);
		NonFocusPage nonFocusPage = new NonFocusPage();

		String tcNumber = "TC_EXPT-002";

		/* This closes the NPS Survey */
		nonFocusPage.closeNPSSurvey(driver);
		pausePage();

		/*
		 * To identify what checkbox will be click and avoid Magic Number
		 */
		int nonFocusPageFirstIndex = 1;

		/* Click the first checkbox */
		nonFocusPage.clickCheckbox(driver, nonFocusPageFirstIndex);
		pausePage(1000);

		takeScreenshot(tcNumber, SummaryPageTabsE.NON_FOCUS_BIDS.getLabel(), this.getClass());

		/*
		 * Click Export Button - Automatically saves the CSV File - no more pop up
		 * message
		 */
		nonFocusPage.clickExportToCSVButton(driver);
		pausePage(1000);

		String flag = nonFocusPage.getValueAtIndex(driver, nonFocusPageFirstIndex, nonFocusPage.FLAG_COL);
		String bidId = nonFocusPage.getValueAtIndex(driver, nonFocusPageFirstIndex, nonFocusPage.BID_ID_COL);
		String ibmSellPrice = nonFocusPage.getValueAtIndex(driver, nonFocusPageFirstIndex,
				nonFocusPage.IBM_SELL_PRICE_COL);

		nonFocusPage.clickDetailLink(driver, nonFocusPageFirstIndex);
		pausePage(5500);

		switchToNewWindow();
		setupNewURL(driver.getCurrentUrl());
		pausePage();

		FlaggingBidDataPage flaggingBidPage = new FlaggingBidDataPage(driver);

		pausePage(8000);

		ExportNonFocusData exportedCsv = getDataFromExportCSV(NON_FOCUS_PROP);
		ExportNonFocusData bretWebData = getDataFromBretWebFields(flaggingBidPage, flag, bidId, ibmSellPrice);

		List<String> dataDifference = exportNonFocusProc.getExportedDataDifference(exportedCsv, bretWebData);

		int dataDiffCount = dataDifference.size();

		if (dataDiffCount > 0) {
			for (String data : dataDifference) {
				System.out.println(data);
			}
		}

		Assert.assertEquals(0, dataDiffCount);

		logOutDetailsPage(driver);
		pausePage();
		pausePage();
	}

	private ExportNonFocusData getDataFromExportCSV(String propertyKey) {
		return exportNonFocusProc
				.getExportNonFocusData(composeExportedFilepath(BretTestUtils.getPropertyValue(propertyKey)));
	}

	private String composeExportedFilepath(String fileName) {
		return BretTestUtils.getPropertyValue(DOWNLOAD_PATH_PROP) + fileName;
	}

	private ExportNonFocusData getDataFromBretWebFields(FlaggingBidDataPage flaggingBidPage, String flag, String bidId,
			String ibmSellPrice) {

		ExportNonFocusData bretWebData = new ExportNonFocusData();

		String type = "From BRET Web";

		if (flag.equalsIgnoreCase("N")) {
			type = "nonfocus";
		} else {
			type = "error";
		}

		bretWebData.setType(type);
		bretWebData
				.setSourceSystem(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_SOURCE_SYSTEM));
		bretWebData.setRegion(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_REGION));
		bretWebData.setBidId(bidId);
		bretWebData
				.setAdditionalId(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_ADDITIONAL_ID));
		bretWebData.setBidDate(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_BID_DATE));
		bretWebData.setCountry(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_COUNTRY_NAME));
		bretWebData.setIbmSellPrice( ibmSellPrice);
		bretWebData
				.setPrimaryBrand(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_PRIMARY_BRAND));
		bretWebData.setDistributor(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_DISTRIBUTOR));
		bretWebData.setCustomerFacingBP(
				flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_CUSTOMER_FACING_BP));
		bretWebData
				.setCustomerName(flaggingBidPage.getValueAtIndexBidDataGrid(driver, flaggingBidPage.ROW_CUSTOMER_NAME));

		// navigate to Remediation log Tab
		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.REMEDIATION_LOG);
		RemediationLogPage remediationLog = new RemediationLogPage(driver);

		bretWebData.setReviewerComments(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_OUTCOME_COMMENT));

		
		return bretWebData;

	}
}
