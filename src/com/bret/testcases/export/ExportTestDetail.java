package com.bret.testcases.export;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import org.junit.Assert;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.obj.export.ExportDetailData;
import com.bret.pages.AdvancedSearchPage;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.BidQPage;
import com.bret.pages.ExportProcessor;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.FocusPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.NonFocusPage;
import com.bret.pages.RemediationLogPage;
import com.bret.util.BretTestUtils;

public class ExportTestDetail extends BaseTest {

	private static final String DOWNLOAD_PATH_PROP = "exportDownloadPath";
	private static final String NON_FOCUS_DETAIL_PROP = "exportNonFocusDetail";
	private static final String FOCUS_DETAIL_PROP = "exportFocusDetail";

	private static final int FIRST_INDEX = 1;

	private ExportProcessor exportProcess = new ExportProcessor();

	/**
	 * Test Case: [EXPT-003]
	 * 
	 * Steps:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Admin/Reviewer role .
	 * 
	 * 3. Go to bid detail of a non focus bid.
	 * 
	 * 4. Click print version button.
	 * 
	 */
	@Test(priority = 1)
	public void testNonFocusDetailsExportIntegrity() {
		/* Get the test data from excel */
		String tcNumber = "TC_EXPT-005";
		// Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber,
		// getTestDataSource());

		/* Gets the value of the bidId value from the excel file test data source */

		// String bidId = testData.get(BID_ID);
		// String bidType = testData.get(BID_TYPE);
		//
		String bidType = "nonfocus";

		exportAndCheckDetails( bidType, NON_FOCUS_DETAIL_PROP, tcNumber, SummaryPageTabsE.NON_FOCUS_BIDS);

	}

	/**
	 * 
	 * Test Case: [EXPT-009]
	 * 
	 * Steps:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Admin/Reviewer role .
	 * 
	 * 3. Go to bid detail of a focus bid.
	 * 
	 * 4. Click export to csv button.
	 */
	@Test(priority = 2)
	public void testFocusDetailsExportIntegrity() {
		/* Get the test data from excel */
		String tcNumber = "TC_EXPT-009";
		// Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber,
		// getTestDataSource());

		/* Gets the value of the bidId value from the excel file test data source */

		// String bidId = testData.get(BID_ID);
		// String bidType = testData.get(BID_TYPE);
		//
		String bidType = "focus";

		exportAndCheckDetails(bidType, FOCUS_DETAIL_PROP, tcNumber, SummaryPageTabsE.FOCUS_BIDS);

	}
	
	/**
	 * Test Case:
	 * 
	 * Steps:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Admin/Reviewer role .
	 * 
	 * 3. Go to bid detail of a non focus bid.
	 * 
	 * 4. Click print version button.
	 * 
	 */
	@Test(priority = 3)
	public void testNonFocusPrintVersion() {
		String tcNumber = "TC_EXPT-003";

		clickPrintVersion(tcNumber, SummaryPageTabsE.NON_FOCUS_BIDS);
	}

	/**
	 * Test Case:
	 * 
	 * Steps:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Admin/Reviewer role .
	 * 
	 * 3. Go to bid detail of a non focus bid.
	 * 
	 * 4. Click print version button.
	 * 
	 */
	@Test(priority = 4)
	public void testFocusPrintVersion() {
		String tcNumber = "TC_EXPT-006";

		clickPrintVersion(tcNumber, SummaryPageTabsE.FOCUS_BIDS);
	}

	/**
	 * Test Case:
	 * 
	 * Steps:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2.Login using Admin/Reviewer role .
	 * 
	 * 3. Go to Advanced Search tab.
	 * 
	 * 4. Click export to csv button.
	 */
	@Test(priority = 5)
	public void testAdvancedTabExportNoResults() {
		String tcNumber = "TC_EXPT-009";

		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getReviewerUsername(), getReviewerPassword());

		/* MyTaskPage closing the NPS Survey and navigating to Tab */
		MyTaskPage myTaskPage = new MyTaskPage();
		myTaskPage.closeNPSSurvey(driver);
		myTaskPage.navigateToTab(driver, SummaryPageTabsE.ADVANCED_SEARCH);

		AdvancedSearchPage advSearchPage = new AdvancedSearchPage();
		pausePage();
		takeScreenshot(tcNumber, SummaryPageTabsE.ADVANCED_SEARCH.getLabel() + "Before Clicking Button",
				this.getClass());
		advSearchPage.clickExportToCsvButton(driver);

		pausePage();
		BufferedImage image;
		try {
			image = new Robot()
					.createScreenCapture(new java.awt.Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
			ImageIO.write(image, "png",
					new File(getScreenshotPath() + "/" + tcNumber + "_advanced_search_alert_screenshot.png"));
		} catch (HeadlessException e) {
			e.printStackTrace();
		} catch (AWTException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		pausePage();
		advSearchPage.clickOkInAlert(driver);
	}

	private void clickPrintVersion(String tcNumber, SummaryPageTabsE tab) {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getReviewerUsername(), getReviewerPassword());

		/* MyTaskPage closing the NPS Survey and navigating to Tab */
		MyTaskPage myTaskPage = new MyTaskPage();
		myTaskPage.closeNPSSurvey(driver);
		myTaskPage.navigateToTab(driver, tab);

		pausePage();

		if (tab.equals(SummaryPageTabsE.NON_FOCUS_BIDS)) {
			/* NonFocusPage searching for the Bid and clicking the detail link */
			NonFocusPage nonFocusPage = new NonFocusPage();
			pausePage(6000);
			int nonFocusRowIndex = FIRST_INDEX;
			nonFocusPage.clickCheckbox(driver, nonFocusRowIndex);
			takeScreenshot(tcNumber, tab.getLabel(), this.getClass());
			nonFocusPage.clickDetailLink(driver, nonFocusRowIndex);
		} else if (tab.equals(SummaryPageTabsE.FOCUS_BIDS)) {
			FocusPage focusPage = new FocusPage();
			pausePage(6000);
			int focusRowIndex = FIRST_INDEX;
			focusPage.clickCheckbox(driver, focusRowIndex); 
			takeScreenshot(tcNumber, tab.getLabel(), this.getClass());
			focusPage.clickDetailLink(driver, focusRowIndex);
		}

		pausePage();
		switchToNewWindow();
		setupNewURL(driver.getCurrentUrl());
		pausePage();

		FlaggingBidDataPage flaggingBidPage = new FlaggingBidDataPage(driver);
		flaggingBidPage.clickPrintVersionButton();

		Assert.assertTrue(flaggingBidPage.checkPrintVersionVisibility());

		switchToNewWindow();
		pausePage();
		takeScreenshot(tcNumber, tab.getLabel(), this.getClass());
		pausePage();

		flaggingBidPage.closePrintVersion(driver);

		pausePage();
		switchToNewWindow();

		logOutDetailsPage(driver);
		pausePage();
		pausePage();
	}

	private void exportAndCheckDetails(String bidType, String propertyKey, String tcNumber,
			SummaryPageTabsE tab) {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getReviewerUsername(), getReviewerPassword());
		String bidId = "";

		/* MyTaskPage closing the NPS Survey and navigate to Tab */
		MyTaskPage myTaskPage = new MyTaskPage();
		myTaskPage.closeNPSSurvey(driver);
		myTaskPage.navigateToTab(driver, tab);

		pausePage();
		if (tab.equals(SummaryPageTabsE.NON_FOCUS_BIDS)) {
			/* NonFocusPage searching for the Bid and clicking the detail link */
			NonFocusPage nonFocusPage = new NonFocusPage();
			pausePage(6000);
//			int nonFocuskRowIndex = nonFocusPage.searchRowIndex(driver, bidId);
			int nonFocusRowIndex = FIRST_INDEX;
			bidId = nonFocusPage.getValueAtIndex(driver, nonFocusRowIndex, NonFocusPage.BID_ID_COL);
			nonFocusPage.clickCheckbox(driver, nonFocusRowIndex);

			takeScreenshot(tcNumber, tab.getLabel(), this.getClass());
			nonFocusPage.clickDetailLink(driver, nonFocusRowIndex);
		} else if (tab.equals(SummaryPageTabsE.FOCUS_BIDS)) {
			FocusPage focusPage = new FocusPage();
			pausePage(6000);
			int focusRowIndex = FIRST_INDEX;
			bidId = focusPage.getValueAtIndex(driver, focusRowIndex, FocusPage.BID_ID_COL);
			focusPage.clickCheckbox(driver, focusRowIndex);
			takeScreenshot(tcNumber, tab.getLabel(), this.getClass());
			focusPage.clickDetailLink(driver, focusRowIndex);
		}

		pausePage();
		switchToNewWindow();
		setupNewURL(driver.getCurrentUrl());
		pausePage();

		FlaggingBidDataPage flaggingBidPage = new FlaggingBidDataPage(driver);
		flaggingBidPage.clickExportToCSVButton();
		pausePage(8000);

		ExportDetailData exportedCsv = getDataFromExportCSV(propertyKey);
		ExportDetailData bretWebData = getDataFromBretWebFields(flaggingBidPage, bidId, bidType);

		List<String> dataDifference = exportProcess.getExportedDataDifference(exportedCsv, bretWebData);

		int dataDiffCount = dataDifference.size();

		if (dataDiffCount > 0) {
			for (String data : dataDifference) {
				System.out.println(data);
			}
		}

		Assert.assertEquals(0, dataDiffCount);

		logOutDetailsPage(driver);
		pausePage();
		pausePage();
	}

	private ExportDetailData getDataFromExportCSV(String propertyKey) {
		return exportProcess.getExportDetailData(composeExportedFilepath(BretTestUtils.getPropertyValue(propertyKey)));
	}

	private ExportDetailData getDataFromBretWebFields(FlaggingBidDataPage flaggingBidPage, String bidId,
			String bidType) {
		ExportDetailData bretWebData = new ExportDetailData();

		bretWebData.setBidId(bidId);
		bretWebData.setType(bidType);
		bretWebData.setSourceSystem(
				flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_SOURCE_SYSTEM));
		bretWebData.setBidDate(flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_BID_DATE));
		bretWebData.setCountryName(
				flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_COUNTRY_NAME));
		bretWebData.setRegion(flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_REGION));
		bretWebData.setPrimaryBrand(
				flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_PRIMARY_BRAND));
		bretWebData.setDistributor(
				flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_DISTRIBUTOR));
		bretWebData.setDistributorCeid(
				flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_DISTRIBUTOR_CEID));
		bretWebData.setCustomerFacingBP(
				flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_CUSTOMER_FACING_BP));
		bretWebData.setCustomerFacingBPCeid(
				flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_CUSTOMER_FACTING_BP_CEID));
		bretWebData.setCustomerName(
				flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_CUSTOMER_NAME));
		bretWebData.setCustomerCMR(
				flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_CUSTOMER_CMR));
		bretWebData.setAdditionalId(
				flaggingBidPage.getValueAtIndexBidDataGrid(driver, FlaggingBidDataPage.ROW_ADDITIONAL_ID));
		bretWebData.setBidLevelMargin(flaggingBidPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_BID_MARGIN_PERC,
				FlaggingBidDataPage.COL_CURRENT_VALUE));
		bretWebData.setBidLevelDiscount(flaggingBidPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_BID_DISC_PERC,
				FlaggingBidDataPage.COL_CURRENT_VALUE));
		bretWebData.setAggregateScoreNdm(flaggingBidPage.getValueAtIndex(driver, FlaggingBidDataPage.ROW_QUEST_BPCOPS,
				FlaggingBidDataPage.COL_SCORE));
		bretWebData.setTotalBidScore(flaggingBidPage.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_TOTAL_BID_LEVEL_SCORE, FlaggingBidDataPage.COL_SCORE));

		// Helps Navigate to another tab
		// navigate to Remediation log Tab
		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.REMEDIATION_LOG);
		RemediationLogPage remediationLog = new RemediationLogPage(driver);

		bretWebData
				.setRevenueSize(remediationLog.getGenBidInforValueAtIndex(driver, RemediationLogPage.ROW_GEN_BID_TCV));
		bretWebData.setSoleSourceProc(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_SOLE_SOURCE_PROC));
		bretWebData.setFocusBP(remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_FOCUS_BP));
		bretWebData.setBpMargPerc(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_BP_MARGIN_PERC));
		bretWebData.setDiscountPerc(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_DISCOUNT_PERC));
		bretWebData.setBpMarginTotal(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_BP_MARGIN_TOTAL));
		bretWebData.setDiscountTotal(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_DISCOUNT_TOTAL));
		bretWebData.setSupportingFactors(
				remediationLog.getFocusBidAttrValueAtIndex(driver, RemediationLogPage.ROW_FBA_SUPPORTING_FACTORS));
		bretWebData.setOperationOwner(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_CONTACTS, RemediationLogPage.ROW_CONTACTS_OO));
		bretWebData.setBidMgr(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_CONTACTS,
				RemediationLogPage.ROW_CONTACTS_BID_MGR));
		bretWebData.setRemediationUser(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_CONTACTS, RemediationLogPage.ROW_CONTACTS_REMEDIATON_USER));
		bretWebData.setT3Name(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_RTM,
				RemediationLogPage.ROW_RTM_T3_NAME));
		bretWebData.setT4Name(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_RTM,
				RemediationLogPage.ROW_RTM_T4_NAME));
		bretWebData.setT5Name(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_RTM,
				RemediationLogPage.ROW_RTM_T5_NAME));
		bretWebData.setBpmT1Fin(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_BPMARGDET,
				RemediationLogPage.ROW_BPMARGDET_BPM_T1_FIN));
		bretWebData.setBpmT1Serv(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_BPMARGDET,
				RemediationLogPage.ROW_BPMARGDET_BPM_T1_SERV));
		bretWebData.setBpmT1Ins(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_BPMARGDET,
				RemediationLogPage.ROW_BPMARGDET_BPM_T1_INS_FREIGHT));
		bretWebData.setBpmT1Profit(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_BPMARGDET, RemediationLogPage.ROW_BPMARGDET_BPM_T1_PROFIT));
		bretWebData.setBpmT2Fin(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_BPMARGDET,
				RemediationLogPage.ROW_BPMARGDET_BPM_T2_FIN));
		bretWebData.setBpmT2Serv(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_BPMARGDET,
				RemediationLogPage.ROW_BPMARGDET_BPM_T2_SERV));
		bretWebData.setBpmT2Ins(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_BPMARGDET,
				RemediationLogPage.ROW_BPMARGDET_BPM_T2_INS_FREIGHT));
		bretWebData.setBpmT2Profit(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_BPMARGDET, RemediationLogPage.ROW_BPMARGDET_BPM_T2_PROFIT));
		bretWebData.setBpmOther(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_BPMARGDET,
				RemediationLogPage.ROW_BPMARGDET_BPM_OTHER));
		bretWebData.setLog(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_REMEDIATION,
				RemediationLogPage.ROW_REM_LOG));
		bretWebData.setChannelProgram(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_CHANNEL_PROGRAM));
		bretWebData.setClosed(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_REMEDIATION,
				RemediationLogPage.ROW_REM_CLOSED));
		bretWebData.setClosedDate(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_CLOSE_DATE));
		bretWebData.setOutcome(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_REMEDIATION,
				RemediationLogPage.ROW_REM_OUTCOME));
		bretWebData.setCondition(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_CONDITION));
		bretWebData.setTransparencyLetter(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_TRANSPARENCY_LETTER));
		bretWebData.setCycle(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_REMEDIATION,
				RemediationLogPage.ROW_REM_CYCLE));
		bretWebData.setExceptionFlag(remediationLog.getRemediationValueAtIndex(driver,
				RemediationLogPage.SUB_TBL_REMEDIATION, RemediationLogPage.ROW_REM_EXCEPTION_FLAG));
		bretWebData.setFulfilled(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_FOLLOWUP,
				RemediationLogPage.ROW_FOLLOWUP_FULFILLED));
		bretWebData.setComment(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_FOLLOWUP,
				RemediationLogPage.ROW_FOLLOWUP_COMMENT));

		// Bid Expired = Y (If Outcome = Expired), N for other outcome
		String outcome = "From BRET Web";
		String isExpired = "";

		if (outcome.equalsIgnoreCase("EXPIRED")) {
			isExpired = "Y";
		} else {
			isExpired = "N";
		}

		bretWebData.setBidExpired(isExpired);


		bretWebData.setExPreFulfillment(
				remediationLog.getPostShipmentValueAtIndex(driver, RemediationLogPage.ROW_POSTSHIP_EX_PRE_FULFILMENT));
		bretWebData.setPostVerifyDate(
				remediationLog.getPostShipmentValueAtIndex(driver, RemediationLogPage.ROW_POSTSHIP_POST_VERIFY_DATE));
		bretWebData.setPostShipAuditFollowUpStatus(remediationLog.getPostShipmentValueAtIndex(driver,
				RemediationLogPage.ROW_POSTSHIP_POST_SHIP_AUDIT_FOLLOW_UP_STATUS));
		bretWebData.setWaitingForExtension(remediationLog.getPostShipmentValueAtIndex(driver,
				RemediationLogPage.ROW_POSTSHIP_WAITING_FOR_EXTENSION));
		bretWebData.setProblemsWithChasing(remediationLog.getPostShipmentValueAtIndex(driver,
				RemediationLogPage.ROW_POSTSHIP_PROBLEMS_WITH_CHASING));
		bretWebData.setRequestSentTooEarly(remediationLog.getPostShipmentValueAtIndex(driver,
				RemediationLogPage.ROW_POSTSHIP_REQUEST_SENT_TOO_EARLY));
		bretWebData.setRealisticallyDue(
				remediationLog.getPostShipmentValueAtIndex(driver, RemediationLogPage.ROW_POSTSHIP_REALISTICALLY_DUE));
		bretWebData.setTestNoteStartedYet(remediationLog.getPostShipmentValueAtIndex(driver,
				RemediationLogPage.ROW_POSTSHIP_TEST_NOT_STARTED_YET));
		bretWebData.setPotentialIssues(
				remediationLog.getPostShipmentValueAtIndex(driver, RemediationLogPage.ROW_POSTSHIP_POTENTIAL_ISSUES));
		bretWebData.setFollowUpLog(
				remediationLog.getPostShipmentValueAtIndex(driver, RemediationLogPage.ROW_POSTSHIP_FOLLOW_UP_LOG));
		bretWebData.setDeadlineOfRequest(remediationLog.getPostShipmentValueAtIndex(driver,
				RemediationLogPage.ROW_POSTSHIP_DEADLINE_OF_REQUEST));
		bretWebData.setBidEvent(remediationLog.getRemediationValueAtIndex(driver, RemediationLogPage.SUB_TBL_FOLLOWUP,
				RemediationLogPage.ROW_FOLLOWUP_FULFILLED));

		// navigate to Bid Q's Tab
		remediationLog.navigateToTab(driver, DetailPageTabsE.BID_Q);
		BidQPage bidQpage = new BidQPage(driver);

		bretWebData.setConfidentiality(bidQpage.getValueAtIndexRelQuestions(driver, BidQPage.ROW_CONFIDENTIALITY,
				BidQPage.COL_OUTLIER_STATUS_VAL));
		bretWebData.setRouteToMarket(bidQpage.getValueAtIndexRelQuestions(driver, BidQPage.ROW_ROUTE_TO_MARKET,
				BidQPage.COL_OUTLIER_STATUS_VAL));
		bretWebData.setOffshorePaymentTerms(bidQpage.getValueAtIndexRelQuestions(driver,
				BidQPage.ROW_OFFSHORE_PAYMENT_TERMS, BidQPage.COL_OUTLIER_STATUS_VAL));
		bretWebData.setBundledSolutions(bidQpage.getValueAtIndexRelQuestions(driver, BidQPage.ROW_BUNDLED_SOLUTIONS,
				BidQPage.COL_OUTLIER_STATUS_VAL));
		bretWebData.setContingencyFee(bidQpage.getValueAtIndexRelQuestions(driver,
				BidQPage.ROW_CONTINGENCY_FEE_PAYMENTS, BidQPage.COL_OUTLIER_STATUS_VAL));

		return bretWebData;
	}

	private String composeExportedFilepath(String fileName) {
		return BretTestUtils.getPropertyValue(DOWNLOAD_PATH_PROP) + fileName;
	}

}
