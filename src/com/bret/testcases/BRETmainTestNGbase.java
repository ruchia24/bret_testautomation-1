// This is the main class that will holds browser launching, closing  

package com.bret.testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import com.PropertiesFiles.BWPropertiesFiles;

public class BRETmainTestNGbase {

	//public String baseURL= "https://bldbz173021.cloud.dst.ibm.com:9443/systems/bret/SBidRiskController";  //BREt web UAT
	public WebDriver driver= new FirefoxDriver ();

	//--------------------------------------------------------------------------------- 
	// [Updated-Belle-9/11/17]
	// Change data inputs from Excel to Property files
	//----------------------------------------------------------------------------------
	BWPropertiesFiles propURL = new BWPropertiesFiles ();
	String  BWcURL = propURL.propBW_URL();
	     
	String baseURL = BWcURL;			

	
	// opens the browser
	@BeforeMethod (enabled = true)
	public void setUp ()
	{
        driver.get(baseURL);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@AfterMethod
	// closes the browser
	public void closeBrowser ()
	{
		//driver.close();
		//driver.quit();
	}
}
