package com.bret.testcases.search;

import org.testng.annotations.Test;

import com.PropertiesFiles.BWPropertiesFiles;
import com.bret.pages.BRETSearchPage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

//extends to class BRETmainTestNGbase for the FF launching methods

public class Search extends BRETmainTestNGbase {
	
	// Initializations
	// reading from data source
	// Account can be from any user role
	
//	 String xlpath = "/home/bellep/BRET_CICD_in_Jenkins/BRET_FE_Test/Data Inputs/BRET DataSource.xls";  //path server
  	 String xlpath =  "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
//	 String sheet1 ="Credentials"; //Sheet name on excel
//	 String Uname = Excel.getCellValue (xlpath, sheet1, 1,1);   //(path, sheet name, row, column)
//	 String Pword = Excel.getCellValue (xlpath, sheet1, 1,2); 
	
	 // Supply bid details prior to running the code
	 // Flags: Y = focus bids, N = Non focus bids, A = bid in Archived
	 
	 String sheet2 ="Bids for Search"; //Sheet name on excel
	 String focusBidFromExcel = Excel.getCellValue (xlpath, sheet2, 1,1);   //focus bid
	 String focusBidFlagFromExcel = Excel.getCellValue (xlpath, sheet2, 1,2); 	
	 String nonFocusBidFromExcel = Excel.getCellValue (xlpath, sheet2, 2,1);   //non focus bid
	 String nonFocusBidFlagFromExcel = Excel.getCellValue (xlpath, sheet2, 2,2); 	
	 String bidInArchiveFromExcel = Excel.getCellValue (xlpath, sheet2, 3,1);   //Bid in Archive Tab
	 String bidInArchiveFlagFromExcel = Excel.getCellValue (xlpath, sheet2, 3,2); 
	 
	
	//--------------------------------------------------------------------------------- 
	// [Updated-Belle-9/11/17]
	// Change data inputs from Excel to Property files
	//----------------------------------------------------------------------------------
	String userRole = "Any";
	BWPropertiesFiles propRead = new BWPropertiesFiles ();
	String[] bretWebCredentials = propRead.propBW_credentials(userRole);
     
	String userName = bretWebCredentials[0];			
	String userPassword = bretWebCredentials[1];
	
    String focusBid =  focusBidFromExcel.trim();
	String focusBidFlag =  focusBidFlagFromExcel.trim();
	String nonFocusBid =  nonFocusBidFromExcel.trim();
	String nonFocusBidFlag =  nonFocusBidFlagFromExcel.trim();
	String bidInArchive =  bidInArchiveFromExcel.trim();
	String bidInArchiveFlag =  bidInArchiveFlagFromExcel.trim();
	
	@Test
	public void tcFocusBidSearch () throws Exception{
		 //--------------------------- 
		 // SRCH_001: Performs FOCUS bid search
	     //---------------------------
		
		//Performs the Login event
	    // calls the method UserLogin from class BRETValidLogin to perform the site login		
		BRETValidLogin userLogin = new BRETValidLogin(driver);  
		userLogin.userValidLogin(userName, userPassword);
		 
	    // Perform Focus bid search
		BRETSearchPage searchFbid = new BRETSearchPage(driver);
		searchFbid.bidSearchAnyRole(focusBid, focusBidFlag);
		
	    // Screen capture 
		ScreenCapture screenShots = new ScreenCapture(driver);  
		String className = Search.class.getSimpleName();
		String methodName = ("TC_SRCH-001_" + className);
	    screenShots.screenCaptures(methodName);
	    	  
	}
	

	@Test
	public void tcNonFocusBidSearch () throws Exception{
		 //------------------------------ 
		 // SRCH_002: Perform Non FOCUS bid search
	     //------------------------------- 
			 
	    // Perform Non Focus bid search
		BRETSearchPage searchNFbid = new BRETSearchPage(driver);
		searchNFbid.bidSearchAnyRole(nonFocusBid, nonFocusBidFlag);
		
	    // Screen capture 
		ScreenCapture screenShots = new ScreenCapture(driver);  
		String className = Search.class.getSimpleName();
		String methodName = ("TC_SRCH-002_" + className);
	    screenShots.screenCaptures(methodName);
	    	  
	}

	 
	 @Test
	 public void tcSearchBidFromArchive () throws Exception{
		//-------------------------------------- 
		// SRCH_004: Performs  bid search on Archive tab
	    //--------------------------------------
			 
	     //Perform search on archive tab
		 BRETSearchPage searchAbid = new BRETSearchPage(driver);
		 searchAbid.bidSearchAnyRole(bidInArchive, bidInArchiveFlag);

	    // Screen capture 
		 ScreenCapture screenShots = new ScreenCapture(driver);  
		 String className = Search.class.getSimpleName();
		 String methodName = ("TC_SRCH-004_" + className);
	    screenShots.screenCaptures(methodName);
	    	  
	 }

}
