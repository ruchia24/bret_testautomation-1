package com.bret.testcases.search;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.BRETAdvSearchPage;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.MyTaskPage;
import com.bret.util.BretTestUtils;

public class AdvancedSearchBid extends BaseTest {

	private static final String FOCUS_BID = "focusBid";
	private static final String FOCUS_BID_FLAG = "focusBidFlag";
	private static final String NONFOCUS_BID = "nonFocusBid";
	private static final String NONFOCUS_BID_FLAG = "nonFocusBidFlag";
	private static final String ERROR_BID = "errorBid";
	private static final String ERROR_BID_FLAG = "errorBidFlag";
	private static final String VALID_HW_BID = "validHWBid";
	private static final String VALID_HW_BID_SOURCESYSTEM = "validHWBidSourceSystem";
	private static final String VALID_SW_BID = "validSWBid";
	private static final String VALID_SW_BID_SOURCESYSTEM = "validSWBidSourceSystem";
	private static final String NON_EXISTING_BID = "nonExistingBid";
	private static final String NON_EXISTING_BID_DISPLAY_MESSAGE = "displayMessage";
	private static final String INVALID_DATE_DISPLAY_MESSAGE_1 = "invalidDateDisplayMessage1";
	private static final String INVALID_DATE_DISPLAY_MESSAGE_2 = "invalidDateDisplayMessage2";
	private static final String BID_FROM = "bidFrom";
	private static final String BID_TO = "bidTo";
	private static final String EXPIRY_DATE_FROM = "expiryDateFrom";
	private static final String EXPIRY_DATE_TO = "expiryDateTo";
	private static final String CLOSE_DATE_FROM = "closeDateFrom";
	private static final String CLOSE_DATE_TO = "closeDateTo";
	private static final String DEADLINE_OF_REQUEST_FROM = "deadlineOfRequestFrom";
	private static final String DEADLINE_OF_REQUEST_TO = "deadlineOfRequestTo";
	private static final String NO_INPUT_CRITERIA = "noInputCriteria";
	private static final String COUNTRY = "country";
	// private static final String SOURCE_SYSTEM = "sourceSystem";
	// private static final String OUTCOME = "outcome";
	private static final String TIER_1_CEID = "tier1ceid";
	private static final String TIER_2_CEID = "tier2ceid";

	// private static final String ARCHIVED_BID = "archivedBid";
	// private static final String SQO_WAITING_FOR_PRICING_BID =
	// "waitingForPricingBid";
	// private static final String SQO_WAITING_FOR_PRICING_BID_DECISION =
	// "waitingForPricingReviewerDecision";
	// private static final String NOT_YET_LOADED_BID_FLAG =
	// "notYetLoadedBidFlag";
	// private static final String INVALID_BID = "invalidBid";
	// private static final String INVALID_BID_FLAG = "invalidBidFlag";
	// private static final String NO_INPUT_BID_ALERT = "noIputBidAlertMessage";

	private MyTaskPage myTaskPage;
	private BRETAdvSearchPage advancedSearchPage;

	@BeforeTest
	public void login() {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getAdminUsername(), getAdminPassword());
	}

	/**
	 * 1. Go to Advanced Search tab. 2. Enter valid focus bid id. 3. Click Search
	 * button.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 1)
	public void advancedSearchFocus() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		advancedSearchPage = new BRETAdvSearchPage(driver);

		String tcNumber = "TC_ASRCH-009-016";
		String testCase = "ASRCH-009";
		String description = "Focus bids can be searched";

		/* Get the test data from excel */
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		String focusBid = testData.get(FOCUS_BID);
		String focusBidFlag = testData.get(FOCUS_BID_FLAG);

		System.out.println("\n============ [" + testCase + "] " + description + " ============\n");

		myTaskPage.navigateToTab(driver, SummaryPageTabsE.ADVANCED_SEARCH);

		/* Set input field - focus bid */
		advancedSearchPage.setInputField(BRETAdvSearchPage.XPATH_BID_ID, focusBid);
		Thread.sleep(5000);

		/* Set new screenshot name to avoid overwriting of screenshots */
		String focusBidInput = "_FocusBidInput";
		String tcFocusBidInput = testCase + focusBidInput;
		String focusBidInputResult = "_focusBidInputResult";
		String tcfocusBidInputResult = testCase + focusBidInputResult;

		/* Take Screenshot */
		takeScreenshot(tcFocusBidInput, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(2000);

		advancedSearchPage.clickSearch();
		Thread.sleep(5000);

		String resultBidID = advancedSearchPage.getSearchResultValue(driver, BRETAdvSearchPage.BID_ID_COL);
		String resultBidFlag = advancedSearchPage.getSearchResultValue(driver, BRETAdvSearchPage.FLAG_COL);

		/* Take Screenshot */
		takeScreenshot(tcfocusBidInputResult, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(2000);

		/* Click the Search Conditions Button */
		advancedSearchPage.clickSearchCondition();
		Thread.sleep(2000);

		System.out.println("\nFocus Bid from Source File : " + focusBid);
		System.out.println("Focus Bid from Search REsult : " + resultBidID);

		System.out.println("\nFocus Bid flag from Source File : " + focusBidFlag);
		System.out.println("Focus Bid flag from Search REsult : " + resultBidFlag);

		Assert.assertTrue(focusBid.equals(resultBidID),
				"The Bid from the source file does not match with the bid on BRETWEB.");
		Assert.assertTrue(focusBidFlag.equals(resultBidFlag),
				"The Bid flag from the source file does not match with the bid on BRETWEB.");

		/* Clear Input field - Focus Bid */
		advancedSearchPage.clearInputField(BRETAdvSearchPage.XPATH_BID_ID);

		Thread.sleep(5000);

	}

	/**
	 * 1. Click Search Conditions drop down 2. Clear Bid ID Criteria 3. Enter valid
	 * Non-focus Bid id 4. Click Search button.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 2)
	public void advancedSearchNonFocus() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		advancedSearchPage = new BRETAdvSearchPage(driver);

		String tcNumber = "TC_ASRCH-009-016";
		String testCase = "ASRCH-010";
		String description = "Non Focus bids can be searched";

		/* Get the test data from excel */
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		String nonFocusBid = testData.get(NONFOCUS_BID);
		String nonFocusBidFlag = testData.get(NONFOCUS_BID_FLAG);

		System.out.println("\n============ [" + testCase + "] " + description + " ============\n");

		/* Set input field - focus bid */
		advancedSearchPage.setInputField(BRETAdvSearchPage.XPATH_BID_ID, nonFocusBid);
		Thread.sleep(5000);

		/* Set new screenshot name to avoid overwriting of screenshots */
		String nonFocusBidInput = "_NonFocusBidInput";
		String tcNonFocusBidInput = testCase + nonFocusBidInput;
		String nonFocusBidInputResult = "_nonFocusBidInputResult";
		String tcNonFocusBidInputResult = testCase + nonFocusBidInputResult;

		/* Take Screenshot */
		takeScreenshot(tcNonFocusBidInput, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(2000);

		advancedSearchPage.clickSearch();
		Thread.sleep(5000);

		String resultBidID = advancedSearchPage.getSearchResultValue(driver, BRETAdvSearchPage.BID_ID_COL);
		String resultBidFlag = advancedSearchPage.getSearchResultValue(driver, BRETAdvSearchPage.FLAG_COL);

		/* Take Screenshot */
		takeScreenshot(tcNonFocusBidInputResult, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(2000);

		/* Click the Search Conditions Button */
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		advancedSearchPage.clickSearchCondition();
		Thread.sleep(2000);

		System.out.println("\nNon Focus Bid from Source File : " + nonFocusBid);
		System.out.println("Non Focus Bid from Search REsult : " + resultBidID);

		System.out.println("\nNon Focus Bid flag from Source File : " + nonFocusBidFlag);
		System.out.println("Non Focus Bid flag from Search REsult : " + resultBidFlag);

		Assert.assertTrue(nonFocusBid.equals(resultBidID),
				"The Bid from the source file does not match with the bid on BRETWEB.");
		Assert.assertTrue(nonFocusBidFlag.equals(resultBidFlag),
				"The Bid flag from the source file does not match with the bid on BRETWEB.");

		/* Clear Input field - Focus Bid */
		advancedSearchPage.clearInputField(BRETAdvSearchPage.XPATH_BID_ID);

		Thread.sleep(5000);

	}

	/**
	 * 
	 * 1. Click Search Conditions drop down 2. Clear Bid ID Criteria 3. Enter Error
	 * Bid id 4. Click Search button.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 3)
	public void advancedSearchError() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		advancedSearchPage = new BRETAdvSearchPage(driver);

		String tcNumber = "TC_ASRCH-009-016";
		String testCase = "ASRCH-011";
		String description = "Error bids can be searched";

		/* Get the test data from excel */
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		String errorBid = testData.get(ERROR_BID);
		String errorBidFlag = testData.get(ERROR_BID_FLAG);

		System.out.println("\n============ [" + testCase + "] " + description + "============\n");

		/* Set input field - focus bid */
		advancedSearchPage.setInputField(BRETAdvSearchPage.XPATH_BID_ID, errorBid);
		Thread.sleep(5000);

		/* Set new screenshot name to avoid overwriting of screenshots */
		String errorBidInput = "_errorBidInput";
		String tcErrorBidInput = testCase + errorBidInput;
		String errorBidInputResult = "_errorBidInputResult";
		String tcErrorBidInputResult = testCase + errorBidInputResult;

		/* Take Screenshot */
		takeScreenshot(tcErrorBidInput, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(2000);

		advancedSearchPage.clickSearch();
		Thread.sleep(5000);

		String resultBidID = advancedSearchPage.getSearchResultValue(driver, BRETAdvSearchPage.BID_ID_COL);
		String resultBidFlag = advancedSearchPage.getSearchResultValue(driver, BRETAdvSearchPage.FLAG_COL);

		/* Take Screenshot */
		takeScreenshot(tcErrorBidInputResult, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(2000);

		/* Click the Search Conditions Button */
		advancedSearchPage.clickSearchCondition();
		Thread.sleep(2000);

		System.out.println("\nError Bid from Source File : " + errorBid);
		System.out.println("Error Bid from Search REsult : " + resultBidID);

		System.out.println("\nError Bid flag from Source File : " + errorBidFlag);
		System.out.println("Error Bid flag from Search REsult : " + resultBidFlag);

		Assert.assertTrue(errorBid.equals(resultBidID),
				"The Bid from the source file does not match with the bid on BRETWEB.");
		Assert.assertTrue(errorBidFlag.equals(resultBidFlag),
				"The Bid flag from the source file does not match with the bid on BRETWEB.");

		/* Clear Input field - Focus Bid */
		advancedSearchPage.clearInputField(BRETAdvSearchPage.XPATH_BID_ID);

		Thread.sleep(5000);

	}

	/**
	 * 
	 * 1. Click Search Conditions drop down 2. Clear Bid ID Criteria 3. Enter a
	 * valid HW bid id. 4. Click Search button."
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 4)
	public void advancedSearchValidHW() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		advancedSearchPage = new BRETAdvSearchPage(driver);

		String tcNumber = "TC_ASRCH-009-016";
		String testCase = "ASRCH-012";
		String description = "Valid Hardware Bid can be searched.";

		/* Get the test data from excel */
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		String validBid = testData.get(VALID_HW_BID);
		String validBidSourceSystem = testData.get(VALID_HW_BID_SOURCESYSTEM);

		System.out.println("\n============ [" + testCase + "] " + description + " ============\n");

		/* Set input field - focus bid */
		advancedSearchPage.setInputField(BRETAdvSearchPage.XPATH_BID_ID, validBid);
		Thread.sleep(5000);

		/* Set new screenshot name to avoid overwriting of screenshots */
		String validHW = "_validHW";
		String tcValidHW = testCase + validHW;
		String validHWResult = "_validHWResult";
		String tcValidHWResult = testCase + validHWResult;

		/* Take Screenshot */
		takeScreenshot(tcValidHW, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(2000);

		advancedSearchPage.clickSearch();
		Thread.sleep(5000);

		String resultBidID = advancedSearchPage.getSearchResultValue(driver, BRETAdvSearchPage.BID_ID_COL);
		String resultValidBidSourceSystem = advancedSearchPage.getSearchResultValue(driver,
				BRETAdvSearchPage.SOURCE_SYSTEM_COL);

		/* Take Screenshot */
		takeScreenshot(tcValidHWResult, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(2000);

		/* Click the Search Conditions Button */
		advancedSearchPage.clickSearchCondition();
		Thread.sleep(2000);

		System.out.println("\nValid Bid from Source File : " + validBid);
		System.out.println("Valid Bid from Search Result : " + resultBidID);

		System.out.println("\nBid's Source System from Source File : " + validBidSourceSystem);
		System.out.println("Bid's Source System from Search Result : " + resultValidBidSourceSystem);

		Assert.assertTrue(validBid.equals(resultBidID),
				"The Bid from the source file does not match with the bid on BRETWEB.");
		Assert.assertTrue(validBidSourceSystem.equals(resultValidBidSourceSystem),
				"The Bid flag from the source file does not match with the bid on BRETWEB.");

		/* Clear Input field - Focus Bid */
		advancedSearchPage.clearInputField(BRETAdvSearchPage.XPATH_BID_ID);

		Thread.sleep(5000);

	}

	/**
	 * 
	 * 1. Click Search Conditions drop down 2. Clear Bid ID Criteria 3. Enter a
	 * valid SW bid id. 4. Click Search button."
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 5)
	public void advancedSearchValidSW() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		advancedSearchPage = new BRETAdvSearchPage(driver);

		String tcNumber = "TC_ASRCH-009-016";
		String testCase = "ASRCH-013";
		String description = "Valid Software Bid can be searched.";

		/* Get the test data from excel */
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		String validBid = testData.get(VALID_SW_BID);
		String validBidSourceSystem = testData.get(VALID_SW_BID_SOURCESYSTEM);

		System.out.println("\n============ [" + testCase + "] " + description + " ============\n");

		/* Set input field - focus bid */
		advancedSearchPage.setInputField(BRETAdvSearchPage.XPATH_BID_ID, validBid);
		Thread.sleep(5000);

		/* Set new screenshot name to avoid overwriting of screenshots */
		String validSW = "_validSW";
		String tcValidSW = testCase + validSW;
		String validSWResult = "_validSWResult";
		String tcValidSWResult = testCase + validSWResult;

		/* Take Screenshot */
		takeScreenshot(tcValidSW, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(2000);

		advancedSearchPage.clickSearch();
		Thread.sleep(5000);

		String resultBidID = advancedSearchPage.getSearchResultValue(driver, BRETAdvSearchPage.BID_ID_COL);
		String resultValidBidSourceSystem = advancedSearchPage.getSearchResultValue(driver,
				BRETAdvSearchPage.SOURCE_SYSTEM_COL);

		/* Take Screenshot */
		takeScreenshot(tcValidSWResult, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(2000);

		/* Click the Search Conditions Button */
		advancedSearchPage.clickSearchCondition();
		Thread.sleep(2000);

		System.out.println("\nValid Bid from Source File : " + validBid);
		System.out.println("Valid Bid from Search Result : " + resultBidID);

		System.out.println("\nBid's Source System from Source File : " + validBidSourceSystem);
		System.out.println("Bid's Source System from Search Result : " + resultValidBidSourceSystem);

		Assert.assertTrue(validBid.equals(resultBidID),
				"The Bid from the source file does not match with the bid on BRETWEB.");
		Assert.assertTrue(validBidSourceSystem.equals(resultValidBidSourceSystem),
				"The Bid flag from the source file does not match with the bid on BRETWEB.");

		/* Clear Input field - Focus Bid */
		advancedSearchPage.clearInputField(BRETAdvSearchPage.XPATH_BID_ID);

		Thread.sleep(5000);

	}

	/**
	 * 
	 * 1. Click Search Conditions drop down 2. Clear Bid ID Criteria 3.Enter a
	 * non-existing bid id. 4. Click Search button.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 6)
	public void advancedSearchNonExistingBid() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		advancedSearchPage = new BRETAdvSearchPage(driver);

		String tcNumber = "TC_ASRCH-009-016";
		String testCase = "ASRCH-014";
		String description = "Non-existing bid cannot be searched.";

		/* Get the test data from excel */
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		String nonExistingBid = testData.get(NON_EXISTING_BID);
		String nonExistingBidDisplayMessage = testData.get(NON_EXISTING_BID_DISPLAY_MESSAGE);

		System.out.println("\n============ [" + testCase + "] " + description + " ============\n");

		// myTaskPage.navigateToTab(driver, SummaryPageTabsE.ADVANCED_SEARCH);

		/* Set input field - focus bid */
		advancedSearchPage.setInputField(BRETAdvSearchPage.XPATH_BID_ID, nonExistingBid);
		Thread.sleep(5000);

		/* Set new screenshot name to avoid overwriting of screenshots */
		String nonExisting = "_nonExistingBid";
		String tcNonExisting = testCase + nonExisting;
		String nonExistingResult = "_nonExistingResult";
		String tcNonExistingResult = testCase + nonExistingResult;

		/* Take Screenshot */
		takeScreenshot(tcNonExisting, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(2000);

		advancedSearchPage.clickSearch();
		Thread.sleep(5000);

		/*
		 * used .getpagesource to check if the "No items to display" can be found on the
		 * page xpath of "no item to display" webelement cannot be found.
		 */

		String resultNonExistingBidDisplayMessage = "";
		Boolean displayMessage = driver.getPageSource().contains("No items to display");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		if (displayMessage) {
			resultNonExistingBidDisplayMessage = "No items to display";
		} else {
			resultNonExistingBidDisplayMessage = " ";
		}

		/* Take Screenshot */
		takeScreenshot(tcNonExistingResult, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(2000);

		System.out.println("\nNon Existing Bid from Source File : " + nonExistingBid);

		System.out.println("\nExpected Display Message from Source File : " + nonExistingBidDisplayMessage);

		System.out.println("Actual Display Message from Search Result : " + resultNonExistingBidDisplayMessage);

		Assert.assertTrue(nonExistingBidDisplayMessage.equals(resultNonExistingBidDisplayMessage),
				"The display message from the source file does not match with the display message on BRETWEB.");

		/* Click the Search Conditions Button */
		advancedSearchPage.clickSearchCondition();

		/* Clear Input field - Focus Bid */
		advancedSearchPage.clearInputField(BRETAdvSearchPage.XPATH_BID_ID);

		Thread.sleep(5000);

	}

	/**
	 * 1. Click Search Conditions drop down 2. Clear Bid ID Criteria 3 Enter Invalid
	 * dates on Date Criteria. 4. Click Search button.
	 * 
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 7)
	public void advancedSearchInvalidDate() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		advancedSearchPage = new BRETAdvSearchPage(driver);

		String tcNumber = "TC_ASRCH-009-016";
		String testCase = "ASRCH-015";
		String description = "Invalid date entered cannot be searched.";

		/* Get the test data from excel */
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		/* Inputs from excel to test the fields */
		String bidFrom = testData.get(BID_FROM);
		String bidTo = testData.get(BID_TO);

		String expiryDateFrom = testData.get(EXPIRY_DATE_FROM);
		String expiryDateTo = testData.get(EXPIRY_DATE_TO);

		String closeDateFrom = testData.get(CLOSE_DATE_FROM);
		String closeDateTo = testData.get(CLOSE_DATE_TO);

		String deadlineOfRequestFrom = testData.get(DEADLINE_OF_REQUEST_FROM);
		String deadlineOfRequestTo = testData.get(DEADLINE_OF_REQUEST_TO);

		String invalidDateDisplayMessage1 = testData.get(INVALID_DATE_DISPLAY_MESSAGE_1);
		String invalidDateDisplayMessage2 = testData.get(INVALID_DATE_DISPLAY_MESSAGE_2);

		System.out.println("\n============ [" + testCase + "] " + description + " ============\n");

//		/* FOR INDIVIDUAL TEST CASE TESTING PURPOSES, REMOVE LATER. */
		myTaskPage.navigateToTab(driver, SummaryPageTabsE.ADVANCED_SEARCH);

		/* Set input field - BID FROM AND BID TO */
		String bidFromTo = "_BidFromTo";
		String tcBidFromTo = testCase + bidFromTo;
		String bidFromToResult = "_BidFromToResult";
		String tcBidFromToResult = testCase + bidFromToResult;

		Thread.sleep(2000);
		advancedSearchPage.setInputField(BRETAdvSearchPage.XPATH_BID_FROM, bidFrom);
		Thread.sleep(2000);
		advancedSearchPage.setInputField(BRETAdvSearchPage.XPATH_BID_TO, bidTo);
		Thread.sleep(2000);

		/* Take Screenshot */
		takeScreenshot(tcBidFromTo, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(2000);

		advancedSearchPage.clickSearch();
		Thread.sleep(5000);

		String resultBidFromToDisplayMessage = advancedSearchPage
				.getValueAt(BRETAdvSearchPage.XPATH_DISPLAY_MESSAGE_NO_ITEM);

		/* Take Screenshot */
		takeScreenshot(tcBidFromToResult, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(2000);

		System.out.println("\nBid Date from on Source File : " + bidFrom);
		System.out.println("Bid Date to on Source File   : " + bidTo);
		System.out.println("Expected Display Message from Source File : " + invalidDateDisplayMessage1);
		System.out.println("Actual Display Message from Source File   : " + resultBidFromToDisplayMessage + "\n");

		Assert.assertEquals(resultBidFromToDisplayMessage, invalidDateDisplayMessage1);

		/* Click Search Condition and clear the previous inputs */
		advancedSearchPage.clickSearchCondition();
		Thread.sleep(2000);
		advancedSearchPage.clearInputField(BRETAdvSearchPage.XPATH_BID_FROM);
		advancedSearchPage.clearInputField(BRETAdvSearchPage.XPATH_BID_TO);
		Thread.sleep(3000);

		/* Set input field - EXPIRE DATE FROM AND EXPIRE DATE TO */
		String expireFromTo = "_ExpireDateFromTo";
		String tcExpireFromTo = testCase + expireFromTo;
		String expireFromToResult = "_ExpireDateFromToResult";
		String tcExpireFromToResult = testCase + expireFromToResult;

		advancedSearchPage.setInputField(BRETAdvSearchPage.XPATH_BID_EXPIRY_DATE_FROM, expiryDateFrom);
		Thread.sleep(2000);
		advancedSearchPage.setInputField(BRETAdvSearchPage.XPATH_BID_EXPIRY_DATE_TO, expiryDateTo);
		Thread.sleep(2000);

		/* Take Screenshot */
		takeScreenshot(tcExpireFromTo, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(2000);

		advancedSearchPage.clickSearch();
		Thread.sleep(5000);

		String resultExpiryDateFromToDisplayMessage = advancedSearchPage
				.getValueAt(BRETAdvSearchPage.XPATH_DISPLAY_MESSAGE_NO_ITEM);

		/* Take Screenshot */
		takeScreenshot(tcExpireFromToResult, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(2000);

		System.out.println("\nExpiry Date from on Source File : " + expiryDateFrom);
		System.out.println("Expiry Date to on Source File   : " + expiryDateTo);
		System.out.println("Expected Display Message from Source File : " + invalidDateDisplayMessage1);
		System.out
				.println("Actual Display Message from Source File   : " + resultExpiryDateFromToDisplayMessage + "\n");

		Assert.assertEquals(resultExpiryDateFromToDisplayMessage, invalidDateDisplayMessage1);

		/* Click Search Condition and clear the previous inputs */
		advancedSearchPage.clickSearchCondition();
		Thread.sleep(2000);
		advancedSearchPage.clearInputField(BRETAdvSearchPage.XPATH_BID_EXPIRY_DATE_FROM);
		advancedSearchPage.clearInputField(BRETAdvSearchPage.XPATH_BID_EXPIRY_DATE_TO);
		Thread.sleep(3000);

		/*
		 * Set input field - DEADLINE OF REQUEST DATE FROM AND DEADLINE OF REQUEST DATE
		 * TO
		 */
		String deadlineFromTo = "_DeadlineFromTo";
		String tcDeadlineFromTo = testCase + deadlineFromTo;
		String deadlineFromToResult = "_DeadlineFromToResult";
		String tcDeadlineFromToResult = testCase + deadlineFromToResult;

		advancedSearchPage.setInputField(BRETAdvSearchPage.XPATH_DEADLINE_OF_REQUEST_FROM, deadlineOfRequestFrom);
		Thread.sleep(2000);
		advancedSearchPage.setInputField(BRETAdvSearchPage.XPATH_DEADLINE_OF_REQUEST_TO, deadlineOfRequestTo);
		Thread.sleep(2000);

		/* Take Screenshot */
		takeScreenshot(tcDeadlineFromTo, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(2000);

		advancedSearchPage.clickSearch();
		Thread.sleep(5000);

		String resultDeadlineFromToDisplayMessage = advancedSearchPage
				.getValueAt(BRETAdvSearchPage.XPATH_DISPLAY_MESSAGE_NO_ITEM);

		/* Take Screenshot */
		takeScreenshot(tcDeadlineFromToResult, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(2000);

		System.out.println("\nDeadline of Request Date from on Source File : " + expiryDateFrom);
		System.out.println("Deadline of Request Date to on Source File   : " + expiryDateTo);
		System.out.println("Expected Display Message from Source File : " + invalidDateDisplayMessage1);
		System.out.println("Actual Display Message from Source File   : " + resultDeadlineFromToDisplayMessage + "\n");

		Assert.assertEquals(resultDeadlineFromToDisplayMessage, invalidDateDisplayMessage1);

		/* Click Search Condition and clear the previous inputs */
		advancedSearchPage.clickSearchCondition();
		Thread.sleep(2000);
		advancedSearchPage.clearInputField(BRETAdvSearchPage.XPATH_DEADLINE_OF_REQUEST_FROM);
		advancedSearchPage.clearInputField(BRETAdvSearchPage.XPATH_DEADLINE_OF_REQUEST_TO);
		Thread.sleep(3000);

		/* Set input field - CLOSE DATE FROM AND CLOSE DATE TO */
		/* Close Date From */
		String closeFromResult = "_CloseDateFromResult";
		String tcCloseDateFrom = testCase + closeFromResult;

		advancedSearchPage.setInputField(BRETAdvSearchPage.XPATH_CLOSE_DATE_FROM, closeDateFrom);
		Thread.sleep(3000);

		String resultCloseDateFromDisplayMessage = "";
		Boolean displayMessageOnCloseDateFrom = driver.getPageSource().contains("The value entered is not valid.");

		if (displayMessageOnCloseDateFrom) {
			resultCloseDateFromDisplayMessage = "The value entered is not valid.";
		} else {
			resultCloseDateFromDisplayMessage = " ";
		}

		/* Take Screenshot */
		takeScreenshot(tcCloseDateFrom, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(2000);

		System.out.println("\nClose Date from on Source File : " + expiryDateFrom);
		System.out.println("Expected Display Message from Source File : " + invalidDateDisplayMessage2);
		System.out.println("Actual Display Message from Source File   : " + resultCloseDateFromDisplayMessage + "\n");

		Assert.assertEquals(resultCloseDateFromDisplayMessage, invalidDateDisplayMessage2);

		/* Clear the Close Date From input */
		advancedSearchPage.clearInputField(BRETAdvSearchPage.XPATH_CLOSE_DATE_FROM);
		Thread.sleep(3000);

		/* Close Date To */
		String closeToResult = "_CloseDateToResult";
		String tcCloseDateTo = testCase + closeToResult;

		advancedSearchPage.setInputField(BRETAdvSearchPage.XPATH_CLOSE_DATE_TO, closeDateTo);
		Thread.sleep(3000);

		String resultCloseDateToDisplayMessage = "";
		Boolean displayMessageOnCloseDateTo = driver.getPageSource().contains("The value entered is not valid.");

		if (displayMessageOnCloseDateTo) {
			resultCloseDateToDisplayMessage = "The value entered is not valid.";
		} else {
			resultCloseDateToDisplayMessage = " ";
		}

		/* Take Screenshot */
		takeScreenshot(tcCloseDateTo, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(3000);

		System.out.println("\nClose Date from on Source File : " + expiryDateFrom);
		System.out.println("Expected Display Message from Source File : " + invalidDateDisplayMessage2);
		System.out.println("Actual Display Message from Source File   : " + resultCloseDateToDisplayMessage + "\n");

		Assert.assertEquals(resultCloseDateToDisplayMessage, invalidDateDisplayMessage2);

		/* Clear the Close Date To input */
		advancedSearchPage.clearInputField(BRETAdvSearchPage.XPATH_CLOSE_DATE_TO);
		Thread.sleep(3000);

	}

	/**
	 * 
	 * 1. Click Search Conditions drop down 2. Clear Bid ID Criteria 3. Click search
	 * button without entering any search criteria. 4. Click Ok 5. Enter one valid
	 * search criteria. 6. Click search button. 7. Click Search Conditions drop down
	 * 8. Enter multiple valid search criteria. 9. Click search button.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 8)
	public void advancedSearchSingleMultipleCriteria() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		advancedSearchPage = new BRETAdvSearchPage(driver);

		String tcNumber = "TC_ASRCH-009-016";
		String testCase = "ASRCH-016";
		String description = " Empty, single and Multiple Criteria Search Result.";

		/* Get the test data from excel */
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		String noInputCriteria = testData.get(NO_INPUT_CRITERIA);
		String country = testData.get(COUNTRY);
		String tier1ceid = testData.get(TIER_1_CEID);
		String tier2ceid = testData.get(TIER_2_CEID);
		// String outcome = testData.get(OUTCOME);
		// String sourceSystem = testData.get(SOURCE_SYSTEM);

		System.out.println("\n============ [" + testCase + "] " + description + " ============\n");

		/* Click and Clear Bid Id field */
		advancedSearchPage.clearInputField(BRETAdvSearchPage.XPATH_BID_ID);

		/* A.) Click Search without Input Criteria */
		System.out.println("\n A.) No Iput Criteria");

		String withoutIputCriteria = "_WithoutIputCriteria";
		String tcWithoutIputCriteria = testCase + withoutIputCriteria;

		/* Take Screenshot */
		takeScreenshot(tcWithoutIputCriteria, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(3000);

		advancedSearchPage.clickSearch();

		/* Switching to Alert */
		Alert alertNoInput = driver.switchTo().alert();

		/* Capturing alert message */
		String alertMessage = driver.switchTo().alert().getText();
		Thread.sleep(3000);

		/* Displaying alert message */
		System.out.println("\nBid's expected alert message is : " + noInputCriteria);
		System.out.println("Bid's actual alert message is   : " + alertMessage);

		/**
		 * TODO: Add screen capture for ALERT
		 * 
		 */
		/* Accepting alert - Clicking OK */
		alertNoInput.accept();
		Thread.sleep(5000);

		/* B.) Enter One Input Criteria and Click Search - Country */
		System.out.println("\n B.) Single Criteria");
		String oneIputCriteria = "_OneIputCriteria";
		String tcOneIputCriteria = testCase + oneIputCriteria;
		String oneIputCriteriaResult = "_OneIputCriteriaResult";
		String tcOneIputCriteriaResult = testCase + oneIputCriteriaResult;

		Thread.sleep(5000);
		advancedSearchPage.setInputField(BRETAdvSearchPage.XPATH_COUNTRY, country);

		/* Take Screenshot */
		takeScreenshot(tcOneIputCriteria, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(3000);

		advancedSearchPage.clickSearch();
		Thread.sleep(15000);

		/* Switching to Alert */
		Alert alertOneCriteria = driver.switchTo().alert();
//		 WebDriverWait wait = new WebDriverWait(driver, 20);
//		 wait.wa
		
		
		
		boolean alertForOneCriteriaIsPresent = advancedSearchPage.isAlertPresent();

		if (alertForOneCriteriaIsPresent) {
			alertOneCriteria.accept();
			System.out.println("\nInput Criteria entered was " + COUNTRY + "with value: " + country);
		} else {
			System.out.println(".");
		}
		Thread.sleep(5000);
		/* Take Screenshot */
		takeScreenshot(tcOneIputCriteriaResult, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(2000);

		advancedSearchPage.clickSearchCondition();
		Thread.sleep(5000);

		/* Clear Input Field */
		advancedSearchPage.clearInputField(BRETAdvSearchPage.XPATH_COUNTRY);

		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

		/*
		 * C.) Enter Multiple Input Criteria and Click Search - CEID1, CEID2, Source
		 * System, Outcome, Country
		 */
		System.out.println("\n C.) Multiple Criteria");
		String multipleIputCriteria = "_MultipleIputCriteria";
		String tcMultipleIputCriteria = testCase + multipleIputCriteria;
		String multipleIputCriteriaResult = "_OneIputCriteriaResult";
		String tcMultipleIputCriteriaResult = testCase + multipleIputCriteriaResult;

		Thread.sleep(5000);
		/* Set input values */
		advancedSearchPage.setInputField(BRETAdvSearchPage.XPATH_COUNTRY, country);
		Thread.sleep(3000);
		advancedSearchPage.setInputField(BRETAdvSearchPage.XPATH_TIER_1_CEID, tier1ceid);
		Thread.sleep(3000);
		advancedSearchPage.setInputField(BRETAdvSearchPage.XPATH_TIER_2_CEID, tier2ceid);
		Thread.sleep(3000);
		// advancedSearchPage.setInputField(BRETAdvSearchPage.XPATH_OUTCOME,
		// outcome);
		// Thread.sleep(3000);
		// advancedSearchPage.setInputField(BRETAdvSearchPage.XPATH_SOURCE_SYSTEM,
		// sourceSystem);
		// Thread.sleep(3000);

		/* Take Screenshot */
		takeScreenshot(tcMultipleIputCriteria, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(2000);

		advancedSearchPage.clickSearch();
		Thread.sleep(2000);

		/* Switching to Alert */

		boolean alertForMultipleCriteriaIsPresent = advancedSearchPage.isAlertPresent();

		if (alertForMultipleCriteriaIsPresent) {
			Alert alertMultipleCriteria = driver.switchTo().alert();
			alertMultipleCriteria.accept();
		} else {
		}

		Thread.sleep(5000);

		System.out.println(
				"\nMultiple Input Criteria entered were: " + "\n" + country + "\n" + tier1ceid + "\n" + tier2ceid);

		Thread.sleep(5000);

		/* Take Screenshot */
		takeScreenshot(tcMultipleIputCriteriaResult, SummaryPageTabsE.ADVANCED_SEARCH.getLabel());
		Thread.sleep(2000);

		advancedSearchPage.clickSearchCondition();
		Thread.sleep(5000);

		/* Clear Input Field */
		advancedSearchPage.clearInputField(BRETAdvSearchPage.XPATH_COUNTRY);
		advancedSearchPage.clearInputField(BRETAdvSearchPage.XPATH_TIER_1_CEID);
		advancedSearchPage.clearInputField(BRETAdvSearchPage.XPATH_TIER_2_CEID);

		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		System.out.println("\n\n");
	}

	/**
	 * Method to take screenshot.
	 * 
	 * @param testCase
	 * @param tabLabel
	 */
	private void takeScreenshot(String testCase, String tabLabel) {
		BretTestUtils.screenCapture(driver, getScreenshotPath(),
				BretTestUtils.getImgFilename(testCase + "_" + tabLabel, this.getClass()));
	}

}
