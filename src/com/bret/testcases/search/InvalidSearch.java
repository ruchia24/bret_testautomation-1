package com.bret.testcases.search;

import org.testng.annotations.Test;

import com.PropertiesFiles.BWPropertiesFiles;
import com.bret.pages.BRETSearchPage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

public class InvalidSearch extends BRETmainTestNGbase {

	// Initializations
	// reading from data source
 
	
//	 String xlpath = "/home/bellep/BRET_CICD_in_Jenkins/BRET_FE_Test/Data Inputs/BRET DataSource.xls";  //path server
	 String xlpath =  "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
//	 String sheet1 ="Credentials"; //Sheet name on excel
//	 String Uname = Excel.getCellValue (xlpath, sheet1, 1, 1);   //(path, sheet name, row, column)
//	 String Pword = Excel.getCellValue (xlpath, sheet1, 1, 2); 
	
	 // Supply bid details prior to running the code
	 
	 String sheet2 ="Bids for Search"; //Sheet name on excel
//	 String notApplicableBidFromExel = Excel.getCellValue (xlpath, sheet2, 6,1);   //bid in landing table only
	 String invalidBidFromExcel = Excel.getCellValue (xlpath, sheet2, 6,1);   //invalid Bid search

	//--------------------------------------------------------------------------------- 
	// [Updated-Belle-9/11/17]
	// Change data inputs from Excel to Property files
	//----------------------------------------------------------------------------------
	String userRole = "Any";
	BWPropertiesFiles propRead = new BWPropertiesFiles ();
	String[] bretWebCredentials = propRead.propBW_credentials(userRole);
	     
	String userName = bretWebCredentials[0];			
	String userPassword = bretWebCredentials[1];
	
	String invalidBid = invalidBidFromExcel.trim();
	
	@Test
	private void tcNonExistingBidSearch() throws Exception{
		 //----------------------------------------------------------------------  
		 // SRCH_006 and SRCH_007: Performs non existing/invalid bid search
		 // [Updated-Belle-9/4/17]
		 // Merge SRCH_006 and SRCH_007		
	     //---------------------------------------------------------------------- 
		
		//Performs the Login event
		// calls the method UserLogin from class BRETValidLogin to perform the site login		
		BRETValidLogin userLogin = new BRETValidLogin(driver);  
		userLogin.userValidLogin(userName, userPassword);
			 
		// Perform "does not exist" in BRET web
		BRETSearchPage searchNAbid = new BRETSearchPage(driver);
		searchNAbid.bidInvalidSearch(invalidBid);
					
	    // Screen capture 
		ScreenCapture screenShots = new ScreenCapture(driver);  
		String className = Search.class.getSimpleName();
		String methodName = ("TC_SRCH-006and7_" + className);
		screenShots.screenCaptures(methodName);
		    	  
		}
	
//	@Test
//	private void tcInvalidBidSearch () throws Exception{
//		 //------------------------------- 
//		 // SRCH_007: Performs invalid bid search
//	     //------------------------------- 
//				
//	    // Perform Invalid bid search
//		BRETSearchPage searchNAbid = new BRETSearchPage(driver);
//		searchNAbid.bidInvalidSearch(invalidBid);
//					
//	    // Screen capture 
//		ScreenCapture screenShots = new ScreenCapture(driver);  
//		String className = Search.class.getSimpleName();
//		String methodName = ("TC_SRCH-007_" + className);
//		screenShots.screenCaptures(methodName);
//		    	  
//		}
	
 @Test
	private void tcSearchWithNoBidProvided () throws Exception {
	    //------------------------------------------ 
        // SRCH_008: Performs bid search with no bid provided
        //------------------------------------------ 
	  
	    String className = Search.class.getSimpleName();
		String methodName = ("TC_SRCH-008_" + className);
		
	    // Perform Invalid bid search
		BRETSearchPage searchNAbid = new BRETSearchPage(driver);
		searchNAbid.noBidID();
		
		// Screen capture 
		ScreenCapture screenShots = new ScreenCapture(driver);  
		screenShots.screenCaptures(methodName);	   		    	  
		}
}
