package com.bret.testcases.sw.remediation;

import java.util.Map;

import org.junit.Assert;
import org.testng.annotations.Test;

import com.bret.base.BaseRemediationTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.ArchivePage;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.FocusPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.NonFocusPage;
import com.bret.pages.RemediationLogPage;
import com.bret.util.BretTestUtils;

/**
 * Test Case execution for Software Focus Remediation.
 * 
 * @author CheyenneFreyLazo
 *
 */
public class SoftwareFocusRemediationTest extends BaseRemediationTest {
	
	private static final String PRESHIP_VERIFICATION = "preshipVerificationHold";

	/**
	 * Test case: RSW-003
	 * 
	 * Steps:
	 * 
	 * 1. Login to BRET Web as Admin
	 * 
	 * 2. Go to My Task Tab
	 * 
	 * 3. Locate the new SW Focus bid (Waiting for Pricing)
	 * 
	 * 4. Select the SW Focus bid
	 * 
	 * 5. Click READY FOR REVIEW Button
	 * 
	 * 
	 */
	@Test(priority=1)
	public void testFocusWaitingForPricingToReadyForReview() {
		String tcNumber = "TC_RSW-003";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String waitingforPricingBidId = testData.get(BID_ID);

		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getAdminUsername(), getAdminPassword());

		/* Doing logging here */
		displayLogHeader(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());
		printToConsole("Verify that Administrator is able to set these bids for READY FOR REVIEW");

		MyTaskPage myTaskPage = new MyTaskPage();
		myTaskPage.closeNPSSurvey(driver);

		int waitingBidIndex = myTaskPage.searchRowIndex(driver, waitingforPricingBidId);
		myTaskPage.clickCheckbox(waitingBidIndex);

		printToConsole("Checking if the Waiting For Pricing Bid exists: " + waitingforPricingBidId);
		checkAndLogBidIfExist(waitingBidIndex, "Waiting For Pricing Bid is found");

		boolean isReadyForReviewBtnVisible = myTaskPage.checkReadyForReviewButtonVisibility(driver);
		logExpectedActualText("Ready for Review Button is visible: ", Boolean.TRUE.toString(),
				Boolean.toString(isReadyForReviewBtnVisible));
		Assert.assertTrue(isReadyForReviewBtnVisible);

		/* Click the release button */
		if (isReadyForReviewBtnVisible) {
			myTaskPage.clickReadyForReviewButton(driver);
			printToConsole("Ready for Review Button is clicked");
		} else {
			printToConsole("Ready for Review Cannot be clicked. Button is not visible.");
		}

		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel(), this.getClass());
		pausePage();
		logOutSummaryPage(driver);
		pausePage();
	}

	/**
	 * Test case: RSW-004
	 * 
	 * Steps:
	 * 
	 * 1. Login to BRET Web as Reviewer
	 * 
	 * 2. Go to My Task/Focus tabs
	 * 
	 * 
	 */
	@Test(priority=2)
	public void testFocusWaitingForPricingToNew() {
		String tcNumber = "TC_RSW-004";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String newBidId = testData.get(BID_ID);

		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getReviewerUsername(), getReviewerPassword());

		/* Doing logging here */
		displayLogHeader(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());
		printToConsole("Verify that NEW bids are made available on My Task and Focus Tab");

		MyTaskPage myTaskPage = new MyTaskPage();
		myTaskPage.closeNPSSurvey(driver);

		int newBidIndex = myTaskPage.searchRowIndex(driver, newBidId);
		myTaskPage.clickCheckbox(newBidIndex);

		printToConsole("Checking if the New Bid exists: " + newBidId);
		checkAndLogBidIfExist(newBidIndex, "New Bid is found");

		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel(), this.getClass());

		myTaskPage.navigateToTab(driver, SummaryPageTabsE.FOCUS_BIDS);

		FocusPage focusPage = new FocusPage();

		printToConsole("Checking if the New Bid exists: " + newBidId);
		int newFocusBidIndex = focusPage.searchRowIndex(driver, newBidId);
		checkAndLogBidIfExist(newFocusBidIndex, "New Bid is found");
		focusPage.clickCheckbox(newFocusBidIndex);

		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel(), this.getClass());
		pausePage();
		logOutSummaryPage(driver);
		pausePage();
	}

	/**
	 * 
	 * Test case: RSW-005
	 * 
	 * Steps:
	 * 
	 * 1. Login to BRET Web as Reviewer
	 * 
	 * 2. Go to Focus tab
	 * 
	 * 3. Click the Bid Detail of the newly loaded Focus bid
	 * 
	 * 4. Click Take This Bid button
	 * 
	 * 5. Refresh the page
	 * 
	 * 6. Go to Remediation Log
	 */
	@Test(priority=3)
	public void testFocusTakeOnNew() {
		String tcNumber = "TC_RSW-005";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String newlyLoadedBidId = testData.get(BID_ID);
		takeOnNewFocusBid(tcNumber, newlyLoadedBidId, testData.get(OUTCOME));
	}
	
	/**
	 * Test case: RSW-006
	 * 
	 * Steps:
	 * 
	 * 1. Go to Focus Tab
	 * 
	 * 2. Locate the Assigned SW focus bid
	 * 
	 */
	@Test(priority=4)
	public void testFocusAssignedBidDisplay() {
		String tcNumber = "TC_RSW-006";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String assignedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole("Verify that Assigned bid have the correct Bid Date, Reviewer Decision and Reviewer Name.");

		checkFocusBidDateAndReviewerFields(assignedBidId, testData.get(BID_DATE), testData.get(REVIEWER_DECISION),
				testData.get(REVIEWER_NAME), tcNumber);
		
		pausePage();
	}

	/**
	 *
	 * Test case: RSW-007
	 * 
	 * Steps:
	 * 
	 * 1. Go to My Task Tab/Focus Tab
	 * 
	 * 2. Locate the Assigned SW focus bid
	 * 
	 * 3. Go to Remediation log and perform: Remediation Outcome = Return Comments =
	 * use default/add/edit comments
	 * 
	 * 4. Save changes
	 * 
	 */
	@Test(priority=5)
	public void testFocusAssignedToReturn() {
		String tcNumber = "TC_RSW-007";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String assignedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole(
				"Verify that Reviewer be able to set the remediation details to 'Return' 'Outcome' and 'Outcome Comment' fields");
		pausePage();
		searchFocusAssignedBidAndRemediate(assignedBidId, tcNumber, RemediationLogPage.REM_OUTCOME_RETURN,
				testData.get(OUTCOME_COMMENT));
		pausePage();
	}
	
	/**
	 * Test case: RSW-008
	 * 
	 * Steps:
	 * 
	 * 1. Go to My Task Tab/Focus Tab
	 * 
	 * 2. Locate the Returned SW focus bid
	 * 
	 */
	@Test(priority=6)
	public void testFocusReturnedBidDisplay() {
		String tcNumber = "TC_RSW-008";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String returnedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole(
				"Verify that the 'Returned' bid have the correct 'Bid Date', 'Reviewer Decision' and 'Reviewer Name'.");
		pausePage();
		checkFocusBidDateAndReviewerFields(returnedBidId, testData.get(BID_DATE), testData.get(REVIEWER_DECISION),
				testData.get(REVIEWER_NAME), tcNumber);
		pausePage();
	}

	/**
	 * 
	 * Test case: RSW-009
	 * 
	 * 1. Go to My Task Tab/Focus Tab
	 * 
	 * 2. Locate the Assigned SW focus bid
	 * 
	 * 3. Go to Remediation log and perform: Remediation Outcome = Reject Comments =
	 * use default/add/edit comments
	 * 
	 * 4. Save changes
	 * 
	 */
	@Test(priority=7)
	public void testFocusAssignedToReject() {
		String tcNumber = "TC_RSW-009";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String assignedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole(
				"Verify that Reviewer be able to set the remediation details to 'Reject' 'Outcome' and 'Outcome Comment' fields");
		
		pausePage();
		searchFocusAssignedBidAndRemediate(assignedBidId, tcNumber, RemediationLogPage.REM_OUTCOME_REJECT,
				testData.get(OUTCOME_COMMENT));
		pausePage();
	}
	
	/**
	 * 
	 * Test case: RSW-010
	 * 
	 * Steps:
	 * 
	 * 1. Go to My Task Tab/Focus Tab
	 * 
	 * 2. Locate the Rejected SW focus bid
	 */
	@Test(priority=8)
	public void testFocusRejectBidDisplay() {
		String tcNumber = "TC_RSW-010";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String rejectedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole(
				"Verify that the 'Reject' bid have the correct 'Bid Date', 'Reviewer Decision' and 'Reviewer Name'.");

		pausePage();
		checkFocusBidDateAndReviewerFields(rejectedBidId, testData.get(BID_DATE), testData.get(REVIEWER_DECISION),
				testData.get(REVIEWER_NAME), tcNumber);
		pausePage();
	}
	
	/**
	 * 
	 * Test case: RSW-010
	 * 
	 * Steps:
	 * 
	 * 1. Go to My Task Tab/Focus Tab
	 * 
	 * 2. Locate the Assigned SW focus bid
	 * 
	 * 3. Go to Remediation log and perform: Remediation Outcome = Release Comments
	 * = use default/add/edit comments Pre-Ship Verification Hold = NONE
	 * 
	 * 4. Save changes
	 * 
	 */
	@Test(priority=9)
	public void testFocusReleaseWithoutSQOHold() {
		String tcNumber = "TC_RSW-011";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String assignedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole(
				"Verify that Reviewer is able to set the remediation details of  'Outcome' => 'Release', 'Outcome Comment' and 'Preshipment Verfication Hold' fields");

		searchFocusAssignedBidAndRemediate(assignedBidId, tcNumber, RemediationLogPage.REM_OUTCOME_RELEASE,
				testData.get(OUTCOME_COMMENT), testData.get(PRESHIP_VERIFICATION));
		pausePage();
	}
	
	/**
	 * 
	 * Test case: RSW-012
	 * 
	 * Steps:
	 * 
	 * 1. Go to My Task Tab/Focus Tab
	 * 
	 * 2. Locate the Released SW focus bid
	 */
	@Test(priority=10)
	public void testFocusReleaseBidDisplayWithoutSQOHold() {
		String tcNumber = "TC_RSW-012";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String releasedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole(
				"Verify that the 'Release' bid have the correct 'Bid Date', 'Reviewer Decision' and 'Reviewer Name'.");
		pausePage(); 

		checkFocusBidDateAndReviewerFields(releasedBidId, testData.get(BID_DATE), testData.get(REVIEWER_DECISION),
				testData.get(REVIEWER_NAME), tcNumber);
		
		pausePage();
	}
	
	/**
	 * 
	 * Test case: RSW-013
	 * 
	 * Steps: 1. Go to My Task Tab/Focus Tab
	 * 
	 * 2. Locate the Assigned SW focus bid
	 * 
	 * 3. Go to Remediation log and perform: Remediation Outcome = Release Comments
	 * = use default/add/edit comments Pre-Ship Verification Hold = SQO_HOLD
	 *
	 * 4. Save changes
	 * 
	 */
	@Test(priority=11)
	public void testFocusReleaseWithSQOHold() {
		String tcNumber = "TC_RSW-013";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String assignedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole(
				"Verify that Reviewer is able to set the remediation details of  'Outcome' => 'Release', 'Outcome Comment' and 'Preshipment Verfication Hold' fields");

		searchFocusAssignedBidAndRemediate(assignedBidId, tcNumber, RemediationLogPage.REM_OUTCOME_RELEASE,
				testData.get(OUTCOME_COMMENT), testData.get(PRESHIP_VERIFICATION));
		pausePage();
	}
	
	/**
	 * 
	 * Test case: RSW-014
	 * 
	 * Steps:
	 * 
	 * 1. Go to My Task Tab/Focus Tab
	 * 
	 * 2. Locate the Released SW focus bid
	 */
	@Test(priority=12)
	public void testFocusReleaseBidDisplayWithSQOHold() {
		String tcNumber = "TC_RSW-014";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());
		String releasedBidId = testData.get(BID_ID);

		/* Doing logging here */
		displayLogHeader(tcNumber);
		printToConsole(
				"Verify that the 'Release' bid have the correct 'Bid Date', 'Reviewer Decision' and 'Reviewer Name'.");
		pausePage(); 

		checkFocusBidDateAndReviewerFields(releasedBidId, testData.get(BID_DATE), testData.get(REVIEWER_DECISION),
				testData.get(REVIEWER_NAME), tcNumber);
		
		pausePage();
	}
	
	/**
	 * Test Case: RSW-015
	 * 
	 * Steps:
	 * 
	 * 1. Run SQO Schedule Load
	 * 
	 * 2. Login to BRET Web as Reviewer/Admin
	 * 
	 * 3. Go to Archive Tab
	 * 
	 * 4. Locate the Remediated SW Focus bid/s
	 * 
	 */
	@Test(priority=13)
	public void testFocusReleasedArchive() {

		/* Get the test data from excel */
		String tcNumber = "TC_RSW-015";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		String remediatedArchivedBid = testData.get(BID_ID);
		adminCheckArchivedBidDisplay(tcNumber, remediatedArchivedBid, testData.get(RELEASED_BY),
				testData.get(RELEASED_DATE));
		pausePage();
	}
	
	/**
	 * 
	 * Test case: RSW-016
	 * 
	 * Steps:
	 * 
	 * 1. Click on Bid detail of the bid
	 * 
	 * 2. Click ROLLBACK button
	 * 
	 * 3. Go to Focus Tab and Search for the Rollback Bid
	 * 
	 */
	@Test(priority=14)
	public void testFocusRollBackArchivedBid() {
		/* Get the test data from excel */
		String tcNumber = "TC_RSW-016";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getLeadReviewerUsername(), getLeadReviewerPassword());

		/*
		 * The landing page upon login is the My Task Tab page - so no need to navigate
		 * MyTaskPage represents the My Task tab page object
		 */
		MyTaskPage myTaskPage = new MyTaskPage();

		/* This closes the NPS Survey */
		myTaskPage.closeNPSSurvey(driver);

		/* Doing logging here */
		logNavigation(true, SummaryPageTabsE.MY_TASK.getLabel());

		/* From MyTask page lets navigate to Archive page */
		myTaskPage.navigateToTab(driver, SummaryPageTabsE.ARCHIVE);

		/* Instantiate archive page */
		ArchivePage archivePage = new ArchivePage();

		/* Doing logging here */
		displayLogHeader(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());

		/* Now lets search for the newly archived bid */
		int archiveBidRowIndex = archivePage.searchRowIndex(driver, testData.get(BID_ID));
		boolean releasedBidIsFound = archiveBidRowIndex > ROW_NOT_FOUND;
		logExpectedActualText("Released bid is displayed: ", Boolean.TRUE.toString(),
				Boolean.toString(releasedBidIsFound));

		archivePage.clickCheckbox(archiveBidRowIndex);

		printToConsole("Clicking the detail link", "Opening Detail page",
				"Current detail tab is: " + DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		/* Doing logging here */
		displayLogHeader(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		printToConsole("Verify that Released bid is displayed on Archived Tab");

		archivePage.clickDetailLink(driver, archiveBidRowIndex);
		pausePage();
		switchToNewWindow();
		setupNewURL(driver.getCurrentUrl());
		pausePage();

		FlaggingBidDataPage flaggingBidPage = new FlaggingBidDataPage(driver);

		/* Check if the Release button is visible for the user with 'Admin' role */
		boolean isRollbackVisible = flaggingBidPage.checkRollbackButtonVisibility(driver);
		logExpectedActualText("Rollback Button is visible: ", Boolean.TRUE.toString(),
				Boolean.toString(isRollbackVisible));
		Assert.assertTrue(isRollbackVisible);
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel(), this.getClass());

		printToConsole("Rollback Button is clicked");
		flaggingBidPage.clickRollbackButton(driver);

		pausePage();
		setupNewURL(driver.getCurrentUrl());

		pausePage();
		printToConsole("After clicking Rollback Button, the user is automatically navigated back to My Task Page");
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel(), this.getClass());
		pausePage();

		myTaskPage = new MyTaskPage();

		myTaskPage.navigateToTab(driver, SummaryPageTabsE.FOCUS_BIDS);
		logNavigation(true, SummaryPageTabsE.MY_TASK.getLabel());
		logNavigation(false, SummaryPageTabsE.FOCUS_BIDS.getLabel());

		FocusPage focusPage = new FocusPage(driver);

		int focusBidRowIndex = focusPage.searchRowIndex(driver, testData.get(BID_ID));
		boolean focusBidIsFound = focusBidRowIndex > ROW_NOT_FOUND;
		Assert.assertTrue(focusBidIsFound);
		focusPage.clickCheckbox(focusBidRowIndex);

		logExpectedActualText("Bid Id is found in Focus tab: ", Boolean.TRUE.toString(),
				Boolean.toString(focusBidIsFound));

		String actualDate = focusPage.getValueAtIndex(driver, focusBidRowIndex, NonFocusPage.BID_DATE_COL);
		String expectedDate = testData.get(BID_DATE);
		logExpectedActualText("Bid Date: ", expectedDate, actualDate);

		Assert.assertEquals(expectedDate, actualDate);

		takeScreenshot(tcNumber, SummaryPageTabsE.FOCUS_BIDS.getLabel(), this.getClass());
	}

}
