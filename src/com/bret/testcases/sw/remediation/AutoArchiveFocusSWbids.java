package com.bret.testcases.sw.remediation;

import org.testng.annotations.Test;

import com.PropertiesFiles.BWPropertiesFiles;
import com.bret.pages.BRETArchivePage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

/**
 * [added-Belle Pondiong]<br>
 * Test case class that will check Auto Archiving of remediated bids.
 * 
 */
public class AutoArchiveFocusSWbids extends BRETmainTestNGbase {
	
	 String xlpath =  "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
	 String sheet2 ="SW Remediation ";                           //Sheet name on excel
	 String bidNumberFromExcel = Excel.getCellValue (xlpath, sheet2, 9, 1);  
	 String bidStatusFromExcel = Excel.getCellValue (xlpath, sheet2, 9, 2);  
	 
	 String userRole = "Administrator";
	 BWPropertiesFiles propRead = new BWPropertiesFiles ();
	 String[] bretWebCredentials = propRead.propBW_credentials(userRole);
	     
	 String userName = bretWebCredentials[0];			
	 String userPassword = bretWebCredentials[1];
	 
	 String bidNumber = bidNumberFromExcel.trim();
	 String bidStatus = bidStatusFromExcel.trim();
	 
	 @Test (description = "RSW-019: Test case that will check that remediated bid is Auto Achived.")
	 public void tcAutoArchiveRemediatedFocusSWBid() throws Exception {
		 //----------------------------------------------------------------------------- 
		 // RSW-019: Test case that will check automatically moved Focus bids in Archive
		 //-----------------------------------------------------------------------------
		 
		 //Performs the Login event
		 BRETValidLogin userLogin = new BRETValidLogin(driver);  
		 userLogin.userValidLogin(userName, userPassword);
		 
		 // check bid in Archive
		 BRETArchivePage autoReleaseFocusBid =  new BRETArchivePage(driver);
		 autoReleaseFocusBid.autoreleasedtoArchiveChck_F(bidNumber, bidStatus, userName);
		 
		 //Screen capture 
		 ScreenCapture screenShots = new ScreenCapture(driver);  
		 String className = AutoArchiveFocusSWbids.class.getSimpleName();
		 String methodName = ("TC_RSW-019_" + className );
		 screenShots.screenCaptures(methodName);
		 
	 }

}
