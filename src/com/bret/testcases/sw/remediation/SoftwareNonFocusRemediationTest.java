package com.bret.testcases.sw.remediation;

import java.util.Map;

import org.junit.Assert;
import org.testng.annotations.Test;

import com.bret.base.BaseRemediationTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.ArchivePage;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.NonFocusPage;
import com.bret.util.BretTestUtils;

public class SoftwareNonFocusRemediationTest extends BaseRemediationTest {
	/**
	 * Test Case: RSW-001
	 * 
	 * Steps:
	 * 
	 * 1. Login to BRET Web as Admin
	 * 
	 * 2. Go to Archive Tab.
	 * 
	 */
	@Test(priority=1)
	public void testNonFocusReleasedArchive() {
		
		/* Get the test data from excel */
		String tcNumber = "TC_RSW-001";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		String newlyArchivedBid = testData.get(BID_ID);
		adminCheckArchivedBidDisplay(tcNumber, newlyArchivedBid, testData.get(RELEASED_BY),
				testData.get(RELEASED_DATE));
	}

	/**
	 * 
	 * Test case: RSW-002
	 * 
	 * Steps:
	 * 
	 * 1. Go to Archive Tab.
	 * 
	 * 2. Locate the released SW Non-focus bid
	 * 
	 * 3. Click Bid Detail of the bid
	 * 
	 * 4. Click ROLLBACK button
	 * 
	 * 5. Go to Non-Focus Tab and Search for the Rollback Bid
	 * 
	 */
	@Test(priority=2)
	public void testNonFocusRollBackArchivedBid() {
		/* Get the test data from excel */
		String tcNumber = "TC_RSW-002";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(tcNumber, getTestDataSource());

		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getAdminUsername(), getAdminPassword());

		/*
		 * The landing page upon login is the My Task Tab page - so no need to navigate
		 * MyTaskPage represents the My Task tab page object
		 */
		MyTaskPage myTaskPage = new MyTaskPage();

		/* This closes the NPS Survey */
		myTaskPage.closeNPSSurvey(driver);

		/* Doing logging here */
		logNavigation(true, SummaryPageTabsE.MY_TASK.getLabel());

		/* From MyTask page lets navigate to Archive page */
		myTaskPage.navigateToTab(driver, SummaryPageTabsE.ARCHIVE);

		/* Instantiate archive page */
		ArchivePage archivePage = new ArchivePage();

		/* Doing logging here */
		displayLogHeader(tcNumber, SummaryPageTabsE.ARCHIVE.getLabel());

		/* Now lets search for the newly archived bid */
		int archiveBidRowIndex = archivePage.searchRowIndex(driver, testData.get(BID_ID));
		boolean releasedBidIsFound = archiveBidRowIndex > ROW_NOT_FOUND;
		logExpectedActualText("Released bid is displayed: ", Boolean.TRUE.toString(),
				Boolean.toString(releasedBidIsFound));

		archivePage.clickCheckbox(archiveBidRowIndex);

		printToConsole("Clicking the detail link", "Opening Detail page",
				"Current detail tab is: " + DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		/* Doing logging here */
		displayLogHeader(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		printToConsole("Verify that Released bid is displayed on Archived Tab");

		archivePage.clickDetailLink(driver, archiveBidRowIndex);
		pausePage();
		switchToNewWindow();
		setupNewURL(driver.getCurrentUrl());
		pausePage();

		FlaggingBidDataPage flaggingBidPage = new FlaggingBidDataPage(driver);

		/* Check if the Release button is visible for the user with 'Admin' role */
		boolean isRollbackVisible = flaggingBidPage.checkRollbackButtonVisibility(driver);
		logExpectedActualText("Rollback Button is visible: ", Boolean.TRUE.toString(),
				Boolean.toString(isRollbackVisible));
		Assert.assertTrue(isRollbackVisible);
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel(), this.getClass());

		printToConsole("Rollback Button is clicked");
		flaggingBidPage.clickRollbackButton(driver);

		pausePage();
		setupNewURL(driver.getCurrentUrl());

		pausePage();
		printToConsole("After clicking Rollback Button, the user is automatically navigated back to My Task Page");
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel(), this.getClass());
		pausePage();

		myTaskPage = new MyTaskPage();

		myTaskPage.navigateToTab(driver, SummaryPageTabsE.NON_FOCUS_BIDS);
		logNavigation(true, SummaryPageTabsE.MY_TASK.getLabel());
		logNavigation(false, SummaryPageTabsE.NON_FOCUS_BIDS.getLabel());

		NonFocusPage nonFocusPage = new NonFocusPage(driver);

		int nonFocusBidRowIndex = nonFocusPage.searchRowIndex(driver, testData.get(BID_ID));
		boolean nonFocusBidIsFound = nonFocusBidRowIndex > ROW_NOT_FOUND;
		Assert.assertTrue(nonFocusBidIsFound);
		nonFocusPage.clickCheckbox(nonFocusBidRowIndex);

		logExpectedActualText("Bid Id is found in Non-Focus tab: ", Boolean.TRUE.toString(),
				Boolean.toString(nonFocusBidIsFound));

		String actualDate = nonFocusPage.getValueAtIndex(driver, nonFocusBidRowIndex, NonFocusPage.BID_DATE_COL);
		String expectedDate = testData.get(BID_DATE);
		logExpectedActualText("Bid Date: ", expectedDate, actualDate);

		Assert.assertEquals(expectedDate, actualDate);

		takeScreenshot(tcNumber, SummaryPageTabsE.NON_FOCUS_BIDS.getLabel(), this.getClass());
	}

}
