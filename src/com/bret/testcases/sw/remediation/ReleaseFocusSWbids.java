package com.bret.testcases.sw.remediation;

import org.testng.annotations.Test;

import com.PropertiesFiles.BWPropertiesFiles;
import com.bret.pages.BRETFocusPage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;

/**
 * [added-Belle Pondiong]<br>
 * Test case class that will perform remediation process.<br>
 * Remediation Outcome = Release where Pre Ship Verification Hold = NONE
 * 
 */
public class ReleaseFocusSWbids extends BRETmainTestNGbase {
	
	 String xlpath =  "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
	 String sheet2 ="SW Remediation ";                           //Sheet name on excel
	 String bidNumberFromExcel = Excel.getCellValue (xlpath, sheet2, 7, 1);  
	 String bidStatusFromExcel = Excel.getCellValue (xlpath, sheet2, 7, 2);  
	 String bidCommentFromExcel = Excel.getCellValue (xlpath, sheet2, 7, 3); 
	 
	 String userRole = "Reviewer";
	 BWPropertiesFiles propRead = new BWPropertiesFiles ();
	 String[] bretWebCredentials = propRead.propBW_credentials(userRole);
	     
	 String userName = bretWebCredentials[0];			
	 String userPassword = bretWebCredentials[1];
	 
	 String bidNumber = bidNumberFromExcel.trim();
	 String bidStatus = bidStatusFromExcel.trim();
	 String bidComment = bidCommentFromExcel.trim();
	 String brand = "SW";

	 
	 @Test (description = "RSW-014, RSW-015: Test case that will perform bid remediation RELEASE.")
		public void tcReleaseFocusBidWithoutSQOHold () throws Exception {
		 //-------------------------------------------------------------------------------------  
		 // RSW-014, RSW-015: Test case that will perform bid Remediation =  Release (without SQO HOLD)
	     // and then checked the remediated bid in Summary and Bid Details page
		 //-------------------------------------------------------------------------------------
			
			 String className = ReleaseFocusSWbids.class.getSimpleName();
			 String methodNameDetailsCheck = ("TC_RSW-014_" + className);
			 String methodNameSummaryCheck = ("TC_RSW-015_" + className);
			 
			 //Performs the Login event
			 BRETValidLogin userLogin = new BRETValidLogin(driver);  
			 userLogin.userValidLogin(userName, userPassword);
			 
			 // Release a bid 
			 BRETFocusPage releaseBid = new BRETFocusPage (driver);
			 releaseBid.bidRemediation(bidNumber, userName, bidStatus, bidComment); 
		 	 
			 // check released bid in Focus tab
			 releaseBid.bidRemediationCheckFocusTab (bidNumber, userName, bidStatus); 
			 
			 //Screen Capture
			 ScreenCapture screenShots = new ScreenCapture(driver);  
			 screenShots.screenCaptures(methodNameSummaryCheck);
					 
			 // Check on Bid Details after refresh
			 releaseBid.bidRemediatonChckbidDetails(bidNumber, bidStatus, bidComment,brand, methodNameDetailsCheck);
				 
			 
	}
	 
}
