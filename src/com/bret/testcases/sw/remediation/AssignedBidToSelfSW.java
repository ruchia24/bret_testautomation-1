package com.bret.testcases.sw.remediation;

import org.testng.annotations.Test;

import com.PropertiesFiles.BWPropertiesFiles;
import com.bret.pages.BRETFocusPage;
import com.bret.pages.BRETValidLogin;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.Excel;
import com.bret.util.ScreenCapture;


/**
 * [added-Belle Pondiong]<br>
 * Test case class that will perform 'assign bid to self'
 * 
 */
public class AssignedBidToSelfSW extends BRETmainTestNGbase {
	
	// Initializations
	// reading from data source
	
//	 String xlpath = "/home/bellep/BRET_CICD_in_Jenkins/BRET_FE_Test/Data Inputs/BRET DataSource.xls";  //path server
	 String xlpath =  "C:/Users/IBM_ADMIN/workspace/BRET_CICD_in_Jenkins/Data Inputs/BRET DataSource.xls";   //path in local
	 String sheet2 ="SW Remediation ";                           //Sheet name on excel
	 String bidNumberfromExcel = Excel.getCellValue (xlpath, sheet2, 4, 1);  
	 
	 String userRole = "Reviewer";
	 BWPropertiesFiles propRead = new BWPropertiesFiles ();
	 String[] bretWebCredentials = propRead.propBW_credentials(userRole);
	     
	 String userName = bretWebCredentials[0];			
	 String userPassword = bretWebCredentials[1];
	 
	 String bidNumber = bidNumberfromExcel.trim();
	 
	 @Test (description = "RSW_008: Test case taht will verify that Reviewer is able to assign focus bid to self.")
	 public void tcAssignFocusBidToSelf () throws Exception {
		 //---------------------------------------------- 
		 // RSW_008: This test case will assign Focus bid to Self
		 //---------------------------------------------- 
		
		 String className = AssignedBidToSelfSW.class.getSimpleName();
		 String methodName = ("TC_RSW_008_" + className);
	 
		 System.out.println("Trace: bid number found: " +bidNumber);
		 
		 //Performs the Login event
		 BRETValidLogin userLogin = new BRETValidLogin(driver);  
		 userLogin.userValidLogin(userName, userPassword);
		 
		 // Assigned bid to self
		 // Screen capture after the bid is assigned
		 // Bid Details window will close
		 BRETFocusPage assignBidToSelf = new BRETFocusPage (driver);
	     assignBidToSelf.bidAssignToSefl(bidNumber, methodName);
	 
		 
	 }
	 
	 @Test (description = "RSW_009: Test case taht will check the assigned bid details in Summary and Bid Details page.")
	 public void tcAssignedBidCheck () throws Exception {
		 //--------------------------------------------------------------------------------
		 // RSW_009: This test case will checked on the assigned bid in Summary and Bid Details page
		 //-------------------------------------------------------------------------------- 
		 
		 Thread.sleep(5000);
		 BRETFocusPage checkAssignedBid = new BRETFocusPage (driver);
		 
		//check in Focus tab
		checkAssignedBid.assignedBidChckFocusTab(bidNumber, userName); 
		 
		//Screen capture 
		ScreenCapture screenShots = new ScreenCapture(driver);  
		String className = AssignedBidToSelfSW.class.getSimpleName();
		String methodName = ("TC_RSW_009a_" + className);
		screenShots.screenCaptures(methodName);
		 
		//check in Bid Details page
		checkAssignedBid.assignedBidChckbidDetails(bidNumber, userName);
		 
		//Screen capture 
		String methodName1 = ("TC_RSW_009b_" + className);
		screenShots.screenCaptures(methodName1);
		 

	 }
	 

}
