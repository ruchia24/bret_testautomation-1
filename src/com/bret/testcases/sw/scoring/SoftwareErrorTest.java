package com.bret.testcases.sw.scoring;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.BidQPage;
import com.bret.pages.DetailPage;
import com.bret.pages.DiscountMarginPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.MyTaskPage;
import com.bret.pages.SupportDataPage;
import com.bret.util.BretTestUtils;

public class SoftwareErrorTest extends BaseTest {

	private static final String OUTLIER_FLAG_Y = "Y";
	private static final String FACT_CONFIDENTIALITY = "CONFIDENTIALITY";
	private static final String FACT_ROUTE_TO_MARKET = "ROUTE TO MARKET";
	private static final String FACT_NON_COMP_BID = "NON COMPETITIVE BID";
	private static final String FACT_OFFSHORE_PAYMT_TERMS = "OFFSHORE PAYMENT TERMS";
	private static final String FACT_BUNDLED_SOLUTIONS = "BUNDLED SOLUTIONS";
	private static final String FACT_CONTINGENCY_FEE = "CONTIGENCY FEE";
	private static final String DISC_MARG_FILE = "discMargFilePath";

	private MyTaskPage myTaskPage;
	private FlaggingBidDataPage flagBidDataPage;
	private BidQPage bidQPage;
	private SupportDataPage supportDataPage;
	private DetailPage detailPage;

	private static final String BID_ID = "bidId";
	private static final String FLAG_ERROR = "E";
	private static final int ROW_NOT_FOUND = 0;
	private int myTaskRowIndex = ROW_NOT_FOUND;
	private String winHandleBefore;

	@Test(priority = 1)
	public void logIn() {
		BRETLoginPage logIn = new BRETLoginPage(driver);
		logIn.userLogin(getLeadReviewerUsername(), getLeadReviewerPassword());
//		logIn.userLogin(getLegalUsername(), getLegalPassword());
	}

	/**
	 * 1153892: [BSSW-011] Error Bids: Flagging
	 * 
	 * To verify that error bids are flagged correctly and can be found under
	 * My Task Tab.
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Administrator role.
	 * 
	 * 3. Go My Task Tab.
	 * 
	 * 4.Verify that the newly loaded error bid is flagged correctly and is
	 * present in My Task Tab.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 2)
	public void errorBidsFlagging() throws InterruptedException {

		myTaskPage = new MyTaskPage();

		String sheetName = "TC_BSSW-011";
		String tcNumber = "TC_BSSW-011";

		System.out.println("========== TC Number: " + tcNumber
				+ " ==========\n");

		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(
				sheetName, getTestDataSource());

		System.out.println(testData.get(BID_ID));
		Thread.sleep(15000);
		/* Now lets search for the newly loaded bid ROW */
		myTaskRowIndex = myTaskPage
				.searchRowIndex(driver, testData.get(BID_ID));

		/* CLICK on the radio button / check box */
		myTaskPage.clickCheckbox(myTaskRowIndex);

		String bidAtMyTaskTab = myTaskPage.getValueAtIndex(driver,
				myTaskRowIndex, MyTaskPage.BID_ID_COL);

		String bidFlag = myTaskPage.getValueAtIndex(driver, myTaskRowIndex,
				MyTaskPage.FLAG_COL);

		String bidExpected = testData.get(BID_ID);

		Assert.assertEquals(bidAtMyTaskTab, bidExpected);
		Assert.assertEquals(bidFlag, FLAG_ERROR);

		System.out.println("Expected Bid id: " + bidExpected);
		System.out.println("Bid id (actual): " + bidAtMyTaskTab);

		System.out.println("Expected Flag: " + FLAG_ERROR);
		System.out.println("Flag (actual): " + bidFlag);

		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());

	}

	/**
	 * 1153893: [BSSW-012] Error Bids: ABCD indicators on Summary Page
	 * 
	 * To verify that the ABCD indicators are not enabled for error bids.
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Administrator role.
	 * 
	 * 3. Go My Task Tab.
	 * 
	 * 4.Verify that the newly loaded error bid is flagged correctly and is
	 * present in My Task Tab.
	 * 
	 * 5. Verify that the ABCD indicators are not enabled for error bids.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 3)
	public void errorBidsABCDIndicator() throws InterruptedException {


		String tcNumber = "TC_BSSW-012";
		System.out.println();
		boolean aHasMarker = myTaskPage.aHasMarker(myTaskRowIndex);
		boolean bHasMarker = myTaskPage.bHasMarker(myTaskRowIndex);
		boolean cHasMarker = myTaskPage.cHasMarker(myTaskRowIndex);
		boolean dHasMarker = myTaskPage.dHasMarker(myTaskRowIndex);

		System.out.println("========== TC Number: " + tcNumber + " ==========");
		System.out.println("========== " + SummaryPageTabsE.MY_TASK.getLabel()
				+ " tab ==========");
		System.out.println("Expected -  'A' Indicator Enabled: false");
		System.out.println("Actual   -  'A' Indicator Enabled: " + aHasMarker);

		System.out.println("Expected -  'B' Indicator Enabled: false");
		System.out.println("Actual   -  'B' Indicator Enabled: " + bHasMarker);

		System.out.println("Expected -  'C' Indicator Enabled: false");
		System.out.println("Actual   -  'C' Indicator Enabled: " + cHasMarker);

		System.out.println("Expected -  'D' Indicator Enabled: false");
		System.out.println("Actual   -  'D' Indicator Enabled: " + dHasMarker);

		Assert.assertEquals(aHasMarker, false);
		Assert.assertEquals(bHasMarker, false);
		Assert.assertEquals(cHasMarker, false);
		Assert.assertEquals(dHasMarker, false);

		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel());
		Thread.sleep(5000);
	}

	/**
	 * 1153895: [BSSW-013] Error Bids: Focus factors
	 * 
	 * To verify that the correct focus factors are being flagged and being
	 * displayed on Factors under Flagging and Bid Data tab on the summary
	 * page of the bid detail.
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Administrator role.
	 * 
	 * 3. Go My Task Tab.
	 * 
	 * 4.Verify that the newly loaded error bid is flagged correctly and is
	 * present in My Task Tab.
	 * 
	 * 5. Verify that the ABCD indicators are not enabled for error bids.
	 * 
	 * 6. Go to Bid details.
	 * 
	 * 7. Verify that the correct focus factors are being flagged and being
	 * displayed on Factors under Flagging and Bid Data tab on the summary
	 * page of the bid detail.
	 * 
	 * @throws InterruptedException
	 */

	@Test(priority = 4)
	public void errorBidsFocusFactors() throws InterruptedException {

		String tcNumber = "TC_BSSW-013";

		System.out.println("========== TC Number: " + tcNumber
				+ " ==========\n");

		System.out.println("Navigating TO: Detail Page");

		myTaskPage.clickDetailLink(driver, myTaskRowIndex);

		/* GET the control of the current window */
		winHandleBefore = driver.getWindowHandle();

		/* SWITCH Control to New Window */
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

//		setupNewURL(driver.getCurrentUrl());

		Thread.sleep(5000);
		flagBidDataPage = new FlaggingBidDataPage(driver);
		System.out.println("Factors: " + flagBidDataPage.getFactorsValue());

		String flaggingBidFactors = flagBidDataPage.getFactorsValue();

		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		System.out.println("Navigating FROM: "
				+ DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		Thread.sleep(5000);

		flagBidDataPage.navigateToTab(driver, DetailPageTabsE.BID_Q);
		bidQPage = new BidQPage(driver);

		boolean factorsIsValid = compareFactorsToBidQOutlier(flaggingBidFactors);

		Assert.assertTrue(factorsIsValid);

		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

	}

	private boolean compareFactorsToBidQOutlier(String flaggingBidFactors) {
		int outlierRow = BidQPage.COL_OUTLIER_STATUS_VAL;

		String confOutlier = bidQPage.getValueAtIndexRelQuestions(driver,
				BidQPage.ROW_CONFIDENTIALITY, outlierRow);

		String routToMarketOutlier = bidQPage.getValueAtIndexRelQuestions(
				driver, BidQPage.ROW_ROUTE_TO_MARKET, outlierRow);

		String nonCompBidOutlier = bidQPage.getValueAtIndexRelQuestions(driver,
				BidQPage.ROW_NON_COMPETITIVE_BID, outlierRow);

		String offshorePaymtOutlier = bidQPage.getValueAtIndexRelQuestions(
				driver, BidQPage.ROW_OFFSHORE_PAYMENT_TERMS, outlierRow);

		String bundledSolOutlier = bidQPage.getValueAtIndexRelQuestions(driver,
				BidQPage.ROW_BUNDLED_SOLUTIONS, outlierRow);

		String contingencyFeeOutlier = bidQPage.getValueAtIndexRelQuestions(
				driver, BidQPage.ROW_CONTINGENCY_FEE_PAYMENTS, outlierRow);

		List<String> factorList = new ArrayList<>();

		if (confOutlier.equalsIgnoreCase(OUTLIER_FLAG_Y)) {
			factorList.add(FACT_CONFIDENTIALITY);
		}

		if (routToMarketOutlier.equalsIgnoreCase(OUTLIER_FLAG_Y)) {
			factorList.add(FACT_ROUTE_TO_MARKET);
		}

		if (nonCompBidOutlier.equalsIgnoreCase(OUTLIER_FLAG_Y)) {
			factorList.add(FACT_NON_COMP_BID);
		}

		if (offshorePaymtOutlier.equalsIgnoreCase(OUTLIER_FLAG_Y)) {
			factorList.add(FACT_OFFSHORE_PAYMT_TERMS);
		}

		if (bundledSolOutlier.equalsIgnoreCase(OUTLIER_FLAG_Y)) {
			factorList.add(FACT_BUNDLED_SOLUTIONS);
		}

		if (contingencyFeeOutlier.equalsIgnoreCase(OUTLIER_FLAG_Y)) {
			factorList.add(FACT_CONTINGENCY_FEE);
		}

		return checkFactorsValidity(flaggingBidFactors, factorList);

	}

	private boolean checkFactorsValidity(String flaggingBidFactors,
			List<String> factorList) {
		boolean isValid = true;

		for (String factor : factorList) {
			if (!flaggingBidFactors.contains(factor)) {
				System.out.println("Factor not found: " + factor);
				return false;
			}
		}
		return isValid;
	}


	/**
	 * 1153902: [BSSW-014] Error Bids: Supporting Factors
	 * 
	 * To verify that the score under the supporting factors tab is correctly
	 * displayed on the summary page of the Bid Detail.
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Administrator role.
	 * 
	 * 3. Go My Task Tab.
	 * 
	 * 4.Verify that the newly loaded error bid is flagged correctly and is
	 * present in My Task Tab.
	 * 
	 * 5. Verify that the ABCD indicators are not enabled for error bids.
	 * 
	 * 6. Go to Bid details.
	 * 
	 * 7. Verify that the correct focus factors are being flagged and being
	 * displayed on Factors under Flagging and Bid Data tab on the summary
	 * page of the bid detail.
	 * 
	 * 8. Verify that the score under the supporting factors tab is correctly
	 * displayed on the summary page of the Bid Detail.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 5)
	public void errorBidsSupportingFactors() throws InterruptedException {

		detailPage = new DetailPage();
		supportDataPage = new SupportDataPage(driver);
		flagBidDataPage = new FlaggingBidDataPage(driver);

		String tcNumber = "TC_BSSW-O14";

		System.out.println("\n========== TC Number: " + tcNumber
				+ " ==========\n");

		/* You are currently at Flagging Bid Data Page */

		detailPage.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);
		Thread.sleep(3000);

		/* actualSupportScore is the score in Flagging and Bid Data Tab */
		String actualSupportScore = flagBidDataPage.getValueAtIndex(driver,
				flagBidDataPage.ROW_SUPPORTING_FACTORS,
				flagBidDataPage.COL_SCORE);

		Thread.sleep(3000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());

		/* NAVIGATE to Support Data Tab to get the scores */
		detailPage.navigateToTab(driver, DetailPageTabsE.SUPPORT_DATA);
		Thread.sleep(3000);

		/* expectedSupportScore is the score in Support Data Tab */
		int expectedSupportScore = supportDataPage.computeTotalScore(driver);

		System.out
				.println("Supporting Factors Score in Flagging and Bid Data Tab (actual) : "
						+ actualSupportScore);
		System.out
				.println("Supporting Factors Score in Support Data Tab (expected)        : "
						+ expectedSupportScore);

		Assert.assertTrue(actualSupportScore.equalsIgnoreCase(String
				.valueOf(expectedSupportScore)));

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.SUPPORT_DATA.getLabel());
		Thread.sleep(3000);
		
		/* You are currently at Support Data Tab */

	}

	/**
	 * 1153906: [BSSW-015] Error Bids: Scores To verify that the score of the
	 * bid is being added to the total bid score and being displayed correctly
	 * on the summary page of bid detail.
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Administrator role.
	 * 
	 * 3. Go My Task Tab.
	 * 
	 * 4.Verify that the newly loaded error bid is flagged correctly and is
	 * present in My Task Tab.
	 * 
	 * 5. Verify that the ABCD indicators are not enabled for error bids.
	 * 
	 * 6. Go to Bid details.
	 * 
	 * 7. Verify that the correct focus factors are being flagged and being
	 * displayed on Factors under Flagging and Bid Data tab on the summary
	 * page of the bid detail.
	 * 
	 * 8. Verify that the score under the supporting factors tab is correctly
	 * displayed on the summary page of the Bid Detail.
	 * 
	 * 9. Verify that the scores form the focus factors are being added to the
	 * total bid score and being displayed correctly on the summary page of bid
	 * detail.
	 * 
	 * -- Flagging and Bid Data tab
	 * 
	 * -- Bid Q tab
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 6)
	public void errorBidsScores() throws InterruptedException {
		String tcNumber = "TC_BSSW-015";

		/* You are currently at Support Bid Data Tab */

		System.out.println("\n========== TC Number: " + tcNumber
				+ " ==========\n");

		/* NAVIGATE to Flagging and Bid Data Tab */
		detailPage = new DetailPage();
		detailPage.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);
		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		
		flagBidDataPage = new FlaggingBidDataPage(driver);

		/* Getting the Support Factors Scores */
		/* actualTotalScore is the score in Flagging and Bid Data Tab */
		String actualTotalScore = flagBidDataPage.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_TOTAL_BID_LEVEL_SCORE,
				FlaggingBidDataPage.COL_SCORE);

		/* expectedTotalScore is the score in Flagging and Bid Data Tab */
		int expectedTotalScore = flagBidDataPage.getTotalBidLevelScore();

		/* get the score value at index of Questions and BPCOPS */
		String actualQbpcops = flagBidDataPage.getValueAtIndex(driver,
				FlaggingBidDataPage.ROW_QUEST_BPCOPS,
				FlaggingBidDataPage.COL_SCORE);

		String tier1Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_DISTRIBUTOR_CEID);
		String tier2Ceid = flagBidDataPage.getValueAtIndexBidDataGrid(driver,
				FlaggingBidDataPage.ROW_CUSTOMER_FACTING_BP_CEID);

		/* NAVIGATE to Bid Q Tab */
		bidQPage = new BidQPage(driver);
		detailPage.navigateToTab(driver, DetailPageTabsE.BID_Q);
		
		Thread.sleep(5000);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel());

		int expectedQbpcops = bidQPage.getTotalForBPCops(driver,
				tier1Ceid.equalsIgnoreCase(tier2Ceid));

		/* Checking on the Total Bid Level Score */
		System.out.println("\nTotal Bid Level Score (actual)   : "
				+ actualTotalScore);
		System.out.println("Total Bid Level Score (calculated) : "
				+ expectedTotalScore);

		/* Checking on the Total BPCOPS Score */
		System.out.println("\nTotal BPCOPS Score (actual)   :    "
				+ actualQbpcops);
		System.out.println("Total BPCOPS Score (calculated) :    "
				+ expectedQbpcops);

		Assert.assertTrue(actualTotalScore.equalsIgnoreCase(String
				.valueOf(expectedTotalScore)));
		Assert.assertTrue(actualQbpcops.equalsIgnoreCase(String
				.valueOf(expectedQbpcops)));

		detailPage.navigateToTab(driver, DetailPageTabsE.FLAGGING_BID_DATA);
		/* YOU ARE NOW AT FLAGGING BID DATA PAGE */

	}

	/**
	 * 
	 * 1153912: [BSSW-016] Error Bids: Display
	 * 
	 * To verify that correctLeading product (SWG for SW) ,bid date, region,
	 * country and line items are being displayed correctly on the bid detail
	 * page.
	 * 
	 * Step/s:
	 * 
	 * 1. Launch BRET web.
	 * 
	 * 2. Login using Administrator role.
	 * 
	 * 3. Go My Task Tab.
	 * 
	 * 4.Verify that the newly loaded error bid is flagged correctly and is
	 * present in My Task Tab.
	 * 
	 * 5. Verify that the ABCD indicators are not enabled for error bids.
	 * 
	 * 6. Go to Bid details.
	 * 
	 * 7. Verify that correctLeading product (SWG for SW) ,bid date, region,
	 * country and line items are being displayed correctly on the bid detail
	 * page.
	 * 
	 * @throws InterruptedException
	 */
	@Test(priority = 7)
	public void errorBidsDisplay() throws InterruptedException {
		String sheetName = "TC_BSSW-016";		
		String tcNumber = "TC_BSSW-016";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(
				sheetName, getTestDataSource());

		/* You are currently at Flagging and Bid Data Tab */

		System.out.println("\n========== TC Number: " + tcNumber
				+ " ==========\n");

		Thread.sleep(5000);
		/* Getting expected values from Excel File */
		String expectedPrimaryBrand = testData.get("primaryBrand");
		String expectedBidDate = testData.get("bidDate");
		String expectedRegion = testData.get("region");
		String expectedCountry = testData.get("country");

		flagBidDataPage = new FlaggingBidDataPage(driver);
		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel());
		
		/* actual values are values shown in BRET WEB */
		String primaryBrand = flagBidDataPage.getValueAtIndexBidDataGrid(
				driver, flagBidDataPage.ROW_PRIMARY_BRAND);
		String country = flagBidDataPage.getValueAtIndexBidDataGrid(driver,
				flagBidDataPage.ROW_COUNTRY_NAME);
		String region = flagBidDataPage.getValueAtIndexBidDataGrid(driver,
				flagBidDataPage.ROW_REGION);
		String bidDate = flagBidDataPage.getValueAtIndexBidDataGrid(driver,
				flagBidDataPage.ROW_BID_DATE);

		System.out
				.println("\n  DETAILS\tDATA INPUT (Expected)\tFLAGGING AND BID DATA TAB (Actual)");
		System.out.println("Primary Brand:\t\t" + expectedPrimaryBrand + "\t\t"
				+ primaryBrand);
		System.out.println("Country Name :\t\t" + expectedCountry + "\t\t"
				+ country);
		System.out.println("Region       :\t\t" + expectedRegion + "\t\t"
				+ region);
		System.out.println("Bid Date     :\t\t" + expectedBidDate + "\t\t"
				+ bidDate);
		System.out.println();

		Assert.assertEquals(primaryBrand, expectedPrimaryBrand);
		Assert.assertEquals(bidDate, expectedBidDate);
		Assert.assertEquals(region, expectedRegion);
		Assert.assertEquals(country, expectedCountry);

		DetailPage detail_page = new DetailPage();
		detail_page.navigateToTab(driver, DetailPageTabsE.DISCOUNT_MARGIN);

		boolean isDiscMargValid = checkDiscountMarginValidity(testData
				.get(DISC_MARG_FILE), tcNumber);
		Assert.assertTrue(isDiscMargValid);

		/* SCREENSHOT, testcase#, currentpage */
		takeScreenshot(tcNumber, DetailPageTabsE.DISCOUNT_MARGIN.getLabel());

	}

	private boolean checkDiscountMarginValidity(String filePath, String tcNumber) {
		List<String> testDataMargDisc = BretTestUtils.readFile(filePath);

		DiscountMarginPage discMargPage = new DiscountMarginPage(driver);
		return discMargPage.checkIsDiscountMarginValid(testDataMargDisc, getScreenshotPath(), BretTestUtils.getImgFilenameNoFormat(tcNumber, this.getClass()));
	}

	private void takeScreenshot(String tcNumber, String tabLabel) {
		BretTestUtils.screenCapture(
				driver,
				getScreenshotPath(),
				BretTestUtils.getImgFilename(tcNumber + "_" + tabLabel,
						this.getClass()));
	}

	@AfterTest
	public void exit() throws InterruptedException {
		/* Switch to Original Window */
		// driver.switchTo().window(winHandleBefore);
		Thread.sleep(5000);
		driver.quit();
	}

}
