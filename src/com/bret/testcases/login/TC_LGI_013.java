package com.bret.testcases.login;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.PropertiesFiles.BWPropertiesFiles;
import com.bret.pages.BRETLoginPage;
import com.bret.testcases.BRETmainTestNGbase;
import com.bret.util.ScreenCapture;

//extends to class BRETmainTestNGbase for the FF launching methods

public class TC_LGI_013 extends BRETmainTestNGbase{ 
	
//	private StringBuffer verificationErrors = new StringBuffer();
	 
	@Test
	  public void tcValidLoginTest () throws Exception {
		 //---------------------------------------------- 
		 // LGI_013: Valid Login test
		 //---------------------------------------------- 
	   
		//--------------------------------------------------------------------------------- 
		// [Updated-Belle-9/11/17]
		// Change data inputs from Excel to Property files
		//----------------------------------------------------------------------------------
		String usrRole = "Any";
		BWPropertiesFiles propRead = new BWPropertiesFiles ();
		String[] bretWebCredentials = propRead.propBW_credentials(usrRole);
		     
		String userName = bretWebCredentials[0];			
		String userPassword = bretWebCredentials[1];
			 
		//Performs the Login event
		// calls the method UserLogin from class BRETLoginPage to perform the site login	
		BRETLoginPage userLogin = new BRETLoginPage(driver);  
		userLogin.userLogin(userName, userPassword);
		//System.out.println("TC_LGI_013 driver = " + this.driver.hashCode());
						

				
		 //--------------------------------------------------------------------------------- 
		 // [Updated-Belle-9/10/17]
		 // Added checking for incorrect login.
		 // Special Bid Risk Analysis - Bid Summary page title is checked on successful login.
		 // Taken out the Try-catch on My Task tab assertion
		 //----------------------------------------------------------------------------------

		String pageTitle = driver.getTitle();

		Assert.assertEquals(pageTitle, "Special Bid Risk Analysis - Bid Summary", "Unable to login. Please check credentials or problem loading the page.!"); 
		
	    //Looking for My Task tab after login to designate successful login
	    
//	    try {
		//Assert.assertEquals(driver.findElement(By.id("dijit_layout_TabContainer_0_tablist_idx_layout_ContentPane_0")).getText(), "My Task");
  		System.out.println("Login was successful!");
	    	
	    	// Clicks the name dropdown Menu
	    	// Locate the build number and print it on the run logs	    	
	    	driver.findElement(By.xpath(".//*[@id='idx_form_DropDownLink_0_link']")).click(); //name dropdown
	    	driver.findElement(By.xpath(".//*[@id='dijit_MenuItem_0_text']")).click();	  //Show My Privilege   
	       
	    	String BuilddNum = driver.findElement(By.xpath(".//*[@id='myPrivilegeDlg']/div[4]/span[1]/a")).getText();	//Gets the build number    	 
	   		System.out.println("BRET web UAT current build is " + BuilddNum); // printing the Build Version
 
        
//	    } catch (Error e) {
//		  Assert.fail("Unable to login to BRET web."); 
//	      verificationErrors.append(e.toString());
//	      System.out.println("Login Failed!");

//	    }
//	    	
	    	  
	    // Screen capture 
		// This will take screenshot at the end of the test
	    ScreenCapture screenShots = new ScreenCapture(driver);  
		String methodName = TC_LGI_013.class.getSimpleName();
	    screenShots.screenCaptures(methodName);
	    
	  }
 
}
