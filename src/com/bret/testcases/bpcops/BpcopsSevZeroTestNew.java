package com.bret.testcases.bpcops;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import com.bret.base.BaseTest;
import com.bret.pages.BRETAdvSearchPage;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.DetailPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.RemediationLogPage;
import com.bret.util.BretTestUtils;
import com.bret.util.ConstUtils;
import com.bret.util.JDBCConnection;

public class BpcopsSevZeroTestNew extends BaseTest {

	final static Logger LOGGER = Logger.getLogger(BpcopsSevZeroTestNew.class);
		
	BretTestUtils utils = new BretTestUtils(driver);
	BRETAdvSearchPage bretAdvSearch = new BRETAdvSearchPage(driver);
	
	private String testBIDSevZero = "TEST_S001";
	private String insertedTestData[] = {"TEST_S001","TEST_S002","TEST_S003","TEST_S004","TEST_S005","TEST_S006","TEST_S007","TEST_S008","TEST_S009","TEST_S010","TEST_S011","TEST_S012","TEST_S013","TEST_S014","TEST_S015"};
	private String deleteTestDataFromLandingTablesQueries[] = {GeneralBPCOPSQueries.deleteFrmFamsWebQuoteLineItemQuery, GeneralBPCOPSQueries.deleteFrmWebQuoteTxt, GeneralBPCOPSQueries.deleteFrmWebQuoteSpclBidQues, GeneralBPCOPSQueries.deleteFrmWebQuoteSpclBidApprover};
	
	private String deleteTestDataFromPersistentTablesQueries[] = {GeneralBPCOPSQueries.deleteFromBRETTxnBidTableQuery, GeneralBPCOPSQueries.deleteFromBRETTxnPartsTableQuery, GeneralBPCOPSQueries.deleteFromBRETReviewedBidFactorsTableQuery, GeneralBPCOPSQueries.deleteFromBRETReviewedBidStatusTableQuery};
	
	private ArrayList<String> nonFocusBidsList = new ArrayList<String>();
	
	@Test
	public void run() throws InterruptedException {
		
//		/** For SEV 0 - Verify that all bids are released for sampling. 
//		 *  By Executing the query to get BRET_FOCUS_FLAG db col value for all the inserted test bids**/
//		ArrayList<String> allBidsList = JDBCConnection.getColumnDataList(JDBCConnection.BZ_ConnectionUrl, insertedTestData, GeneralBPCOPSQueries.checkIfBidsLoadedQuery, ConstUtils.BRET_FOCUS_FLAG_COL_NAME);
//		for(String bid : allBidsList) {
//			if(bid.equals(ConstUtils.NON_FOCUS_FLAG)) {
//				LOGGER.debug("Bid is flagged as Non Focus: Save it in Non Focus Bids List");
//				nonFocusBidsList.add(bid);
//			}
//		}
//		LOGGER.info("Verify that all bids for sev 0 are released as Non-Focus");
//		Assert.assertEquals(nonFocusBidsList.size(), ConstUtils.INDEX_FIFTEEN, "FAIL : As all the bids with Sev 0 are not flagged as NonFocus");
//			
//		/* This is done for direct navigation to bid page
//		 * Check the runId(batch ID) from BRET.PERSISTENT_BID table in DB, which has loaded test bid*/	
//		String runId = JDBCConnection.getSingleColumnValue(JDBCConnection.BZ_ConnectionUrl, testBIDSevZero, GeneralBPCOPSQueries.selectRunIDQuery, "batch_id");
//		LOGGER.info("run id "+runId);		
//		
//		/* Login as Lead Reviewer */
//		BRETLoginPage loginPage = new BRETLoginPage(driver);
//		loginPage.userLogin(getLeadReviewerUsername(), getLeadReviewerPassword());
//		
//		/* Directly Navigate to Test Bid Detail Page*/
//		utils.navigateTo("?page=detail&bid="+ testBIDSevZero +"&source=SQO&batchId=" + runId);
//		utils.waitForElement(By.xpath("//td[contains(.,'Total Bid Level Score')]"));
//		
//		LOGGER.info("Verify focus factor on the bid");
//		Assert.assertEquals(utils.getElement(By.xpath(FlaggingBidDataPage.FOCUS_BP_TIER_1)).isDisplayed(), true, "Focus BP : Tier1 Investigation Findings Factor is not displayed");
//		
//		LOGGER.info("Click on Remediation Log Btn and Verify Outcome comment");
//		utils.clickAndWait(By.xpath(DetailPage.XPATH_REMEDIATION_LOG));
//		Assert.assertEquals(utils.getElement(By.xpath(RemediationLogPage.OUTCOME_COMMENT_FLAGGED_BID)).isDisplayed(), true, "Expected Outcome Comment is missing");
//		
//		LOGGER.info("Click on Bid Q Tab & Verify that correct BPCOPS category displayed on page.");
//		utils.clickAndWait(By.xpath(DetailPage.XPATH_BID_Q_TAB));
	}
	
	@AfterTest
	public void tearDown() {
		
		LOGGER.info("Delete the test data");
		
		//Delete the Dummy CEID created for severity level zero
		System.out.println(GeneralBPCOPSQueries.deleteCEIDsQuery + "'" + ConstUtils.testCEIDSevZero + "'");
		JDBCConnection.deleteInsertedRecords(JDBCConnection.BZ_ConnectionUrl, null, deleteTestDataFromLandingTablesQueries, GeneralBPCOPSQueries.deleteCEIDsQuery + ConstUtils.testCEIDSevZero);
				
		
		//Delete the inserted test data for Sev0 from SQO Landing Tables
		JDBCConnection.deleteInsertedRecords(JDBCConnection.BZ_ConnectionUrl, insertedTestData, deleteTestDataFromLandingTablesQueries, null);
		
		//Delete the inserted test data for Sev0 from Persistent Tables in BZ
		JDBCConnection.deleteInsertedRecords(JDBCConnection.BZ_ConnectionUrl, insertedTestData, deleteTestDataFromPersistentTablesQueries, null);
				
		//Delete the inserted test data for Sev0 from Persistent Tables in GZ
		JDBCConnection.deleteInsertedRecords(JDBCConnection.GZ_ConnectionUrl, insertedTestData, deleteTestDataFromPersistentTablesQueries, null);
				
		tearDownAfterTestCase();
	}
	

}
