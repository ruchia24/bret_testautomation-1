package com.bret.testcases.bpcops;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.bret.base.BaseTest;
import com.bret.enums.DetailPageTabsE;
import com.bret.enums.SummaryPageTabsE;
import com.bret.pages.BRETLoginPage;
import com.bret.pages.BidQPage;
import com.bret.pages.FlaggingBidDataPage;
import com.bret.pages.MyTaskPage;
import com.bret.util.BretTestUtils;

public class BpcopsSevFourTest extends BaseTest {
	private static final String BID_ID = "sev4_focusBid";
	private static final String BID_ID_FLAG = "sev4_focusBidFlag";
	private static final String BPCOPS_DISTRIBUTOR_CATEGORY_VALUE = "sev4_distributorCategoryValue";
	private static final String BPCOPS_CUSTOMER_CATEGORY_VALUE = "sev4_customerCategoryValue";
	private static final String FACTORS = "sev4_factor";
	private static final int ROW_NOT_FOUND = 0;
	private int myTaskRowIndex = ROW_NOT_FOUND;
	private static final String sheetName = "TC_BPCOPS-001-030";

	private MyTaskPage myTaskPage;
	private FlaggingBidDataPage flaggingBidPage;
	private BidQPage bidQPage;
	
	@BeforeTest
	public void logIn() {
		BRETLoginPage loginPage = new BRETLoginPage(driver);
		loginPage.userLogin(getLeadReviewerUsername(), getLeadReviewerPassword());
	}
	
	/**
	 * [BPCOPS-018] Random Sampling > Severity Level 4: Bid Flagging and Focus factors
	 * Steps:
	 * 1. Launch BRET web.
	 * 2. Login using Lead reviewer role.
	 * 3. Search for the newly loaded bids.
	 * 4. Check summary tab and bids details page of the selected bids.
	 * 
	 * @param detailLinkPart
	 * @throws InterruptedException
	 */

	@Test(priority = 1, description = "[BPCOPS-018] Random Sampling > Severity Level 4: Bid Flagging and Focus factors - "
			+ "To verify that all bids are flagged as Focus and has focus factor on the bid: BPCOPS: Tier2(Tier1) Terminated/ to be terminated BP in RTM")
	public void testFocusFlagging() throws InterruptedException {
		myTaskPage = new MyTaskPage();
		flaggingBidPage = new FlaggingBidDataPage(driver);

		/* GET TEST DATA FROM EXCEL */
		String tcNumber = "BPCOPS-018";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());

		String newlyLoadedBidIdFocus = testData.get(BID_ID);
		String expectedFlagFocus = testData.get(BID_ID_FLAG);
		String expectedFactor = testData.get(FACTORS);

		System.out.println("\n========== TC Number: " + tcNumber + " ==========");
		System.out.println("\n========== " + SummaryPageTabsE.MY_TASK.getLabel() + " tab ==========");
		System.out.println("Verify that the newly loaded bid is present in My Task Tab");
		System.out.println("Verify that the newly loaded bid is flagged as focus");
		
		/* SEARCH FOR THE NEWLY LOADED FOCUS BID */
		myTaskRowIndex = myTaskPage.searchRowIndex(driver, testData.get(BID_ID));
		String bidAtMyTaskTab = myTaskPage.getValueAtIndex(driver, myTaskRowIndex, MyTaskPage.BID_ID_COL);
		String bidFlag = myTaskPage.getValueAtIndex(driver, myTaskRowIndex, MyTaskPage.FLAG_COL);

		/* CLICK CHECK BOX FOR THE SELECTED BID */
		myTaskPage.clickCheckbox(myTaskRowIndex);

		Assert.assertEquals(bidAtMyTaskTab, newlyLoadedBidIdFocus);
		Assert.assertEquals(bidFlag, expectedFlagFocus);
		
		System.out.println("\nChecking BID ID and Flagging: ");
		System.out.println("FOCUS BID");
		
		System.out.println("\n> Expected Bid id: " + newlyLoadedBidIdFocus);
		System.out.println("> Bid id (actual): " + bidAtMyTaskTab +"\n");

		System.out.println("> Expected Flag: " + expectedFlagFocus);
		System.out.println("> Flag (actual): " + bidFlag +"\n\n");

		/* SCREENSHOT FOR THE BID SUMMARY (MY TASK TAB) */
		takeScreenshot(tcNumber, SummaryPageTabsE.MY_TASK.getLabel(), this.getClass());
		
		Thread.sleep(5000);
		
		myTaskPage.clickDetailLink(driver, myTaskRowIndex);
		
		/* SWITCH Control to New Window */
		pausePage();
		switchToNewWindow();
		setupNewURL(driver.getCurrentUrl());
		pausePage();
		
		FlaggingBidDataPage flaggingBidPage = new FlaggingBidDataPage(driver);
		
		String actualFactorFocus = flaggingBidPage.getFactorsValue();

		System.out.println("\n\nChecking Focus Factor: ");
		
		System.out.println("\n> Expected Factor: " + expectedFactor);
		System.out.println("> Actual Factor  : " + actualFactorFocus + "\n\n");
		Assert.assertTrue(expectedFactor.contains(actualFactorFocus), "Factor cannot be found.\n");

		pausePage();
		
		/* SCREENSHOT OF FLAGGING AND BID DATA TAB */
		takeScreenshot(tcNumber, DetailPageTabsE.FLAGGING_BID_DATA.getLabel(), this.getClass());
		pausePage();
	}
	
	/**
	 * [BPCOPS-019] BRET Web Details > BPCOPS Category: Severity Level 4
	 * Steps:
	 * 1. Go to Bid Q tab.
	 * 
	 * @throws InterruptedException
	 */
	
		/* BPCOPS CATEGORY FOR FOCUS BID (BPCOPS-019) */
	@Test(priority = 2, description = "[BPCOPS-019] BRET Web Details > BPCOPS Category: Severity Level 4")
	public void testBPCOPSCategoryFocus() throws InterruptedException {
		flaggingBidPage = new FlaggingBidDataPage(driver);
		bidQPage = new BidQPage(driver);

		/* NAVIGATE TO BID Q TAB */
		flaggingBidPage.navigateToTab(driver, DetailPageTabsE.BID_Q);

		/* GET TEST DATA FROM EXCEL */
		String tcNumber = "BPCOPS-019";
		Map<String, String> testData = BretTestUtils.getTestDataFromExcel(sheetName, getTestDataSource());
		
		String expectedDistributorCategoryValue = testData.get(BPCOPS_DISTRIBUTOR_CATEGORY_VALUE);
		String expectedCustomerCategoryValue = testData.get(BPCOPS_CUSTOMER_CATEGORY_VALUE);

		/* BPCOPS CATEGORY FOR FOCUS BID */
		String actualDistributorCategoryValue = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_DISTRIBUTOR, BidQPage.COL_HIST_BPC_OPS_CAT);
		String actualCustomerCategoryValue = bidQPage.getValueAtIndexHistory(driver, BidQPage.ROW_HIST_CUST_FACING_BP, BidQPage.COL_HIST_BPC_OPS_CAT);
		
		System.out.println("\nChecking BPCOPS Category: \n");
			
		System.out.println("> Expected bpcops category value for T1: "   + expectedDistributorCategoryValue);
		System.out.println("> Actual bpcops category value for T1  : "   + actualDistributorCategoryValue);

		System.out.println("\n> Expected bpcops category value for T2: " + expectedCustomerCategoryValue);
		System.out.println("> Actual bpcops category value for T2  : " + actualCustomerCategoryValue +"\n\n");

		Assert.assertTrue(expectedDistributorCategoryValue.contains(actualDistributorCategoryValue), "Factor cannot be found.\n");
		Assert.assertTrue(expectedCustomerCategoryValue.contains(actualCustomerCategoryValue), "Factor cannot be found.\n");
		
		/* SCREENSHOT FOR BID Q TAB */
		takeScreenshot(tcNumber, DetailPageTabsE.BID_Q.getLabel(), this.getClass());
		Thread.sleep(5000);
		
	}
}
