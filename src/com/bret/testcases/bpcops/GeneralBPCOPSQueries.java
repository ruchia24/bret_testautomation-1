package com.bret.testcases.bpcops;


public class GeneralBPCOPSQueries {

	public static String selectCEIDQuery = "SELECT F.CEID, F.COUNTRY, F.ISO_CC, F.LEGAL_NAME, F.SEVERITY_LEVEL, F.OUTLIER_SAMPLES_INFRACTIONS, S.INVESTIGATION_SCORE, F.OUTLIER_SAMPLES_NDS, S.CONFIDENT_SCORE, F.IAI_COUNT\r\n" + 
			"	FROM BPC_OPS.FLAGS as F\r\n" + 
			"	Left join BRET.BPCOPS_SCORE as S on F.CEID = S.CEID\r\n" + 
			"	WHERE F.CEID IN (?)";
	
	public static String deleteCEIDsQuery = "DELETE from BPC_OPS.FLAGS WHERE CEID = ";
	
	public static String insertCEIDQuery = "INSERT INTO BPC_OPS.FLAGS (COUNTRY,ISO_CC,CEID,LEGAL_NAME,ALIAS,LOCATION_ADDRESS_LINE1,LOCATION_CITY,LOCATION_STATE,LOCATION_ZIP,CONTACT_NAME,CONTACT_PHONE,NUM_SAMPLES,SAMPLES_INFRACTIONS,OUTLIER_SAMPLES_INFRACTIONS,SAMPLES_NDS,OUTLIER_SAMPLES_NDS,SAMPLES_MISSING_DOC,SAMPLES_INSERTED_TIER,OUTLIER_SAMPLES_INSERTED_TIER,SAMPLES_OVERBILLING,OUTLIER_SAMPLES_OVERBILLING,IAI_COUNT,OUTLIER_SUMMARY,LAST_NAFB_FLAGGED,SEVERITY_LEVEL)\r\n" + 
			"VALUES ('Slovakia','SK',?,'Asbis SK Spol','Asbis SK Spol',null,null,null,null,'Compliance Control Desk','(720) 395-8466 T/L 676-8466',0,0,'Y',0,'N',0,0,'N',0,'N',1,'Y',0,?)";
	
	/**DELETE QUERIES For Test Data Deletion From SQO Landing Tables **/
 	public static String deleteFrmFamsWebQuoteLineItemQuery = "DELETE from bretsqo.FAMS_WEB_QUOTE_LINE_ITEM where WEB_QUOTE_NUM in ";
 	public static String deleteFrmWebQuoteTxt = "DELETE from bretsqo.WEB_QUOTE_TXT  where WEB_QUOTE_NUM in ";
 	public static String deleteFrmWebQuoteSpclBidQues = "DELETE from bretsqo.WEB_QUOTE_SPECL_BID_QUESTN  where WEB_QUOTE_NUM in ";
 	public static String deleteFrmWebQuoteSpclBidApprover = "DELETE from bretsqo.WEB_QUOTE_SPECL_BID_APPRVR where WEB_QUOTE_NUM in ";
 	
 	/**DELETE QUERIES For Test Data Deletion from Persistent Tables **/
 	public static String deleteFromBRETTxnBidTableQuery = "DELETE FROM bret.txn_bid WHERE BID_ID in ";
 	public static String deleteFromBRETTxnPartsTableQuery = "DELETE FROM bret.txn_parts WHERE BID_ID in ";
 	public static String deleteFromBRETReviewedBidFactorsTableQuery = "DELETE FROM bret.reviewed_bid_factors WHERE BID_ID in ";
 	public static String deleteFromBRETReviewedBidStatusTableQuery = "DELETE FROM bret.reviewed_bid_status WHERE BID_ID in ";
 	
 	
 	/**  Check that the bids are loaded using the query below: **/
 	public static String checkIfBidsLoadedQuery = "SELECT DISTINCT BB.SOURCE_SYS_CD, BB.RECORD_CREATE_TS, BB.BID_ID,BB.TIER1_CEID, BB.TIER2_CEID, SB.BRET_FOCUS_FLAG, SB.TOTAL_BID_SCORE, SB.TOTAL_HIGHCUT_THRESHOLD,SB.BID_FACTOR_LIST, SB.COMMENTS, BPC.SEVERITY_LEVEL FROM BRET.PERSISTENT_BID AS BB LEFT OUTER JOIN BRET.SCORED_BID AS SB ON BB.BID_ID = SB.BID_ID LEFT OUTER JOIN BPC_OPS.FLAGS AS BPC ON BB.TIER1_CEID = BPC.CEID OR BB.TIER2_CEID = BPC.CEID WHERE BB.BID_ID in ";
 	
 	public static String selectRunIDQuery = "SELECT batch_id FROM BRET.PERSISTENT_BID WHERE bid_id = ?";
 	
 	


}


