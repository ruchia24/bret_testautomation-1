package com.bret.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

//import org.apache.poi.xssf.usermodel.XSSFWorkbook; // class file in order to read .xlsx file

public class Excel {

	public static int getRowCount(String xlpath, String sheet) {
		int rc = 1;
		try {
			System.out.println(sheet + "" + xlpath);

			FileInputStream fis = new FileInputStream(xlpath);

			Workbook wb = WorkbookFactory.create(fis);

			rc = wb.getSheet(sheet).getLastRowNum();

		} catch (Exception e) {

		}
		return rc;
	}

	public static String getCellValue(String xlpath, String sheet, int row,
			int cell) {
		String value = "";
		try {
			FileInputStream fis = new FileInputStream(xlpath);
			Workbook wb = WorkbookFactory.create(fis);

			value = wb.getSheet(sheet).getRow(row).getCell(cell)
					.getStringCellValue();

		} catch (Exception e) {

		}
		return value;
	}

	public static long CellValue(String xlpath, String sheet, int row, int cell) {
		long value = 0;
		try {
			FileInputStream fis = new FileInputStream(xlpath);
			Workbook wb = WorkbookFactory.create(fis);

			value = (long) wb.getSheet(sheet).getRow(row).getCell(cell)
					.getNumericCellValue();

		} catch (Exception e) {

		}
		return value;
	}

	/**
	 * NOTE: Starting from base index = 0;
	 * 
	 * Gets the values from Column 1 only. Rows 1 to max row number provided
	 * 
	 * @param excelPath
	 * @param sheet
	 * @return 
	 */
	public static Map<String, String> getRowValues(String excelPath, String sheetName,
			int maxRowNum) {
		
		// Key would be the ROW Index and value is the test data value (cell value)
		Map<String, String> testDataMap = new HashMap<>(); 
		
		int colIndex = 1;
		int startRowIndex = 1;

		try (InputStream inputStream = new FileInputStream(excelPath)) {
			Workbook workbook = WorkbookFactory.create(inputStream);

			Sheet sheet = workbook.getSheet(sheetName);
			if (null != sheet) {
				for (int i = startRowIndex; i <= maxRowNum; i++) {
					String currentCellValue = "";
					
					Row currentRow = sheet.getRow(i);
					if (null != currentRow) {
						Cell currentCell = currentRow.getCell(colIndex);
						if (null != currentCell) {
							currentCellValue = currentCell.getStringCellValue();
							String[] cellValues = currentCellValue.split("=(?=(?:[^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)");

							if (null != cellValues && cellValues.length == 2) {
								String key = cellValues[0];
								String value = cellValues[1];

								if (null != key && null != value) {
									testDataMap.put(key.trim(), value.trim());
								}
							}
									
						}
					}
					
				}
			}
		} catch (IOException | InvalidFormatException e) {
			e.printStackTrace();
		}

		return testDataMap;
	}

}
