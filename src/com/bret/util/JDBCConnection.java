package com.bret.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;
import org.apache.log4j.Logger;

public class JDBCConnection {
  /*
   * static block is executed when a class is loaded into memory this block loads MySQL's JDBC
   * driver
   */
  private static Logger LOGGER = Logger.getLogger(JDBCConnection.class);

  private static String JDBC_DETAILS_PROPERTIES_FILE = "./properties/brettest.properties";
  public static String BZ_ConnectionUrl = null;
  public static String GZ_ConnectionUrl = null;
  public static String dbUser = null;
  public static String dbPwd = null;
   
  private static Connection con = null;
  private static PreparedStatement psSelect = null;
  private static PreparedStatement psInsert = null;
  private static Statement stmt = null;
  private static ResultSet resultSet = null;

  private static Properties props = null;
  private static String sslConnectionStatus = null;
  private static String sslTrustStoreLocation = null;
  private static String sslTrustStorePassword = null;
 
  /**
   * Private constructor
   */
  private JDBCConnection() {
    LOGGER.info("Constructor executed.");
    }

  static {
    try {
       // loads com.ibm.db2.jcc.DB2Driver into memory
	   Class.forName("com.ibm.db2.jcc.DB2Driver");
  
      props = getJDBCProperties();
      if (props.isEmpty()) {
    	  LOGGER.info("Empty properties.");
      } else {
    	  dbUser = props.getProperty("DB_USERNAME");
    	  dbPwd = props.getProperty("DB_PASSWORD");
    	  sslConnectionStatus = props.getProperty("sslConnectionStatus");
    	  sslTrustStoreLocation = props.getProperty("sslTrustStoreLocation");
    	  sslTrustStorePassword = props.getProperty("sslTrustStorePassword");
    	  
    	  BZ_ConnectionUrl = props.getProperty("BZ_DB_CONNECTION_URL");
    	  GZ_ConnectionUrl = props.getProperty("GZ_DB_CONNECTION_URL");
    	  
    	  BZ_ConnectionUrl = BZ_ConnectionUrl + ":" + "sslConnection=" + sslConnectionStatus + "sslTrustStoreLocation=" + sslTrustStoreLocation + ";" + "sslTrustStorePassword=" + sslTrustStorePassword + ";";
    	  LOGGER.info("BZ_ConnectionUrl ->   "+ BZ_ConnectionUrl);    
    	  
    	  GZ_ConnectionUrl = GZ_ConnectionUrl + ":" + "sslConnection=" + sslConnectionStatus + "sslTrustStoreLocation=" + sslTrustStoreLocation + ";" + "sslTrustStorePassword=" + sslTrustStorePassword + ";";
    	  LOGGER.info("GZ_ConnectionUrl ->   "+ GZ_ConnectionUrl);
      }

    } catch (ClassNotFoundException cnf) {
      LOGGER.error("Driver could not be loaded: " + cnf);
    }
  }

  
  /**
   * Execute .sql Script file 
   * @param filePath
   * @throws IOException
   * @throws ClassNotFoundException
   */
  public static void executeSqlScript(String filePath) throws IOException, ClassNotFoundException
  {
      String s = new String();
      StringBuffer sb = new StringBuffer();
      try
      {
          FileReader fr = new FileReader(new File(filePath));
          // be sure to not have line starting with "--" or "/*" or any other non alphabetical character
          BufferedReader br = new BufferedReader(fr);
          while((s = br.readLine()) != null)
          {
              sb.append(s);
          }
          br.close();

          // here is our splitter ! We use ";" as a delimiter for each request
          // then we are sure to have well formed statements
          String[] queries = sb.toString().split(";");

          con = DriverManager.getConnection(BZ_ConnectionUrl, dbUser, dbPwd);
          stmt = con.createStatement();

          for(int i = 0; i<queries.length; i++)
          {
              // we ensure that there is no spaces before or after the request string
              // in order to not execute empty statements
              if(!queries[i].trim().equals(""))
              {                  
                  LOGGER.info(">>"+queries[i]);
                  stmt.addBatch(queries[i]);
              }
          }
          LOGGER.info("queries.length --- "+queries.length);
          
          // Create an int[] to hold returned values
          int[] testRecordsInserted = stmt.executeBatch();
          LOGGER.info("testRecordsInserted -- " +testRecordsInserted.length);         		  
          
          //Explicitly commit statements to apply changes
          con.commit();
 
      } catch (BatchUpdateException ex) {    	  
          int[] updateCount = ex.getUpdateCounts();           
          int count = 1;
          for (int i : updateCount) {
              if  (i == Statement.EXECUTE_FAILED) {
                  LOGGER.info("Error on request " + count +": Execute failed");
              } else {
                  LOGGER.info("Request " + count +": OK");
              }
              count++;               
          }
      } catch (SQLException e) {
    	  LOGGER.error("Issue while while executing .sql script: " + e.getMessage());          
      } finally {

	        try {
	          if (null != con) {
	            con.close();
	          }	         
	          if (null != stmt) {
	        	  stmt.close();
	          }

	        }  catch (SQLException ex) {
	        	LOGGER.error("SQLException information while executing .sql script");
	            while(ex!=null) {
	            	LOGGER.error("Error msg: " + ex.getMessage());
	            	LOGGER.error("SQLSTATE: " + ex.getSQLState());
	            	LOGGER.error("Error code: " + ex.getErrorCode());
	            	ex.printStackTrace();
	            	ex = ex.getNextException(); // For drivers that support chained exceptions
	            }
	        }
      }
  }

  
  /**
   * Insert record if search data does not exist in table
   * @param selectQuery
   * @param insertQuery
   * @param searchData
   */
  public static void insertIfNoExistingData(String selectQuery, String insertQuery, String searchData, String severityLevel) {	  
	  try {	     
	      
	      con = DriverManager.getConnection(BZ_ConnectionUrl, dbUser, dbPwd);	      
	      psSelect = con.prepareStatement(selectQuery);
	      psSelect.setString(1, searchData);	      
	      resultSet = psSelect.executeQuery();

	      if (!resultSet.next()) {
	       
	    	  	 LOGGER.info("Since resultset is empty, it means that there is no such existing data in table & hence insert the new record");	    	  	
	    	  	 psInsert = con.prepareStatement(insertQuery);
	    	  	 psInsert.setString(1, searchData); 
	    	  	 psInsert.setString(2, severityLevel);
	    	  	 psInsert.executeUpdate();
	       }	
	       else {
	    	   LOGGER.info("Data not inserted in table since it already exists");
	       }

	      con.commit();
	    } catch (Throwable e) {
	    	e.printStackTrace();	      
	    } finally {

	        try {
	          if (null != con) {
	            con.close();
	          }
	          if (null != resultSet) {
	            resultSet.close();
	          }
	          if (null != psSelect) {
	        	  psSelect.close();
	          }
	          if (null != psInsert) {
	        	  psInsert.close();
	          }
	        } 
	        catch (SQLException ex) {
	        	LOGGER.error("SQLException information");
	            while(ex!=null) {
	            	LOGGER.error("Error msg: " + ex.getMessage());
	            	LOGGER.error("SQLSTATE: " + ex.getSQLState());
	            	LOGGER.error("Error code: " + ex.getErrorCode());
	            	ex.printStackTrace();
	            	ex = ex.getNextException(); // For drivers that support chained exceptions
	            }
	        }
	      }
  }
  
/**
 * a) If a list of data(multiple records) needs to be deleted from the tables, by a set of multiple queries
 * 		-	pass an array of data to be deleted
 * 		-   pass an array of queries that need to be executed
 * 
 * b) If only a single record needs to be deleted with a single query
 * 		-   pass the data[] as null
 *      -   pass the delete query
 * 
 * @param connectionURL
 * @param data
 * @param deleteQueryList
 * @param deleteQuery
 */
  public static void deleteInsertedRecords( String connectionURL, String[] data, String[] deleteQueryList, String deleteQuery ) {
	  
	try {		
	
	  con = DriverManager.getConnection(connectionURL, dbUser, dbPwd);	
	  stmt =  con.createStatement();	  
	  StringBuilder builder = new StringBuilder();
	  
	  //If multiple records need to be deleted with a set of queries
	  if(data != null) {
		  for(int i = 0 ; i < data.length ; i++) {
			  builder.append( "\'" + data[i] + "\'" + ",");
		  }
		  builder.deleteCharAt( builder.length() -1 ).toString();
		  
		  for( String singleDeleteQuery : deleteQueryList ) {
			  
			  singleDeleteQuery = singleDeleteQuery + "(" + builder + ")";
			  LOGGER.info(singleDeleteQuery);
			  stmt.addBatch(singleDeleteQuery);
		  }
	  } else {
	   // If single record needs to be deleted with a single query	  
		  stmt.addBatch(deleteQuery);
	  }
	  // Create an int[] to hold returned values
      int[] deletedRecords = stmt.executeBatch();
      LOGGER.info("Test Data Records Deleted --> " +deletedRecords.length);
      con.commit();	    
	} catch (Throwable e) {
    	e.printStackTrace();	      
    } finally {
        try {
          if (null != con) {
            con.close();
          }         
          if (null != stmt) {
        	  stmt.close();
          }
        } 
        catch (SQLException ex) {        	
            while(ex!=null) {
              LOGGER.error("Error msg: " + ex.getMessage());
              LOGGER.error("SQLSTATE: " + ex.getSQLState());
              LOGGER.error("Error code: " + ex.getErrorCode());
              LOGGER.error("SQLException while deleting data from table" + ex.getStackTrace());
              ex = ex.getNextException(); // For drivers that support chained exceptions
            }
        }
      }	  
  }

  /** Method will return an array of values existing 
   * in the colName parameter 
   * for given testData[]  
   * @param connectionURL
   * @param testData
   * @param selectQuery
   * @param colName
   * @return
   */
  public static ArrayList<String> getColumnDataList(String connectionURL, String[] testData, String selectQuery, String colName) {
	  
	  ArrayList<String> dataList = new ArrayList<String>();
	  try {
		  con = DriverManager.getConnection(connectionURL, dbUser, dbPwd);
	
		  StringBuilder builder = new StringBuilder();
		  for(int i = 0 ; i < testData.length ; i++) {
			  builder.append( "\'" + testData[i] + "\'" + ",");
		  }
		  builder.deleteCharAt( builder.length() -1 ).toString();			  
		  selectQuery = selectQuery + "(" + builder + ")";
		  LOGGER.info(selectQuery);	 	  
		  
		  PreparedStatement ps = con.prepareStatement(selectQuery);
	      ResultSet rs = ps.executeQuery();
	      while (rs.next()) {
	          // Read values using column name
	          String colValue = rs.getString(colName);
	          dataList.add(colValue);
	      }
	      return dataList;	      
		  } catch (SQLException e) {
				LOGGER.error("Error while getting column values" + e.getStackTrace());				
				return null;
		}	
  }
  
  /**
   * Get the data in given column
   * @param connectionURL
   * @param data
   * @param selectQuery
   * @param colName
   * @return
   */
  public static String getSingleColumnValue(String connectionURL, String data, String selectQuery, String colName) {
	  String colValue = null;
	  try {
		  con = DriverManager.getConnection(connectionURL, dbUser, dbPwd);
		  PreparedStatement ps = con.prepareStatement(selectQuery);
		  ps.setString(1, data);
	      ResultSet rs = ps.executeQuery();
	      while (rs.next()) {
	          // Read values using column name
	          colValue = rs.getString(colName);
	          LOGGER.info("colValue -> "+ colValue);	          
	      }
	      return colValue;	      
		  } catch (SQLException e) {
				LOGGER.error("Error while getting column values" + e.getStackTrace());				
				return null;
		}	
  }
  
  
  /**
   * Read the user properties file.
   * 
   * @return The <code>Properties</code> file.
   */
  public static Properties getJDBCProperties() {

    Properties pp = new Properties();
    InputStream is = null;
    try {
      is = new FileInputStream(JDBC_DETAILS_PROPERTIES_FILE);
      pp.load(is);
    } catch (IOException e) {
      LOGGER.error("Couldn't load in default properties", e);
    } finally {
      if (is != null) {
        try {
          is.close();
        } catch (IOException e) {
          LOGGER.error("Unexpected error when closing config file", e);
        }
      }
    }
    return pp;
  }
  
  /**
   * Method to call the shell script
   */
  public static void callShellScript() {
	 
	  String rootDir = System.getProperty("user.dir");
	  
	  Process p;
			try {
				//p=Runtime.getRuntime().exec("cmd /c start cmd.exe /K \"cd c:/BRET/BretTestAutomation/src/com/bret/util && sh remote-exec.sh\"");
				//run sh c:/BRET/BretTestAutomation/src/com/bret/util/remote-exec.sh
				//cmd /c start mintty -e sh c:/BRET/BretTestAutomation/src/com/bret/util/remote-exec.sh
				p=Runtime.getRuntime().exec("run --wait  bash -l -c c:/BRET_TestAutomation/src/com/bret/util/remote-exec.sh");
				//p=Runtime.getRuntime().exec("cmd /c start mintty cd c:/BRET/BretTestAutomation/src/com/bret/util && sh remote-exec.sh");
				//set all \" (quotes) carefully. The parameter \k is used to leave the command prompt open after the execution.
			
				p.wait(50000);

		    BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		    BufferedReader errorReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
		    String line = "";
		    while ((line = reader.readLine()) != null) {
		        LOGGER.info(line);
		    }
		    line = "";
		    while ((line = errorReader.readLine()) != null) {
		        LOGGER.info(line);
		    }		    
			} catch (IOException | InterruptedException e) {
				LOGGER.error("Error while executing shell script" + e.getStackTrace());
			}
		}  
}

